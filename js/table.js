

TableToolsInit.sSwfPath = "js/TableTools/media/swf/ZeroClipboard.swf";

$( function() {
    
    
    $( '#tabs' ).tabs({
        fx: {height: 'toggle', opacity: 'toggle'}
    });
    
    $('.createTab').click(function(){
        updateTime();
    });
    
    readData();
    
    $( '#msgDialog' ).dialog({
        autoOpen: false,
        
        buttons: {
            'Ok': function() {
                $( this ).dialog( 'close' );
            }
        }
    });
    
    $( '#updateDialog' ).dialog({
        autoOpen: false,
        buttons: {
            'Update': function() {
                $( '#ajaxLoadAni' ).fadeIn( 'slow' );
                $( this ).dialog( 'close' );
                
                $.ajax({
                    url: updateHref,
                    type: 'POST',
                    data: $( '#updateDialog form' ).serialize(),
                    
                    success: function( response ) {
                        
                        $( '#msgDialog > p' ).html( response );
                        $( '#msgDialog' ).dialog( 'option', 'title', 'Success' ).dialog( 'open' );
                        
                        $( '#ajaxLoadAni' ).fadeOut( 'slow' );

                        readData();
                        
                        //--- clear form ---
                        $( '#updateDialog form input' ).val( '' );
                        
                    } //end success
                    
                }); //end ajax()
            },
            
            'Cancel': function() {
                $( this ).dialog( 'close' );
            }
        },
        width: '400px'
    }); //end update dialog
    
    $( '#delConfDialog' ).dialog({
        autoOpen: false,
        
        buttons: {
            'No': function() {
                $( this ).dialog( 'close' );
            },
            
            'Yes': function() {
                //display ajax loader animation here...
                $( '#ajaxLoadAni' ).fadeIn( 'slow' );
                
                $( this ).dialog( 'close' );
                
                $.ajax({
                    url: delHref,
                    
                    success: function( response ) {
                        //hide ajax loader animation here...
                        $( '#ajaxLoadAni' ).fadeOut( 'slow' );
                        
                        $( '#msgDialog > p' ).html( response );
                        $( '#msgDialog' ).dialog( 'option', 'title', 'Success' ).dialog( 'open' );
                        
                        $( 'a[href=' + delHref + ']' ).parents( 'tr' )
                        .fadeOut( 'slow', function() {
                            $( this ).remove();
                        });
                        
                    } //end success
                });
                
            } //end Yes
            
        } //end buttons
        
    }); //end dialog
    
    $( '#records' ).delegate( 'a.updateBtn', 'click', function(event) {
        event.preventDefault();
        updateHref = $( this ).attr( 'href' );
        updateId = $( this ).parents( 'tr' ).attr( "id" );
        
        //console.log(updateHref);
        
        $( '#ajaxLoadAni' ).fadeIn( 'slow' );
        
        $.ajax({
            url: getByIdURL + updateId ,
            dataType: 'json',
            success: function( response ) {

                $.each(fieldArray, function(index, value) {
                    $( '#'+value ).val( response[value] );
                    console.log('$( #'+value +').val( ' + response[value] +' );');
                });

                $( '#ajaxLoadAni' ).fadeOut( 'slow' );

                
                $( '#updateDialog' ).dialog( 'open' );
            }
        });
        
        //return false;
    }); //end update delegate
    
    $( '#records' ).delegate( 'a.deleteBtn', 'click', function() {
        delHref = $( this ).attr( 'href' );
        
        $( '#delConfDialog' ).dialog( 'open' );
        
        return false;
    
    }); //end delete delegate
    
    
    // --- Create Record with Validation ---
    $( '.ui-tabs-panel form' ).validate({
        rules: {
            sample_date: { required: true },
            weight: { required: true }
            },
        submitHandler: function( form ) {
            $( '#ajaxLoadAni' ).fadeIn( 'fast' );
            $.ajax({
                url: createURL,
                type: 'POST',
                data: $( form ).serialize(),
                success: function( response ) {
                    $( '#msgDialog > p' ).html( 'New entry added successfully!' );
                    $( '#msgDialog' ).dialog( 'option', 'title', 'Success' ).dialog( 'open' );
                    
                    //clear all input fields in create form
                    $( 'input', this ).val( '' );

                    readData();

                    //open Read tab
                    $( '#tabs' ).tabs( 'select', 0 );
                    $( '#ajaxLoadAni' ).fadeOut( 'slow' );
                }
            });
            
            return false;
        }

    });



}); //end document ready


function readData() {
    //display ajax loader animation
    $( '#ajaxLoadAni' ).fadeIn( 'fast' );
    
    $.ajax({
        url: readUrl,
        dataType: 'json',
        success: function( response ) {
                for( var i in response ) {
                response[ i ].updateLink = updateUrl + '/' + response[ i ].id + '/';
                response[ i ].deleteLink = delUrl + '/' + response[ i ].id + '/';
            }
                //clear old rows
                $( '#records > tbody' ).html( '' );

                //append new rows
                $( rowTemplate ).render( response ).appendTo( "#records > tbody" );




                //apply dataTable to #records table and save its object in dataTable variable
                if( typeof dataTable == 'undefined' )
                    dataTable = $( '#records' ).dataTable({
                        "aaSorting": [[ 0, sort ]],
                        "sDom": 'T<"clear">lfrtip',
                        ///"aoColumns": [null, null, null, null, null],
                        "bJQueryUI": true

                    });

                //hide ajax loader animation here...
                $( '#tabs' ).fadeIn( 'slow' );
                $( '#ajaxLoadAni' ).fadeOut( 'slow' );
        },
        error: function ( response ) {
           // console.log(response);
            $(readTemplate).html(response.responseText);
        }
    });
} // end readUsers

$( ".dPicker" ).datepicker({ dateFormat: 'yy-mm-dd' });


function updateTime() {
    console.log(getTimeUrl);
    $.ajax({
        url: getTimeUrl,
        success: function( response ) {
            $( '.sampleDate' ).val(response);
        }
    });
}


