$(function() {
    var ft = 0;
    var inch = 0;

    var base_url = "/gp/";

    function redrawChart() {
        chart.redraw();
    }

    $(".dropSubmit").change(function() {
        $(this).parent('form').submit();
    });

    $("#email_link").fancybox({
        'titlePosition'		: 'inside',
        'transitionIn'		: 'none',
        'transitionOut'		: 'none'
    });



    $(".popUp").fancybox({
        'width'				: '75%',
        'height'			: '75%',
        'type'				: 'iframe'
    });

    $(".popUpSmall").fancybox({
        'width'				: 620,
        'height'			: 400,
        'type'				: 'iframe',
        'onClosed'                      : function() {
            parent.location.reload(true);
        }
    });

    $(".popUp980").fancybox({
        'width'				: 970,
        'height'			: 500,
        'type'				: 'iframe',
        'onClosed'                      : function() {
            parent.location.reload(true);
        }
    });




    $("ul.dropdown").find("a").click(function(event) {
        event.preventDefault();
    }
    );

    $("label.error").addClass("curves5").append('<span></span>');

    $( ".datepicker" ).datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true
    });
    $.extend(
        $.datepicker,
        {
            _gotoToday: function(id) {
		var target = $(id);
		var inst = this._getInst(target[0]);
                this._hideDatepicker();
                this._clearDate(target);
            }
	}
    );
    $('.monthPicker').datepicker( {
        changeMonth: true,
        changeYear: true,
        autoSize: true,
        yearRange: '-100:+00',
        showButtonPanel: true,
        closeText: 'Done',
        currentText: 'Clear',
        dateFormat: 'yy mm',
        create : function (e,ui) {
            console.dir(e);
            console.dir(ui);
        },
        onClose: function(dateText, inst) {
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(year, month, 1));
           // $(this).datepicker('setDate', '');
        },
        beforeShow : function(input, inst) {
            inst.dpDiv.addClass('monthPick');
            if ((datestr = $(this).val()).length > 0) {
                year = datestr.substring(0, datestr.length-3);
                month = datestr.substring(datestr.length-2, datestr.length);
                $(this).datepicker('option', 'defaultDate', new Date(year, month-1, 1));
                $(this).datepicker('setDate', new Date(year, month-1, 1));
            }
        }
    });



    $('.measurement').bind('unitChange', function(e) {
        var $unit = $(this);
        var impBlock = $unit.find('#imperialBlock');
        var input_ft = impBlock.find('#ft');
        var input_in = impBlock.find('#inch');
        var metricBlock = $unit.find('#metricBlock');
        var input_m = metricBlock.find('#m');
        var imp_ft = parseFloat(input_ft.val());
        var imp_in = parseFloat(input_in.val());
        var imp_m = parseFloat(input_m.val());
        var inches = imp_ft * 12 + imp_in;

        console.log('ft: ' + imp_ft + ' in:' + imp_in +  ' m:' + imp_m + ' all inch:' + inches);

        if($( '#profile_form' ).validate(profileOptions)){

            if($('#imperial').attr('checked')===true) {
                var imp = metric2imperial(imp_m);
                input_ft.val(imp.ft);
                input_in.val(imp.inch);
                impBlock.css({
                    'display' : 'block'
                });
                metricBlock.css({
                    'display' : 'none'
                });
                input_m.val('');
            } else {
                var m = imperial2metric(inches);
                impBlock.css({
                    'display' : 'none'
                });
                input_ft.val('');
                input_in.val('');
                metricBlock.css({
                    'display' : 'block'
                });
                input_m.val(m);
            }
        }
    });

    $('.unit').change(function(){
        $('.measurement').trigger('unitChange');
    });

    function metric2imperial(cm){
        var inches =  parseFloat(cm) / 2.54;
        console.log(parseFloat(cm));
        imp = new Object();

        imp.ft = Math.floor(inches / 12);
        imp.inch = Math.round(inches % 12);

        return imp;
    }


    function imperial2metric(inches){
        return Math.round(parseFloat(inches) * 2.54);
    }

    invitePatient();


    if($('.enlargeFont')[0]){
        var Fsize = 0;
        var percent = 62.5;
        var fontPercent;
        var prevFsize;

        $('.enlargeFont').click(function(e){
            e.preventDefault();
            prevFsize = Fsize;
            Fsize = (Fsize<2) ? (Fsize + 1) : 0;
            $('body').removeClass('fsize'+ prevFsize).addClass('fsize'+ Fsize);
            $.get(base_url+'patientbar/setfontsize/'+Fsize);


        });


    }



    //*********VALIDATIONS*************//
    jQuery.validator.addMethod("require_from_group", function(value, element, options) {
        var numberRequired = options[0];
        var selector = options[1];
        //Look for our selector within the parent form
        var validOrNot = $(selector, element.form).filter(function() {
            // Each field is kept if it has a value
            return $(this).val();
        // Set to true if there are enough, else to false
        }).length >= numberRequired;

        //The elegent part - this element needs to check the others that match the
        //selector, but we don't want to set off a feedback loop where all the
        //elements check all the others which check all the others which
        //check all the others...
        //So instead we
        //  1) Flag all matching elements as 'currently being validated'
        //  using jQuery's .data()
        //  2) Re-run validation on each of them. Since the others are now
        //     flagged as being in the process, they will skip this section,
        //     and therefore won't turn around and validate everything else
        //  3) Once that's done, we remove the 'currently being validated' flag
        //     from all the elements
        if(!$(element).data('being_validated')) {
            var fields = $(selector, element.form);
            fields.data('being_validated', true);
            //.valid() means "validate using all applicable rules" (which
            //includes this one)
            fields.valid();
            fields.data('being_validated', false);
        }
        return validOrNot;
    // {0} below is the 0th item in the options field
    }, jQuery.format("Please fill out at least {0} of these fields."));






    $( '#add_bp' ).validate({
        rules: {
            sample_date: {
                required: true
            },
            systolic: {
                required: true,
                min: 35,
                max: 260
            },
            diastolic: {
                required: true,
                min: 35,
                max: 260
            }
        }
    });

    $( '#add_cholesterol' ).validate({
        rules: {
            //sample_date: { required: true },
            HDL: {
                min: 0.5,
                max: 10
            },
            total: {
                min: 0.5,
                max: 10
            }
        }
    });

    $( '#add_bodysize' ).validate({
        rules: {
            //sample_date: { required: true },
            weight: {
                min: 30,
                max: 180
            },
            girth: {
                min: 60,
                max: 150
            }
        }
    });

    $( '#add_diabetes' ).validate({
        rules: {
            //sample_date: { required: true },
            diabetes: {
                required: true,
                min: 1,
                max: 25
            }
        }
    });

    $( '#add_ltbs' ).validate({
        rules: {
            //sample_date: { required: true },
            ltbs: {
                required: true,
                min: 3,
                max: 15
            }
        }
    });

    var profileOptions = {
        rules: {
            //sample_date: { required: true },
            height_m: {
                //                require_from_group: [1,".height"],
                number: true
            },
            height_ft: {
                //                require_from_group: [1,".height"],
                number: true
            },
            height_in: {
                //                require_from_group: [1,".height"],
                number: true
            }
        }
    };

    $( '#profile_form' ).validate(profileOptions);

//*********VALIDATIONS END*************//



//$('table.sortable').dataTable();

});


function invitePatient(){
    if($('#patient_invite_form')[0]){
        var inviteForm = $('#patient_invite_form');
        var inviteInput = $('input[type=text]',inviteForm);
        var inputText = 'enter patient email';
        var invitePatientBtn = $('#invitePatientBtn');
        var invitePatientSubmit = $('#invitePatientSubmit');


        invitePatientSubmit.click(function(e){
            e.stopPropagation();
        });

        inviteInput.val(inputText);

        invitePatientBtn.click(function(e){
            e.preventDefault();
            e.stopPropagation();
            invitePatientBtn.parent('div').addClass('active');
            inviteForm.parent('div').slideDown('fast');
        });

        $('body').click(function(){
            invitePatientBtn.parent('div').removeClass('active');
            inviteForm.parent('div').slideUp('fast');
        });


        $(inviteForm).validate({
            rules: {
                patient_email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                patient_email: {
                    required: "Patient email is required",
                    email: "The email must be in the format of name@domain.com"
                }
            }
            ,
            invalidHandler:function(form, validator){
                $(form).addClass('error');
                $(form).find('label:not(.error, [for=contact_url])').hide();
            },
            submitHandler:function(form){
                $(form).append('<input type="hidden" name="ajax_submit" value="true" />');
                $(form).append('<div id="processing"><img src="img/ajax-loader.gif" alt="Processing..." width="32" height="32" /></div>');
                $(form).ajaxSubmit({
                    type: 'POST',
                    url: '/gp/auth/invite_patient/',
                    success: function($data){
                        $("#processing").remove();
                        $(form).append($data)
                    }
                });
                return false;
            }
        });

        inviteInput.click(function(e){
            e.stopPropagation();
        });

        inviteInput.focusin(function(e){
            $(this).val('');
            inviteForm.find('.appMsg').slideUp().end().find('.reinvite').slideUp();
        });
        inviteInput.focusout(function(){
            if(!$(this).val()){
                $(this).val(inputText);
            }
        });
    }
}