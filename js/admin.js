$(function(){
						   
						   
////////////_GENERAL_///////////////////

        $(".dropSubmit").change(function() {
            $(this).parent().submit();
        });
        $(".selectAll").click(function() {
            $(this).siblings().find(':checkbox').attr('checked','checked');

        });
	
	$(".toggle_btn").click(function () {
		var cl = $(this).attr('id').substring(4);
		$("."+cl).slideToggle("fast");
		return false;
        });

	$(".tr_toggle_btn").click(function () {
			var cl = $(this).attr('id').substring(4);
			$("."+cl).toggle();
		return false;
        });

        $('.delete').click(function(){
            var answer = confirm('Are sure you want to delete '+ $(this).attr('title') +'?');
            return answer; 
        });

        $(".popUp").fancybox({
                'width'				: '75%',
                'height'			: '75%',
                'type'				: 'iframe'
        });

	$(".popUpSmall").fancybox({
                'width'				: 620,
                'height'			: 400,
                'type'				: 'iframe',
                'onClosed'                      : function() {
                                                    parent.location.reload(true);
                                                  }
        });

        
	
	
////////////_INPUTS_///////////////////   	   
	
	$(".inputs").focus(function(){
		$(this).css("background-position","0px -20px");
	 }); 
	


////////////add new link///////////////////



    $('#cat_toggle_NewLink').click(function() {
        $('.toggle_NewLink').addClass('AddCat').removeClass('AddURL AddFile');
    });

    $('#url_toggle_NewLink').click(function() {
        $('.toggle_NewLink').addClass('AddURL').removeClass('AddFile AddCat');
    });

    $('#fil_toggle_NewLink').click(function() {
        $('.toggle_NewLink').addClass('AddFile').removeClass('AddURL AddCat');;
    });

    if($('.pdfUploader').length){
        $('.pdfUploader').customFileInput();
    }
	


////////////_MENU_///////////////////   	   

   
});

