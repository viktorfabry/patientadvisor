(function($){
    $.fn.extend({
        //plugin name - animatemenu
        dynamicSelect: function(options) {

            //Settings list and the default values
            var defaults = {
                url: '',
                newSelectName: '',
                newSelectLabel: null,
                newSelectBoxID: null
            };

            var options = $.extend(defaults, options);

            return this.each(function() {
                var o = options;
                var obj = $(this);

                var $url = o.url,
                    $box = null,
                    $newLabel = null,
                    $newSelect = null,
                    selectName = o.newSelectName,
                    selectLabel = (o.newSelectLabel !== null) ? o.newSelectLabel : o.newSelectName,
                    selectBoxID = (o.newSelectBoxID !== null) ? o.newSelectBoxID : o.newSelectName + 'Box';


                obj.change(function(){
                    //console.log($gpCom.val());
                    if(obj.val()!=='0'){

                        $.getJSON($url + $(this).val(), function(data) {
                            var options = '',
                                option = '';

                            if(o.newSelectBoxID !== null){
                                $box = $('#' + o.newSelectBoxID);
                                console.log($box);
                            } else {
                                $box = $('<div id="' + selectBoxID + '"></div>');
                                obj.after($box);
                            }
                            if(data){
                                if($newSelect==null){
                                    $newLabel = $('<label for="' + o.newSelectName + '">' + selectLabel + '</label>');
                                    $newSelect = $('<select name="' + o.newSelectName + '" id="' + o.newSelectName + '" ></select>');
                                }
                                for (var i = 0; i < data.length; i++) {
                                    option = data[i];
                                    var val=[];
                                    b=0;
                                    for(var n in option){
                                        val[b] = option[n];
                                        b++;
                                    }
                                    options += '<option value="' + val[0] + '">' + val[1] + '</option>';
                                }
                                $newSelect.html(options);
                                $box.empty().append($newLabel).append($newSelect);
                            } else {
                                $box.html('<p class="error">No ' + selectLabel + ' exist for the selected item.</p>');
                            }

                        })
                    } else {
                        $box.empty()
                    }
                })

            });
        }
    });
})(jQuery);