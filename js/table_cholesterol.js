var readUrl   = 'table_cholesterol/read/',
    updateUrl = 'table_cholesterol/update',
    delUrl    = 'table_cholesterol/delete',
    getTimeUrl    = 'table_cholesterol/getTime/',
    delHref,
    updateHref,
    updateId;

TableToolsInit.sSwfPath = "js/TableTools/media/swf/ZeroClipboard.swf";


$( function() {
    
    $( '#tabs' ).tabs({
        fx: { height: 'toggle', opacity: 'toggle' }
    });
    
    $('.createTab').click(function(){
        updateTime()
    });
    
    readData();
    
    $( '#msgDialog' ).dialog({
        autoOpen: false,
        
        buttons: {
            'Ok': function() {
                $( this ).dialog( 'close' );
            }
        }
    });
    
    $( '#updateDialog' ).dialog({
        autoOpen: false,
        buttons: {
            'Update': function() {
                $( '#ajaxLoadAni' ).fadeIn( 'slow' );
                $( this ).dialog( 'close' );
                
                $.ajax({
                    url: updateHref,
                    type: 'POST',
                    data: $( '#updateDialog form' ).serialize(),
                    
                    success: function( response ) {
                        
                        $( '#msgDialog > p' ).html( response );
                        $( '#msgDialog' ).dialog( 'option', 'title', 'Success' ).dialog( 'open' );
                        
                        $( '#ajaxLoadAni' ).fadeOut( 'slow' );
                        
                        //--- update row in table with new values ---
                        var sample = $( 'tr#' + updateId + ' td' )[ 0 ];
                        var sample_date = $( 'tr#' + updateId + ' td' )[ 1 ];
                        var HDL = $( 'tr#' + updateId + ' td' )[ 2 ];
                        var total = $( 'tr#' + updateId + ' td' )[ 3 ];
                        
                        $( sample ).html( $( '#sample' ).val() );
                        $( sample_date ).html( $( '#sample_date' ).val() );
                        $( HDL ).html( $( '#HDL' ).val() );
                        $( total ).html( $( '#total' ).val() );
                        
                        //--- clear form ---
                        $( '#updateDialog form input' ).val( '' );
                        
                    } //end success
                    
                }); //end ajax()
            },
            
            'Cancel': function() {
                $( this ).dialog( 'close' );
            }
        },
        width: '350px'
    }); //end update dialog
    
    $( '#delConfDialog' ).dialog({
        autoOpen: false,
        
        buttons: {
            'No': function() {
                $( this ).dialog( 'close' );
            },
            
            'Yes': function() {
                //display ajax loader animation here...
                $( '#ajaxLoadAni' ).fadeIn( 'slow' );
                
                $( this ).dialog( 'close' );
                
                $.ajax({
                    url: delHref,
                    
                    success: function( response ) {
                        //hide ajax loader animation here...
                        $( '#ajaxLoadAni' ).fadeOut( 'slow' );
                        
                        $( '#msgDialog > p' ).html( response );
                        $( '#msgDialog' ).dialog( 'option', 'title', 'Success' ).dialog( 'open' );
                        
                        $( 'a[href=' + delHref + ']' ).parents( 'tr' )
                        .fadeOut( 'slow', function() {
                            $( this ).remove();
                        });
                        
                    } //end success
                });
                
            } //end Yes
            
        } //end buttons
        
    }); //end dialog
    
    $( '#records' ).delegate( 'a.updateBtn', 'click', function(event) {
        event.preventDefault();
        updateHref = $( this ).attr( 'href' );
        updateId = $( this ).parents( 'tr' ).attr( "id" );
        
        //console.log(updateHref);
        
        $( '#ajaxLoadAni' ).fadeIn( 'slow' );
        
        $.ajax({
            url: 'table_cholesterol/getById/' + updateId + '/',
            dataType: 'json',
            success: function( response ) {
                $( '#sample_date' ).val( response.sample_date );
                $( '#HDL' ).val( response.HDL );
                $( '#total' ).val( response.total );
                
                $( '#ajaxLoadAni' ).fadeOut( 'slow' );
                
                //--- assign id to hidden field ---
                $( '#rowId' ).val( updateId );
                
                $( '#updateDialog' ).dialog( 'open' );
            }
        });
        
        //return false;
    }); //end update delegate
    
    $( '#records' ).delegate( 'a.deleteBtn', 'click', function() {
        delHref = $( this ).attr( 'href' );
        
        $( '#delConfDialog' ).dialog( 'open' );
        
        return false;
    
    }); //end delete delegate
    
    
    // --- Create Record with Validation ---
    $( '#create form' ).validate({
        rules: {
            sample_date: { required: true },
            HDL: { required: true },
            total: { required: true }
            },
        
        /*
        //uncomment this block of code if you want to display custom messages
        messages: {
            cName: { required: "Name is required." },
            cEmail: {
                required: "Email is required.",
                email: "Please enter valid email address."
            }
        },
        */
        
        submitHandler: function( form ) {
            $( '#ajaxLoadAni' ).fadeIn( 'slow' );
            
            $.ajax({
                url: 'table_cholesterol/create/',
                type: 'POST',
                data: $( form ).serialize(),
                
                success: function( response ) {
                    $( '#msgDialog > p' ).html( 'New entry added successfully!' );
                    $( '#msgDialog' ).dialog( 'option', 'title', 'Success' ).dialog( 'open' );
                    
                    //clear all input fields in create form
                    $( 'input', this ).val( '' );
                    
                    //refresh list of users by reading it
                    //readUsers();
                    dataTable.fnAddData([
                       //response,
                        $( '#cSample' ).val(),
                        $( '#cSample_date' ).val(),
                        $( '#cHDL' ).val(),
                        $( '#cTotal' ).val(),
                        '<a class="updateBtn" href="' + updateUrl + response + '/' + '">Update</a> | <a class="deleteBtn" href="' + delUrl + response + '/' + '">Delete</a>'
                    ]);
                    
                    //open Read tab
                    $( '#tabs' ).tabs( 'select', 0 );
                    $( '#ajaxLoadAni' ).fadeOut( 'slow' );
                }
            });
            
            return false;
        }
    });
    
}); //end document ready


function readData() {
    //display ajax loader animation
    $( '#ajaxLoadAni' ).fadeIn( 'slow' );
    
    $.ajax({
        url: readUrl,
        dataType: 'json',
        success: function( response ) {
            
            for( var i in response ) {
                response[ i ].updateLink = updateUrl + '/' + response[ i ].id + '/';
                response[ i ].deleteLink = delUrl + '/' + response[ i ].id + '/';
            }
            
            //clear old rows
            $( '#records > tbody' ).html( '' );
            
            //append new rows
            $( '#readTemplate' ).render( response ).appendTo( "#records > tbody" );
            
            //apply dataTable to #records table and save its object in dataTable variable
            if( typeof dataTable == 'undefined' )
                dataTable = $( '#records' ).dataTable({
                    "aaSorting": [[ 0, "desc" ]],
                    "sDom": 'T<"clear">lfrtip',
                    ///"aoColumns": [null, null, null, null, null],
                    "bJQueryUI": true
                   
                     //"sDom": 'T<"clear">lfrtip'
                    
                });
            //TableToolsInit.sSwfPath = "/js/TableTools/media/swf/ZeroClipboard.swf";
            //hide ajax loader animation here...
            $( '#tabs' ).fadeIn( 'slow' );
            $( '#ajaxLoadAni' ).fadeOut( 'slow' );
        }
    });
} // end readUsers


function updateTime() {
    $.ajax({
        url: getTimeUrl,
        success: function( response ) {
            $( '#cSample_date' ).val(response);
        }
    });
}