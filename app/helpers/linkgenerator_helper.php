<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    
     function createLink($link,$name) {

            $out = '';
            if($name == '') {
                    $out = '';
            } elseif ($link == "" || $link == "#" ) {
                    $out = '<a href="#">'.$name.'</a>';
            } elseif (substr($link, -3) == "pdf" || substr($link, -3) == "doc") {
                    $out = '<a class="popUp" rel="nofollow" target="_blank" href="http://docs.google.com/gview?url='.addProtocol($link).'&embedded=true" >'.$name.'</a>'."\n";
            } else {
                    $out = '<a class="popUp" rel="nofollow" target="_blank" href="'.addProtocol($link).'" >'.$name.'</a>'."\n";
            }
            return $out;
    }

     function addProtocol ($link) {
            if ((substr($link, 0, 4)) == "http") {
                    $out = $link;
            } else {
                    $out = 'http://'.$link;
            }
            return $out;
    }
?>
