<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('test_method')) {

    function test_method($var = '') {
        return $var;
    }

}



/**
 * Compare 2 values
 *
 * @param	any
 * @param	any
 * @return	bool
 */
function compare($con1, $con2) {
    return ($con1 === $con2) ? TRUE : FALSE;
}

/**
 * create a message wrapper tag with classes
 *
 * @param	string message
 * @param	string or array of extra class names
 * @param	string tag default 'span'
 * @return	string
 */
function app_msg($msg, $class = '', $tag='span') {
    $class = (is_array($class) && $class) ? implode(' ', $class) : $class;
    if(isset($msg) && $msg){
        return '<' . $tag . ' class="appMsg ' . $class . '">' . $msg . '</' . $tag . '>';
    }
}

/**
 * convert X ft Y in; X' Y"; X feet Y inch into metre  
 *
 * @param	string message
 * @return	number
 */
function imperial2metric($number, $to = 'cm', $round = 0) {
    if ($number) {
        // Get rid of whitespace on both ends of the string.
        $number = trim($number);

        // This results in the number of feet getting multiplied by 12 when eval'd
        // which converts them to inches.
        $number = str_replace(' ', '', $number);
        $number = str_replace("'", '*12 ', $number);

        // We don't need the double quote.
        $number = str_replace('"', '', $number);

        $number = str_replace("feet", '*12 ', $number);
        $number = str_replace("ft", '*12 ', $number);

        $number = str_replace("inch", '', $number);
        $number = str_replace("in", '', $number);

        
       
        // Convert other whitespace into a plus sign.
        $number = preg_replace('/\s+/', '+', $number);

        // Make sure they aren't making us eval() evil PHP code.
        if (preg_match('/[^0-9\/\.\+\*\-]/', $number)) {
            return false;
        } else {
            // Evaluate the expression we've built to get the number of inches.
            $inches = eval("return ($number);");

            switch ($to) {
                case "cm":
                    return round($inches * 2.54, $round);
                    break;
                case "m":
                    return round($inches * 0.0254, $round);
                    break;
            }
        }
    }
}

/**
 * convert metre to X ft Y in;
 *
 * @param	number
 * @return	string message
 */
//function metric2imperial($meters) {
//
//    $inches = $meters / 0.0254;
//    $ft = floor($inches / 12);
//    $inch = $inches % 12;
//
//    return $ft . ' ft ' . $inch . ' in';
//}
function metric2imperial($value, $from = 'cm', $to = 'in') {
    $conv = ($from == 'cm') ? 2.54 : 0.0254;
    $inches = $value / $conv;

    switch ($to) {
        case "in":
            return $inches;
            break;
        case "ft_decimal":
            return $inches / 12;
            break;
        case "ft_in":
            $ft = floor($inches / 12);
            $inch = $inches % 12;
            return $ft . 'ft ' . $inch . 'in';
            break;
        case "ft_in_array":
            $ft = floor($inches / 12);
            $inch = $inches % 12;
            return array('ft' => $ft, 'in' => $inch);
            break;
        default:
            $ft = floor($inches / 12);
            $inch = $inches % 12;
            return $ft . '\' ' . $inch . '"';
            break;
    }
}

/**
 * convert kg to lb (pounds);
 *
 * @param	number
 * @return	number
 */
function kg2lb($kg, $round = 1) {
    return round((int) $kg / 0.45359237, $round);
}

/**
 * return an SQL expression to get either kg or lb (pounds)
 *
 * @param	string kg / lb || default kg
 * @return	string
 */
function getSQL_weight($measure = 'kg',$name = 'weight'){
    switch ($measure){
        case "kg":
            return $name;
            break;
        case "lb":
            return 'ROUND('.$name.' / 0.45359237) AS '.$name.'';
            break;
        default:
            return $name;
            break;
    }
}

/**
 * return an SQL expression to get either inch or cm
 *
 * @param	string cm or in || default in
 * @return	string
 */
function getSQL_length($measure = 'cm',$name = 'length'){
    switch ($measure){
        case "cm":
            return $name;
            break;
        case "in":
        case "inch":
            return 'ROUND('.$name.' / 2.54) AS '.$name.'';
            break;
        default:
            return $name;
            break;
    }
}
/**
 * convert lb (pounds) to kg;
 *
 * @param	number
 * @return	number
 */
function lb2kg($lb, $round = 1) {
    return round((int) $lb / 2.2046213, $round);
}

?>