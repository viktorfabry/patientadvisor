<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$CI =& get_instance();

$CI->load->helper('fileuploader');

class Patients_csv_imp {
    private $allowedExtensions = array();
    private $sizeLimit = 10485760;
    private $file;
    public $ext = "";
    public $fileBase = "";

//    function __construct(array $allowedExtensions = array(), $sizeLimit = 10485760){
//        parent::__construct($allowedExtensions, $sizeLimit);
//    }

    function __construct(array $allowedExtensions = array(), $sizeLimit = 10485760){
        $allowedExtensions = array_map("strtolower", $allowedExtensions);

        $this->allowedExtensions = $allowedExtensions;
        $this->sizeLimit = $sizeLimit;

        $this->checkServerSettings();

        if (isset($_GET['qqfile'])) {
            $this->file = new qqUploadedFileXhr();
        } elseif (isset($_FILES['qqfile'])) {
            $this->file = new qqUploadedFileForm();
        } else {
            $this->file = false;
        }
    }

    private function checkServerSettings(){
        $postSize = $this->toBytes(ini_get('post_max_size'));
        $uploadSize = $this->toBytes(ini_get('upload_max_filesize'));

        if ($postSize < $this->sizeLimit || $uploadSize < $this->sizeLimit){
            $size = max(1, $this->sizeLimit / 1024 / 1024) . 'M';
            die("{'error':'increase post_max_size and upload_max_filesize to $size'}");
        }
    }

    private function toBytes($str){
        $val = trim($str);
        $last = strtolower($str[strlen($str)-1]);
        switch($last) {
            case 'g': $val *= 1024;
            case 'm': $val *= 1024;
            case 'k': $val *= 1024;
        }
        return $val;
    }

    /**
     * Returns array('success'=>true) or array('error'=>'error message')
     */
    function handleUpload($uploadDirectory, $replaceOldFile = FALSE){

        $CI =& get_instance();


        if (!is_writable($uploadDirectory)){
            return array('error' => "Server error. Upload directory isn't writable.");
        }

        if (!$this->file){
            return array('error' => 'No files were uploaded.');
        }

        $size = $this->file->getSize();

        if ($size == 0) {
            return array('error' => 'File is empty');
        }

        if ($size > $this->sizeLimit) {
            return array('error' => 'File is too large');
        }

        $pathinfo = pathinfo($this->file->getName());
        $this->fileBase = md5(uniqid());
        //$filename = $pathinfo['filename'];
        //$filename = md5(uniqid());
        $this->ext = $pathinfo['extension'];

        if($this->allowedExtensions && !in_array(strtolower($this->ext), $this->allowedExtensions)){
            $these = implode(', ', $this->allowedExtensions);
            return array('error' => 'File has an invalid extension, it should be one of '. $these . '.');
        }

        if(!$replaceOldFile){
            /// don't overwrite previous files that were uploaded
            while (file_exists($uploadDirectory . $this->fileBase . '.' . $this->ext)) {
                $this->filename .= rand(10, 99);
            }
        }

        $file_id = '';
                $fileName = $this->fileBase . '.' . $this->ext;
                $filePath = $uploadDirectory . $fileName;

                if ($this->file->save($filePath)) {

                    $datestring = "Y-m-d H:i:s";
                    $now = date($datestring, time());

                    $file_id = $CI->db_fnc->insert('files', array('fileName' => $fileName, 'user_id' => 1, 'group_id' => 1, 'created' => $now));


                    if ($file_id) {
                        return array('success' => true, 'file_id' => $file_id);
                    } else {
                        return array('error' => 'The file name could not be saved in the database.');
                    }
                } else {
                    return array('error' => 'Could not save uploaded file.' .
                        'The upload was cancelled, or server error encountered');
                }

    } 
}


