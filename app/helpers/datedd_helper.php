<?

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('test_method'))
{
    function test_method($var = '')
    {
        return $var;
    }   
}

    
    function buildDayDropdown($name='',$value='')
    {
        $days='';
        while ( $days <= '31'){
            $day[]=$days;$days++;
        }
        return form_dropdown($name, $day, $value);
    }
    
    function buildYearDropdown($name='', $startYear = '', $endYear = '',$value='')
    {
        $years=date('Y');
        $year[]='';
        while ( $years > '1900'){
            $year[$years]=$years;
            $years--;
        }
        return form_dropdown($name, $year, $value);
    }
    
   
    function buildMonthDropdown($name='',$value='')
    {
         
        $month=array(
            '0'=>'',
            '01'=>'January',
            '02'=>'February',
            '03'=>'March',
            '04'=>'April',
            '05'=>'May',
            '06'=>'June',
            '07'=>'July',
            '08'=>'August',
            '09'=>'September',
            '10'=>'October',
            '11'=>'November',
            '12'=>'December');
        return form_dropdown($name, $month, $value);
    }
?>  