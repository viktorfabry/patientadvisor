<?php

header ('Content-Type: text/xml');
header('Content-Disposition: inline; filename=sample.xml');
	print "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>\n";
	//print "<videos>\n";
	//print "<video url=\"".base_url().$lesson['video']."\" desc=\"".$lesson['title']."\" />\n";
	//print "</videos>\n";
	
?>
<chart>
	   <license>GTAF0UAVUJ17T1X5CWK-2XOI1X0-7L</license>   
   <!-- the parent chart (bottom, non-scrolling chart) -->
	
   <axis_value alpha='0' />
   <axis_category alpha='0' />
   <axis_ticks value_ticks='false' category_ticks='false' />
   
   
            
   <chart_border top_thickness='0' bottom_thickness='0' left_thickness='0' right_thickness='0' />
	<chart_data>
<row>
	   <null/>
<?php
$i = 0;
while ($i < $chart['lines']) {
		echo $chart['dates'][$i];
			$i++;											
} ?>
</row>
<row>				
<string>diastolic</string>
<?php
$i = 0;
while ($i < $chart['lines']) {
		echo $chart['diastolic'][$i];
			$i++;											
} ?>			
</row>
<row>
<string>Systolic</string>
			<?php
$i = 0;
while ($i < $chart['lines']) {
		echo $chart['systolic'][$i];
			$i++;											
} ?>

		</row>
		
	</chart_data>
	
	
	<chart_grid_h alpha='0' />
	<chart_grid_v alpha='5' color='000000' thickness='1' />
	<chart_pref line_thickness='1' point_shape='none' fill_shape='false' connect='true' />
	<chart_rect x='40' width='500' positive_alpha='0' y='240' height='40' />

	<chart_transition type='dissolve' delay='1' />

	<chart_type>Line</chart_type>
	
	<draw>
		<rect transition='dissolve' delay='1' bevel='bg' layer='background' x='0' y='0' width='800' height='500' fill_color='eeeeee' line_thickness='0' />
		<!--circle transition='dissolve' delay='0' shahow='low' layer='background' x='115' y='225' radius='100' fill_alpha='0' line_color='4c5e6f' line_alpha='10' line_thickness='80' />
		<circle transition='dissolve' delay='0' shahow='low' layer='background' x='115' y='225' radius='200' fill_alpha='0' line_color='4c5e6f' line_alpha='8' line_thickness='60' />
		<circle transition='dissolve' delay='0' shahow='low' layer='background' x='115' y='225' radius='300' fill_alpha='0' line_color='4c5e6f' line_alpha='6' line_thickness='40' /-->
		<rect transition='dissolve' delay='3' shadow='high2' layer='background' x='40.5' y='60' width='500' height='220' line_thickness='0' fill_color='ffffff' fill_alpha='100' />
		<rect transition='dissolve' delay='1' layer='background' x='40.5' y='220' width='500' height='60' line_thickness='1' fill_color='ffeeff' fill_alpha='50' />

		<!--text transition='dissolve' delay='1' color='ffffff' alpha='50' size='62' x='85' y='209' width='400' height='300' shadow='high'>production</text-->
		<!--image transition='dissolve' url='<?php echo base_url() ?>charts.swf?library_path=charts_library&xml_source=xml%2FGallery_Preview_2.xml' /-->
		<image transition='dissolve' url='<?php echo base_url() ?>charts.swf?library_path=<?php echo base_url() ?>charts_library&xml_source=<?php echo base_url() ?>chart/xml/2/' />
	</draw>
	<filter>
		<!--shadow id='high' distance='5' angle='45' alpha='35' blurX='10' blurY='10' />
		<shadow id='high2' distance='5' angle='45' alpha='35' blurX='10' blurY='10' knockout='true' />
		<shadow id='low' distance='2' angle='45' alpha='35' blurX='5' blurY='5' />
		<bevel id='bg' angle='90' blurX='0' blurY='100' distance='25' highlightAlpha='10' highlightColor='ffffff' shadowColor='4c5e6f' shadowAlpha='20' inner='true' /-->

	</filter>

	<legend layout='hide' />
	<series_color>
		<color>ff0000</color>
		<color>eeaa55</color>
	</series_color>
		
</chart>

