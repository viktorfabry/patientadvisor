<html>
<head>
<title><?php echo $title; ?></title>


<link rel="stylesheet" href="<?php echo base_url();?>css/style_admin.css" type="text/css" />
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.4.2.js"></script>
<script type="text/javascript">
$(function(){
    $(".fancyClose").click(function(){
       parent.$.fancybox.close();
    })
})
</script>

</head>
<body>
    <div class="smallPopUp">
        <h1><?php echo $title; ?></h1>
        <?php echo $msg; ?>
    </div>
</body>
</html>