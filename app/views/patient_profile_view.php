<?php echo $head; ?>
<?php echo $topBar; ?>
<?php echo $header; ?>

<div id="main" class="patient-profile container">
    <div class="row_wrapper curves10">
        <div class="row">
            <?php echo $msgBlock; ?>

            <div class="fourcol clear">
                <h3>Patient information</h3>

                <table>
                <tr>
                    <td><span>Gender:</span></td>
                    <td><?php echo $gender; ?></td>
                </tr>

                <tr>
                    <td><span>Date of Birth:</span></td>
                    <td><?php echo $dob; ?></td>
                </tr>

                <tr>
                    <td><span>Height:</span></td>
                    <td><?php echo $height; ?></td>
                </tr>

                <tr>
                    <td><span>Active:</span></td>
                    <td><?php echo $active; ?></td>
                </tr>

                <tr>
                    <td><span>Smoker:</span></td>
                    <td><?php echo $smoker; ?></td>
                </tr>

                <tr>
                    <td><span>Date stopped smoking:</span></td>
                    <td><?php echo $date_stopped_smoking; ?></td>
                </tr>
                <tr>
                    <td><span>Has diabetes:</span></td>
                    <td><?php echo $diabetes; ?></td>
                </tr>
                <tr>
                    <td><span>Has own Blood Pressure Machine:</span></td>
                    <td><?php echo $ownbpmachine; ?></td>
                </tr>
                <tr>
                    <td><span>Has High Blood Pressure:</span></td>
                    <td><?php echo $hypertension; ?></td>
                </tr>
                <tr>
                    <td><span>Has heart disease:</span></td>
                    <td><?php echo $IHD; ?></td>
                </tr>
                </table>

                <?php

                $formC = form_label('I prefer to use', 'gender');
                $formC .= form_radio('imp_measure', 'N', set_radio('MOI', $imp_measure, $metric), 'id="metric" class="unit"');
                $formC .= '<span class="radioLabel">Metric</span>';
                $formC .= form_radio('imp_measure', 'Y', set_radio('MOI', $imp_measure, $imp), 'id="imperial" class="unit"');
                $formC .= '<span class="radioLabel">Imperial</span>';
                $formC .= form_error('MOI');


                echo $formC;
                ?>


            </div>
            <div class="twocol">
            </div>
            <div class="fourcol">

                <h3>Account information</h3>


                <table>
                <tr>
                    <td><span>Username:</span></td>
                    <td><?php echo $username; ?></td>
                </tr>

                <tr>
                    <td><span>Email Address:</span></td>
                    <td><?php echo $email; ?></td>
                </tr>
                </table>


                <h3>Personal information</h3>
                <table>
                <tr>
                    <td><span>First Name:</span></td>
                    <td><?php echo $fname; ?></td>
                </tr>
                <tr>
                    <td><span>Last Name:</span></td>
                    <td><?php echo $lname; ?></td>
                </tr>
                <tr>
                    <td><span>Address:</span></td>
                    <td><?php echo $address; ?></td>
                </tr>
                <tr>
                    <td><span>City:</span></td>
                    <td><?php echo $city; ?></td>
                </tr>
                <tr>
                    <td><span>Post Code:</span></td>
                    <td><?php echo $postcode; ?></td>
                </tr>
                <tr>
                    <td><span>Phone:</span></td>
                    <td><?php echo $phone; ?></td>
                </tr>
                </table>


            </div>
            <div class="twocol last">
            </div>

        </div>
    </div> <!-- main inner -->
</div> <!-- main -->

<?php echo $footer; ?>
<?php $this->load->view('foot'); ?>