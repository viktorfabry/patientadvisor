<?php echo $head; ?>
<?php echo $topBar; ?>
<?php echo $header; ?>

<div id="main" class="container">
    <div class="row_wrapper curves10">
        <div class="row padTop">
            <!--/////////////////// weight start///////////////////////////-->
            <div id="weight" class="eightcol graphContainer small"> </div>
            <div class="fourcol last">


                <div class="inputfields">
                    <?php
                    $form = form_open('bodysize/', 'id="add_weight"');
                    $form .= ($this->session->flashdata('weightForm')) ? app_msg($this->session->flashdata('weightForm')) : '';
                    $form .= form_hidden('url', $this->uri->uri_string());
                    $form .= form_hidden('weightForm', 1);
                    $form .= form_label($weight_label, 'weight');
                    $form .= form_input('weight',set_value('weight'));
                    $form .= form_error('weight');
                    $form .= form_label('Note <span>(160)</span>', 'note');
                    $form .= form_textarea(array('name'=>'note','rows'=>'3' ),set_value('note'),'id="note"');
                    $form .= form_error('note');
                    $form .= form_submit('submit', 'submit', 'class="btn orange big marginT15 right"');
                    $form .= anchor(base_url() . 'table_weight/', 'Historical Readings', 'class="btn big marginT15 left popUp980"');
                    $form .= form_close();
                    echo $form;
                    ?>                    
                </div>

            </div>
        </div>

    <!--/////////////////// weight end///////////////////////////-->

    <!--/////////////////// bmi start///////////////////////////-->
        <div class="row padTop">
            <div id="BMI" class="eightcol graphContainer small"> </div>
            <div class="fourcol last">

            </div>
        </div>
    <!--/////////////////// bmi end///////////////////////////-->

    <!--/////////////////// waist girth start///////////////////////////-->
        <div class="row">
            <div id="waistgirth" class="eightcol graphContainer small"> </div>
            <div class="fourcol last">
                <div class="inputfields">
                    <?php
                    $form = form_open('bodysize/', 'id="add_girth"');
                    $form .= $this->session->flashdata('girthForm');
                    $form .= form_hidden('url', $this->uri->uri_string());
                    $form .= form_hidden('girthForm', 1);
                    $form .= form_label($girth_label, 'girth');
                    $form .= form_input('girth',set_value('girth'));
                    $form .= form_error('girth');
                    $form .= form_label('Note <span>(160)</span>', 'girthNote');
                    $form .= form_textarea(array('name'=>'girthNote','rows'=>'3' ),set_value('girthNote'),'id="note"');
                    $form .= form_error('girthNote');
                    $form .= form_submit('submit', 'submit', 'class="btn orange big marginT15 right"');
                    $form .= anchor(base_url() . 'table_girth/', 'Historical Readings', 'class="btn big marginT15 left popUp980"');
                    $form .= form_close();
                    echo $form;
                    ?>

                </div>
            </div>
        </div>
    </div>
    <!--/////////////////// waist girth end///////////////////////////-->

</div> <!-- main -->

<?php echo $footer; ?>
<?php echo $hc_weight; ?>
<?php echo $hc_bmi; ?>
<?php echo $hc_waistgirth; ?>
<?php $this->load->view('foot'); ?>