<?php echo $head; ?>
<?php echo $topBar; ?>
<?php echo $header; ?>

<div id="main" class="container">
    <div class="row_wrapper curves10">
        <div class="row">
            <!--/////////////////// bloodpressure start/////////////////////////// style="width: 680px; height:300px"-->
            <div id="graph" class="eightcol graphContainer"> </div>


            <div class="fourcol last">

                <?php echo modules::run('bp_advice'); ?>

                <div class="inputfields">
                    <?php
                    $formBP = form_open('bloodpressure/', 'id="add_bp"');
                    $formBP .= form_hidden('url', $this->uri->uri_string());
                    $formBP .= form_label('Systolic', 'systolic');
                    $formBP .= form_input('systolic',set_value('systolic'),'id="systolic"');
                    $formBP .= form_error('systolic');
                    $formBP .= form_label('Diastolic', 'diastolic');
                    $formBP .= form_input('diastolic',set_value('diastolic'),'id="diastolic"');
                    $formBP .= form_error('diastolic');
                    $formBP .= form_label('Note <span>(160)</span>', 'note');
                    $formBP .= form_textarea(array('name'=>'note','rows'=>'3' ),set_value('note'),'id="note"');
                    $formBP .= form_error('note');
                    $formBP .= form_submit('submit', 'submit', 'class="btn orange big marginT15 right"');
                    $formBP .= anchor(base_url() . 'table_bp/', 'Historical Readings', 'class="btn big marginT15 left popUp980"');
                    $formBP .= form_close();
                    echo $formBP;
                    ?>

                </div>


            </div>
            <!--/////////////////// bloodpressure end///////////////////////////-->

        </div> <!-- row -->
    </div> <!-- row wrapper -->
</div> <!-- main -->

<?php echo $footer ?>
<?php echo $hc_bp ?>
<?php $this->load->view('foot'); ?>