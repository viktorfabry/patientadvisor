<?php echo modules::run('head'); ?>
<?php echo modules::run('header'); ?>


<div id="main">
<div class="inner registration">

<?php echo validation_errors(); ?>


<form class="form registerForm" method="post" action="<?php echo base_url(); ?>dr_register/" name="register">        
<ul>
	<li class="account clearfix">
		<h3><span>1.</span> Account information</h3>
		<label>Username</label>
		<input size="50" name="uname" type="text" value="<?php echo set_value('uname'); ?>"  />

		<input type="submit" value="Submit" />

		<label>Email</label><input size="50" name="email" type="text" value="<?php echo set_value('email'); ?>" />
		<label>Confirm Email</label><input size="50" name="email_confirmed" type="text" value="<?php echo set_value('email_confirmed'); ?>" /> 	
		<label>Password</label><input size="50" name="pword" type="text" value="<?php echo set_value('pword'); ?>" /> 
		<label>Confirm Password</label><input size="50" name="passconf" type="text" value="<?php echo set_value('passconf'); ?>" /> 
</li>	    

<li class="personal clearfix">
	<h3><span>2.</span> Personal information</h3>

	<label>First Name</label>
	<input size="50" name="fname" type="text" value="<?php echo set_value('fname'); ?>"  />
	
	<label>Last Name</label>
	<input size="50" name="lname" type="text" value="<?php echo set_value('lname'); ?>"  />
	
	<label>Phone</label>
	<input size="50" name="phone" type="text" value="<?php echo set_value('phone'); ?>"  />
</li>	    

<li class="data clearfix">
	<h3><span>3.</span> Practice information</h3>
	
	

	<label class="long">Record Number (EMIS)</label>
	<input size="50" name="EMIS" type="text" value="<?php echo set_value('EMIS'); ?>"  />
	
	<label class="long">Opening</label>
	<input size="50" name="opening" type="text" value="<?php echo set_value('opening'); ?>"  
	
</li>
</ul>
</form>


<div class="clear"></div> 
</div> <!-- main inner -->
</div> <!-- main -->

<?php echo modules::run('footer'); ?>
