<?php echo modules::run('head'); ?>
<?php echo modules::run('header'); ?>


<div id="main">
<div class="inner registration">

<?php echo validation_errors(); ?>


<form class="form registerForm" method="post" action="<?php echo base_url(); ?>patient/profile/" name="register">        
<ul>
	<li class="account clearfix">
		<h3><span>1.</span> Account information</h3>
		<label>Username</label>
		<input size="50" name="uname" type="text" value="<?php echo set_value('uname', $uname); ?>"  />

		<input type="submit" value="Submit" />

		<label>Email</label><input size="50" name="email" type="text" value="<?php echo set_value('email', $email); ?>" />
		<div style="display: none">
			<input type="hidden" name="newpass" value="N" />
			<label>Password</label><input size="50" name="pword" type="password" value="<?php echo set_value('pword', $pword); ?>" /> 
			<label>Confirm Password</label><input size="50" name="passconf" type="text" value="<?php echo set_value('passconf'); ?>" />
		</div>
</li>	    

<li class="personal clearfix">
	<h3><span>2.</span> Personal information</h3>

	<label>First Name</label>
	<input size="50" name="fname" type="text" value="<?php echo set_value('fname', $fname); ?>"  />
	
	<label>Last Name</label>
	<input size="50" name="lname" type="text" value="<?php echo set_value('lname', $lname); ?>"  />
	
	<label>Address</label>
	<input size="50" name="address" type="text" value="<?php echo set_value('address', $address); ?>"  />
	
	<label>City or Town</label>
	<input size="50" name="city" type="text" value="<?php echo set_value('city', $city); ?>"  />
	
	<label>Post Code</label>
	<input size="50" name="postcode" type="text" value="<?php echo set_value('postcode', $postcode); ?>"  />
	
	<label>Phone</label>
	<input size="50" name="phone" type="text" value="<?php echo set_value('phone', $phone); ?>"  />
</li>	    

<li class="data clearfix">
	<h3><span>3.</span> Personal information</h3>
	
	<label>Gender:</label>
	<p class="note">
	<input type="radio" name="gender" value="M" <?php echo set_radio('gender', 'M', compare('M',$gender)); ?> /> Male
	<input type="radio" name="gender" value="F" <?php echo set_radio('gender', 'F', compare('F',$gender)); ?> /> Female
	</p>
	
	<label>Date of Birth</label>

	
	
	
	<input size="50" name="dob" type="text" value="<?php echo set_value('dob', $dob); ?>" />
	<p class="note">DD/MM/YYYY</p>
	
	
		
	<div class="measurement"
	    <label>I prefer to use</label>
	    <p class="note unit">
		<input type="radio" id="imperial" name="imp_measure" value="N" <?php echo set_radio('imp_measure', 'N', compare('N',$imp_measure)); ?> /> Metric units
	    	<input type="radio" id="metric" name="imp_measure" value="Y" <?php echo set_radio('imp_measure', 'Y', compare('Y',$imp_measure)); ?> /> Imperial units
	</p>
	     <div class="metricBlock">
			<label>height (cm)</label>
			<input size="50" id="height" name="height" type="text" value="<?php echo set_value('height'); ?>" />
	    </div>
	    <div class="imperialBlock">  
			<label>height (Imp)</label>
			<input size="50" id="height_imp" name="height_ft" type="text" value="<?php echo set_value('height_imp'); ?>" /><span>ft</span>
			
			<input size="50" id="height_imp" name="height_inch" type="text" value="<?php echo set_value('height_imp'); ?>" /><span>inch</span>
	    </div>
	</div>	
	
	
			
	<label>active</label>
	<select  name="active"> 
		<option value="">Please Select</option> 
		<option value="Y" <?php echo set_select('active', 'Y', compare('Y',$active)); ?> >Yes</option>
		<option value="N" <?php echo set_select('active', 'N', compare('N',$active)); ?> >No</option>
	</select>
	
	
	
	
	 
	<label>smoker</label>
	<select  name="smoker">  
		<option value="">Please Select</option>
		<option value="Y" <?php echo set_select('smoker', 'Y', compare('Y',$smoker)); ?> >Yes</option>
		<option value="N" <?php echo set_select('smoker', 'N', compare('N',$smoker)); ?> >No</option>
	</select>
	
	<label>Date stopped smoking</label><input size="50" name="date_stopped_smoking" type="text" value="<?php echo set_value('date_stopped_smoking, $date_stopped_smoking'); ?>" />  
	<p class="note">MM/YYYY</p>
	
	
	
	<label class="long">My doctor has told me I have diabetes</label>
	<select  name="diabetes"> 
		<option value="">Please Select</option> 
		<option value="Y" <?php echo set_select('diabetes', 'Y', compare('Y',$diabetes)); ?> >Yes</option>
		<option value="N" <?php echo set_select('diabetes', 'N', compare('N',$diabetes)); ?> >No</option>
	</select>
	
	<label class="long">I have my own Blood Pressure Machine at home</label>
	<select  name="ownbpmachine"> 
		<option value="">Please Select</option> 
		<option value="Y" <?php echo set_select('ownbpmachine', 'Y', compare('Y',$ownbpmachine)); ?> >Yes</option>
		<option value="N" <?php echo set_select('ownbpmachine', 'N', compare('N',$ownbpmachine)); ?> >No</option>
	</select>
	
	
	
	
	  
	<label class="long">My doctor has told me I have High Blood Pressure</label>
	<select  name="hypertension">
		<option value="">Please Select</option>  
		<option value="Y" <?php echo set_select('hypertension', 'Y', compare('Y',$hypertension)); ?> >Yes</option>
		<option value="N" <?php echo set_select('hypertension', 'N', compare('N',$hypertension)); ?> >No</option>
	</select>
	
	
	<label class="long">My doctor has told me I have some heart disease</label>
	<select  name="IHD">  
		<option value="">Please Select</option>
		<option value="Y" <?php echo set_select('IHD', 'Y', compare('Y',$IHD)); ?> >Yes</option>
		<option value="N" <?php echo set_select('IHD', 'N', compare('N',$IHD)); ?> >No</option>
	</select>
	 
	    <label class="long">Please select your Doctor</label>
<?php
     $atr = 'class="dr_dd" id="doctor"';
	 echo form_dropdown('doctor', $dr_dd, $dr_id, $atr); 
    ?>



	<label class="long">Doctor's Record Number (EMIS)</label>
	<input size="50" name="EMIS" type="text" value="<?php echo set_value('EMIS', $EMIS); ?>"  />
	
	<label class="long">NHS Number</label>
	<input size="50" name="NHS" type="text" value="<?php echo set_value('NHS', $NHS); ?>"  
	
</li>
</ul>
</form>


<div class="clear"></div> 
</div> <!-- main inner -->
</div> <!-- main -->

<?php echo modules::run('footer'); ?>
