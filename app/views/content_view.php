<?php echo $head; ?>
<?php echo $topBar; ?>
<?php echo $header; ?>


<div id="main" class="content container">
    <div class="row_wrapper curves10">
        <div class="row">
            <div id="container">
                   <h1><?php echo (isset($title) && $title) ? $title : ''; ?></h1>
                   <?php if (isset($body) && $body){
                       $columns = '';
                       $cbody = explode('<p>{|}</p>',$body);
                       $columns .= '<div class="fivecol">'.((isset($cbody[0]) && $cbody[0]) ? $cbody[0] : '').'</div>';
                       $columns .= '<div class="fivecol">'.((isset($cbody[1]) && $cbody[1]) ? $cbody[1] : '').'</div>';
                       
                       echo $columns;
                   }
                ?>
                   <div class="twocol last">
                   <?php
                   $links ='';
                        if (isset($side_links) && $side_links) {
                            foreach ($side_links as $catName => $cat) {
                        $links .= '<ul>';
                        $links .= '<li><h3>'.$catName.'</h3></li>';

                            if (isset($cat) && $cat) {
                                foreach ($cat as $link) {
                                    if(isset($link['url']) && isset($link['title'])){
                                    $links .=  '<li>'.anchor($link['url'],$link['title']).'</li>';
                                    }
                                }

                            }
                        $links .= '</ul>';
                        }
                        }
                        echo $links;
                   ?>
                   </div>
            </div>

        </div> <!-- row -->
    </div> <!-- row wrapper -->
</div> <!-- main -->

<?php echo $footer; ?>
<?php $this->load->view('foot'); ?>