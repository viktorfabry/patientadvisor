<?php echo modules::run('adminhead'); ?>
<?php echo modules::run('adminheader'); ?>
<?php echo modules::run('msg',$msg = ''); ?>

<div id="main">
<div class="inner">
    
  <ul id="menu">
        <?php
          if(isset($main_menu) && $main_menu!=''){
                $menu = "";
                foreach ($main_menu as $key => $value) {
                    $menu .= '<li><a class="curvesTop10 ';
                    if($active == $key) $menu .= 'active';
                    $menu .= '" href="'.base_url().$key.'"';
                    $menu .= 'title="'.$value.'">'.$value.'</a></li>';
                }
                echo $menu;
          }
        ?>
     </ul>

<div class="clear"></div>




</div> <!-- main inner -->
</div> <!-- main -->
 
<?php echo modules::run('adminfooter'); ?>
<?php echo modules::run('foot'); ?>