<?php echo modules::run('adminhead'); ?>
<?php echo modules::run('adminheader'); ?>
<?php echo modules::run('msg',$msg = ''); ?>

<div id="main">
    <div class="inner">

        <div class="fileUploader">
            <div id="file-uploader-demo1">
                <noscript>
                    <p>Please enable JavaScript to use file uploader.</p>
                    <!-- or put a simple form for upload here -->
                </noscript>
            </div>
        </div>

        <div id="invitePatientsSubmit">
            <form class="form" method="post" action="<?php echo base_url(); ?>auth/invite_users/" name="set_access" id="set_access">
                <input id="file_id" name="file_id" type="hidden" value="" />
                <p class="left">Please confirm that you want to send invitation for the people below.</p>
                <input  class="btn orange big left marginL15 marginB15" type="submit" value="Submit" />
            </form>
        </div>

        <script>
            function createUploader(){
                var uploader = new qq.FileUploader({
                    element: document.getElementById('file-uploader-demo1'),
                    action: '<?php echo base_url() . 'admin/patients/upload_patient_list' ?>',
                    allowedExtensions: ['csv'],
                    onSubmit: function(id, fileName){},
                    onProgress: function(id, fileName, loaded, total){},
                    onComplete: function(id, fileName, responseJSON){
                        if(responseJSON.file_id) getPatientTable(responseJSON.file_id);
                    },
                    onCancel: function(id, fileName){},
                    debug: false
                });
            }

            function getPatientTable(file_id){
                $.ajax({
                    url: "<?php echo base_url() . 'admin/patients/generate_patient_table/' ?>" + file_id,
                    success: function(data){
                        $('#file_id').val(file_id);
                        $('#patientsTable').html(data);
                        $('#invitePatientsSubmit').slideDown();
                    }
                });


            }

            // in your app create uploader as soon as the DOM is ready
            // don't wait for the window to load
            window.onload = createUploader;
        </script>

        <div id="patientsTable">
            <p>The CSV file should have the following column titles: fName, lName, email, NINo, EMIS</p>
        </div>
        <div class="clear"></div>
    </div> <!-- main inner -->
</div> <!-- main -->

<?php echo modules::run('adminfooter'); ?>
<?php echo modules::run('foot'); ?>
