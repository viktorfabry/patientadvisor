<?php echo modules::run('adminhead'); ?>
<?php echo modules::run('adminheader'); ?>
<?php echo modules::run('msg',$msg = ''); ?>
    
<div id="main">
<div class="inner">
<p class="message"><?php echo $this->session->flashdata('message'); ?></p>
<a href="<?php echo base_url(); ?>admin/commissionings/add/" class="popUpSmall btn orange big m1">Add new Local Health Authority</a>

<form class="form" method="post" action="<?php echo base_url(); ?>admin/commissionings/" name="set_access" id="set_access">


<table class="display" >
    <thead>
    <tr>
        <th width="300">Name</th>
        <th>Description</th>
        <th width="150">Action</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $list = '';

    foreach ($gpcoms as $c) {
        $list .= '<tr>';
        $list .= '<td>'.$c['name'].'</td>';
        $list .= '<td>'.$c['description'].'</td>';
        $list .= '<td>';
        $list .= anchor('admin/commissionings/edit/'.$c['id'], 'edit', 'class="popUpSmall btn  right"');
        $list .= anchor('admin/commissionings/delete/'.$c['id'], 'delete', 'title="'.$c['name'].'" class="delete btn red marginR15 right" rel="no-follow"');
        $list .= '</td>';
        $list .= '</tr>';
    }

    echo $list;
    ?>
    </tbody>
</table>
</form>
<div class="clear"></div>
</div> <!-- main inner -->
</div> <!-- main -->
 
<?php echo modules::run('adminfooter'); ?>
<?php echo modules::run('foot'); ?>
