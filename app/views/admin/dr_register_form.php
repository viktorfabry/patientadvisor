<?php echo modules::run('adminhead'); ?>
<?php echo modules::run('adminheader'); ?>
<?php echo modules::run('msg',$msg = ''); ?>

<?php
if ($use_username) {
    $username = array(
        'name' => 'username',
        'id' => 'username',
        'value' => set_value('username'),
        'maxlength' => $this->config->item('username_max_length', 'tank_auth'),
        'size' => 30,
    );
}
$email = array(
    'name' => 'email',
    'id' => 'email',
    'value' => set_value('email'),
    'maxlength' => 80,
    'size' => 30,
);
$password = array(
    'name' => 'password',
    'id' => 'password',
    'value' => set_value('password'),
    'maxlength' => $this->config->item('password_max_length', 'tank_auth'),
    'size' => 30,
);
$confirm_password = array(
    'name' => 'confirm_password',
    'id' => 'confirm_password',
    'value' => set_value('confirm_password'),
    'maxlength' => $this->config->item('password_max_length', 'tank_auth'),
    'size' => 30,
);

$fname = array(
    'name' => 'fname',
    'id' => 'fname',
    'value' => set_value('fname'),
    'maxlength' => 150,
    'size' => 30,
);
$lname = array(
    'name' => 'lname',
    'id' => 'lname',
    'value' => set_value('lname'),
    'maxlength' => 150,
    'size' => 30,
);

$phone = array(
    'name' => 'phone',
    'id' => 'phone',
    'value' => set_value('phone'),
    'maxlength' => 150,
    'size' => 30,
);
$EMIS = array(
    'name' => 'EMIS',
    'id' => 'EMIS',
    'value' => set_value('EMIS'),
    'maxlength' => 150,
    'size' => 30,
);
$opening = array(
    'name' => 'opening',
    'id' => 'opening',
    'value' => set_value('opening'),
    'maxlength' => 150,
    'size' => 30,
);
$captcha = array(
    'name' => 'captcha',
    'id' => 'captcha',
    'maxlength' => 8,
);
?>

<div id="main">
    <div class="inner drRegistration">

        <?php //echo validation_errors(); ?>


        <?php echo form_open($this->uri->uri_string()); ?>

        
        <ul>
            <li class="account clearfix">
                <h3><span>1.</span> Account information</h3>

                <?php echo form_label('Username', $username['id']); ?>
                <?php echo form_input($username); ?>
                <?php echo app_msg(form_error($username['name']),'error'); ?>
                <?php echo isset($errors[$username['name']])?app_msg($errors[$username['name']],'error'):'';    ?>

                <?php echo form_label('Email Address', $email['id']); ?>
                <?php echo form_input($email); ?>
                <?php echo app_msg(form_error($email['name']),'error'); ?>
                <?php echo isset($errors[$email['name']]) ? app_msg($errors[$email['name']],'error') : ''; ?>

                <?php echo form_label('Password', $password['id']); ?>
                <?php echo form_password($password); ?>
                <?php echo app_msg(form_error($password['name']),'error'); ?>

                <?php echo form_label('Confirm Password', $confirm_password['id']); ?>
                <?php echo form_password($confirm_password); ?>
                <?php echo app_msg(form_error($confirm_password['name']),'error'); ?>



            </li>

            <li class="personal clearfix">
                <h3><span>2.</span> Personal information</h3>

                <?php echo form_label('First Name', $fname['id']); ?>
                <?php echo form_input($fname); ?>
                <?php echo app_msg(form_error($fname['name']),'error'); ?>

                <?php echo form_label('Last Name', $lname['id']); ?>
                <?php echo form_input($lname); ?>
                <?php echo app_msg(form_error($lname['name']),'error'); ?>


                <?php echo form_label('Phone', $phone['id']); ?>
                <?php echo form_input($phone); ?>
                <?php echo app_msg(form_error($phone['name']),'error'); ?>
            </li>

            <li class="data clearfix">
                <h3><span>3.</span> Practice information</h3>

                <?php echo $groupSelector; ?>


                <?php echo form_label('Record Number (EMIS)', $EMIS['id']); ?>
                <?php echo form_input($EMIS); ?>
                <?php echo app_msg(form_error($EMIS['name']),'error'); ?>

                <?php echo form_label('Opening hours', $opening['id']); ?>
                <?php echo form_input($opening); ?>
                <?php echo app_msg(form_error($opening['name']),'error'); ?>

            </li>
        </ul>
        <input type="submit" value="Submit" class="submit btn orange big " />
        </form>


        <div class="clear"></div>
    </div> <!-- main inner -->
</div> <!-- main -->



<?php echo modules::run('adminfooter'); ?>
<script src="<?php echo base_url(); ?>js/dynamicSelect.jQuery.js" type="text/javascript"></script>

<script type="text/javascript">
$(function(){

        var options  = {
                url: '<?php echo base_url(); ?>admin/practices/getpracticenames/',
                newSelectName: 'practices',
                newSelectLabel: 'Practice Names'
            };

      $("#gpcom").dynamicSelect(options);
});
</script>
<?php echo modules::run('foot'); ?>