<?php echo modules::run('adminhead'); ?>
<?php echo modules::run('adminheader'); ?>
<?php echo modules::run('msg',$msg = ''); ?>

<div id="main">
<div class="inner">

<h1>Patient links</h1>
<button id="btn_toggle_NewLink" class="toggle_btn toggle_NewLink" >Add New Link</button>
<div class="toggle_NewLink hidden relative curves10 bGray bgGray ds5">
		<div id="btn_toggle_NewLink" class="toggle_btn close curves10 bGray" >x</div>
		
<form class="form" method="post" action="<?php echo base_url(); ?>admin/patient_links/add/" name="">        
<ul>
		<li>
				<label>link name</label>		
				<input size="100" name="name" type="text" value=""  />
		</li>
		<li>
				<label>url</label>
				<input size="100" name="url" type="text" value="" />
		</li>
		<li class="categorySelect">
				<label>category</label>
				<input class="right addLinkSubmit" type="submit" value="Submit" />
				<?php echo form_dropdown('links', $links, $link_id, 'id = "links"'); ?>
		</li>

</ul>
</form>
</div> <!-- Add new -->

<div class="linkList">
		<?php echo $linklist; ?>
</div>

<div class="clear"></div> 
</div> <!-- main inner -->
</div> <!-- main -->
 
<?php echo modules::run('adminfooter'); ?>
<?php echo modules::run('foot'); ?>