<html>
<head>
<title>Add New GP Commissioning</title>


<link rel="stylesheet" href="<?php echo base_url();?>css/style_admin.css" type="text/css" />
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.4.2.js"></script>
<script type="text/javascript">
$(function(){
    $(".fancyClose").click(function(){
       parent.$.fancybox.close();
    })
})
</script>
</head>
<body>
    <div class="smallPopUp">
        <h1>Add New Local Health Authority</h1>
        <p class="message"><?php echo $this->session->flashdata('message'); ?></p>

            <?php
                $form = '';
                $form .= form_open('admin/commissionings/add');
                $form .= form_label('Local Health Authority Name', 'name');
                $form .= form_input('name', '');
                $form .= form_label('Local Health Authority Description', 'description');
                $form .= form_textarea(array('name' => 'description', 'rows' => '6'));
                $form .= form_submit('submit_access', 'submit', 'class="btn orange big"');
                $form .= form_submit('submit_access', 'cancel', 'class="fancyClose  btn big"');
                $form .= form_close();
                echo $form;
            ?>
    </div>
</body>
</html>