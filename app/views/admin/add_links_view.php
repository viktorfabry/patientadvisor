<?php echo modules::run('adminhead'); ?>
<?php echo modules::run('adminheader'); ?>
<?php echo modules::run('msg',$msg = ''); ?>
<div id="main">
<div class="inner">
<h2>Add new link</h2>
<form class="form" method="post" action="<?php echo base_url(); ?>admin/dr_links/add/" name="">        
<ul>
		<li>
				<label>link name</label>		
				<input size="100" name="link" type="text" value=""  />
		</li>
		<li>
				<label>url</label>
				<input size="100" name="url" type="text" value="" />
		</li>
		<li class="categorySelect">
				<label>category</label>
				<?php echo form_dropdown('categories', $categories, $cat_id, 'id = "categories"'); ?>
				<button class="addNewCat" type="button" >Or Add New Category</button>
		</li>
		<li class="newCat hidden">
				<label>Add new category</label>
				<input size="100" name="new_cat" type="text" value="" />
				<button class="addNewCat" type="button" >Or select an existing category</button>
		</li>
		<li class="">
				<input class="addLinkSubmit" type="submit" value="Submit" />
				
		</li>
</ul>
</form>


<?php
//print_r($advice); 
?>

<div class="clear"></div> 
</div> <!-- main inner -->
</div> <!-- main -->
 
<?php echo modules::run('adminfooter'); ?>
<?php echo modules::run('foot'); ?>