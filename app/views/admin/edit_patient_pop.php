<html>
<head>
<title>Edit Patient</title>


<link rel="stylesheet" href="<?php echo base_url();?>css/style_admin.css" type="text/css" />
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.4.2.js"></script>
<script type="text/javascript">
$(function(){
    $(".fancyClose").click(function(){
       parent.$.fancybox.close();
    })
})
</script>
</head>
<body>
    <div class="smallPopUp clearfix">
        <h1>Edit Patient</h1>

            <?php
                $form = '';
                $form .= form_open('admin/patients/edit');
                $form .= form_hidden('patient_id', $p['patient_id']);
                $form .= form_label('First name', 'fname');
                $form .= form_input('fname', $p['fname']);
                $form .= form_label('Last name', 'lname');
                $form .= form_input('lname', $p['lname']);
                $form .= form_label('GP Name', 'doctor');
                $form .= form_dropdown('doctor',$doctors,$doctor_id);

                $form .= form_label('Address', 'address');
                $form .= form_input('address', $p['address']);
                $form .= form_label('City', 'city');
                $form .= form_input('city', $p['city']);
                $form .= form_label('Postcode', 'postcode');
                $form .= form_input('postcode', $p['postcode']);
                $form .= form_label('email', 'email');
                $form .= form_input('email', $p['email']);
                $form .= form_label('Phone', 'phone');
                $form .= form_input('phone', $p['phone']);

                $form .= form_label('NHS', 'NHS');
                $form .= form_input('NHS', $p['NHS']);
                $form .= '<div class="btn_box">';
                $form .= form_submit('submit_access', 'submit', 'class="btn orange big"');
                $form .= form_submit('submit_access', 'cancel', 'class="fancyClose  btn big"');
                $form .= '</div> <!-- btn_box close -->';
                $form .= form_close();
                echo $form;
            ?>
    </div>
</body>
</html>