<?php echo modules::run('adminhead'); ?>
<?php echo modules::run('adminheader'); ?>
<?php echo modules::run('msg',$msg = ''); ?>

<div id="main" class="contents">
<div class="inner">
    <?php echo anchor('admin/contents/newcontent/', 'Add new content', 'class="btn orange big right m1"'); ?>
<h1>Contents</h1>

<ul class="list clear marginT15">
<?php

if (isset($contents)) {
		
		
foreach($contents as $c): ?>
		<li>
		<h3>
                <a class="btn  right" href="<?php echo base_url().'admin/contents/edit/'.$c['id'].'/'; ?>">edit</a>
		<a class="btn delete marginR15 red right" href="<?php echo base_url().'admin/contents/delete/'.$c['id'].'/'; ?>">delete</a>
		
		<?php echo $c['title']; ?>
		
		</h3>
		</li>
		
<?php endforeach; } ?>
</ul>


<div class="clear"></div> 
</div> <!-- main inner -->
</div> <!-- main -->
 
<?php echo modules::run('adminfooter'); ?>
<?php echo modules::run('foot'); ?>