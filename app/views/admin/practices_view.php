<?php echo modules::run('adminhead'); ?>
<?php echo modules::run('adminheader'); ?>
<?php echo modules::run('msg',$msg = ''); ?>
    
<div id="main">
<div class="inner">
<p class="message"><?php echo $this->session->flashdata('message'); ?></p>
<a href="<?php echo base_url(); ?>admin/practices/add/" class="popUpSmall btn orange big m1">Add new practice</a>

<form class="form" method="post" action="<?php echo base_url(); ?>admin/practices/" name="set_access" id="set_access">


<table class="display" >
    <thead>
    <tr>
        <th width="300">Name</th>
        <th width="300">Commissioning</th>
        <th>Description</th>
        <th width="150">Action</th>
    </tr>
    </thead>
    <tbody>

    <?php
    $list = '';

    foreach ($practices as $p) {
        $list .= '<tr>';
        $list .= '<td>'.$p['name'].'</td>';
        $list .= '<td>'.$p['gpcom'].'</td>';
        $list .= '<td>'.$p['description'].'</td>';
        $list .= '<td>'.anchor('admin/practices/edit/'.$p['id'], 'edit', 'class="popUpSmall btn  right"');
        $list .= anchor('admin/practices/delete/'.$p['id'], 'delete', 'title="'.$p['name'].'" class="delete btn red marginR15 right" rel="no-follow"').'</td>';
        $list .= '</tr>';
    }

    echo $list;
    ?>
    </tbody>
</table>
</form>
<div class="clear"></div>
</div> <!-- main inner -->
</div> <!-- main -->
 
<?php echo modules::run('adminfooter'); ?>
<?php echo modules::run('foot'); ?>
