<html>
    <head>
        <title>Edit GP</title>


        <link rel="stylesheet" href="<?php echo base_url(); ?>css/style_admin.css" type="text/css" />
        <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.4.2.js"></script>
        <script type="text/javascript">
            $(function(){
                $(".fancyClose").click(function(){
                    parent.$.fancybox.close();
                })
            })
        </script>
    </head>
    <body>
        <div class="smallPopUp">
            <h1>Edit GP</h1>
            <p class="message"><?php echo $this->session->flashdata('message'); ?></p>

            <?php
            $form = '';
            $form .= form_open('admin/doctors/edit/' . $dr['user_id']);
            $form .= form_label('First name', 'fname');
            $form .= form_input('fname', $dr['fname']);
            $form .= form_label('Last name', 'lname');
            $form .= form_input('lname', $dr['lname']);
            $form .= '<div id="LASelector" >'.((isset($LASelector) && $LASelector) ? $LASelector : '').'</div>';
            $form .= '<div id="practiceSelector" >'.((isset($practiceSelector) && $practiceSelector) ? $practiceSelector : '').'</div>';


            $form .= form_label('EMIS', 'EMIS');
            $form .= form_input('EMIS', $dr['EMIS']);

            $form .= form_label('email', 'email');
            $form .= form_input('email', $dr['email']);
            $form .= form_label('Phone', 'phone');
            $form .= form_input('phone', $dr['phone']);

            $form .= form_label('GP opening time', 'opening');
            $form .= form_textarea(array('name' => 'opening', 'value' => $dr['opening'], 'rows' => '2'));
            $form .= form_submit('submit_access', 'submit', 'class="btn orange big"');
            $form .= form_submit('submit_access', 'cancel', 'class="fancyClose  btn big"');
            $form .= form_close();
            echo $form;
            ?>
        </div>

        <script src="<?php echo base_url(); ?>js/dynamicSelect.jQuery.js" type="text/javascript"></script>

        <script type="text/javascript">
            $(function(){

                var options  = {
                    url: '<?php echo base_url(); ?>admin/practices/getpracticenames/',
                    newSelectName: 'practice',
                    newSelectLabel: 'Practice Names',
                    newSelectBoxID: 'practiceSelector'
                };

                $("#localAuth").dynamicSelect(options);
            });
        </script>

    </body>
</html>