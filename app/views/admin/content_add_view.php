<?php echo modules::run('adminhead'); ?>
<?php echo modules::run('adminheader'); ?>
<?php echo modules::run('msg',$msg = ''); ?>

<?php echo $this->load->view('wysiwyg'); ?>

<div id="main">
<div class="inner">

<h1>Content</h1>
<div id="addNewLink" class="">
<form class="form" method="post" action="<?php echo base_url(); ?>admin/contents/add/" name="">        
<ul>
		<li>
				<label>Title</label>		
				<input size="100" name="title" type="text" value=""  />
		</li>
                <li>
				<label>Friendly Url</label>
				<input size="100" name="url" type="text" value=""  />
		</li>
		<li>
				<label>Tags</label>
				<input size="100" name="tags" type="text" value="" />
		</li>
		<li  class="categorySelect">
				<label>category</label>
				<?php echo form_dropdown('categories', $categories, $cat_id, 'id = "categories"'); ?>
				<button id="btn_newCat" class="toggle_btn addNewCat" type="button" >Or Add New Category</button>
		</li>
		<li id="newCat" class="hidden">
				<label>Add new category</label>
				<input size="100" name="new_cat" type="text" value="" />
				<button id="btn_newCat" class="toggle_btn" type="button" >Or select an existing category</button>
		</li>
                <li>
                    <p>For the 2 column layout, please use the column divider below</p>
                    <p>{|}</p>
                </li>
		<li>
                    
      <textarea name="content" class="tinymce" style="width:100%">
      </textarea>
				
				
		</li>
		<li>
				<input name="submit" type="submit" value="Save content" class="btn orange big" />
		</li>
</ul>
</form>
</div> <!-- Add new -->

<div class="clear"></div> 
</div> <!-- main inner -->
</div> <!-- main -->
 
<?php echo modules::run('adminfooter'); ?>
<?php echo modules::run('foot'); ?>