<?php echo modules::run('adminhead'); ?>
<?php echo modules::run('adminheader'); ?>
<?php echo modules::run('msg',$msg = ''); ?>

<div id="main">
<div class="inner">

<h1><?php echo $pageTitle; ?></h1>

<button id="cat_toggle_NewLink" class="toggle_btn btn orange big toggle_NewLink margin30" >Add New Category</button>
<button id="url_toggle_NewLink" class="toggle_btn btn orange big toggle_NewLink margin30" >Add New Link from URL</button>
<button id="fil_toggle_NewLink" class="toggle_btn btn orange big toggle_NewLink margin30" >Add New Link from file</button>


<div class="toggle_NewLink hidden relative clearfix left curves10 bGray bgGray margin15 padding15">
<div id="btn_toggle_NewLink" class="toggle_btn close curves10 bGray" >x</div>
<form class="form" enctype="multipart/form-data" method="post" action="<?php echo base_url().'admin/linkadmin/add/'.$type; ?>" name="">

<ul>
		<li>
                <?php
                    echo form_label('Link name', 'fname');
                    echo form_input('name');
                ?>

		</li>

		<li class="categorySelect">
				<label>parent category</label>
                                <?php echo form_dropdown('links', $links, $link_id, 'id = "links"'); ?>

		</li>

		<li class="linkFromURL">
				<label>link from url</label>
				<div class="url">
                                    <?php echo form_input('url', ''); ?>
                                </div>

		</li>

		<li class="linkFromFile">
				<label for="file">Link from PDF </label>
                                <?php echo form_upload('userfile', '','class="pdfUploader"'); ?>
		</li>
                <li>
                    <?php echo form_submit('submit_access', 'submit', 'class="btn orange big right marginT15"'); ?>
                </li>




</ul>
</form>
</div> <!-- Add new -->


<!--div class="file"><?php echo modules::run('upload'); ?></div-->

<div class="linkList">
		<?php echo $linklist; ?>
</div>

<div class="clear"></div> 
</div> <!-- main inner -->
</div> <!-- main -->
 
<?php echo modules::run('adminfooter'); ?>
<?php echo modules::run('foot'); ?>