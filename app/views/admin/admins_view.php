<?php echo modules::run('adminhead'); ?>
<?php echo modules::run('adminheader'); ?>
<?php echo modules::run('msg',$msg = ''); ?>
    
<div id="main">
<div class="inner">
<p class="message"><?php echo $this->session->flashdata('message'); ?></p>
<a  href="<?php echo base_url(); ?>adminauth/create_admin/" class="btn orange big m1">Add new admin user</a>

<form class="form" method="post" action="<?php echo base_url(); ?>admin/practices/" name="set_access" id="set_access">


<table class="display" >
    <thead>
    <tr>
        <th width="300">User name</th>
        <th width="150">Role</th>
        <th width="300">Local Authority</th>
        <th width="300">Practice</th>
        <th width="150">Action</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $list = '';

    foreach ($admins as $a) {
        $list .= '<tr>';
        $list .= '<td>'.$a['username'].'</td>';
        $list .= '<td>'.$a['role'].'</td>';
        $list .= '<td>'.(($a['role_id'] == 5) ? $a['groupName'] : $a['groupParentName']).'</td>';
        $list .= '<td>'.(($a['role_id'] == 5) ? '' : $a['groupName']).'</td>';
        $list .= '<td>'.anchor('admin/administrators/edit/'.$a['admin_id'], 'edit', 'class="popUpSmall btn  right"');
        $list .= anchor('admin/administrators/delete/'.$a['admin_id'], 'delete', 'title="delete '.$a['username'].'" class="delete btn red marginR15 right" rel="no-follow"').'</td>';
        $list .= '</tr>';
    }

    echo $list;
    ?>
    </tbody>
</table>
</form>
<div class="clear"></div>
</div> <!-- main inner -->
</div> <!-- main -->
 
<?php echo modules::run('adminfooter'); ?>
<?php echo modules::run('foot'); ?>
