<ul class="admin_submenu clearfix">
    <li><a href="<?php echo base_url(); ?>admin/patients_doctors/">Main</a></li>
    <li><a href="<?php echo base_url(); ?>admin/patients_doctors/patients/">Patient List</a></li>
    <li><a href="<?php echo base_url(); ?>admin/patients_doctors/doctors/">Doctor List</a></li>
    <li><a href="<?php echo base_url(); ?>admin/patients_doctors/assign/">Assign Doctors to Patients</a></li>
</ul>

