<?php echo modules::run('adminhead'); ?>
<?php echo modules::run('adminheader'); ?>
<?php echo modules::run('msg',$msg = ''); ?>
    
<div id="main">
<div class="inner">
<p class="message"><?php echo $this->session->flashdata('message'); ?></p>
<a href="<?php echo base_url(); ?>admin/practices/add/" class="popUpSmall btn orange big margin30">Add new practice</a>

<form class="form" method="post" action="<?php echo base_url(); ?>admin/practices/" name="set_access" id="set_access">


<table class="styled" >
    <tr>
        <th width="300">Name</th>
        <th width="300">EMIS</th>
        <th width="300">Commissioning</th>
        <th width="300">Practice</th>
        <th width="150">Action</th>
    </tr>

    <?php
    $list = '';

    foreach ($doctors as $d) {
        $list .= '<tr>';
        $list .= '<td>'.$d['doctor'].'</td>';
        $list .= '<td>'.$d['EMIS'].'</td>';
        $list .= '<td>'.$d['gpcom'].'</td>';
        $list .= '<td>'.$d['practice'].'</td>';
        $list .= '<td>'.anchor('admin/doctors/edit/'.$d['user_id'], 'edit', 'class="popUpSmall btn  right"');
        $list .= anchor('admin/doctors/delete/'.$d['user_id'], 'delete', 'title="'.$d['doctor'].'" class="delete btn red marginR15 right" rel="no-follow"').'</td>';
        $list .= '</tr>';
    }

    echo $list;
    ?>
</table>
</form>
<div class="clear"></div>
</div> <!-- main inner -->
</div> <!-- main -->
 
<?php echo modules::run('adminfooter'); ?>
<?php echo modules::run('foot'); ?>
