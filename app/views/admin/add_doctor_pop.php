<html>
<head>
<title>Add New GP</title>


<link rel="stylesheet" href="<?php echo base_url();?>css/style_admin.css" type="text/css" />
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.4.2.js"></script>
<script type="text/javascript">
$(function(){
    $(".fancyClose").click(function(){
       parent.$.fancybox.close();
    })
})
</script>
</head>
<body>
    <div class="smallPopUp">
        <h1>Add New GP</h1>
        <p class="message"><?php echo $this->session->flashdata('message'); ?></p>

            <?php
                $form = '';
                $form .= form_open('admin/doctors/add');
                $form .= form_label('First name', 'fname');
                $form .= form_input('fname', '');
                $form .= form_label('Last name', 'lname');
                $form .= form_input('lname', '');
                $form .= form_label('GP Commissioning Name', 'gpcom');
                $form .= form_dropdown('gpcom',$gpcoms,$gpcom_id);
                $form .= form_label('GP Practice Name', 'practice');
                $form .= form_dropdown('practice',$practices,$practice_id);

                $form .= form_label('EMIS', 'EMIS');
                $form .= form_input('EMIS', '');
                $form .= form_label('Address', 'address');
                $form .= form_input('address', '');
                $form .= form_label('City', 'city');
                $form .= form_input('city', '');
                $form .= form_label('Postcode', 'postcode');
                $form .= form_input('postcode', '');
                $form .= form_label('email', 'email');
                $form .= form_input('email', '');
                $form .= form_label('Phone', 'phone');
                $form .= form_input('phone', '');

                $form .= form_label('GP opening time', 'opening');
                $form .= form_textarea(array( 'name' => 'opening','rows'=>'2'));
                $form .= form_submit('submit_access', 'submit', 'class="btn orange big"');
                $form .= form_submit('submit_access', 'cancel', 'class="fancyClose  btn big"');
                $form .= form_close();
                echo $form;
            ?>
    </div>
</body>
</html>