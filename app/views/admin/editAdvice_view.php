<?php echo modules::run('adminhead'); ?>
<?php echo modules::run('adminheader'); ?>
<?php echo modules::run('msg',$msg = ''); ?>

<div id="main">
<div class="inner">
<table cellpadding="0" cellspacing="0" width="100%" class="editAdvice">
<!--tr>
	<th>diabetes</th>
    <th>own bp machine</th>
    <th>hypertension</th>
    <th>last reading days</th>
    <th>num readings today</th>
    <th>systolic today avg</th>
    <th></th>
    <th>num readings month</th>
    <th>last systolic</th>
    <th>last diastolic</th>
    <th>systolic month avg</th>
    <th>diastolic month avg</th>
    <th>condition</th>
    <th>priority</th>
   
</tr-->
<tr>
<td colspan="14" class="rbg cWhite"><h2>Add new advice</h2></td>
</tr>
<form method="post" action="<?php echo base_url(); ?>admin/advice/newAdvice/" name="">        
<tr class="firstRow">
            <td><label>diabetes</label><input size="1" name="diabetes" type="text" value=""  /></td>
			<td><label>own bp machine</label><input size="1" name="ownbpmachine" type="text" value="" /></td>
            <td><label>hypertension</label><input size="1" name="hypertension" type="text" value="" /> </td>
            <td><label>last reading days</label><input size="8" name="last_reading_days" type="text" value="" /> </td>
            <td><label>num readings today</label><input size="8" name="num_readings_today" type="text" value="" /> </td>
            <td><label>systolic today avg</label><input size="8" name="systolic_today_avg" type="text" value="" /> </td>
            <td><label>diastolic today avg</label><input size="8" name="diastolic_today_avg" type="text" value="" />  </td>
            <td><label>num readings month</label><input size="8" name="num_readings_month" type="text" value="" />  </td>
            <td><label>last systolic</label><input size="8" name="last_systolic" type="text" value="" />  </td>
            <td><label>last diastolic</label><input size="8" name="last_diastolic" type="text" value="" />  </td>
            <td><label>systolic month avg</label><input size="8" name="systolic_month_avg" type="text" value="" />  </td>
            <td><label>diastolic month avg</label><input size="8" name="diastolic_month_avg" type="text" value="" />  </td>
            <td><label>condition</label><input size="4" name="condition" type="text" value="" />  </td>
            <td><label>priority</label><input size="4" name="priority" type="text" value="" />  </td>
</tr>
<tr class="secondRow">            
            <td colspan="4"><label>action</label><textarea name="action"></textarea> </td>
            <td colspan="3"><label>first entry action</label><textarea name="first_entry_action"></textarea> </td>
            <td colspan="3">
            	<label>patient email</label>
            	<input size="1" name="send_patient_email" type="text" value="" />
            	<textarea name="patient_email"></textarea>
            </td>
            <td colspan="3">
            	<label>doctor email</label>
            	<input size="1" name="send_doctor_email" type="text" value="" /> 
                <textarea name="doctor_email"></textarea>
            </td>
            <td>
            	<input type="submit" value="Save" />
            </td>
</tr>
</form>
<tr>
<td colspan="14" style="background-color: #fff; border: 1px solid #fff;"></td>
</tr>
<tr>
<td colspan="14" class="rbg cWhite"><h2>Edit Advice</h2></td>
</tr>
<?php
if (isset($advice)) {
	foreach($advice as $a): ?>
<form method="post" action="<?php echo base_url(); ?>admin/advice/saveAdvice/<?php echo $a['id']; ?>/" name="">        
<tr class="firstRow">
            <td><label>diabetes</label><input size="1" name="diabetes" type="text" value="<?php echo $a['diabetes']; ?>"  /></td>
			<td><label>own bp machine</label><input size="1" name="ownbpmachine" type="text" value="<?php echo $a['ownbpmachine']; ?>" /></td>
            <td><label>hypertension</label><input size="1" name="hypertension" type="text" value="<?php echo $a['hypertension']; ?>" /> </td>
            <td><label>last reading days</label><input size="8" name="last_reading_days" type="text" value="<?php echo $a['last_reading_days']; ?>" /> </td>
            <td><label>num readings today</label><input size="8" name="num_readings_today" type="text" value="<?php echo $a['num_readings_today']; ?>" /> </td>
            <td><label>systolic today avg</label><input size="8" name="systolic_today_avg" type="text" value="<?php echo $a['systolic_today_avg']; ?>" /> </td>
            <td><label>diastolic today avg</label><input size="8" name="diastolic_today_avg" type="text" value="<?php echo $a['diastolic_today_avg']; ?>" />  </td>
            <td><label>num readings month</label><input size="8" name="num_readings_month" type="text" value="<?php echo $a['num_readings_month']; ?>" /> </td>
            <td><label>last systolic</label><input size="8" name="last_systolic" type="text" value="<?php echo $a['last_systolic']; ?>" /> </td>
            <td><label>last diastolic</label><input size="8" name="last_diastolic" type="text" value="<?php echo $a['last_diastolic']; ?>" /></td>
            <td><label>systolic month avg</label><input size="8" name="systolic_month_avg" type="text" value="<?php echo $a['systolic_month_avg']; ?>" /> </td>
            <td><label>diastolic month avg</label><input size="8" name="diastolic_month_avg" type="text" value="<?php echo $a['diastolic_month_avg']; ?>" />  </td>
            <td><label>condition</label><input size="4" name="condition" type="text" value="<?php echo $a['condition']; ?>" /> </td>
            <td><label>priority</label><input size="4" name="priority" type="text" value="<?php echo $a['priority']; ?>" /> </td>
</tr>

<tr class="secondRow">            
            <td colspan="4"><label>action</label><textarea name="action"><?php echo $a['action']; ?></textarea> </td>
            <td colspan="3"><label>first entry action</label><textarea name="first_entry_action"><?php echo $a['first_entry_action']; ?></textarea> </td>
            <td colspan="3">
            <label>patient email</label>
            	<input size="1" name="send_patient_email" type="text" value="<?php echo $a['send_patient_email']; ?>" />
            	<textarea name="patient_email"><?php echo $a['patient_email']; ?></textarea>
            </td>
            <td colspan="3">
            <label>doctor email</label>
            	<input size="1" name="send_doctor_email" type="text" value="<?php echo $a['send_doctor_email']; ?>" /> 
                <textarea name="doctor_email"><?php echo $a['doctor_email']; ?></textarea>
            </td>
            <td>
            	<!--input type="hidden" name="advice_id" value="<?php echo $a['advice_id']; ?>" /-->
            	<input type="submit" value="Update" />
            </td>
</tr>
</form>
<?php endforeach; } ?>
</table>


<?php
//print_r($advice); 
?>

<div class="clear"></div> 
</div> <!-- main inner -->
</div> <!-- main -->
 
<?php echo modules::run('adminfooter'); ?>
<?php echo modules::run('foot'); ?>