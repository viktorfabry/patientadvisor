<?php echo modules::run('adminhead'); ?>
<?php echo modules::run('adminheader'); ?>
<?php echo modules::run('msg',$msg = ''); ?>
    
<div id="main">
<div class="inner">
<p class="message"><?php echo $this->session->flashdata('message'); ?></p>
<form class="form" method="post" action="<?php echo base_url(); ?>admin/rolesaccess/" name="update_modules"  id="update_modules">
    <?php     echo form_submit('refresh_modules', 'refresh modules', 'class="btn margin15 left"') ?>
</form>
<form class="form" method="post" action="<?php echo base_url(); ?>admin/rolesaccess/" name="setRole" id="setRole">
<?php
    echo form_dropdown('role', $rolesList, $selectRole_id, 'class="roles dropSubmit left" id="selectRole"');
?>
</form>
<form class="form" method="post" action="<?php echo base_url(); ?>admin/rolesaccess/" name="set_access" id="set_access">
<?php
    echo form_button('selectAll', 'select all','class="selectAll btn left"');
    echo form_submit('submit_access', 'submit','class="btn orange left margin15"');
    echo form_hidden('accessRole_id', $selectRole_id);
?>

    <ul class="moduleList clear" >
<?php
$list = '';
$prevType = '';
$type = '';

foreach ($modules as $m) {
    $type = $m['type'];
    $list .= ($prevType!=$type) ? '<li><h4>'.$type.'</h4></li>' : '';
    $tick = (in_array($m['module_id'], $roleModules) ) ? TRUE : FALSE;
    $list .= '<li>'.$m['name'].' '.form_checkbox('access[]', $m['module_id'], $tick).'</li>';
    $prevType = $type;
} 

echo $list;
?>
</ul>
</form>
<div class="clear"></div>
</div> <!-- main inner -->
</div> <!-- main -->
 
<?php echo modules::run('adminfooter'); ?>
<?php echo modules::run('foot'); ?>
