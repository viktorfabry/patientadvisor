<html>
<head>
<title>Add New GP Practice</title>


<link rel="stylesheet" href="<?php echo base_url();?>css/style_admin.css" type="text/css" />
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.4.2.js"></script>
<script type="text/javascript">
$(function(){
    $(".fancyClose").click(function(){
       parent.$.fancybox.close();
    })
})
</script>
</head>
<body>
    <div class="smallPopUp">
        <h1>Add New GP Practice</h1>
        <p class="message"><?php echo $this->session->flashdata('message'); ?></p>

            <?php
                $form = '';
                $form .= form_open('admin/practices/add');
                $form .= form_label('GP Practice Name', 'name');
                $form .= form_input('name', '');
                if($admin){
                    $form .= form_label('GP Commissioning Name', 'gpcom');
                    $form .= form_dropdown('gpcom',$gpcoms,$gpcom_id);
                }
                $form .= form_label('GP Practice Description', 'description');
                $form .= form_textarea(array( 'name' => 'description','rows'=>'8'));
                $form .= form_submit('submit_access', 'submit', 'class="btn orange big"');
                $form .= form_submit('submit_access', 'cancel', 'class="fancyClose  btn big"');
                $form .= form_close();
                echo $form;
            ?>
    </div>
</body>
</html>