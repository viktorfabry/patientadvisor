<html>
<head>
<title>Edit Administrator</title>


<link rel="stylesheet" href="<?php echo base_url();?>css/style_admin.css" type="text/css" />
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.4.2.js"></script>
<script type="text/javascript">
$(function(){
    $(".fancyClose").click(function(){
       parent.$.fancybox.close();
    })
})
</script>
</head>
<body>
    <div class="smallPopUp clearfix">
        <h1>Edit Administrator Profile</h1>

            <?php
                $form = '';
                $form .= form_open('admin/administrators/edit');
                $form .= form_hidden('admin_id', $admin['admin_id']);
                $form .= form_label('username', 'username');
                $form .= form_input('username', $admin['username']);
                $form .= form_label('email', 'email');
                $form .= form_input('email', $admin['email']);

                $form .=  $LASelector;
                $form .=  '<div id="practiceSelector">'.$practiceSelector.'</div>';


//                $form .= form_label('First name', 'fname');
//                $form .= form_input('fname', $p['fname']);
//                $form .= form_label('Last name', 'lname');
//                $form .= form_input('lname', $p['lname']);
//                $form .= form_label('Address', 'address');
//                $form .= form_input('address', $p['address']);
//                $form .= form_label('City', 'city');
//                $form .= form_input('city', $p['city']);
//                $form .= form_label('Postcode', 'postcode');
//                $form .= form_input('postcode', $p['postcode']);
//                $form .= form_label('Phone', 'phone');
//                $form .= form_input('phone', $p['phone']);

                
                


                $form .= '<div class="btn_box">';
                $form .= form_submit('submit_access', 'submit', 'class="btn orange big"');
                $form .= form_submit('submit_access', 'cancel', 'class="fancyClose  btn big"');
                $form .= '</div> <!-- btn_box close -->';
                $form .= form_close();
                echo $form;
            ?>
    </div>

<script src="<?php echo base_url(); ?>js/dynamicSelect.jQuery.js" type="text/javascript"></script>

<script type="text/javascript">
$(function(){

        var options  = {
                url: '<?php echo base_url(); ?>admin/practices/getpracticenames/',
                newSelectName: 'practice',
                newSelectLabel: 'Practice Names',
                newSelectBoxID: 'practiceSelector'
            };

      $("#localAuth").dynamicSelect(options);
});
</script>
</body>
</html>