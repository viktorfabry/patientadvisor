<?php echo modules::run('adminhead'); ?>
<?php echo modules::run('adminheader'); ?>
<?php echo modules::run('msg',$msg = ''); ?>
    
<div id="main">
<div class="inner">
<p class="message"><?php echo $this->session->flashdata('message'); ?></p>
<a target="_blank" href="<?php echo base_url(); ?>register/" class="btn orange big m1">Add new patient</a>
<a href="<?php echo base_url(); ?>admin/patients/invite_patients/" class="btn orange big margin30">Import & Invite Patients</a>

<form class="form" method="post" action="<?php echo base_url(); ?>admin/patients/" name="set_access" id="set_access">


<table class="display" >
    <thead>
    <tr>
        <th>Name</th>
        <th>Username</th>
        <th>Email</th>
        <th>NHS</th>
        <th>Doctor</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $list = '';

    foreach ($patients as $p) {
        $list .= '<tr>';
        $list .= '<td>'.$p['patient'].'</td>';
        $list .= '<td>'.$p['username'].'</td>';
        $list .= '<td>'.$p['email'].'</td>';
        $list .= '<td>'.$p['NHS'].'</td>';
        $list .= '<td>'.$p['doctor'].'</td>';
        $list .= '<td>'.anchor('admin/patients/edit/'.$p['patient_id'], 'edit', 'class="popUpSmall btn  right"');
        $list .= anchor('admin/patients/delete/'.$p['patient_id'], 'delete', 'title="'.$p['patient'].'" class="delete btn red marginR15 right" rel="no-follow"').'</td>';
        $list .= '</tr>';
    }

    echo $list;
    ?>
    </tbody>
</table>
</form>
<div class="clear"></div>
</div> <!-- main inner -->
</div> <!-- main -->
 
<?php echo modules::run('adminfooter'); ?>
<?php echo modules::run('foot'); ?>
