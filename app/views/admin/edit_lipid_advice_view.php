<?php echo modules::run('adminhead'); ?>
<?php echo modules::run('adminheader'); ?>
<?php echo modules::run('msg',$msg = ''); ?>

<div id="main">
<div class="inner">
<table cellpadding="0" cellspacing="0" width="100%" class="editAdvice">
<!--tr>
	<th>diabetes</th>
    <th>own bp machine</th>
    <th>hypertension</th>
    <th>last reading days</th>
    <th>num readings today</th>
    <th>systolic today avg</th>
    <th></th>
    <th>num readings month</th>
    <th>last systolic</th>
    <th>last diastolic</th>
    <th>systolic month avg</th>
    <th>diastolic month avg</th>
    <th>condition</th>
    <th>priority</th>
   
</tr-->
<tr>
<td colspan="14" class="rbg cWhite"><h2>Add new advice</h2></td>
</tr>
<form method="post" action="<?php echo base_url(); ?>admin/lipid_advice/newAdvice/" name="">        
<tr class="firstRow">
            <td><label>DM, IHD, Hypertension</label><input size="1" name="DM_IHD_Hyp" type="text" value=""  /></td>
            <td><label>last total cholesterol</label><input size="8" name="last_total_cholesterol" type="text" value="" /> </td>
            <td><label>last HDL</label><input size="8" name="last_HDL" type="text" value="" /> </td>
            <td><label>last reading days</label><input size="8" name="last_reading_days" type="text" value="" /> </td>
            <td><label>condition</label><input size="4" name="condition" type="text" value="" />  </td>
            <td><label>priority</label><input size="4" name="priority" type="text" value="" />  </td>
</tr>
<tr class="secondRow">            
            <td><label>action</label><textarea name="action"></textarea> </td>
            <td><label>high cholesterol btn</label><input size="1" name="high_cholesterol_btn" type="text" value=""  /></td>

            <td>
            	<label>patient email</label>
            	<input size="1" name="send_patient_email" type="text" value="" />
            	<textarea name="patient_email"></textarea>
            </td>
            <td colspan="2">
            	<label>doctor email</label>
            	<input size="1" name="send_doctor_email" type="text" value="" /> 
                <textarea name="doctor_email"></textarea>
            </td>
            <td>
            	<input type="submit" value="Save" />
            </td>
</tr>
</form>
<tr>
<td colspan="6" style="background-color: #fff; border: 1px solid #fff;"></td>
</tr>
<tr>
<td colspan="6" class="rbg cWhite"><h2>Edit Advice</h2></td>
</tr>
<?php
if (isset($advice)) {
	foreach($advice as $a): ?>
<form method="post" action="<?php echo base_url(); ?>admin/lipid_advice/saveAdvice/<?php echo $a['id']; ?>/" name="">        
<tr class="firstRow">
            <td><label>DM, IHD, Hypertension</label><input size="1" name="DM_IHD_Hyp" type="text" value="<?php echo $a['DM_IHD_Hyp']; ?>"  /></td>
			<td><label>last total cholesterol</label><input size="8" name="last_total_cholesterol" type="text" value="<?php echo $a['last_total_cholesterol']; ?>" /></td>
            <td><label>last HDL</label><input size="8" name="last_HDL" type="text" value="<?php echo $a['last_HDL']; ?>" /> </td>
            <td><label>last reading days</label><input size="8" name="last_reading_days" type="text" value="<?php echo $a['last_reading_days']; ?>" /> </td>
            <td><label>condition</label><input size="4" name="condition" type="text" value="<?php echo $a['condition']; ?>" /> </td>
            <td><label>priority</label><input size="4" name="priority" type="text" value="<?php echo $a['priority']; ?>" /> </td>
</tr>

<tr class="secondRow">            
            <td><label>action</label><textarea name="action"><?php echo $a['action']; ?></textarea> </td>
            <td><label>high cholesterol btn</label><input size="1" name="high_cholesterol_btn" type="text" value="<?php echo $a['high_cholesterol_btn']; ?>"  /></td>

            <td>
            <label>patient email</label>
            	<input size="1" name="send_patient_email" type="text" value="<?php echo $a['send_patient_email']; ?>" />
            	<textarea name="patient_email"><?php echo $a['patient_email']; ?></textarea>
            </td>
            <td colspan="2">
            <label>doctor email</label>
            	<input size="1" name="send_doctor_email" type="text" value="<?php echo $a['send_doctor_email']; ?>" /> 
                <textarea name="doctor_email"><?php echo $a['doctor_email']; ?></textarea>
            </td>
            <td>
            	<!--input type="hidden" name="advice_id" value="<?php echo $a['id']; ?>" /-->
            	<input type="submit" value="Update" />
            </td>
</tr>
</form>
<?php endforeach; } ?>
</table>


<?php
//print_r($advice); 
?>

<div class="clear"></div> 
</div> <!-- main inner -->
</div> <!-- main -->
 
<?php echo modules::run('adminfooter'); ?>
<?php echo modules::run('foot'); ?>