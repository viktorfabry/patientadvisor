<?php echo $head; ?>
<?php echo $topBar; ?>
<?php echo $header; ?>


<div id="main" class="home container">
    <div class="row_wrapper curves10">
        <div class="row">
            <div id="container">
                   <div class="banner">
                       <div class="sixcol push_onecol">
                       <h1>Take&nbsp;control of&nbsp;your&nbsp;health</h1>
                       <h2>Get an update of your health every day, track your blood pressure, cholesterol and diabetes for free.</h2>
                       <?php echo anchor('register','Start your healthy life today','class="btn signUp huge green"') ?>
                       </div>
                       <div class="bannerImg fourcol last pull_onecol "></div>
                   </div>
                
            </div>

        </div> <!-- row -->
    </div> <!-- row wrapper -->
</div> <!-- main -->

<?php echo $footer; ?>
<?php $this->load->view('foot'); ?>