<?php echo modules::run('head'); ?>
<?php echo modules::run('header'); ?>


<div id="main">
<div class="inner registration">

<?php echo validation_errors(); ?>
<form class="form registerForm" method="post" action="<?php echo base_url(); ?>dr_profile/update/" name="register">        
<ul>
	<li class="account clearfix">
		<h3><span>1.</span> Account information</h3>
		<label>Username</label>
		<input size="50" name="uname" type="text" value="<?php echo set_value('uname', $uname); ?>"  />

		<input type="submit" value="Submit" />

		<label>Email</label><input size="50" name="email" type="text" value="<?php echo set_value('email', $email); ?>" />
		<div style="display: none">
			<input type="hidden" name="newpass" value="N" />
			<label>Password</label><input size="50" name="pword" type="password" value="<?php echo set_value('pword', $pword); ?>" /> 
			<label>Confirm Password</label><input size="50" name="passconf" type="text" value="<?php echo set_value('passconf'); ?>" />
		</div>
</li>	    

<li class="personal clearfix">
	<h3><span>2.</span> Personal information</h3>

	<label>First Name</label>
	<input size="50" name="fname" type="text" value="<?php echo set_value('fname', $fname); ?>"  />
	
	<label>Last Name</label>
	<input size="50" name="lname" type="text" value="<?php echo set_value('lname', $lname); ?>"  />
	
	<label>Address</label>
	<input size="50" name="address" type="text" value="<?php echo set_value('address', $address); ?>"  />
	
	<label>City or Town</label>
	<input size="50" name="city" type="text" value="<?php echo set_value('city', $city); ?>"  />
	
	<label>Post Code</label>
	<input size="50" name="postcode" type="text" value="<?php echo set_value('postcode', $postcode); ?>"  />
	
	<label>Phone</label>
	<input size="50" name="phone" type="text" value="<?php echo set_value('phone', $phone); ?>"  />
	
	<label>EMIS</label>
	<input size="50" name="EMIS" type="text" value="<?php echo set_value('EMIS', $EMIS); ?>"  />
</li>	    

<li class="practice clearfix">
	<h3><span>2.</span> Practice information</h3>
	<label>EMIS</label>
	<input size="50" name="EMIS" type="text" value="<?php echo set_value('EMIS', $EMIS); ?>"  />
	<label>Opening hours</label>
	<textarea name="opening" ><?php echo set_value('opening', $opening); ?></textarea>
</li>
</ul>
</form>


<div class="clear"></div> 
</div> <!-- main inner -->
</div> <!-- main -->

<?php echo modules::run('footer'); ?>
<?php $this->load->view('foot'); ?>