<?php
if ($use_username) {
    $username = array(
        'name' => 'username',
        'id' => 'username',
        'value' => set_value('username'),
        'maxlength' => $this->config->item('username_max_length', 'tank_auth'),
        'size' => 30,
    );
}
$email = array(
    'name' => 'email',
    'id' => 'email',
    'value' => set_value('email'),
    'maxlength' => 80,
    'size' => 30,
);
$password = array(
    'name' => 'password',
    'id' => 'password',
    'value' => set_value('password'),
    'maxlength' => $this->config->item('password_max_length', 'tank_auth'),
    'size' => 30,
);
$confirm_password = array(
    'name' => 'confirm_password',
    'id' => 'confirm_password',
    'value' => set_value('confirm_password'),
    'maxlength' => $this->config->item('password_max_length', 'tank_auth'),
    'size' => 30,
);

$fname = array(
    'name' => 'fname',
    'id' => 'fname',
    'value' => set_value('fname'),
    'maxlength' => 150,
    'size' => 30,
);
$lname = array(
    'name' => 'lname',
    'id' => 'lname',
    'value' => set_value('lname'),
    'maxlength' => 150,
    'size' => 30,
);
$address = array(
    'name' => 'address',
    'id' => 'address',
    'value' => set_value('address'),
    'maxlength' => 150,
    'size' => 30,
);
$city = array(
    'name' => 'city',
    'id' => 'city',
    'value' => set_value('city'),
    'maxlength' => 150,
    'size' => 30,
);
$postcode = array(
    'name' => 'postcode',
    'id' => 'postcode',
    'value' => set_value('postcode'),
    'maxlength' => 150,
    'size' => 30,
);
$phone = array(
    'name' => 'phone',
    'id' => 'phone',
    'value' => set_value('phone'),
    'maxlength' => 150,
    'size' => 30,
);
$captcha = array(
    'name' => 'captcha',
    'id' => 'captcha',
    'maxlength' => 8,
);
$NHS = array(
    'name' => 'NHS',
    'id' => 'NHS',
    'value' => set_value('NHS'),
    'maxlength' => 150,
    'size' => 30,
);
$EMIS = array(
    'name' => 'EMIS',
    'id' => 'EMIS',
    'value' => set_value('EMIS'),
    'maxlength' => 150,
    'size' => 30,
);
?>

<?php echo $head; ?>
<?php echo $patientBar; ?>
<?php echo $header; ?>

<div id="main" class="container register">
    <div class="row_wrapper curves10">
        <div class="row">
            <?php echo form_open($this->uri->uri_string()); ?>

            <div class="fourcol">
                <?php // echo validation_errors(); ?>


                <h3><span>1.</span> Account information</h3>
                <?php

                $formA = form_label('Username', $username['id']);
                $formA .= form_input($username);
                $formA .= form_error($username['name']);
                $formA .= isset($errors[$username['name']]) ? '<span class="validationError">'.$errors[$username['name']].'</span>' : '';

                $formA .= form_label('Email Address', $email['id']);
                $formA .= form_input($email);
                $formA .= form_error($email['name']);
                $formA .= isset($errors[$email['name']]) ? '<span class="validationError">'.$errors[$email['name']].'</span>' : '';

                $formA .= form_label('Password', $password['id']);
                $formA .= form_password($password);
                $formA .= form_error($password['name']);

                $formA .= form_label('Confirm Password', $confirm_password['id']);
                $formA .= form_password($confirm_password);
                $formA .= form_error($confirm_password['name']);

                $formA .= form_label('First Name', $fname['id']);
                $formA .= form_input($fname);
                $formA .= form_error($fname['name']);

                $formA .= form_label('Last Name', $lname['id']);
                $formA .= form_input($lname);
                $formA .= form_error($lname['name']);

                echo $formA;
                ?>
            </div>
            <div class="twocol">
            </div>

            <div class="fourcol">

                <h3><span>2.</span> Personal information</h3>
                <?php


                $formB = form_label('Address', $address['id']);
                $formB .= form_input($address);
                $formB .= form_error($address['name']);

                $formB .= form_label('City or Town', $city['id']);
                $formB .= form_input($city);
                $formB .= form_error($city['name']);

                $formB .= form_label('Post Code', $postcode['id']);
                $formB .= form_input($postcode);
                $formB .= form_error($postcode['name']);

                $formB .= form_label('Phone', $phone['id']);
                $formB .= form_input($phone);
                $formB .= form_error($phone['name']);

                echo $formB;

                ?>
                <input type="submit" value="Submit" class="btn right orange big " />


            </div> <!-- coll -->
            <div class="twocol last">
            </div> <!-- coll -->
            <?php echo form_close(); ?>
            </div> <!-- row -->
        </div> <!-- row wrapper -->
    </div> <!-- main -->

<?php echo modules::run('footer'); ?>
<?php $this->load->view('foot'); ?>


<?php if (false) {
 ?>
                    <li class="data clearfix">
                        <h3><span>3.</span> Personal information</h3>
                        <p class="note">
                            <label>Gender:</label>
                        <p class="note">
                            <input type="radio" name="gender" value="M" <?php echo set_radio('gender', 'M', TRUE); ?> /> Male
                            <input type="radio" name="gender" value="F" <?php echo set_radio('gender', 'F'); ?> /> Female
                        </p>

                        <label>Date of Birth</label>
    <?php
                    //$this->load->helper('datedd');
                    //echo buildDayDropdown('dob_D',$value='').' ';
                    //echo buildMonthDropdown('dob_M',$value='').'';
                    //echo buildYearDropdown('dob_Y',$value='').'<br />';
    ?>

                    <input size="50" name="dob" type="text" value="<?php echo set_value('dob'); ?>" />
                    <p class="note">DD/MM/YYYY</p>

                    <label>I prefer to use</label>
                    <p class="note unit">
                        <input type="radio" id="metric" name="MOI" value="1" <?php echo set_radio('MOI', 'metric', TRUE); ?> /> Metric units
                        <input type="radio" id="imperial" name="MOI" value="2" <?php echo set_radio('MOI', 'imperial'); ?> /> Imperial units
                    </p>

                    <div class="measurement">
                        <div class="metric">
                            <label>height (cm)</label>
                            <input size="50" id="height" name="height" type="text" value="<?php echo set_value('height'); ?>" />
                        </div>
                        <div class="imperial">
                            <label>height (Imp)</label>
                            <input size="50" id="height_imp" name="height_imp" type="text" value="<?php echo set_value('height_imp'); ?>" />
                        </div>
                    </div>

                    <label>active</label>
                    <select  name="active">
                        <option value="" <?php echo set_select('active', 'Y', TRUE); ?> >Please Select</option>
                        <option value="Y" <?php echo set_select('active', 'Y'); ?> >Yes</option>
                        <option value="N" <?php echo set_select('active', 'N'); ?> >No</option>
                    </select>





                    <label>smoker</label>
                    <select  name="smoker">
                        <option value="" <?php echo set_select('active', 'N', TRUE); ?> >Please Select</option>
                        <option value="Y" <?php echo set_select('smoker', 'Y'); ?> >Yes</option>
                        <option value="N" <?php echo set_select('smoker', 'N'); ?> >No</option>
                    </select>

                    <label>Date stopped smoking</label><input size="50" name="date_stopped_smoking" type="text" value="<?php echo set_value('date_stopped_smoking'); ?>" />
                    <p class="note">MM/YYYY</p>



                    <label class="long">My doctor has told me I have diabetes</label>
                    <select  name="diabetes">
                        <option value="" <?php echo set_select('active', 'N', TRUE); ?> >Please Select</option>
                        <option value="Y" <?php echo set_select('diabetes', 'Y'); ?> >Yes</option>
                        <option value="N" <?php echo set_select('diabetes', 'N'); ?> >No</option>
                    </select>

                    <label class="long">I have my own Blood Pressure Machine at home</label>
                    <select  name="ownbpmachine">
                        <option value="" <?php echo set_select('active', 'N', TRUE); ?> >Please Select</option>
                        <option value="Y" <?php echo set_select('ownbpmachine', 'Y'); ?> >Yes</option>
                        <option value="N" <?php echo set_select('ownbpmachine', 'N'); ?> >No</option>
                    </select>





                    <label class="long">My doctor has told me I have High Blood Pressure</label>
                    <select  name="hypertension">
                        <option value="" <?php echo set_select('active', 'N', TRUE); ?> >Please Select</option>
                        <option value="Y" <?php echo set_select('hypertension', 'Y'); ?> >Yes</option>
                        <option value="N" <?php echo set_select('hypertension', 'N'); ?> >No</option>
                    </select>


                    <label class="long">My doctor has told me I have some heart disease</label>
                    <select  name="IHD">
                        <option value="" <?php echo set_select('active', 'N', TRUE); ?> >Please Select</option>
                        <option value="Y" <?php echo set_select('IHD', 'Y'); ?> >Yes</option>
                        <option value="N" <?php echo set_select('IHD', 'N'); ?> >No</option>
                    </select>

                    <label class="long">Please select your Doctor</label>
    <?php
                    $formD = form_dropdown('doctor', $dr_dd, $dr_id, 'class="dr_dd" id="doctor"');

//         $formD .= form_label('Practice EMIS Number', $phone['id']);
//         $formD .= form_input($EMIS);
//         $formD .= form_error($EMIS['name']);

                    $formD .= form_label('NI Number', $phone['id']);
                    $formD .= form_input($NHS);
                    $formD .= form_error($NHS['name']);

                    echo $formD;
    ?>

                </li>
                </ul>
                </form>
<?php } ?>