<?php echo $head; ?>
<?php echo $topBar; ?>
<?php echo $header; ?>

<div id="main" class="container">
    <div class="row_wrapper curves10">
        <div class="row">

            <!--/////////////////// choloesterol start///////////////////////////-->
            <div id="graph" class="eightcol graphContainer"> </div>

            <div class="fourcol last">

                <?php echo modules::run('lipid_advice'); ?>


                <div class="inputfields">
                    <?php
                    $form = form_open('cholesterol/addSample/', 'id="add_cholesterol"');
                    $form .= form_hidden('url', $this->uri->uri_string());
                    $form .= form_label('HDL', 'HDL');
                    $form .= form_input('HDL');
                    $form .= form_label('Total', 'total');
                    $form .= form_input('total');
                    $form .= form_submit('submit', 'submit', 'class="btn orange big marginT15 right"');
                    $form .= anchor(base_url() . 'table_cholesterol/', 'Historical Readings', 'class="btn big marginT15 left popUp980"');
                    $form .= form_close();
                    echo $form;
                    ?>

                </div>

            </div>
            <!--/////////////////// choloesterol end///////////////////////////-->
        </div> <!-- row  -->
    </div> <!-- row wrapper -->
</div> <!-- main -->

<?php echo $footer; ?>
<?php echo $hc_cholesterol; ?>
<?php $this->load->view('foot'); ?>