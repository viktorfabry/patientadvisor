<?php

header ('Content-Type: text/xml');
header('Content-Disposition: inline; filename=sample.xml');
	print "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>\n";
	//print "<videos>\n";
	//print "<video url=\"".base_url().$lesson['video']."\" desc=\"".$lesson['title']."\" />\n";
	//print "</videos>\n";
	
?>
<chart>
<license>GTAF0UAVUJ17T1X5CWK-2XOI1X0-7L</license>  
	<!-- the child chart (top, scrolling chart) -->
	<axis_category shadow='low' skip='11' size='10' alpha='80' />
	<axis_value color='333333' shadow='low' show_min='false' size='10' />
	
	<axis_value max='120' />

	<chart_note type='flag'
		    size='10'
               color='ff0000' 
               alpha='100'
               background_color_1='ff0000'
		background_color_2='eeeeee' 
               background_alpha='90'
               />


	<chart_border top_thickness='1' bottom_thickness='1' left_thickness='1' right_thickness='1' color='999999'  />              
	<chart_data>
<row>
							<null/>
<?php
$i = 0;
while ($i < $chart['lines']) {
		echo $chart['dates'][$i];
			$i++;											
} ?>
</row>
<row>				
<string>diastolic</string>
<?php
$i = 0;
while ($i < $chart['lines']) {
		echo $chart['diastolic'][$i];
			$i++;											
} ?>			
</row>
<row>
<string>Systolic</string>
			<?php
$i = 0;
while ($i < $chart['lines']) {
		echo $chart['systolic'][$i];
			$i++;											
} ?>

		</row>
		
	</chart_data>
	<chart_grid_h alpha='10' thickness='1' />
	<chart_grid_v alpha='7' thickness='1' />
	<chart_guide vertical='true' horizontal='true' thickness='1' color='0' alpha='75' type='dotted' snap_h='true' snap_v='true' radius='3' fill_alpha='75' fill_color='FF0000' line_thickness='1' text_h_alpha='90' text_v_alpha='90' text_color='ffffff' background_color='ff0000' size='10' />
	<chart_pref line_thickness='2' point_shape='none' fill_shape='false' connect='true' />
	<chart_rect x='40' width='500' bevel='bg' shadow='high' height='160' positive_alpha='75' />
	<chart_type>line</chart_type>

	<draw>

		<rect transition='dissolve' delay='1' layer='background' x='40.5' y='60' width='500' height='100' line_thickness='0' fill_color='ff0000' fill_alpha='100' />
		<rect transition='dissolve' delay='1' layer='background' x='40.5' y='110' width='500' height='30' line_thickness='0' fill_color='ffff00' fill_alpha='100' />
		<rect transition='dissolve' delay='1' layer='background' x='40.5' y='140' width='500' height='60' line_thickness='0' fill_color='00ff00' fill_alpha='100' />
	</draw>
	
	<filter>
		<!--bevel id="bg" angle="45" blurX="50" blurY="50" distance="25" highlightAlpha="75" shadowAlpha="25" type="inner"/-->
		<shadow id="high" distance="5" angle="45" alpha="0" blurX="15" blurY="15"/>
		<shadow id="low" distance="2" angle="45" alpha="0" blurX="5" blurY="5"/>
	</filter>
	
	<scroll scroll_detail='100' transition='dissolve' delay='1' x='40' y='241' width='501' height='40' url_button_1_idle='default' url_button_2_idle='default' url_slider_body='<?php echo base_url() ?>images/black.swf' url_slider_handle_1='<?php echo base_url() ?>images/preview_handle_1.swf' url_slider_handle_2='<?php echo base_url() ?>images/preview_handle_2.swf' button_length='0' slider_handle_length='22' start='20' span='40' reverse_handle='true' drag='true' />

	<legend shadow='data' x='40' y='35' width='500' bullet='circle' fill_alpha='0' />

	
	<series_color>
		<color>ffff00</color>
		<color>ff0000</color>
		
	</series_color>
	
</chart>


