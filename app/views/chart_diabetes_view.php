<?php echo $head; ?>
<?php echo $topBar; ?>
<?php echo $header; ?>

<div id="main" class="container">
    <div class="row_wrapper curves10">
        <div class="row padTop">
            <!--/////////////////// ltbs start/////////////////////////// style="width: 680px; height:300px"-->
            <div id="ltbs" class="eightcol graphContainer small"> </div>
            <div class="fourcol last">
                <div class="inputfields">
                    <?php
                    $form = form_open('diabetes/addLtbs/', 'id="add_ltbs"');
                    $form .= form_hidden('url', $this->uri->uri_string());
                    $form .= form_label('HbA1c', 'ltbs');
                    $form .= form_input('ltbs');
                    $form .= form_submit('submit', 'submit', 'class="btn orange big marginT15 right"');
                    $form .= anchor(base_url() . 'table_ltbs/', 'Historical Readings', 'class="btn big marginT15 left popUp980"');
                    $form .= form_close();
                    echo $form;
                    ?>

                </div>
            </div>
        </div> <!-- row end -->
    <!--/////////////////// ltbs end///////////////////////////-->

    <!--/////////////////// diabetes start///////////////////////////-->
        <div class="row">
            <div id="diabetes" class="eightcol graphContainer small"> </div>

            <div class="fourcol last">

                <div class="inputfields">
                                   <?php
                    $form = form_open('diabetes/addDiabetes/', 'id="add_diabetes"');
                    $form .= form_hidden('url', $this->uri->uri_string());
                    $form .= form_label('Blood Glucose', 'diabetes');
                    $form .= form_input('diabetes');
                    $form .= form_submit('submit', 'submit', 'class="btn orange big marginT15 right"');
                    $form .= anchor(base_url() . 'table_ltbs/', 'Historical Readings', 'class="btn big marginT15 left popUp980"');
                    $form .= form_close();
                    echo $form;
                    ?>
                </div>
            </div>
        </div>
    </div>
    <!--/////////////////// diabetes end///////////////////////////-->

</div> <!-- main -->

<?php echo $footer; ?>
<?php echo $hc_ltbs; ?>
<?php echo $hc_diabetes; ?>
<?php $this->load->view('foot'); ?>