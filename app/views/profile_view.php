<?php echo $head; ?>
<?php echo $topBar; ?>
<?php echo $header; ?>

<div id="main" class="profile container">
    <div class="row_wrapper curves10">
        <div class="row">
            <?php  echo $msgBlock; ?>
            <?php  if($progress<100){
                    echo '<div class="progressBar fourcol ds5 curves5"><span class="curves5" style="width: '.$progress.'%">'.$progress.'%</span></div>';
                }
            ?>


            <?php echo form_open('profile','id="profile_form"'); ?>
            <div class="fourcol clear">
                <h3>Personal information</h3>

                <?php
                //echo 'f: ' . $female . ' m: ' . $male;
                $formC = form_label('Gender', 'gender');
                $formC .= form_radio('gender', 'M', set_radio('gender', $gender, $male));
                $formC .= '<span class="radioLabel">Male</span>';
                $formC .= form_radio('gender', 'F', set_radio('gender', $gender, $female));
                $formC .= '<span class="radioLabel">Female</span>';
                $formC .= form_error('gender');



                $formC .= form_label('Date of Birth', 'dob');
                $formC .= form_input('dob', set_value('dob', $dob), 'class="datepicker"');
                $formC .= form_error('dob');
                $formC .= '<p class="note">YYYY-MM-DD</p>';

                $formC .= form_label('I prefer to use', 'gender');
                $formC .= form_radio('imp_measure', 'N', set_radio('MOI', $imp_measure, $metric),'id="metric" class="unit"');
                $formC .= '<span class="radioLabel">Metric</span>';
                $formC .= form_radio('imp_measure', 'Y', set_radio('MOI', $imp_measure, $imp),'id="imperial" class="unit"');
                $formC .= '<span class="radioLabel">Imperial</span>';
                $formC .= form_error('MOI');

                $formC .= '<div class="measurement">';
                    $formC .= '<div id="metricBlock" class="'.$metricClass.'">';
                        $formC .= form_label('Height (cm)', 'height');
                        $formC .= form_input('height_m', set_value('height_m', $height),'id="m" class="height"');
                        $formC .= form_error('height');
                    $formC .= '</div>';
                    $formC .= '<div id="imperialBlock" class="'.$impClass.'">';
                        $formC .= form_label('Height (imperial)', 'height_imp');
                        $formC .= form_input('height_ft', set_value('height_ft', $height_imp['ft']),'id="ft" class="height"','short');
                        $formC .= '<span class="shortLabel">ft</span>';
                        $formC .= form_input('height_in', set_value('height_in', $height_imp['in']),'id="inch" class="height"','short');
                        $formC .= '<span class="shortLabel">in</span>';
                        $formC .= form_error('height_imp');
                    $formC .= '</div>';
                $formC .= '</div>';

                $formC .= form_label('Active', 'active');
                $formC .= form_dropdown('active', $yepnope_dd, set_value('active', $active), 'class="dd" id="active"');
                $formC .= form_error('active');

                $formC .= form_label('Smoker', 'smoker');
                $formC .= form_dropdown('smoker', $yepnope_dd, set_value('smoker', $smoker), 'class="dd" id="smoker"');
                $formC .= form_error('smoker');

                $formC .= form_label('Date stopped smoking', 'date_stopped_smoking');
                $formC .= form_input('date_stopped_smoking', set_value('date_stopped_smoking', $date_stopped_smoking), 'class="monthPicker"');
                $formC .= form_error('date_stopped_smoking');
                $formC .= '<p class="note">YYYY-MM</p>';

                $formC .= form_label('My doctor has told me I have diabetes', 'diabetes');
                $formC .= form_dropdown('diabetes', $yepnope_dd, set_value('diabetes', $diabetes), 'class="dd" id="diabetes"');
                $formC .= form_error('diabetes');

                $formC .= form_label('I have my own Blood Pressure Machine at home', 'ownbpmachine');
                $formC .= form_dropdown('ownbpmachine', $yepnope_dd, set_value('ownbpmachine', $ownbpmachine), 'class="dd" id="ownbpmachine"');
                $formC .= form_error('ownbpmachine');

                $formC .= form_label('My doctor has told me I have High Blood Pressure', 'hypertension');
                $formC .= form_dropdown('hypertension', $yepnope_dd, set_value('hypertension', $hypertension), 'class="dd" id="hypertension"');
                $formC .= form_error('hypertension');

                $formC .= form_label('My doctor has told me I have some heart disease', 'IHD');
                $formC .= form_dropdown('IHD', $yepnope_dd, set_value('IHD', $IHD), 'class="dd" id="IHD"');
                $formC .= form_error('IHD');

//                $formC .= form_label('Please select your Doctor', 'smoker');
//                $formC .= form_dropdown('doctor', $dr_dd, set_value('smoker', $smoker), 'class="dr_dd" id="doctor"');
//                $formC .= form_error('doctor');


//         $formD .= form_label('Practice EMIS Number', $phone['id']);
//         $formD .= form_input($EMIS);
//         $formD .= form_error($EMIS['name']);

//                $formC .= form_label('NI Number', 'NHS');
//                $formC .= form_input('NHS', set_value('NHS', $NHS));
//                $formC .= form_error('NHS');

                echo $formC;
                ?>


        </div>
        <div class="twocol">
        </div>
        <div class="fourcol">

                    <h3>Account information</h3>
                    <?php
                    $formA = form_label('Username', 'username');
                    $formA .= form_input('username', set_value('username', $username));
                    $formA .= form_error('username');

                    $formA .= form_label('Email Address', 'email');
                    $formA .= form_input('email', set_value('email', $email));
                    $formA .= form_error('email');

                    $formA .= anchor('auth/change_password','Change your password','class="btn orange big mT1"');

//                    $formA .= form_label('New password', 'new_password');
//                    $formA .= form_password('new_password');
//                    $formA .= form_error('new_password');
//
//                    $formA .= form_label('Confirm New Password', 'confirm_password');
//                    $formA .= form_password('confirm_password');
//                    $formA .= form_error('confirm_password');

                    echo $formA;
                    ?>

                    <h3>Personal information</h3>
                    <?php
                    $formB = form_label('First Name', 'fname');
                    $formB .= form_input('fname', set_value('fname', $fname));
                    $formB .= form_error('fname');

                    $formB .= form_label('Last Name', 'lname');
                    $formB .= form_input('lname', set_value('lname', $lname));
                    $formB .= form_error('lname');

                    $formB .= form_label('Address', 'address');
                    $formB .= form_input('address', set_value('address', $address));
                    $formB .= form_error('address');

                    $formB .= form_label('City or Town', 'city');
                    $formB .= form_input('city', set_value('city', $city));
                    $formB .= form_error('city');

                    $formB .= form_label('Post Code', 'postcode');
                    $formB .= form_input('postcode', set_value('postcode', $postcode));
                    $formB .= form_error('postcode');

                    $formB .= form_label('Phone', 'phone');
                    $formB .= form_input('phone', set_value('phone', $phone));
                    $formB .= form_error('phone');

                    echo $formB;
                    ?>

        </div>
        <div class="twocol last">
                        <input type="submit" value="Submit" class="btn orange big " />
        </div>
        <?php echo form_close(); ?>

                </div>
            </div> <!-- main inner -->
            </div> <!-- main -->

<?php echo $footer; ?>
<?php $this->load->view('foot'); ?>