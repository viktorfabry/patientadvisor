<?php
$header = modules::run('header');
$drBar = modules::run('drBar');
$head = modules::run('head');
$footer = modules::run('footer');
?>

<?php echo $head; ?>
<?php echo $drBar; ?>
<?php echo $header; ?>

<div id="main" class="container">
    <div class="row_wrapper curves10">
        <div class="row">
            <div id="container" class="eightcol">

                <p>Please select a patient from the dropdown</p>
            </div>

        </div> <!-- row -->
    </div> <!-- row wrapper -->
</div> <!-- main -->

<?php echo $footer; ?>
<?php $this->load->view('foot'); ?>