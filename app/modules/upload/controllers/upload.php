<?php

class Upload extends MX_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		//$this->msg = ($this->session->flashdata('msg')) ? $this->session->flashdata('msg') : array('file' => 'index');
	}
	
	function index() {
		$data['msg'] = ($this->session->flashdata('msg')) ? $this->session->flashdata('msg') : array('file' => 'index');	
		$this->load->view('upload_view', $data);
	}

	function upload_pdf() {
		$filename = str_replace(' ','_',$this->input->post('fileName'));
		$url = 'admin/dr_links/';
		//$url = $this->input->post('url');
		
		if($filename) $config['file_name'] = $filename;
		$config['upload_path'] = './Documents/';
		$config['allowed_types'] = 'pdf';
		$config['max_size']	= '5000';
		

		$this->load->library('upload', $config);
		$msg = array('file' => 'data');
		$msg =  ($this->upload->do_upload()) ? $this->upload->data() : $this->upload->display_errors();
		//print_r($msg); die;
		//echo 'FILE: '; print_r($this->upload->display_errors()); print_r( $this->upload->data()); die;
		$this->session->set_flashdata($msg);
		//$this->load->view('upload_view');
		redirect($url);
	}	
}
?>