<script type="text/javascript">
		
    var chart;
    $(document).ready(function() {
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'waistgirth',
                type: 'spline',
                zoomType: 'x',
                backgroundColor: '#D8D8D2',
                borderRadius: 10,
                margin: [50, 20, 40, 60]
            },
            title: {
                text: 'Waist Girth'
            },
            subtitle: {
                text: 'Click and drag in the plot area to zoom in',
                align: 'left',
                verticalAlign: 'bottom',
                x: 50,
                y: 7
            },
            xAxis: {
                type: 'datetime',
                maxZoom: 7 * 24 * 3600000, // fourteen days
                title: {
                    text: null
                },
                showFirstLabel: false
            },
            yAxis: {
                title: {
                    text: '<?php echo $graphLabel; ?>'
                },
                //min: <?php echo $min ?>,
                //max: <?php echo $max ?>,
                //tickInterval: 10,
                //tickColor: '#FFFFFF',
                //minorTickInterval: 5,
                //minorTickLength: 0,
                gridLineColor: '#ffffff',
                startOnTick: false,
                endOnTick: false,
                showFirstLabel: false,
                plotBands: [<?php echo $js_bonds ?>]
            },
            tooltip: {
                formatter: function() {
                    var note = (this.point.note) ? '<span style="color: <?php echo $color['line1'] ?>">"' + this.point.note + '"</span><br> ' : '';

                    return '<strong>'+ this.series.name + ': '+
                        Highcharts.numberFormat(this.y, 0) + "</strong><br> "+
                        note +
                        Highcharts.dateFormat('%B %e %Y, %A', this.x);
                },
                style: {
                    color: '#333333',
                    fontSize: '12px',
                    padding: '10px 15px'
                }
            },
            legend: {
                enabled: false,
                align: 'right',
                verticalAlign: 'top',
                backgroundColor: '#fff',
                x: -80,
                y: 10
            },
            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: [0, 0, 0, 300],
                        stops: [
                            [0, '#4572A7'],
                            [1, 'rgba(2,0,0,0)']
                        ]
                    },
                    lineWidth: 1,
                    marker: {
                        enabled: false,
                        states: {
                            hover: {
                                enabled: true,
                                radius: 5
                            }
                        }
                    },
                    shadow: false,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    }
                }
            },
            series: [
                {name: 'Abdominal Girth', color: '<?php echo $color['line1'] ?>', data: <?php echo $girth['girth_jsArray'] ?> }
            ],
            navigation: {
                buttonOptions: {
                    height: 26,
                    width: 26,
                    symbolSize: 12,
                    symbolX: 13,
                    symbolY: 13
                }
            },
            exporting: {
                buttons: {
                    exportButton: {
                        x: -20, y: 20
                    },
                    printButton: {
                        x: -50, y: 20
                    }
                }
            }
        });});
</script>
