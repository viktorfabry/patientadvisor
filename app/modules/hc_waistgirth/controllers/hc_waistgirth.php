<?php

class Hc_waistgirth extends MX_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        } else {
            $this->module = 'hc_waistgirth';
            $this->user_id = $this->tank_auth->get_user_id();
            $this->username = $this->tank_auth->get_username();
            $this->role_id = $this->session->userdata('role');
            $this->imp = $this->session->userdata('imp');
            $this->patient_id = $this->session->userdata('patient_id');
            $this->load->model('color');
        }
    }


    function index() {
        $data = array();
        $initArray = $this->db_fnc->getRows('patient_id', $this->patient_id, 'body_size', 'sample_date ASC');
        $this->load->model('patient');
        $p = $this->patient->getPatientByID($this->patient_id);
        $this->load->model('color');
        $color = $this->color->getColors(array('module_id' => 0));
        $graphBonds = array();
        $data['min'] = 0;
        $data['max'] = 0;
        $data['js_bonds'] = "";
        $gMeasure = ($this->imp == 'Y') ? 'inch' : 'cm';

        if (isset($p) && $p) {

            $data['min'] = ($p['gender'] == 'M') ? $this->app_functions->getCorrectLengthValue(65) : $this->app_functions->getCorrectLengthValue(60);
            $data['max'] = ($p['gender'] == 'M') ? $this->app_functions->getCorrectLengthValue(150) : $this->app_functions->getCorrectLengthValue(140);

            $bonds[0]['from'] = ($p['gender'] == 'M') ? $this->app_functions->getCorrectLengthValue(65) : $this->app_functions->getCorrectLengthValue(60);
            $bonds[0]['to'] = ($p['gender'] == 'M') ? $this->app_functions->getCorrectLengthValue(100) : $this->app_functions->getCorrectLengthValue(90);
            $bonds[1]['from'] = ($p['gender'] == 'M') ? $this->app_functions->getCorrectLengthValue(100) : $this->app_functions->getCorrectLengthValue(90);
            $bonds[1]['to'] = ($p['gender'] == 'M') ? $this->app_functions->getCorrectLengthValue(150) : $this->app_functions->getCorrectLengthValue(140);

            $graphBonds = array(
                array('color' => $color['bound_lower'], 'from' => -100, 'to' => $bonds[0]['from']),
                array('color' => $color['bound_normal'], 'from' => $bonds[0]['from'], 'to' => $bonds[0]['to']),
                array('color' => $color['bound_over'], 'from' => $bonds[1]['from'], 'to' => $bonds[1]['to'])
            );

        }


        $this->load->library('highcharts');
        $this->highcharts->initialize('chart_template');
        $yAxis = array(
            'plotBands' => $graphBonds,
            'showFirstLabel' => false,
            'gridLineColor' => 'rgba(255,255,255,0.5)',
            'allowDecimals' => false,
            'title' => array(
                'text' => 'Abdo Girth ('.$gMeasure.')',
                'style' => array(
                    'font' => 'normal 12px Verdana, sans-serif',
                    'color' => '#333'
                )
            ),
        );
        $this->highcharts->set_legend(array('enabled' => false));
        $this->highcharts->set_title('Waist girth');
        $this->highcharts->render_to('waistgirth');
        $this->highcharts->set_yAxis($yAxis);
        $ch = $this->createChartArray($initArray);

        return $data['charts'] = $this->highcharts->set_serie($ch['waistgirth'])->render(true);
    }

    function createChartArray($initArray) {
        if ($initArray) {
            $b['waistgirth'] = array();
            $datestring = "Y, n ,j";
            foreach ($initArray as $sample) {
                if (isset($sample['girth']) && $sample['girth']) {

                    $b['waistgirth'][] = "{x:" . gmt_to_local(strtotime($sample['sample_date']), 'UTC').'000'
                            . ",y:" . $this->app_functions->getCorrectLengthValue($sample['girth'])
                            . ",note:'" . (isset($sample['note']) ? $sample['note'] : '')
                            . "'}";
                }
            }
            $js['waistgirth'] = array('name' => 'Waist girth', 'data' => '[' . implode(',', $b['waistgirth']) . ']');
        } else {
            $js['waistgirth'] = array('name' => 'Waist girth', 'data' => '[]');
        }

        return $js;
    }


}

