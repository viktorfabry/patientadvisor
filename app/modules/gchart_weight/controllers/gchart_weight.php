<?php

class Gchart_weight extends MX_Controller {

	function  __construct() {
            parent::__construct();

            if (!$this->tank_auth->is_logged_in()) {
			redirect('/auth/login/');
            } else {
                $this->module       = 'gchart_weight';
                $this->user_id	= $this->tank_auth->get_user_id();
                $this->username	= $this->tank_auth->get_username();
                $this->role_id      = $this->session->userdata('role');
            }
         }
	
	function index()
	{
		$chart_all = $this->db_fnc->getRows('patient_id','1','body_size','sample_date ASC');
		
		
		$data['users'] = $this->db_fnc->getKeyValueArray('id','uname','patients');		
		$data['chart_all'] = $chart_all;	
		$data['weight'] = $this->createChartArray($chart_all);
		
		$data['patient_id'] = 0;
		
		$this->load->view('welcome_message',$data);
	}
	
	function createChartArray($initArray)
	{
		$chart = array();
		$datestring = "Y, n ,j";
		$r = 0;
		$n = 0;
		foreach ($initArray as $sample)
   {
		$chart[$n][0] = $r;
		$chart[$n][1] = 0;
		$chart[$n][2] = "new Date(". date(
									$datestring,
									strtotime($sample['sample_date']. "-1 month")
									).")";
		$n++;
		
		$chart[$n][0] = $r;
		$chart[$n][1] = 1;
		$chart[$n][2] = (int) $sample['weight'];
		
		$n++;
		
	
		$r++;
   }
   
   return $chart;
		
}
	
	
		
	function user($id){
		
		$data['users'] = $this->db_fnc->getKeyValueArray('id','uname','patients');

			
		$chart_all = $this->db_fnc->getRows('patient_id',$id,'body_size');
			
		$data['weight'] = $this->createChartArray($chart_all);

		$data['patient_id'] = $id;
			
		$this->load->view('weight_view',$data);
			
	}
	
	
	function addSample(){
		$patient_id = $_POST['patient_id'];
		$sample = $this->db_fnc->getMax('sample','patient_id',$patient_id,'body_size');
		$sample++;
		
		
		$datestring = "Y-m-d H:i:s";
		$sample_date = date($datestring, time());
		
		$tablename = 'body_size';
		$fieldarray = array('patient_id' => $patient_id, 'sample' => $sample, 'sample_date' => $sample_date, 'weight' => $_POST['weight'], 'girth' => $_POST['girth']);
		
		$this->db_fnc->insert($tablename,$fieldarray);

		redirect($_POST['url']);	 	 	 	 	 	
	}
	
}

