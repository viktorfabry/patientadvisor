<?php

class Bp_advice_test extends Bp_advice {

    function __construct() {
        parent::__construct();

        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        } else {
            $this->user_id = $this->tank_auth->get_user_id();
            $this->username = $this->tank_auth->get_username();
            $this->role_id = $this->session->userdata('role');
            $this->patient_id = $this->session->userdata('patient_id');
            $this->load->model('patient');
            $this->patient = $this->patient->getPatientData($this->patient_id);
            $this->importance = '';
            $this->message = '';
        }
    }

    function index() {

        //condition 1


        $this->load->view('bp_advice_test_view', $data);
    }

    
}

class Patient {
   var $diabetes = null;
    var $hypertension = null;
    var $IHD = null;
    var $ownbpmachine = null;
    var $systolic_today_avg = null;
    var $diastolic_today_avg = null;
    var $systolic_month_avg = null;
    var $diastolic_month_avg = null;
    var $num_readings_today = null;
    var $num_readings_month = null;
    var $last_reading_days = null;
    var $last_systolic = null;
    var $last_diastolic = null;

//    [dob] => 1956-07-20
//    [height] => 180
//    [gender] => M
//    [smoker] => N
//    [date_stopped_smoking] => 0000-00-00

}