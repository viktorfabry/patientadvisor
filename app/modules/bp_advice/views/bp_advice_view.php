<?php if ($message) : ?>
    <div class="advice corner10 <?php echo $importance; ?>">
        <p><?php echo $message; ?></p>
        <span class="spike">&nbsp</span>
    </div>
<?php endif; ?>

