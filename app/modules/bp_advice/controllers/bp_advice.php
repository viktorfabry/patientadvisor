<?php

class Bp_advice extends MX_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        } else {
            $this->user_id = $this->tank_auth->get_user_id();
            $this->username = $this->tank_auth->get_username();
            $this->role_id = $this->session->userdata('role');
            $this->patient_id = $this->session->userdata('patient_id');
            $this->load->model('patient');
            $this->patient = $this->patient->getPatientData($this->patient_id);
            $this->importance = '';
            $this->message = '';
        }
    }

    function index() {

        $this->getBP();

        $this->getAdvice($this->patient);

        $data['message'] = $this->message;
        $data['importance'] = $this->importance;
        $this->load->view('bp_advice_view', $data);
    }

    function getBP() {
        $this->patient['systolic_today_avg'] = 0;
        $this->patient['diastolic_today_avg'] = 0;
        $this->patient['systolic_month_avg'] = 0;
        $this->patient['diastolic_month_avg'] = 0;

        $num_readings_today = 0;
        $num_readings_month = 0;
        $today_all_systolic = 0;
        $today_all_diastolic = 0;
        $month_all_systolic = 0;
        $month_all_diastolic = 0;

        $bp = $this->db_fnc->getRows('patient_id', $this->patient_id, 'bloodpressure', 'sample_date DESC');

        foreach ($bp as $b):
            $sample_time_dif = (time() - strtotime($b['sample_date'])) / (60 * 60 * 24);
            ////////////systolic_today_avg////////////////
            //echo '<br /><b>'.$sample_time_dif.'</b><br />';
            if ($sample_time_dif <= 1) {
                $num_readings_today++;
                $today_all_systolic += $b['systolic'];
                $today_all_diastolic += $b['diastolic'];
            }


            ////////////systolic_month_avg////////////////
            if ($sample_time_dif <= 30) {
                $num_readings_month++;
                $month_all_systolic += $b['systolic'];
                $month_all_diastolic += $b['diastolic'];
            }

        endforeach;

        $this->patient['num_readings_today'] = $num_readings_today;
        $this->patient['num_readings_month'] = $num_readings_month;

        if ($num_readings_today > 0) {
            $this->patient['systolic_today_avg'] = $today_all_systolic / $num_readings_today;
            $this->patient['diastolic_today_avg'] = $today_all_diastolic / $num_readings_today;
        }

        if ($num_readings_month > 0) {
            $this->patient['systolic_month_avg'] = $month_all_systolic / $num_readings_month;
            $this->patient['diastolic_month_avg'] = $month_all_diastolic / $num_readings_month;
        }

        if (isset($bp[0]['sample_date']) && $bp[0]['sample_date']) {
            $last_sample_date = $bp[0]['sample_date'];
            $lsd = strtotime($last_sample_date);
            $last_reading = time() - $lsd;

            $this->patient['last_reading_days'] = floor($last_reading / (60 * 60 * 24));
            $this->patient['last_systolic'] = $bp[0]['systolic'];
            $this->patient['last_diastolic'] = $bp[0]['diastolic'];
        }
    }

    function getAdvice($p) {

        //print_r($p); die;
        $A = array();

        $datestring = "Y-m-d H:i:s";
        $now = date($datestring, time());
        $today = date($datestring, time());
        $yearAgo = date($datestring, (time() - (60 * 60 * 24 * 365)));
        $monthAgo = date($datestring, (time() - (60 * 60 * 24 * 30)));


        $A['patient_id'] = $this->patient_id;
        $A['userData'] = $p;
        //.'<br />';
        //$where = array('diabetes' => $p['diabetes'], 'ownbpmachine' => $p['ownbpmachine'], 'hypertension' => $p['hypertension']);



        $diabetes = (isset($p['diabetes']) && $p['diabetes']) ? $p['diabetes'] : '';
        $ownbpmachine = (isset($p['ownbpmachine']) && $p['ownbpmachine']) ? $p['ownbpmachine'] : '';
        $hypertension = (isset($p['hypertension']) && $p['hypertension']) ? $p['hypertension'] : '';

        $where = array(
            'diabetes !=' => $this->_makeOposit($diabetes),
            'ownbpmachine !=' => $this->_makeOposit($ownbpmachine),
            'hypertension !=' => $this->_makeOposit($hypertension)
        );
        $A['conditiions_where'] = $where;

        $advice = array();
        $advice = $this->db_fnc->getRowsWhere('advice_bp', $where);

        //print_r($advice); die;
        $data['trigers'] = $advice;
        $prev_priority = 1000000;
        if ($advice && isset($p['last_systolic']) && isset($p['last_diastolic']) && isset($p['last_reading_days']) ) {
            foreach ($advice as $a):

                $conLS = $this->_goCompare($a['last_systolic'], $p['last_systolic']);
                $conLD = $this->_goCompare($a['last_diastolic'], $p['last_diastolic']);
                $OLS = strpos($a['last_systolic'], '>');

                if ($OLS === false) {
                    $con_LS_LD = $this->_checkAnd($conLS, $conLD);
                } else {
                    $con_LS_LD = $this->_checkOr($conLS, $conLD);
                }

                $conLRD = $this->_goCompare($a['last_reading_days'], $p['last_reading_days']);

                $conNRT = $this->_goCompare($a['num_readings_today'], $p['num_readings_today']);
                $conSTA = $this->_goCompare($a['systolic_today_avg'], $p['systolic_today_avg']);
                $conDTA = $this->_goCompare($a['diastolic_today_avg'], $p['diastolic_today_avg']);

                if ($conNRT) {
                    $OSTA = strpos($a['systolic_today_avg'], '>');
                    if ($OSTA === false) {
                        $con_STA_DTA = $this->_checkAnd($conSTA, $conDTA);
                    } else {
                        $con_STA_DTA = $this->_checkOr($conSTA, $conDTA);
                    }
                } else {
                    $con_STA_DTA = FALSE;
                }

                $conNRM = $this->_goCompare($a['num_readings_month'], $p['num_readings_month']);
                $conSMA = $this->_goCompare($a['systolic_month_avg'], $p['systolic_month_avg']);
                $conDMA = $this->_goCompare($a['diastolic_month_avg'], $p['diastolic_month_avg']);

                if ($conNRM) {
                    $OSMA = strpos($a['systolic_month_avg'], '>');
                    if ($OSMA === false) {
                        $con_SMA_DMA = $this->_checkAnd($conSMA, $conDMA);
                    } else {
                        $con_SMA_DMA = $this->_checkOr($conSMA, $conDMA);
                    }
                } else {
                    $con_SMA_DMA = false;
                }

                $conditions = array($con_LS_LD, $conLRD, $con_STA_DTA, $con_SMA_DMA);


                if ($con_LS_LD && $conLRD && $con_STA_DTA && $con_SMA_DMA) {
                    $allCon = TRUE;
                } else {
                    $allCon = false;
                }

                $id = $a['condition'];
                //$A['conditions'][$id] = $id;

                $A['conditions'][$id] = array(
                    'conditions' => $conditions,
                    'diabetes' => $a['diabetes'],
                    'ownbpmachine' => $a['ownbpmachine'],
                    'hypertension' => $a['hypertension'],
                    'patient_last_systolic' => $p['last_systolic'],
                    'last_systolic' => $a['last_systolic'],
                    'patient_last_diastolic' => $p['last_diastolic'],
                    'last_diastolic' => $a['last_diastolic'],
                    'con_LS_LD' => $con_LS_LD,
                    'patient_last_reading_days' => $p['last_reading_days'],
                    'last_reading_days' => $a['last_reading_days'],
                    'conLRD' => $conLRD,
                    'patient_num_readings_today' => $p['num_readings_today'],
                    'num_readings_today' => $a['num_readings_today'],
                    '$conNRT' => $conNRT,
                    'patient_systolic_today_avg' => $p['systolic_today_avg'],
                    'systolic_today_avg' => $a['systolic_today_avg'],
                    'patient_diastolic_today_avg' => $p['diastolic_today_avg'],
                    'diastolic_today_avg' => $a['diastolic_today_avg'],
                    'con_STA_DTA' => $con_STA_DTA,
                    'num_readings_month' => $a['num_readings_month'],
                    'patient_num_readings_month' => $p['num_readings_month'],
                    'conNRM' => $conNRM,
                    'patient_systolic_month_avg' => $p['systolic_month_avg'],
                    'systolic_month_avg' => $a['systolic_month_avg'],
                    'patient_diastolic_month_avg' => $p['diastolic_month_avg'],
                    'diastolic_month_avg' => $a['diastolic_month_avg'],
                    'con_SMA_DMA' => $con_SMA_DMA,
                    'action' => $a['action']
                );



                if ($allCon) {
                    $messages[] = $a['action'];
                    $priorities[] = $a['priority'];
                    $conD = $a['condition'];

                    if ($prev_priority > $a['priority']) {

                        $conD = $a['condition'];
                        $message = $a['action'];
                        $prev_priority = $a['priority'];
                    }
                }




            endforeach;

            if (isset($conD) && $conD) {
                $A['conditions']['MATCH'] = $conD;
                $A['conditions']['ACTION'] = $message;
                echo '<!-- condition match: ' . $conD . ' priority: ' .$prev_priority. ' -->';
            } else {
                echo '<div class="error">No condition matched!</div>';
            }

            // print_r($A); die;
            $this->message = (isset($message) && $message) ? $message : '';

            $priority = intval($prev_priority);
            if ($priority < 100) {
                $this->importance = 'important';
            } elseif ($priority < 200) {
                $this->importance = 'notice';
            } else {
                $this->importance = '';
            }
        }
    }

    function _makeOposit($bool) {
        if ($bool == 'Y') {
            return 'N';
        } else {
            return 'Y';
        }
    }

    function _checkAnd($cond1, $cond2) {
        if ($cond1 && $cond2) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function _checkOr($cond1, $cond2) {
        if ($cond1 || $cond2) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //function _checkConditions($conditions){
    //	if(($conditions[0] || $conditions[1]) && $conditions[2] && $conditions[3]) {
    //		return TRUE;
    //	} else {
    //		return FALSE;
    //	}
    //}

    function _goCompare($field, $p) {
        if ($field != '') {
            $cond = $this->_compareToField($field, $p);
        } else {
            $cond = TRUE;
        }
        return $cond;
    }

    function _compareToField($field, $p) {

        $operator = preg_replace('([0-9/./ /])', '', $field);
        $number = preg_replace('([^0-9/.])', '', $field);


        //print_r($operator); echo '  '; print_r($number); echo '<br />';

        if ($operator == '=') {
            return $p == $number ? TRUE : FALSE;
        } elseif ($operator == '>') {
            return $p > $number ? TRUE : FALSE;
        } elseif ($operator == '>=') {
            return $p >= $number ? TRUE : FALSE;
        } elseif ($operator == '<') {
            return $p < $number ? TRUE : FALSE;
        } elseif ($operator == '<=') {
            return $p <= $number ? TRUE : FALSE;
        }
    }

    function edit() {
        $data['advice'] = $this->db_fnc->getAll('advice');
        $this->load->view('editAdvice_view', $data);
    }

    function saveAdvice($id) {
        $update = $_POST;
        $where = array('id' => $id);
        $data['update'] = $this->db_fnc->update('advice', $update, $where);
        redirect('admin/advice/edit');
    }

    function newAdvice() {
        $insert = $_POST;
        $data['insert'] = $this->db_fnc->insert('advice', $insert);
        redirect('admin/advice/edit');
    }

}
