<!doctype html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"> <!--<![endif]-->


    <head>
        <meta charset="UTF-8">

        <title></title>
        <meta name="description" content="">
        <meta name="keywords" content="" />
        <meta name="author" content="">
        <link rel="shortcut icon" href="/favicon.ico">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="apple-touch-icon" href="/apple-touch-icon.png">
        <meta name="robots" content="index, follow"/>

        <base href="<?php echo base_url() ?>" />

	<!--[if lte IE 9]><link rel="stylesheet" href="css/ie.css" type="text/css" media="screen" /><![endif]-->
	<!--script type="text/javascript" src="js/css3-mediaqueries.js"></script-->

        <?php Tools::showCss(); ?>

        
        <!--link rel="alternate" type="application/rss+xml" title="RSS Feed" href="/articles/feed" /-->

        <noscript><style>.jsError{display: block;}</style></noscript>
        <!--script src="js/libs/modernizr-1.7.min.js"></script-->
    </head>
    <body <?php echo (isset($fsize) && $fsize) ? 'class="'.$fsize.'"' : ''; ?> >
