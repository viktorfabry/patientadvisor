<?php

class Head extends MX_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        Tools::addCSS("css/reset.css");
        Tools::addCSS("css/1140.css");
        Tools::addCSS("css/style_common.css");
        Tools::addCSS("css/style.css");
        Tools::addCSS("css/feedback.css");
        Tools::addCSS("js/fancybox/jquery.fancybox-1.3.1.css");
        //Tools::addCSS("js/jquery-ui/css/custom-theme/jquery-ui-1.8.14.custom.css");
        Tools::addCSS("js/jquery-ui/css/Aristo/Aristo.css");

        $fsize = $this->session->userdata('fsize');
        $data['fsize'] = (isset($fsize) && $fsize) ? 'fsize'.$fsize : '';


        $this->load->view('head_view',$data);

    }

}
