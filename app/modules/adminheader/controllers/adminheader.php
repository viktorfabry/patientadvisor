<?php

class Adminheader extends MX_Controller {

	function  __construct() {
            parent::__construct();

            if (!$this->tank_auth->is_logged_in()) {
			redirect('adminauth/login/');
            } else {
                $this->module   = 'adminheader';
                $this->user_id	= $this->tank_auth->get_user_id();
                $this->username	= $this->tank_auth->get_username();
                $this->role_id  = $this->session->userdata('role');
                $this->group_id = $this->session->userdata('group_id');
                $this->load->model('group');
            }
         }

	
	function index() {
            if($this->access->has_access($this->role_id ,$this->module)){
                $data = array();
                $activeMethod = ($this->router->fetch_method() != 'index') ? '/'.$this->router->fetch_method() : '';
                $active    = 'admin/'.$this->router->fetch_class().$activeMethod;

                $activeModule    =  $this->access->getModuleByName($active); 
                $data['active']    = $active;
                $parent = $this->access->getParent($active);

                $group = $this->group->getGroupByID($this->group_id);
                $data['heading'] = (isset($group['name']) && $group['name']) ? $group['name'] : 'Patient Advisor Admin';

                $data['main_menu'] = $this->access->getModulesForRole($this->role_id,'admin_menu');
                $data['main_menu'] += ($this->user_id) ? array('adminauth/logout'=>'logout') : array('adminauth/login'=>'login') ;

                $id = ($parent['module_id']!='') ? $parent['module_id'] : $activeModule['module_id'] ;

                if($id) $sub_menu = $this->access->getModulesForRole($this->role_id,'admin_submenu', $id);

                if(isset($sub_menu) && $sub_menu){
                    $data['mainactive'] = ($parent['link']!='') ? $parent['link'] : $active;
                    $data['sub_menu'] = ($parent['link']!='') ? array($parent['link']=>$parent['name']) : array($activeModule['link']=>$activeModule['name']);
                    $data['sub_menu'] += $sub_menu;
                }

                $this->load->view('adminheader_view',$data);
            }
        }
}
