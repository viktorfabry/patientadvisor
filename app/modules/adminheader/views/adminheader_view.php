
  <div id="wrapper">
  <div id="header" class="ui-widget-header">
  <h2 class="adminLogo"><?php echo $heading; ?></h2>

  
<?php
  if(isset($main_menu) && $main_menu!=''){
        $menu = '<ul id="adminMenu">';
        foreach ($main_menu as $key => $value) {
            $menu .= '<li><a class="curvesTop10 ';
            if(isset($mainactive) && $mainactive == $key) $menu .= 'mainactive ';
            if($active == $key) $menu .= 'active';
            $menu .= '" href="'.base_url().$key.'"';
            $menu .= 'title="'.$value.'">'.$value.'</a></li>';
        }
        $menu .= '</ul>';
        echo $menu;
  }
?>
  </div> <!-- header -->
  <?php
  if(isset($sub_menu) && $sub_menu!=''){
        $menu = '<ul class="admin_submenu clearfix">';
        foreach ($sub_menu as $key => $value) {
            $menu .= '<li><a class="curves5 ';
            if($active == $key) $menu .= 'active';
            $menu .= '" href="'.base_url().$key.'"';
            $menu .= 'title="'.$value.'">'.$value.'</a></li>';
        }
        $menu .= '</ul>';
        echo $menu;
  }
?>