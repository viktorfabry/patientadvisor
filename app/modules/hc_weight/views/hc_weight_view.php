<script type="text/javascript">
    var url = "<?php echo base_url(); ?>index.php/weight/user/";
	
    var chart;
    $(document).ready(function() {
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'weight',
                type: 'spline',
                zoomType: 'x',
                backgroundColor: '#D8D8D2',
                borderRadius: 10,
                margin: [50, 20, 40, 60],
                color: '#333'
            },
            title: {
                text: 'Weight'
            },
            subtitle: {
                text: 'Click and drag in the plot area to zoom in',
                align: 'left',
                verticalAlign: 'bottom',
                x: 50,
                y: 7
            },
            xAxis: {
                type: 'datetime',
                maxZoom: 7 * 24 * 3600000, // fourteen days
                title: {
                    text: null
                },
                labels: {
                    style: {
                        font: 'normal 12px Verdana, sans-serif'
                    }
                },
                showFirstLabel: false

            },
            yAxis: {
                title: {
                    text: '<?php echo $graphLabel; ?>'
                },
                labels: {
                    align: 'right',
                    style: {
                        font: 'normal 14px Verdana, sans-serif'
                    }
                },
                //min: 30,
                //max: 180,
                tickInterval: 10,
                //tickColor: '#FFFFFF',
                //minorTickInterval: 5,
                //minorTickLength: 0,
                gridLineColor: '#ffffff',
                startOnTick: false,
                endOnTick: false,
                showFirstLabel: false,
                plotBands: [
                    {color: '#DBE0C5', from: 0, to: 600}
                ]
            },
            tooltip: {
//                formatter: function() {
//                    var note = (this.point.note) ? '<span style="color: <?php echo $color['line1'] ?>">"' + this.point.note + '"</span><br> ' : '';
//
//                    return '<strong>'+ this.series.name + ': '+
//                        Highcharts.numberFormat(this.y, 0) + "</strong><br> "+
//                        note +
//                        Highcharts.dateFormat('%B %e %Y, %A', this.x);
//                },
                "formatter":function() {
        var note = (this.point.note) ? '<span style="color: #f00;">' + this.point.note + '</span><br> ' : '';
        return '<strong>' + this.series.name + ': ' + Highcharts.numberFormat(this.y, 0) + '</strong><br> ' + note + Highcharts.dateFormat('%B %e %Y, %A', this.x);
    },
                style: {
                    color: '#333333',
                    fontSize: '14px',
                    padding: '10px 15px'
                }
            },
            legend: {
                enabled: false,
                align: 'right',
                verticalAlign: 'top',
                backgroundColor: '#fff',
                x: 0,
                y: 0
            },
            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: [0, 0, 0, 300],
                        stops: [
                            [0, '<?php echo $color['bound_normal']; ?>'],
                            [1, 'rgba(2,0,0,0)']
                        ]
                    },
                    lineWidth: 1,
                    marker: {
                        enabled: false,
                        states: {
                            hover: {
                                enabled: true,
                                radius: 5
                            }
                        }
                    },
                    shadow: false,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    }
                }
            },
            series: [
                {name: 'Weight', color: '#B60910', data: <?php echo $weight['weight_jsArray'] ?> }
            ],
            navigation: {
                buttonOptions: {
                    height: 26,
                    width: 26,
                    symbolSize: 12,
                    symbolX: 13,
                    symbolY: 13
                }
            },
            exporting: {
                buttons: {
                    exportButton: {
                        x: -20, y: 20
                    },
                    printButton: {
                        x: -50, y: 20
                    }
                }
            }
        });});
</script>


