<?php

class Hc_weight extends MX_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        } else {
            $this->module = 'hc_weight';
            $this->user_id = $this->tank_auth->get_user_id();
            $this->username = $this->tank_auth->get_username();
            $this->role_id = $this->session->userdata('role');
            $this->imp = $this->session->userdata('imp');
            $this->patient_id = $this->session->userdata('patient_id');
            $this->load->model('color');
        }
    }

    function index() {
        $data = array();
        $initArray = $this->db_fnc->getRows('patient_id', $this->patient_id, 'body_size', 'sample_date ASC');
        $this->load->model('patient');
        $p = $this->patient->getPatientByID($this->patient_id);
        $this->load->model('color');
        $color = $this->color->getColors(array('module_id' => 0));
        $graphBonds = array();

        if (isset($p) && $p) {
            $graphBonds = array(
                array('color' => $color['bound_normal'], 'from' => 0, 'to' => 600)
            );
        }

        $this->load->library('highcharts');
        $this->highcharts->initialize('chart_template');
        $yAxis = array(
            'plotBands' => $graphBonds,
            'showFirstLabel' => false,
            'allowDecimals' => false,
            'gridLineColor' => 'rgba(255,255,255,0.5)',
            'title' => array(
                'text' => 'Weight (' . $this->app_functions->getWeightMeasure() . ')',
                'style' => array(
                    'font' => 'normal 12px Verdana, sans-serif',
                    'color' => '#333'
                )
            ),
        );
        $this->highcharts->set_legend(array('enabled' => false));
        $this->highcharts->set_title('Weight');
        $this->highcharts->render_to('weight');
        $this->highcharts->set_yAxis($yAxis);
        $ch = $this->createChartArray($initArray);

        return $data['charts'] = $this->highcharts->set_serie($ch['weight'])->render(true);
    }

    function createChartArray($initArray) {
        if ($initArray) {
            $b['weight'] = array();
            $datestring = "Y, n, j, H, i, s, u";
            foreach ($initArray as $sample) {
                if (isset($sample['weight']) && $sample['weight']) {

                    $weight = ($this->imp == 'Y') ? kg2lb($sample['weight']) : $sample['weight'];

                    $b['weight'][] = "{x:". gmt_to_local(strtotime($sample['sample_date']), 'UTC').'000' 
                            . ",y:" . $weight
                            . ",note:'"
                            . (isset($sample['note']) ? $sample['note'] : '')
                            . "'}";
                }
            }
            $js['weight'] = array('name' => 'weight', 'data' => '[' . implode(',', $b['weight']) . ']');
        } else {
            $js['weight'] = array('name' => 'weight', 'data' => '[]');
        }

        return $js;
    }

}

