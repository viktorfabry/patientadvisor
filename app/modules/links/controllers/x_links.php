<?php

class Links extends MX_Controller {

	function  __construct() {
            parent::__construct();

            if (!$this->tank_auth->is_logged_in()) {
			redirect('/auth/login/');
            } else {
                $this->module   = 'links';
                $this->user_id	= $this->tank_auth->get_user_id();
                $this->username	= $this->tank_auth->get_username();
                $this->role_id  = $this->session->userdata('role');
                $this->dr       = $this->load->model('Doctor');
                $this->links    = $this->load->model('links');
            }
         }
	
	function index(){
                if($this->access->has_access($this->role_id ,$this->module)){

                    $orderBy = 'link_id ASC, name ASC';
                    $links = $this->links->getLinks('drlinks', $orderBy);

                       
                    $dr_menu = $this->load->library('multilevelmenu',$menulinks);
                    $dr_menu->setFlink('Doctor Links');

                    $data['menu'] = $dr_menu->getMenu();
                    $this->load->view('drbar_view',$data);
                }
	}

 


	
	
}

