<?php

class Table_diabetes extends MX_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        } else {
            $this->module = 'table_diabetes';
            $this->user_id = $this->tank_auth->get_user_id();
            $this->username = $this->tank_auth->get_username();
            $this->role_id = $this->session->userdata('role');
            $this->patient_id = $this->session->userdata('patient_id');
            $this->load->model('diabetes');
        }
    }

    public function index() {
        $data['maxSample'] = $this->db_fnc->getMax('sample', 'patient_id', $this->patient_id, 'diabetes') + 1;
        $this->load->view('table_diabetes_view', $data);
    }

    public function getTime() {
        $datestring = "Y-m-d H:i:s";
        echo $now = date($datestring, time());
    }

    public function getById($id) {
        if (isset($id))
            echo json_encode($this->diabetes->getById($id));
    }

    public function create() {
        if (!empty($_POST)) {
            echo $this->diabetes->create($this->patient_id);
            //echo 'New user created successfully!';
        }
    }

    public function read() {
        echo json_encode($this->diabetes->getAllSamples($this->patient_id));
    }

    public function update() {
        if (!empty($_POST)) {
            $this->diabetes->update();
            echo 'Record updated successfully!';
        }
    }

    public function delete($id = null) {
        if (is_null($id)) {
            echo 'ERROR: Id not provided.';
            return;
        }

        $this->diabetes->delete($id);
        echo 'Records deleted successfully';
    }

}

//end class
