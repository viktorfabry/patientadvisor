<?php

if ((isset($msg) && $msg) || (isset($sessionMsg) && $sessionMsg)) {
    $html = "<!-- msg -->\n";
    $html .= '<div id="msg">';

    if (isset($sessionMsg) && $sessionMsg)
        $html .= $sessionMsg;
    if (isset($msg) && $msg)
        $html .= $msg;


    $html .= '</div> <!-- msg -->';
    echo $html;
}

if (isset($errors) && $errors) {
    $html = "<!-- errors -->\n";
    $html .= '<div id="errors">';
    $html .= $errors;
    $html .= '</div> <!-- errors -->';
    echo $html;
}
?>
