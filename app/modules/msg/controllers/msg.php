<?php

class Msg extends MX_Controller {

	function __construct()
	{
		parent::__construct();
	}
	
	function index($data = array()) {
            $data['sessionMsg'] = $this->session->flashdata('msg');
            $data['errors'] = $this->session->flashdata('errors');
            $this->load->view('msg_view',$data);
	}
	
			
}
