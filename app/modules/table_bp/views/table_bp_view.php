<html>
<head>
<title>Blood Pressure History</title>

<base href="<?php echo base_url() ?>" />

<!--link type="text/css" rel="stylesheet" href="css/demo_table.css" /-->
<link type="text/css" rel="stylesheet" href="css/smoothness/jquery-ui-1.8.2.custom.css" />
<link type="text/css" rel="stylesheet" href="css/styles.css" />
<link type="text/css" rel="stylesheet" href="js/TableTools/media/css/TableTools.css" />

<script type="text/javascript" charset="utf-8">
			//$(document).ready( function () {
			//	/* You might need to set the sSwfPath! Something like:
			//	 *   TableToolsInit.sSwfPath = "/media/swf/ZeroClipboard.swf";
			//	 */
			//	$('#example').dataTable( {
			//		"sDom": 'T<"clear">lfrtip'
			//	} );
			//} );
</script>


</head>
<body>

<div id="ajaxLoadAni" >
    <img src="img/ajax-loader.gif" alt="Ajax Loading Animation" />
    <span>Loading...</span>
</div>

<div id="tabs" class="hide">
    
    <ul >
        <li><a href="#read">Read</a></li>
        <li><a class="createTab" href="#create">Create</a></li>
    </ul>
    
    <div id="read" >
        <table id="records">
            <thead>
                <tr>
                    <th>sample</th>
                    <th>sample date</th>
                    <th>systolic</th>
                    <th>diastolic</th>
                    <th>note</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
    <div id="create" >
        <form action="" method="post">
           <p>
               <label for="cSample_date">sample_date:</label>
               <input type="text" id="cSample_date" name="cSample_date" value="" />
            </p>
            
            <p>
               <label for="cSystolic">systolic:</label>
               <input type="text" id="cSystolic" name="cSystolic" />
            </p>
            
            <p>
               <label for="cDiastolic">diastolic:</label>
               <input type="text" id="cDiastolic" name="cDiastolic" />
            </p>
            <p>
               <label for="cNote">note:</label>
               <input type="text" id="cNote" name="cNote" />
            </p>
           <p>
               <label>&nbsp;</label>
               <input type="hidden" id="cSample" name="cSample" value="<?php echo $maxSample; ?>" />
               <input type="submit" name="createSubmit" value="Submit" />
           </p>
        </form>
    </div>

</div> <!-- end tabs -->

<!-- update form in dialog box -->
<div id="updateDialog" class="hide" title="Update">
    <div>
        <form action="" method="post">
            <p>
               <label for="sample_date">sample_date:</label>
               <input type="text" id="sample_date" name="sample_date" />
            </p>
            
            <p>
               <label for="systolic">systolic:</label>
               <input type="text" id="systolic" name="systolic" />
            </p>
            
            <p>
               <label for="diastolic">diastolic:</label>
               <input type="text" id="diastolic" name="diastolic" />
            </p>

            <p>
               <label for="note">note:</label>
               <input type="text" id="note" name="note" />
            </p>
            
            <input type="hidden" id="userId" name="id" />
        </form>
    </div>
</div>

<!-- delete confirmation dialog box -->
<div id="delConfDialog" class="hide" title="Confirm">
    <p>Are you sure you want to delete this record?</p>
</div>


<!-- message dialog box -->
<div id="msgDialog"><p></p></div>


<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui/js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/jquery-templ.js"></script>
<script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf-8" src="js/TableTools/media/js/TableTools.min.js"></script>
<script type="text/javascript" charset="utf-8" src="js/TableTools/media/ZeroClipboard/ZeroClipboard.js"></script>


<script type="text/template" id="readTemplate">
    <tr id="${id}">
        <td>${sample}</td>
        <td>${sample_date}</td>
        <td>${systolic}</td>
        <td>${diastolic}</td>
        <td>${note}</td>
        <td><a class="updateBtn" href="${updateLink}">Update</a> | <a class="deleteBtn" href="${deleteLink}">Delete</a></td>
    </tr>
</script>

<script type="text/javascript" src="js/table_bp.js"></script>

</body>
</html>