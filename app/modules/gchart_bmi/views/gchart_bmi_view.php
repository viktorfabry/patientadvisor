<!--/////////////////// bmi start///////////////////////////-->
<div class="module bmi corner10 clearfix">
<h2>Body Mass Index (BMI)</h2>
<script>
var url = "<?php echo base_url();?>index.php/bmi/user/";
</script>

<!--form method="POST" name="bmi_users"  >
<?php
$js = 'class="tutorials" onchange="location.href=url+bmi_users.users.options[selectedIndex].value"';
echo form_dropdown('users', $users, $patient_id, $js); 
?>
</form-->

<div class="inputfields">
<?php echo form_open('bmi/addSample');?>

<input type="hidden" name="patient_id" value="<?php echo $patient_id; ?>">
<input type="hidden" name="url" value="<?php echo $this->uri->uri_string(); ?>">
<label>Weight</label>
<input type="text" name="weight" value="">
<label>Waist girth</label>
<input type="text" name="girth" value="">
<input class="submitBtn"  type="submit" value="submit">

<?php echo form_close();?>
</div>


<?php
$this->load->library('qgooglevisualapi/ConfigInc');
	

$chart = new QAnnotatedtimelineGoogleGraph();
$chart	
		//->ignoreContainer()
		->addDrawProperties(
			array(
				"title"=>'Company Performance',
				"thickness"=>'3',
				"fill"=>'0',
				"wmode"=>'transparent',
				)
			)
		->addColumns(
			array(
		        array('date', 'Date'),
		        array('number', 'BMI'),
		        array('string', 'title'),
		        array('string', 'text'),			
				)
			)
		->setValues($BMI);
echo $chart->render();

//echo $chart->getReferenceLink();


?>
</div>
<!--/////////////////// bmi end///////////////////////////-->
