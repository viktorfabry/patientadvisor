<?php

class Gchart_bmi extends MX_Controller {

	function  __construct() {
            parent::__construct();

            if (!$this->tank_auth->is_logged_in()) {
			redirect('/auth/login/');
            } else {
                $this->module       = 'gchart_bmi';
                $this->user_id	= $this->tank_auth->get_user_id();
                $this->username	= $this->tank_auth->get_username();
                $this->role_id      = $this->session->userdata('role');
            }
         }
	
	function index()
	{
		$initArray = $this->db_fnc->getRows('patient_id','1','body_size');
		
		
		$data['users'] = $this->db_fnc->getKeyValueArray('id','uname','patients');		
		$data['bp_all'] = $chart_all;	
		$data['BMI'] = $this->createChartArray($initArray);
		
		$data['patient_id'] = 0;
		
		$this->load->view('bmi_view',$data);
	}
	
	
	function createChartArray($initArray,$id) {
		$chart = array();
		$datestring = "Y, n ,j";
		$r = 0;
		$n = 0;
		
		$person = $this->db_fnc->getRow('id',$id,'patients');
		
				//print_r($person); die;

		
		$height = (int) $person['height'] / 100;
		
		
		
		
		foreach ($initArray as $sample)
   {
		$chart[$n][0] = $r;
		$chart[$n][1] = 0;
		$chart[$n][2] = "new Date(". date(
									$datestring,
									strtotime($sample['sample_date']. "-1 month")
									).")";
		$n++;
		
		$chart[$n][0] = $r;
		$chart[$n][1] = 1;
		//$chart[$n][2] = (int) $sample['weight'];
		$chart[$n][2] = $this->calculateBMI($sample['weight'],$height);

		$n++;
		
	
		$r++;
   }
   
   return $chart;
		
}
	
	function calculateBMI($weight, $height){
		$BMI = $weight / ($height * $height);
		return $BMI;
	}
	
	
	
	function user($id){
		
		
		
		$data['id'] = $id;
		
		$data['users'] = $this->db_fnc->getKeyValueArray('id','uname','patients');

			
		$initArray = $this->db_fnc->getRows('patient_id',$id,'body_size');
			
		$data['BMI'] = $this->createChartArray($initArray,$id);

		$data['patient_id'] = $id;
			
		$this->load->view('bmi_view',$data);
			
	}
	
	function addSample(){
		$patient_id = $_POST['patient_id'];
		$sample = $this->db_fnc->getMax('sample','patient_id',$patient_id,'body_size');
		$sample++;
		
		
		$datestring = "Y-m-d H:i:s";
		$sample_date = date($datestring, time());
		
		$tablename = 'body_size';
		$fieldarray = array('patient_id' => $patient_id, 'sample' => $sample, 'sample_date' => $sample_date, 'weight' => $_POST['weight'], 'girth' => $_POST['girth']);
		
		$this->db_fnc->insert($tablename,$fieldarray);

		redirect($_POST['url']);	 	 	 	 	 	
	}
	
}

