</div> <!-- wrapper -->

<footer>
    <div id="bigfoot" class="">
        <div class="container">
            <div class="row_wrapper">
                <div class="row">
                    <nav>
                        <?php
                        $links ='';
                        if (isset($footer_links) && $footer_links) {
                            foreach ($footer_links as $catName => $cat) {
                        $links .= '<ul class="threecol">';
                        $links .= '<li><h3>'.$catName.'</h3></li>';
                            
                            if (isset($cat) && $cat) {
                                foreach ($cat as $link) {
                                    if(isset($link['url']) && isset($link['title'])){
                                    $links .=  '<li>'.anchor($link['url'],$link['title']).'</li>';
                                    }
                                }
                                
                            }
                        $links .= '</ul>';
                        }
                        }
                        echo $links;

                         ?>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div id="footer">
        <p class="credit"><?php echo $credit; ?></p>
    </div> <!-- footer -->
</footer>
<?php
    Tools::showJS();
?>
                            <!--[if lt IE 7 ]>
                            <script src="js/libs/dd_belatedpng.js"></script>
                            <script> DD_belatedPNG.fix('img, .png_bg');</script>
                                            	<![endif]-->

                            <!--script>
                                window._gaq = [['_setAccount','UAXXXXXXXX1'],['_trackPageview'],['_trackPageLoadTime']];
                                Modernizr.load({
                                  load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
                                });
                            </script-->

                            <!-- Prompt IE 6 users to install Chrome Frame. Remove this if you want to support IE 6.
                                 chromium.org/developers/how-tos/chrome-frame-getting-started -->
                            <!--[if lt IE 7 ]>
                              <script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
                              <script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
                            <![endif]-->

