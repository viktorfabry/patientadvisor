<?php

class Footer extends MX_Controller {

	function __construct()
	{
		parent::__construct();
                $this->load->model('content');
	}
	
	function index() {
                $profiler = $this->config->item('profiler');
                if(isset($profiler) && $profiler)  $this->output->enable_profiler(TRUE);

                $data['footer_links'] = $this->content->getContentLinksByCatId(1);

                $data['credit'] = 'PATIENT ADVISOR © 2011';
		$this->load->view('footer_view',$data);
	}
	
			
}
