<?php

class Feedback extends MX_Controller {

    function __construct() {
        parent::__construct();

        if ($this->tank_auth->is_logged_in()) {
            $this->module = 'table_weight';
            $this->user_id = $this->tank_auth->get_user_id();
            $this->username = $this->tank_auth->get_username();
            $this->role_id = $this->session->userdata('role');
            $this->full_name = $this->session->userdata('full_name');
            $this->imp = $this->session->userdata('imp');
            $this->patient_id = $this->session->userdata('patient_id');
            $this->load->model('patient');
            $this->p = $this->patient->getPatientByID($this->user_id);
        }
    }

    public function index() {
        if ($this->tank_auth->is_logged_in()) {
            $data['name'] = ($this->full_name) ? $this->full_name : $this->username;
            $data['email'] = isset($this->p['email']) ? $this->p['email'] : '';
            $this->load->view('feedback_view', $data);
        }
    }

    public function send_feedback() {
        if ($this->tank_auth->is_logged_in()) {
            if (!empty($_POST)) {
                $data = array(
                    'name' => $this->input->post('name'),
                    'email' => $this->input->post('email'),
                    'feedback' => $this->input->post('feedback'),
                    'page' => $this->input->post('page'),
                    'useragent' => $this->session->userdata('user_agent'),
                    'session_id' => $this->session->userdata('session_id')
                    //'ip_address' => $this->session->userdata('ip_address')
                );


                $this->db_fnc->insert('feedbacks',$data);
                echo app_msg('Your feedback successfully sent. Thank you.', 'success fadeOut');
            }
        }
    }

}

//end class
