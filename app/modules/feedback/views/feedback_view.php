<section id="feedback">
    <div class="inner dark_bg corner10 shadow clearfix">
        <?php // echo anchor('/', 'close <span class="corner10">x</span>', 'class="close corner10"'); ?>

        <div id="feedback_btn" class="dark_bg shadow corner10" >
            <?php echo anchor('feedback', '<span>feedback</span>', 'class="dark_bg corner10"'); ?>
        </div>
        <div id="ajaxLoadAni" class="corner10" >
            <img src="img/ajax-loader.gif" alt="Ajax Sending Animation" />
            <span>Sending...</span>
        </div>

        <?php
            $form = form_open('feedback/send_feedback/');
            $form .= form_label('Name', 'name');
            $form .= form_input('name', set_value('username', $name), 'class=""');
            $form .= form_error('name');

            $form .= form_label('Email Address', 'email');
            $form .= form_input('email', set_value('email', $email), 'class=""');
            $form .= form_error('email');

            $form .= form_label('Feedback message', 'feedback');
            $form .= form_textarea(array('name' => 'feedback', 'rows' => '3'), set_value('feedback'), 'id="feedback_msg" class="input"');
            $form .= form_error('feedback');

            $form .= form_hidden('page', current_url());

            $form .= form_submit('submit', 'send feedback', 'class="btn orange big marginT15 right"');

            $form .= form_close();

            echo $form;
        ?>
    </div>
</section>


<script type="text/javascript">
//$(document).ready(function() {

        $( '#feedback_btn').bind('toggleFeedback', function(){},

        $('#feedback_btn').toggle(
            function(){
                var inside = "-5em";
                $('#feedback').animate({"left": inside}, 300);
            }, function(){
                var outside = "-40em";
                $('#feedback').animate({"left": outside}, 300);
            }
        )
    );
    $('#feedback_btn a').click(function(e){
        e.preventDefault();
        $('#feedback').trigger('toggleFeedback');
    });
    $('#feedback .close').click(function(e){
        e.preventDefault();
        $('#feedback_btn').trigger('toggleFeedback');
    });
    // --- Create Record with Validation ---
    $( '#feedback form' ).validate({
        rules: {
            msg: { required: true }
        },
        submitHandler: function( form ) {
            $( '#ajaxLoadAni' ).fadeIn( 'fast' );
            $.ajax({
                url: 'feedback/send_feedback',
                type: 'POST',
                data: $( form ).serialize(),
                success: function( response ) {
                    var fb_form = $( '#feedback form' );
                    fb_form.before( response );
                    //clear all input fields in create form
                    $( '.input', fb_form ).val( '' );

                    //open Read tab
                    $( '#ajaxLoadAni' ).fadeOut( 'slow' );
                    fadeIt();
                }
            });

            return false;
        }

    });

$(".appMsg").click(function () {
        $(".appMsg").fadeOut("slow");
    });


    var fadeIt = function(){
        setTimeout(
            function(){
                $(".appMsg").fadeOut("slow");
            },3000);
    }

//}

</script>

