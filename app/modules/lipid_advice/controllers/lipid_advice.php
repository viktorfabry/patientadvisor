<?php

class Lipid_advice extends MX_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        } else {
            $this->module = 'hc_weight';
            $this->user_id = $this->tank_auth->get_user_id();
            $this->username = $this->tank_auth->get_username();
            $this->role_id = $this->session->userdata('role');
            $this->patient_id = $this->session->userdata('patient_id');
            $this->importance = '';
            $this->message = '';
        }
    }

    function index() {
        $A = array();
        $usertype = $this->session->userdata('usertype');

        if ($this->input->post('people')) {
            $patient_id = $this->input->post('people');
            $this->session->set_userdata('patient_id', $patient_id);
        } elseif ($this->session->userdata('patient_id')) {
            $patient_id = $this->session->userdata('patient_id');
        } else {
            $patient_id = 0;
        }
        $this->load->model('patient');

        $p = $this->patient->getPatientByID($this->patient_id);
        $chol = $this->db_fnc->getRows('patient_id', $patient_id, 'cholesterol', 'sample_date DESC');


        if ($chol) {

            $datestring = "Y-m-d H:i:s";
            $now = date($datestring, time());
            $today = date($datestring, time());
            $yearAgo = date($datestring, (time() - (60 * 60 * 24 * 365)));
            $monthAgo = date($datestring, (time() - (60 * 60 * 24 * 30)));

            //echo 'now: '.$now;
//			echo '<br />';
//			echo '30 days before: '.$monthAgo; 
            //print_r($chol);

            $last_sample_date = $chol[0]['sample_date'];
            $lsd = strtotime($last_sample_date);
            $last_reading = time() - $lsd;
            $last_reading_days = floor($last_reading / (60 * 60 * 24));
            //echo '<br /><b>'.$last_reading_days.'</b><br />';

            $userData = array(
                'last_HDL' => $chol[0]['HDL'],
                'last_total_cholesterol' => $chol[0]['total'],
                'last_reading_days' => $last_reading_days
            );

            $A['user_id'] = $patient_id;
            $A['userData'] = $userData;
            $A['chol'] = $chol;



            if (isset($p['diabetes']) && isset($p['hypertension']) && isset($p['IHD'])) {
                if ($p['diabetes'] == 'Y' || $p['hypertension'] == 'Y' || $p['IHD'] == 'Y') {
                    $where = array('DM_IHD_Hyp' => 'Y');
                } else {
                    $where = array('DM_IHD_Hyp' => 'N');
                }

                $advice = $this->db_fnc->getRowsWhere('advice_lipid', $where);
                $data['trigers'] = $advice;
                $prev_priority = 1000000;

                //$A['conditions_where']	= $where;
                //$A['all_advice']	= $advice;
                //print_r($advice); print_r($userData); die;



                foreach ($advice as $a):
                    $con1 = $this->_goCompare($a['last_total_cholesterol'], $userData['last_total_cholesterol']);
                    $con2 = $this->_goCompare($a['last_HDL'], $userData['last_HDL']);
                    $con3 = $this->_goCompare($a['last_reading_days'], $userData['last_reading_days']);

                    $conditions = array('total' => $con1, 'HDL' => $con2, 'reading_days' => $con3);


                    $allCon = $this->_checkConditions($conditions);
                    $id = $a['condition'][0];
                    $A['conditions'][$id] = $id;



                    $A['conditions'][$id] = array(
                        'conditions' => $conditions,
                        'patient_last_total_cholesterol' => $userData['last_total_cholesterol'],
                        'last_total_cholesterol' => $a['last_total_cholesterol'],
                        'patient_last_HDL' => $userData['last_HDL'],
                        'last_HDL' => $a['last_HDL'],
                        'patient_last_reading_days' => $userData['last_reading_days'],
                        'last_reading_days' => $a['last_reading_days'],
                        'action' => $a['action']
                    );





                    if ($allCon) {
                        $messages[] = $a['action'];
                        $priorities[] = $a['priority'];

                        if ($prev_priority > $a['priority']) {
                            $conD = $a['condition'];
                            $message = $a['action'];
                            $prev_priority = $a['priority'];
                        }
                    }
                endforeach;

                if (isset($conD) && $conD) {
                    $A['conditions']['MATCH'] = $conD;
                    $A['conditions']['ACTION'] = $message;
                    echo '<!-- condition match: ' . $conD . ' priority: ' . $prev_priority . ' -->';
                } else {
                    echo '<div class="error">No condition matched!</div>';
                }


                //print_r($A);
                //die;

                $priority = intval($prev_priority);
                if ($priority < 100) {
                    $this->importance = 'important';
                } elseif ($priority < 200) {
                    $this->importance = 'notice';
                } else {
                    $this->importance = '';
                }
            } else {
                $data['importance'] = 'important';
                $message = 'Please fill out every field in your profile to get appropiate advice.';
            }

            $data['importance'] = $this->importance;
            $data['message'] = (isset($message) && $message) ? $message : '';
            $this->load->view('lipid_advice_view', $data);
        }
    }

    function _makeOposit($bool) {
        if ($bool == 'Y') {
            return 'N';
        } elseif ($bool == 'N') {
            return 'Y';
        } else {
            return $bool;
        }
    }

    function _checkConditions($conditions) {
        if ($conditions['total'] && $conditions['HDL'] && $conditions['reading_days']) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function _goCompare($field, $userData) {
        if ($field != '') {
            //$field = '>=4.5 & <8';
            if (preg_match("/&/i", $field)) {
                $fieldArray = $this->_separateField($field);
                $condFirst = $this->_compareToField($fieldArray[0][0], $userData);
                $condSecond = $this->_compareToField($fieldArray[1][0], $userData);
                if ($condFirst && $condSecond) {
                    $cond = TRUE;
                } else {
                    $cond = FALSE;
                }
            } else {
                $cond = $this->_compareToField($field, $userData);
            }
        } else {
            $cond = TRUE;
        }
        return $cond;
    }

    function _compareToField($field, $userData) {

        $operator = preg_replace('([0-9/./ /])', '', $field);
        $number = preg_replace('([^0-9/.])', '', $field);


        //print_r($operator); echo '|<br />'; print_r($number); echo '<br />';

        if ($operator == '=') {
            return $userData == $number ? TRUE : FALSE;
        } elseif ($operator == '>') {
            return $userData > $number ? TRUE : FALSE;
        } elseif ($operator == '>=') {
            return $userData >= $number ? TRUE : FALSE;
        } elseif ($operator == '<') {
            return $userData < $number ? TRUE : FALSE;
        } elseif ($operator == '<=') {
            return $userData <= $number ? TRUE : FALSE;
        }
    }

    function _separateField($field) {
        $opArray = preg_split('/&/', $field, -1, PREG_SPLIT_OFFSET_CAPTURE);
        //echo 'oparray: '; print_r($opArray);
        return $opArray;
    }

    function edit() {
        $data['advice'] = $this->db_fnc->getAll('lipid_advice');
        $this->load->view('edit_lipid_advice_view', $data);
    }

    function saveAdvice($id) {
        $update = $_POST;
        $where = array('id' => $id);
        $data['update'] = $this->db_fnc->update('advice_lipid', $update, $where);
        redirect('admin/advice/edit');
    }

    function newAdvice() {
        //print_r($_POST); die;
        $insert = $_POST;
        $data['insert'] = $this->db_fnc->insert('advice_lipid', $insert);
        redirect('admin/advice/edit');
    }

}
