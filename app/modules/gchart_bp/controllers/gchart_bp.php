<?php

class Gchart_bp extends MX_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        } else {
            $this->module = 'gchart_bp';
            $this->user_id = $this->tank_auth->get_user_id();
            $this->username = $this->tank_auth->get_username();
            $this->role_id = $this->session->userdata('role');
        }
    }

    function index() {
        $this->load->view('welcome_message');
    }

    function user($id) {
        $datestring = "Y, n ,j";
        $today = date($datestring, time());
        $yearAgo = date($datestring, (time() - (60 * 60 * 24 * 365)));

        $bp_all = array();
        $bp_all = $this->db_fnc->getRows('patient_id', $id, 'bloodpressure', 'sample_date ASC');

        //bp advice need this
        $data['patient_id'] = $id;

        $data['bp'] = $this->createChartArray($bp_all);

        $data['startDate'] = "new Date(" . $yearAgo . ")";

        $data['endDate'] = "new Date(" . $today . ")";

        $this->load->view('bloodpressure_view', $data);
        //redirect('/',$data);
    }

    function createChartArray($initArray) {
        $bp = array();
        $datestring = "Y, n ,j";
        $r = 0;
        $n = 0;
        foreach ($initArray as $sample) {
            $bp[$n][0] = $r;
            $bp[$n][1] = 0;
            $bp[$n][2] = "new Date(" . date(
                            $datestring,
                            strtotime($sample['sample_date'] . "-1 month")
                    ) . ")";
            $n++;

            $bp[$n][0] = $r;
            $bp[$n][1] = 1;
            $bp[$n][2] = (int) $sample['diastolic'];

            $n++;

            $bp[$n][0] = $r;
            $bp[$n][1] = 2;
            $bp[$n][2] = (int) $sample['systolic'];

            $n++;

            $bp[$n][0] = $r;
            $bp[$n][1] = 3;
            $bp[$n][2] = 90;

            $n++;

            $bp[$n][0] = $r;
            $bp[$n][1] = 4;
            $bp[$n][2] = 150;

            $n++;

            $bp[$n][0] = $r;
            $bp[$n][1] = 5;
            $bp[$n][2] = 260;

            $n++;

            $r++;
        }

        return $bp;
    }

    function addSample() {
        $patient_id = $_POST['patient_id'];

        $sample = $this->db_fnc->getMax('sample', 'patient_id', $patient_id, 'bloodpressure');

        //print_r($sample); die;
        $sample++;


        $datestring = "Y-m-d H:i:s";
        $sample_date = date($datestring, time());

        $tablename = 'bloodpressure';
        $fieldarray = array('patient_id' => $patient_id, 'sample' => $sample, 'sample_date' => $sample_date, 'systolic' => $_POST['systolic'], 'diastolic' => $_POST['diastolic']);

        $this->db_fnc->insert($tablename, $fieldarray);



        $params = array('patient_id' => $patient_id);

        $chart = $this->load->library('Mc_mailer', $params);

        redirect($_POST['url']);
    }

}

