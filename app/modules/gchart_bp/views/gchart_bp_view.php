<!--/////////////////// bloodpressure start///////////////////////////-->
<div class="module bloodpressure corner10 clearfix">
    <h2>Blood Pressure</h2>
    <script>
        var url = "<?php echo base_url(); ?>index.php/welcome/viewUser/";
    </script>

    <?php echo modules::run('bp_advice', $patient_id); ?>

    <div class="inputfields">
        <?php echo form_open('bloodpressure/addSample'); ?>

        <input type="hidden" name="patient_id" value="<?php echo $patient_id; ?>">
        <input type="hidden" name="url" value="<?php echo $this->uri->uri_string(); ?>">
        <label>Systolic</label>
        <input type="text" name="systolic" value="">
        <label>Diastolic</label>
        <input type="text" name="diastolic" value="">
        <input class="submitBtn"  type="submit" value="submit">

        <?php echo form_close(); ?>
    </div>


    <?php
        $this->load->library('qgooglevisualapi/ConfigInc', 'QAnnotatedtimelineGoogleGraph');

        $res = '';
        $chart = new QAnnotatedtimelineGoogleGraph();
        $chart
                //->ignoreContainer()
                ->addDrawProperties(
                        array(
                            //"title"=>'Company Performance',
                            "width" => 530,
                            "height" => 160,
                            "thickness" => '0',
                            "scaleType" => 'fixed',
                            "smoothLine" => true,
                            'scaleColumns' => '2',
                            "colors" => "['#0000ff', '#ff0000', '#ffffff', '#ffffff', '#ff0000']",
                            "fill" => '0',
                            "wmode" => 'transparent',
                            "min" => '35',
                            "max" => '200',
                            "displayAnnotations" => "false",
                        //"zoomEndTime"=>$endDate,
                        )
                )
                ->addColumns(
                        array(
                            array('date', 'Date'),
                            array('number', 'Diastolic'),
                            array('number', 'Systolic'),
                            array('number', ''),
                            array('number', ''),
                            array('number', ''),
                            array('string', 'title'),
                            array('string', 'text'),
                        )
                )
                ->setValues($bp);
        $res .= $chart->render(false, true) . "\n";


        echo $res;
    ?>







</div>
<!--/////////////////// bloodpressure end///////////////////////////-->

