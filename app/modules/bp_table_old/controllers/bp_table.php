<?php

class Bp_table extends MX_Controller {

	function  __construct() {
            parent::__construct();

            if (!$this->tank_auth->is_logged_in()) {
			redirect('/auth/login/');
            } else {
                $this->module       = 'bp_table';
                $this->user_id	= $this->tank_auth->get_user_id();
                $this->username	= $this->tank_auth->get_username();
                $this->role_id      = $this->session->userdata('role');
            }
         }
	
	function index()
	{
		$this->load->view('bp_table_view');
	}

	function user($id){
		$datestring = "Y, n ,j";
		$today = date($datestring, time());
		$yearAgo = date($datestring, (time() - (60*60*24*365)) );
		$select = array('sample', 'sample_date', 'systolic', 'diastolic');
		$where = array('patient_id' => $id);
		$data['bp'] = $this->db_fnc->getTheseWhere($select, $where,'bloodpressure','sample_date DESC');
		//$p = $this->db_fnc->getRow('id',$id,'patients');

		//bp advice need this
		$data['patient_id'] = $id;
			

		//echo $data['js_bonds']; die;	
		$this->load->view('bp_table_view2',$data);
		//redirect('/',$data);
			
	}
	
	
	
	



	
	
	function addSample(){
		$patient_id = $_POST['patient_id'];
				
		$sample = $this->db_fnc->getMax('sample','patient_id',$patient_id,'bloodpressure');

		//print_r($sample); die;
		$sample++;
		
		
		$datestring = "Y-m-d H:i:s";
		$sample_date = date($datestring, time());
		
		$tablename = 'bloodpressure';
		$fieldarray = array('patient_id' => $patient_id, 'sample' => $sample, 'sample_date' => $sample_date, 'systolic' => $_POST['systolic'], 'diastolic' => $_POST['diastolic']);
		
		$this->db_fnc->insert($tablename,$fieldarray);


		
		$params = array('patient_id' => $patient_id);
		
		$chart = $this->load->library('Mc_mailer',$params);

		redirect($_POST['url']);	 	 	 	 	 	
	}
	
	
}

