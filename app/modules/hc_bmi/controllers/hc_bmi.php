<?php

class Hc_bmi extends MX_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        } else {
            $this->module = 'hc_bmi';
            $this->user_id = $this->tank_auth->get_user_id();
            $this->username = $this->tank_auth->get_username();
            $this->role_id = $this->session->userdata('role');
            $this->patient_id = $this->session->userdata('patient_id');
            $this->load->model('patient');
            $this->patient = $this->patient->getPatientByID($this->patient_id);
            //date_default_timezone_set('Europe/London');
        }
    }

    function index() {
        
//        echo strtotime('2011-10-29 16:27:55 GMT');
//        echo PHP_EOL;
//        echo $timestmp = strtotime('2011-10-29 16:27:55');
//        echo PHP_EOL;
//        //echo strtotime(date("Y-m-d H:i:s", $timestmp));
//        echo gmt_to_local($timestmp,'UTC');
//        die;

        $data = array();
        $initArray = $this->db_fnc->getRows('patient_id', $this->patient_id, 'body_size', 'sample_date ASC');
        
        $this->load->model('color');
        $color = $this->color->getColors(array('module_id' => 0));
        $graphBonds = array();
        

        if (isset($this->patient) && $this->patient) {
            $graphBonds = array(
                array('color' => $color['bound_lower'], 'from' => 0, 'to' => 18.5),
                array('color' => $color['bound_normal'], 'from' => 18.5, 'to' => 25),
                array('color' => $color['bound_over'], 'from' => 25, 'to' => 500)
            );
        }

        $this->load->library('highcharts');
        $this->highcharts->initialize('chart_template');
        $yAxis = array(
            'plotBands' => $graphBonds,
            'showFirstLabel' => false,
            'gridLineColor' => 'rgba(255,255,255,0.5)',
            'allowDecimals' => false,
            'title' => array(
                'text' => 'BMI',
                'style' => array(
                    'font' => 'normal 12px Verdana, sans-serif',
                    'color' => '#333'
                )
            ),
        );
        $this->highcharts->set_legend(array('enabled' => false));
        $this->highcharts->set_title('BMI');
        $this->highcharts->render_to('BMI');
        $this->highcharts->set_yAxis($yAxis);
        $ch = $this->createChartArray($initArray);

        return $data['charts'] = $this->highcharts->set_serie($ch['BMI'])->render(true);
    }


    function createChartArray($initArray) {
        if (isset($initArray) && isset($this->patient['height']) && $this->patient['height']) {
            $b['BMI'] = array();
            $height = $this->patient['height'] / 100;
           // $datestring = "Y, n, j, H, i, s, u";
            $mkstring = "H, i, s, n, j, Y";
            $this->load->helper('date');
            //date_default_timezone_set('Europe/London');

            foreach ($initArray as $sample) {
                if (isset($sample['weight']) && $sample['weight']) {
                    $b['BMI'][] = "{x:" . gmt_to_local(strtotime($sample['sample_date']), 'UTC').'000'
                            . ",y:" . $this->calculateBMI($sample['weight'], $height)
                            . ",note:'" . (isset($sample['note']) ? $sample['note'] : '')
                            . "'}";
                    //$b['BMI'][] = "{x:Date.UTC(" . date($datestring, strtotime($sample['sample_date'] . "-1 month")) . "),y:" . $this->calculateBMI($sample['weight'], $height) . ",note:'" . $sample['note'] . "'}";
                }
            }
            $js['BMI'] = array('name' => 'BMI', 'data' => '[' . implode(',', $b['BMI']) . ']');
        } else {
            $js['BMI'] = array('name' => 'BMI', 'data' => '[]');
        }

        return $js;
    }

    function calculateBMI($weight, $height) {
        $BMI = ($height && $weight) ? $weight / ($height * $height) : '';
        return $BMI;
    }

}

