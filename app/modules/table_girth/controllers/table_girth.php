<?php

class Table_girth extends MX_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        } else {
            $this->module = 'table_weight';
            $this->user_id = $this->tank_auth->get_user_id();
            $this->username = $this->tank_auth->get_username();
            $this->role_id = $this->session->userdata('role');
            $this->imp = $this->session->userdata('imp');
            $this->patient_id = $this->session->userdata('patient_id');
            $this->load->model('body_size');
        }
    }

   public function index() {
        $data['girth_label'] = 'Waist girth (' . $this->app_functions->getLengthMeasure() . ')';
        $data['maxSample'] = $this->db_fnc->getMax('sample', 'patient_id', $this->patient_id, 'body_size') + 1;
        $this->load->view('table_girth_view', $data);
    }

    public function getTime() {
        $datestring = "Y-m-d H:i:s";
        echo $now = date($datestring, time());
    }

    public function getById($id) {
        if (isset($id))
            echo json_encode($this->body_size->getGirthById($id,$this->app_functions->getLengthMeasure()));
    }

    public function create() {
        if (!empty($_POST)) {
            $this->body_size->createGirth($this->patient_id);
            echo 'New user created successfully!';
        }
    }

    public function read() {
        echo json_encode($this->body_size->getAllGirth($this->patient_id,$this->app_functions->getLengthMeasure()));
    }

    public function update() {
        if (!empty($_POST)) {
            $this->body_size->updateGirth($this->app_functions->getLengthMeasure());
            echo 'Record updated successfully!';
        }
    }

    public function delete($id = null) {
        if (is_null($id)) {
            echo 'ERROR: Id not provided.';
            return;
        }

        $this->body_size->delete($id);
        echo 'Records deleted successfully';
    }

}

//end class
