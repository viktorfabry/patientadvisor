<?php

class Table_cholesterol extends MX_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        } else {
            $this->module = 'table_cholesterol';
            $this->user_id = $this->tank_auth->get_user_id();
            $this->username = $this->tank_auth->get_username();
            $this->role_id = $this->session->userdata('role');
            $this->patient_id = $this->session->userdata('patient_id');
            $this->load->model('cholesterol');
        }
    }

    public function index() {
        $data['maxSample'] = $this->db_fnc->getMax('sample', 'patient_id', $this->patient_id, 'cholesterol') + 1;
        $this->load->view('table_cholesterol_view', $data);
    }

    public function getTime() {
        $datestring = "Y-m-d H:i:s";
        echo $now = date($datestring, time());
    }

    public function getById($id) {
        if (isset($id))
            echo json_encode($this->cholesterol->getById($id));
    }

    public function create() {
        if (!empty($_POST)) {
            echo $this->cholesterol->create($this->patient_id);
            //echo 'New user created successfully!';
        }
    }

    public function read() {
        echo json_encode($this->cholesterol->getAllSamples($this->patient_id));
    }

    public function update() {
        if (!empty($_POST)) {
            $this->cholesterol->update();
            echo 'Record updated successfully!';
        }
    }

    public function delete($id = null) {
        if (is_null($id)) {
            echo 'ERROR: Id not provided.';
            return;
        }

        $this->cholesterol->delete($id);
        echo 'Records deleted successfully';
    }

}

//end class
