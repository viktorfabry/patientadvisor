<?php

class Hc_ltbs extends MX_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        } else {
            $this->module = 'hc_ltbs';
            $this->user_id = $this->tank_auth->get_user_id();
            $this->username = $this->tank_auth->get_username();
            $this->role_id = $this->session->userdata('role');
            $this->patient_id = $this->session->userdata('patient_id');
        }
    }


    function index() {
        $data = array();
        $initArray = $this->db_fnc->getRows('patient_id', $this->patient_id, 'ltbs', 'sample_date ASC');
        $this->load->model('color');
        $color = $this->color->getColors(array('module_id' => 0));

            $graphBonds = array(
                array('color' => $color['bound_normal'], 'from' => 0, 'to' => 6.5),
                array('color' => $color['bound_over'], 'from' => 6.5, 'to' => 15)
            );

        $this->load->library('highcharts');
        $this->highcharts->initialize('chart_template');
        $yAxis = array(
            'plotBands' => $graphBonds,
            'showFirstLabel' => false,
            'gridLineColor' => 'rgba(255,255,255,0.5)',
            'allowDecimals' => false,
            'title' => array(
                'text' => 'HbA1c',
                'style' => array(
                    'font' => 'normal 12px Verdana, sans-serif',
                    'color' => '#333'
                )
            ),
        );
        $this->highcharts->set_legend(array('enabled' => false));
        $this->highcharts->set_title('Longer Term Blood Sugar (HbA1c)');
        $this->highcharts->render_to('ltbs');
        $this->highcharts->set_yAxis($yAxis);
        $ch = $this->createChartArray($initArray);

        return $this->highcharts->set_serie($ch['ltbs'])->render(true);
    }

    function createChartArray($initArray) {
        if ($initArray) {
            $b['ltbs'] = array();
            $datestring = "Y, n ,j";
            foreach ($initArray as $sample) {
                if (isset($sample['ltbs']) && $sample['ltbs']) {

                    $b['ltbs'][] = "{x:" . gmt_to_local(strtotime($sample['sample_date']), 'UTC').'000'
                            . ",y:" . $sample['ltbs']
                            . ",note:'" . (isset($sample['note']) ? $sample['note'] : '')
                            . "'}";
                }
            }
            $js['ltbs'] = array('name' => 'ltbs', 'data' => '[' . implode(',', $b['ltbs']) . ']');
        } else {
            $js['ltbs'] = array('name' => 'ltbs', 'data' => '[]');
        }

        return $js;
    }


}

