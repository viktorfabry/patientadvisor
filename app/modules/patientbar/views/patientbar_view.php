<div id="topBar" class="container ds5">
    <div class="ins clearfix">
    <div class="row_wrapper">
        <div class="row">
            <?php
            if ($is_logged_in) {
                echo '<div class="wht right curves5 ds5 last">';
                echo anchor('profile', $full_name);
                echo anchor('auth/logout', '<span>log out</span>', 'class="logout"');
                echo '</div>';
            }
            ?>
            <?php echo anchor('patientbar/setfontsize/0', 'Enlarge the font size <span></span>', 'class="enlargeFont gridMarginRight right"'); ?>
            <?php  echo ($is_logged_in) ? $menu : ''; ?>
            <span class="errorMsg jsError">Your javaScript is disabled, please enable it to use the site.</span>

        </div>
    </div>
    </div>
</div>