<?php

class Patientbar extends MX_Controller {

    function __construct() {
        parent::__construct();

        $this->module = 'patientbar';
        $this->user_id = $this->tank_auth->get_user_id();
        $this->admin = $this->load->model('tank_auth/admins');
        $this->username = $this->tank_auth->get_username();
        $this->role_id = $this->session->userdata('role');
        $this->full_name = $this->session->userdata('full_name');
        $this->patient_id = $this->session->userdata('patient_id');
        $this->fsize = $this->session->userdata('fsize');
        $this->load->model('patient');
        $this->load->model('xlink');
        $this->practice_id = null;
        $this->gpCom_id = null;
    }

    function index() {

        $data['is_logged_in'] = $this->tank_auth->is_logged_in();
        $data['patient_id'] = $this->patient_id;
        $data['full_name'] = $this->full_name;
        $url = $this->input->post('url');

        $links = $this->xlink->getLinks(1, $this->practice_id, $this->gpCom_id);

        $dr_menu = $this->load->library('multilevelmenu', $links);
        $dr_menu->setFlink('Useful Links');

        $data['menu'] = $dr_menu->getMenu();

        $this->load->view('patientbar_view', $data);
    }

    function setfontsize($fsize){
        $this->load->library('app_functions');
        (int) $fsize;
        if ($fsize == 0){
            if($this->fsize >= 2){
                $fNewSize = '';
            } else {
                $fNewSize = $this->fsize + 1;
            }
        } else {
          $fNewSize = $fsize;
        }
        $this->app_functions->setFontSize($fNewSize);
        if ($fsize == 0) redirect('/');
    }

}

