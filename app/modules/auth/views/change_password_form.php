<?php
$header = modules::run('header');
$patientBar = modules::run('patientbar');
$msgBlock = modules::run('msg', $msg = '');
$head = modules::run('head');
$footer = modules::run('footer');
?>

<?php
$old_password = array(
    'name' => 'old_password',
    'id' => 'old_password',
    'value' => set_value('old_password'),
    'size' => 30,
);
$new_password = array(
    'name' => 'new_password',
    'id' => 'new_password',
    'maxlength' => $this->config->item('password_max_length', 'tank_auth'),
    'size' => 30,
);
$confirm_new_password = array(
    'name' => 'confirm_new_password',
    'id' => 'confirm_new_password',
    'maxlength' => $this->config->item('password_max_length', 'tank_auth'),
    'size' => 30,
);
?>

<?php echo $head; ?>
<?php echo $patientBar; ?>
<?php echo $header; ?>
<div id="main" class="container">
    <div class="row_wrapper curves10">
        <div class="row">
            <div class="fourcol">

                <?php echo form_open($this->uri->uri_string()); ?>

                <?php echo form_label('Old Password', $old_password['id']); ?>
                <?php echo form_password($old_password); ?>
                <?php echo form_error($old_password['name']); ?><?php echo isset($errors[$old_password['name']]) ? app_msg($errors[$old_password['name']],'error') : ''; ?>

                <?php echo form_label('New Password', $new_password['id']); ?>
                <?php echo form_password($new_password); ?>
                <?php echo form_error($new_password['name']); ?><?php echo isset($errors[$new_password['name']]) ? $errors[$new_password['name']] : ''; ?>

                <?php echo form_label('Confirm New Password', $confirm_new_password['id']); ?>
                <?php echo form_password($confirm_new_password); ?>
                <?php echo form_error($confirm_new_password['name']); ?><?php echo isset($errors[$confirm_new_password['name']]) ? $errors[$confirm_new_password['name']] : ''; ?>

                <?php echo form_submit('change', 'Change Password', 'class="btn orange big right mT1"'); ?>
                <?php echo form_close(); ?>

            </div>
        </div> <!-- row -->
    </div> <!-- row wrapper -->
</div> <!-- main -->


<?php echo $footer; ?>
<?php $this->load->view('foot'); ?>