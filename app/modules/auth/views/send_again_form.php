<?php
$header = modules::run('header');
$patientBar = modules::run('patientbar');
$msgBlock = modules::run('msg');
$head = modules::run('head');
$footer = modules::run('footer');
?>

<?php echo $head; ?>
<?php echo $patientBar; ?>
<?php echo $header; ?>

<div id="main" class="container loginForm">
    <div class="row_wrapper curves10">
        <div class="row">
            <div class="fourcol">
                <?php
                $email = array(
                    'name' => 'email',
                    'id' => 'email',
                    'value' => set_value('email'),
                    'maxlength' => 80,
                    'size' => 30,
                );
                ?>
                <?php echo form_open($this->uri->uri_string()); ?>
                <table>
                    <tr>
                        <td><?php echo form_label('Email Address', $email['id']); ?></td>
                        <td><?php echo form_input($email); ?></td>
                        <td style="color: red;">
                            <?php echo form_error($email['name']); ?>
                            <?php echo isset($errors[$email['name']]) ? $errors[$email['name']] : ''; ?>
                        </td>
                    </tr>
                </table>
                <?php echo form_submit('send', 'Send'); ?>
                <?php echo form_close(); ?>
            </div>
        </div> <!-- row -->
    </div> <!-- row wrapper -->
</div> <!-- main -->
<?php echo $footer; ?>
<?php $this->load->view('foot'); ?>