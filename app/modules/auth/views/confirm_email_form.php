<?php   echo modules::run('head'); ?>
<?php  echo modules::run('header'); ?>
<div id="main">

<?php
$login = array(
	'name'	=> 'login',
	'id'	=> 'login',
	'value' => set_value('login'),
	'maxlength'	=> 80,
	'size'	=> 30,
);
if ($this->config->item('use_username', 'tank_auth')) {
	$login_label = 'Email or username';
} else {
	$login_label = 'Email';
}
?>
<?php echo form_open($this->uri->uri_string()); ?>
<?php echo form_label($login_label, $login['id']); ?>
<?php echo form_input($login); ?>
<?php echo form_error($login['name']); ?><?php echo isset($errors[$login['name']])?$errors[$login['name']]:''; ?>
    
<?php echo form_submit('reset', 'Please confirm your email', 'class="clear left btn orange big margin15"'); ?>
<?php echo form_close(); ?>

</div> <!-- main -->
<?php echo modules::run('footer'); ?>