<?php
$header = modules::run('header');
$patientBar = modules::run('patientbar');
$msgBlock = modules::run('msg');
$head = modules::run('head');
$footer = modules::run('footer');
?>

<?php echo $head; ?>
<?php echo $patientBar; ?>
<?php echo $header; ?>


<div id="main">
<div class="row_wrapper curves10">
        <div class="row">
            <div class="fourcol">
                <?php echo $msgBlock; ?>
<?php
$login = array(
	'name'	=> 'login',
	'id'	=> 'login',
	'value' => set_value('login'),
	'maxlength'	=> 80,
	'size'	=> 30,
);
if ($this->config->item('use_username', 'tank_auth')) {
	$login_label = 'Email or username';
} else {
	$login_label = 'Email';
}
?>
<?php echo form_open($this->uri->uri_string()); ?>
<?php echo form_label($login_label, $login['id']); ?>
<?php echo form_input($login); ?>
<?php echo form_error($login['name']); ?><?php echo isset($errors[$login['name']])?$errors[$login['name']]:''; ?>
    
<?php echo form_submit('reset', 'Get a new password','class="right btn orange big  marginT15"'); ?>
<?php echo form_close(); ?>

</div>
        </div> <!-- row -->
    </div> <!-- row wrapper -->
</div> <!-- main -->
<?php echo $footer; ?>
<?php $this->load->view('foot'); ?>