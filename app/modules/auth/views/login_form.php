<?php
$header = modules::run('header');
$patientBar = modules::run('patientbar');
$msgBlock = modules::run('msg');
$head = modules::run('head');
$footer = modules::run('footer');
?>

<?php echo $head; ?>
<?php echo $patientBar; ?>
<?php echo $header; ?>

<div id="main" class="container loginForm">
    <div class="row_wrapper curves10">
        <div class="row">
            <div class="fourcol">
                <?php echo $msgBlock; ?>

                <?php
                $login = array('name' => 'login', 'id' => 'login', 'value' => set_value('login'), 'maxlength' => 80, 'size' => 30,);

                if ($login_by_username AND $login_by_email) {
                    $login_label = 'Email or username';
                } else if ($login_by_username) {
                    $login_label = 'Login';
                } else {
                    $login_label = 'Email';
                }
                $password = array('name' => 'password', 'id' => 'password', 'size' => 30,);
                $remember = array('name' => 'remember', 'id' => 'remember', 'value' => 1, 'checked' => set_value('remember'), 'style' => 'margin:0;padding:0',);
                $captcha = array('name' => 'captcha', 'id' => 'captcha', 'maxlength' => 8,);
                ?>
                <?php echo form_open(base_url() . 'auth/login/'); ?>
                <?php // echo anchor('/auth/confirm_email/', 'I had an account previously', 'class="left btn orange big margin15"'); ?>

                <?php echo form_label($login_label, $login['id']); ?>
                <?php echo form_input($login); ?>
                <?php echo form_error($login['name']); ?>
                <?php echo isset($errors[$login['name']])? app_msg($errors[$login['name']],'error right'):''; ?>

                <?php echo form_label('Password', $password['id']); ?>
                <?php echo form_password($password); ?>

                <?php echo form_error($password['name']); ?>
                <?php echo isset($errors[$password['name']])? app_msg($errors[$password['name']],'error right'):''; ?>

                <?php
                if ($show_captcha) {
                    if ($use_recaptcha) {
                ?>
                        <div id="recaptcha_image"></div>
                        <a href="javascript:Recaptcha.reload()">Get another CAPTCHA</a>
                        <div class="recaptcha_only_if_image"><a href="javascript:Recaptcha.switch_type('audio')">Get an audio CAPTCHA</a></div>
                        <div class="recaptcha_only_if_audio"><a href="javascript:Recaptcha.switch_type('image')">Get an image CAPTCHA</a></div>
                        <div class="recaptcha_only_if_image">Enter the words above</div>
                        <div class="recaptcha_only_if_audio">Enter the numbers you hear</div>
                        <input type="text" id="recaptcha_response_field" name="recaptcha_response_field" />
                <?php echo form_error('recaptcha_response_field'); ?>
                <?php echo $recaptcha_html; ?>
                <?php } else {
 ?>
                        <p>Enter the code exactly as it appears:</p>
<?php echo $captcha_html; ?>

                <?php echo form_label('Confirmation Code', $captcha['id']); ?>
                <?php echo form_input($captcha); ?>
                <?php }
                } ?>
                <br />
                <div class="clear left marginT15 marginB15">
                <?php echo form_checkbox(array('name' => $remember, 'class' => 'clear left')); ?>
                <?php echo form_label('Remember me', $remember['id'], array('class' => 'remember left noclear')); ?>
<?php echo anchor('/auth/forgot_password/', 'Forgot password', 'class="left marginL15"'); ?>
                    </div>
                <br />
                <?php if ($this->config->item('allow_registration', 'tank_auth'))
                    echo anchor('/register/', 'Register', 'class="clear left btn big "'); ?>

                <?php echo form_submit('submit', 'Login', 'class="left btn orange big  marginL15"'); ?>
                
<?php echo form_close(); ?>
            </div>
        </div> <!-- row -->
    </div> <!-- row wrapper -->
</div> <!-- main -->
<?php echo $footer; ?>
<?php $this->load->view('foot'); ?>