<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <meta http-equiv="Content-Language" content="EN"/>
        <link rel="SHORTCUT ICON" href="/favicon.ico" />
        <title></title>

        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta name="robots" content="index, follow"/>
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/style_admin.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>js/DataTables-1.8.2/media/css/demo_table_jui.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>js/jquery-ui/css/Aristo/Aristo.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/fileuploader.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>js/fancybox/jquery.fancybox-1.3.1.css" type="text/css" media="screen" />
        
        

        <link rel="alternate" type="application/rss+xml" title="RSS Feed" href="/articles/feed" />


    </head>

    <body>