<html>
<head>
<title>Blood Pressure History</title>

<base href="<?php echo base_url() ?>" />

<!--link type="text/css" rel="stylesheet" href="css/demo_table.css" /-->
<link type="text/css" rel="stylesheet" href="css/smoothness/jquery-ui-1.8.2.custom.css" />
<link type="text/css" rel="stylesheet" href="css/styles.css" />
<link type="text/css" rel="stylesheet" href="js/TableTools/media/css/TableTools.css" />

<script type="text/javascript" charset="utf-8">
			//$(document).ready( function () {
			//	/* You might need to set the sSwfPath! Something like:
			//	 *   TableToolsInit.sSwfPath = "/media/swf/ZeroClipboard.swf";
			//	 */
			//	$('#example').dataTable( {
			//		"sDom": 'T<"clear">lfrtip'
			//	} );
			//} );
</script>


</head>
<body>

<div id="ajaxLoadAni" >
    <img src="img/ajax-loader.gif" alt="Ajax Loading Animation" />
    <span>Loading...</span>
</div>

<div id="tabs" class="hide">
    
    <ul >
        <li><a href="#read">Read</a></li>
        <li><a class="createTab" href="#create">Create</a></li>
    </ul>
    
    <div id="read" >
        <table id="records">
            <thead>
                <tr>
                    <th>sample</th>
                    <th>sample date</th>
                    <th><?php echo $weight_label; ?></th>
                    <th>note</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
    <div id="create" >
        <form action="" method="post">
           <p>
               <label for="cSample_date">sample_date:</label>
               <input type="text" id="cSample_date" name="cSample_date" class="sampleDate" value="" />
            </p>
            
            <p>
               <label for="cWeight"><?php echo $weight_label; ?>:</label>
               <input type="text" id="cWeight" name="cWeight" />
            </p>
            <p>
               <label for="cNote">note:</label>
               <input type="text" id="cNote" name="cNote" />
            </p>
           <p>
               <label>&nbsp;</label>
               <input type="hidden" id="cSample" name="cSample" value="<?php echo $maxSample; ?>" />
               <input type="submit" name="createSubmit" value="Submit" />
           </p>
        </form>
    </div>

</div> <!-- end tabs -->

<!-- update form in dialog box -->
<div id="updateDialog" class="hide" title="Update">
    <div>
        <form action="" method="post">
            <p>
               <label for="sample_date">sample_date:</label>
               <input type="text" id="sample_date" name="sample_date" />
            </p>
            
            <p>
               <label for="weight"><?php echo $weight_label; ?>:</label>
               <input type="text" id="weight" name="weight" />
            </p>

            <p>
               <label for="note">note:</label>
               <input type="text" id="note" name="note" />
            </p>
            
            <input type="hidden" id="id" name="id" />
        </form>
    </div>
</div>

<!-- delete confirmation dialog box -->
<div id="delConfDialog" class="hide" title="Confirm">
    <p>Are you sure you want to delete this record?</p>
</div>


<!-- message dialog box -->
<div id="msgDialog"><p></p></div>


<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.2.min.js"></script>

<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/jquery-templ.js"></script>
<script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf-8" src="js/TableTools/media/js/TableTools.min.js"></script>
<script type="text/javascript" charset="utf-8" src="js/TableTools/media/ZeroClipboard/ZeroClipboard.js"></script>


<script type="text/template" id="rowTemplate">
    <tr id="${id}">
        <td>${sample}</td>
        <td>${sample_date}</td>
        <td>${weight}</td>
        <td>${note}</td>
        <td><a class="updateBtn" href="${updateLink}">Update</a> | <a class="deleteBtn" href="${deleteLink}">Delete</a></td>
    </tr>
</script>
<script type="text/javascript">
    var fieldArray      = ["id",'sample',"sample_date","weight","note"],
    readUrl   = 'table_weight/read/',
    updateUrl = 'table_weight/update',
    createURL = 'table_weight/create/',
    getByIdURL = 'table_weight/getById/',
    delUrl    = 'table_weight/delete',
    getTimeUrl    = 'table_weight/getTime/',
    rowTemplate = '#rowTemplate',
    readTemplate = '#read',
    sort = 'desc',
    delHref,
    updateHref,
    updateId;
</script>
<script type="text/javascript" src="js/table.js"></script>

</body>
</html>