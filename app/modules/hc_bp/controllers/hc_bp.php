<?php

class Hc_bp extends MX_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        } else {
            $this->module = 'hc_bp';
            $this->user_id = $this->tank_auth->get_user_id();
            $this->username = $this->tank_auth->get_username();
            $this->role_id = $this->session->userdata('role');
            $this->patient_id = $this->session->userdata('patient_id');
            $this->load->model('patient');
            $this->load->model('color');
            $this->load->model('chart_data');
        }
    }

    function index() {

        $bp_all = $this->db_fnc->getRows('patient_id', $this->patient_id, 'bloodpressure', 'sample_date ASC');
        $p = $this->patient->getPatientByID($this->patient_id);
        $color = $this->color->getColors(array('module_id'=>0));
        $chart = $this->chart_data->getChartData('bp');
        $graphBonds = array();
        
        if (isset($p['diabetes']) && $p['diabetes']) {
            $bonds[0]['to'] = ($p['diabetes'] == 'Y') ? 80 : 90;
            $bonds[1]['from'] = ($p['diabetes'] == 'Y') ? 80 : 90;
            $bonds[1]['to'] = ($p['diabetes'] == 'Y') ? 140 : 150;
            $bonds[2]['from'] = ($p['diabetes'] == 'Y') ? 140 : 150;
            $graphBonds = array(
                array('color' => $color['bound_lower'],'from' => 0, 'to' => $bonds[0]['to']),
                array('color' => $color['bound_normal'],'from' => $bonds[1]['from'], 'to' => $bonds[1]['to']),
                array('color' => $color['bound_over'],'from' => $bonds[2]['from'], 'to' => 260),
                );
        }

        $this->load->library('highcharts');
        $this->highcharts->initialize('chart_template');
        $yAxis = array(
            'plotBands' => $graphBonds,
            'showFirstLabel' => false,
            'gridLineColor' => 'rgba(255,255,255,0.5)',
            'allowDecimals' => false,
            'title' => array(
                'text' => null
            )
        );
        $this->highcharts->set_title('Blood pressure');
        $this->highcharts->set_yAxis($yAxis);
        $bp = $this->createChartArray($bp_all);

       // echo $bp['systolic']; die;

        return $data['charts'] = $this->highcharts->set_serie($bp['systolic'])->set_serie($bp['diastolic'])->render(true);
        //$this->load->view('hc_bp_view', $data);
    }

    function createChartArray($initArray) {
        if ($initArray) {
            $datestring = "Y, n ,j";
            $b['systolic'] = array();
            $b['diastolic'] = array();
            foreach ($initArray as $sample) {
                if (isset($sample['systolic']) && $sample['systolic']) {
                    $b['systolic'][] = "{x:" . gmt_to_local(strtotime($sample['sample_date']), 'UTC').'000'
                            . ",y:". $sample['systolic']
                            . ",note:'". (isset($sample['note']) ? $sample['note'] : '')
                            ."'}";
                }
//                array(
//                        "Date.UTC(" . date($datestring, strtotime($sample['sample_date'] . "-1 month")) . ")", $sample['systolic'], " note:'hello'"
//                    );
                if (isset($sample['diastolic']) && $sample['diastolic']) {
                    $b['diastolic'][] = "{x:" . gmt_to_local(strtotime($sample['sample_date']), 'UTC').'000'
                            . ",y:". $sample['diastolic']
                            . ",note:'". (isset($sample['note']) ? $sample['note'] : '')
                            ."'}";
                }
            }

            $bp['systolic'] = array('name' => 'systolic', 'data' => '['.implode(',', $b['systolic']).']');
            $bp['diastolic'] = array('name' => 'diastolic', 'data' =>'['.implode(',', $b['diastolic']).']');
        } else {
            $bp = array('systolic_jsArray' => '[]', 'diastolic_jsArray' => '[]');
        }

        return $bp;
    }

    function getSystolic(){
        $data['bp'] = $this->createChartArray($bp_all);
    }

}

