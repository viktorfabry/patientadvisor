<script type="text/javascript">

    var url = "<?php echo base_url(); ?>index.php/welcome/viewUser/";

		
    var chart;
    $(document).ready(function() {
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'container',
                type: 'spline',
                zoomType: 'x',
                backgroundColor: '#D8D8D2',
                borderRadius: 10,
                margin: [50, 20, 40, 60]
            },
            title: {
                text: ''
            },
            subtitle: {
                text: 'Click and drag in the plot area to zoom in',
                align: 'left',
                zIndex: 10000,
                verticalAlign: 'bottom',
                backgroundColor: '#333',
                x: 50,
                y: 7
            },
            xAxis: {
                type: 'datetime',
                maxZoom: 7 * 24 * 3600000, // fourteen days
                title: {
                    text: null
                },
                labels: {
                    style: {
                        font: 'normal 12px Verdana, sans-serif'
                    }
                },
                showFirstLabel: false
            },
            yAxis: {
                title: {
                    text: 'BP mm Hg',
                    color: '#666666'
                },
                labels: {
                    align: 'right',
                    style: {
                        font: 'normal 12px Verdana, sans-serif'
                    }
                },
                //min: 35,
                //max: 260,
                tickInterval: 20,
                gridLineColor: '#fff',
                //minorTickInterval: 5,
                startOnTick: false,
                endOnTick: false,
                showFirstLabel: false,
                plotBands: [<?php echo $js_bonds ?>]
            },
            tooltip: {
                formatter: function() {
                    var note = (this.point.note) ? '<span style="color: <?php echo $colour['line1'] ?>">"' + this.point.note + '"</span><br> ' : '';

                    return '<strong>'+ this.series.name + ': '+
                        Highcharts.numberFormat(this.y, 0) + "</strong><br> "+
			note +
                        Highcharts.dateFormat('%B %e %Y, %A', this.x);
                },
                style: {
                    color: '#333333',
                    fontSize: '12px',
                    padding: '10px 15px'
                }
            },
            legend: {
                align: 'right',
                verticalAlign: 'top',
                backgroundColor: '#fff',
                x: -80,
                y: 10
            },
            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: [0, 0, 0, 300],
                        stops: [
                            [0, '#ffffff'],
                            [1, 'rgba(2,0,0,0)']
                        ]
                    },
                    lineWidth: 1,
                    marker: {
                        enabled: false,
                        states: {
                            hover: {
                                enabled: true,
                                radius: 5
                            }
                        }
                    },
                    shadow: false,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    }
                }
            },
            series: [
                {name: 'Systolic', color: '<?php echo $colour['line1'] ?>', data: <?php echo $bp['systolic_jsArray'] ?> },
                {name: 'Diastolic', color: '<?php echo $colour['line2'] ?>', data: <?php echo $bp['diastolic_jsArray'] ?> }
            ],
            navigation: {
                buttonOptions: {
                    height: 26,
                    width: 26,
                    symbolSize: 12,
                    symbolX: 13,
                    symbolY: 13
                }
            },
            exporting: {
                buttons: {
                    exportButton: {
                        x: -20, y: 20
                    },
                    printButton: {
                        x: -50, y: 20
                    }
                }
            }
        });});
</script>
