<?php echo modules::run('adminhead'); ?>
<?php echo modules::run('adminheader'); ?>
<?php echo modules::run('msg', $msg = ''); ?>
<div id="main" class="clearfix">

    <?php
    if ($use_username) {
        $username = array(
            'name' => 'username',
            'id' => 'username',
            'value' => set_value('username'),
            'maxlength' => $this->config->item('username_max_length', 'tank_auth'),
            'size' => 30,
        );
    }
    $email = array(
        'name' => 'email',
        'id' => 'email',
        'value' => set_value('email'),
        'maxlength' => 80,
        'size' => 30,
    );
    $password = array(
        'name' => 'password',
        'id' => 'password',
        'value' => set_value('password'),
        'maxlength' => $this->config->item('password_max_length', 'tank_auth'),
        'size' => 30,
    );
    $confirm_password = array(
        'name' => 'confirm_password',
        'id' => 'confirm_password',
        'value' => set_value('confirm_password'),
        'maxlength' => $this->config->item('password_max_length', 'tank_auth'),
        'size' => 30,
    );
    $captcha = array(
        'name' => 'captcha',
        'id' => 'captcha',
        'maxlength' => 8,
    );
    ?>
    <?php echo form_open($this->uri->uri_string(), 'class="left clearfix"'); ?>
    <?php if ($use_username) {
 ?>
    <?php echo form_label('Username', $username['id']); ?>
    <?php echo form_input($username); ?>
    <?php echo form_error($username['name']); ?><?php echo isset($errors[$username['name']]) ? $errors[$username['name']] : ''; ?>
    <?php } ?>
    <?php echo form_label('Email Address', $email['id']); ?>
    <?php echo form_input($email); ?>
    <?php echo form_error($email['name']); ?><?php echo isset($errors[$email['name']]) ? $errors[$email['name']] : ''; ?>

    <?php echo form_label('GP Commissioning Name', 'gpcom'); ?>
    <?php echo form_dropdown('gpcom', $gpcoms, $gpcom_id, 'id="gpcom"'); ?>

    <?php echo form_label('Password', $password['id']); ?>
    <?php echo form_password($password); ?>
    <?php echo form_error($password['name']); ?>

    <?php echo form_label('Confirm Password', $confirm_password['id']); ?>
    <?php echo form_password($confirm_password); ?>
    <?php echo form_error($confirm_password['name']); ?>

    <?php echo form_submit('register', 'Register', 'class="clear right btn orange big"'); ?>
    <?php echo form_close(); ?>

</div> <!-- main -->

<script src="<?php echo base_url(); ?>js/dynamicSelect.jQuery.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function(){

        var options  = {
            url: '<?php echo base_url(); ?>admin/practices/getpracticenames/',
            newSelectName: 'practices',
            newSelectLabel: 'Practice Names'
        };

        $("#gpcom").dynamicSelect(options);
    });
</script>

<?php echo modules::run('footer'); ?>