<html>
    <head>
        <title>Edit GP</title>


        <link rel="stylesheet" href="<?php echo base_url(); ?>css/style_admin.css" type="text/css" />
        <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.4.2.js"></script>
        <script type="text/javascript">
            $(function(){
                $(".fancyClose").click(function(){
                    parent.$.fancybox.close();
                })
            })
        </script>
    </head>
    <body>
        <div class="smallPopUp">
            <h1>Edit GP</h1>
            <p class="message"><?php echo $this->session->flashdata('message'); ?></p>

            <?php
            if ($use_username) {
                $username = array(
                    'name' => 'username',
                    'id' => 'username',
                    'value' => set_value('username'),
                    'maxlength' => $this->config->item('username_max_length', 'tank_auth'),
                    'size' => 30,
                );
            }
            $email = array(
                'name' => 'email',
                'id' => 'email',
                'value' => set_value('email'),
                'maxlength' => 80,
                'size' => 30,
            );
            $password = array(
                'name' => 'password',
                'id' => 'password',
                'value' => set_value('password'),
                'maxlength' => $this->config->item('password_max_length', 'tank_auth'),
                'size' => 30,
            );
            $confirm_password = array(
                'name' => 'confirm_password',
                'id' => 'confirm_password',
                'value' => set_value('confirm_password'),
                'maxlength' => $this->config->item('password_max_length', 'tank_auth'),
                'size' => 30,
            );
            $captcha = array(
                'name' => 'captcha',
                'id' => 'captcha',
                'maxlength' => 8,
            );
            ?>
            <?php
            $form = '';
            $form .= form_open($this->uri->uri_string(), 'class="left clearfix"');
            if ($use_username) {
                $form .= form_label('Username', $username['id']);
                $form .= form_input($username);
                $form .= form_error($username['name']);
                $form .= isset($errors[$username['name']]) ? $errors[$username['name']] : '';
            }
            $form .= form_label('Email Address', $email['id']);
            $form .= form_input($email);
            $form .= form_error($email['name']);
            $form .= isset($errors[$email['name']]) ? $errors[$email['name']] : '';

            $form .= form_label('GP Commissioning Name', 'gpcom');
            $form .= form_dropdown('gpcom', $gpcoms, $gpcom_id, 'id="gpcom"');

            $form .= form_label('Password', $password['id']);
            $form .= form_password($password);
            $form .= form_error($password['name']);

            $form .= form_label('Confirm Password', $confirm_password['id']);
            $form .= form_password($confirm_password);
            $form .= form_error($confirm_password['name']);

            $form .= form_submit('register', 'Register', 'class="clear right btn orange big"');
            $form .= form_close();

            echo $form;
            ?>
        </div> <!-- main -->

        <script src="<?php echo base_url(); ?>js/dynamicSelect.jQuery.js" type="text/javascript"></script>

        <script type="text/javascript">
            $(function(){

                var options  = {
                    url: '<?php echo base_url(); ?>admin/practices/getpracticenames/',
                    newSelectName: 'practices',
                    newSelectLabel: 'Practice Names'
                };

                $("#gpcom").dynamicSelect(options);
            });
        </script>

    </body>
</html>