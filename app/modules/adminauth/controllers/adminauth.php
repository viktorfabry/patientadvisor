<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Adminauth extends MX_Controller {

    function __construct() {
        parent::__construct();

        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('security');
        $this->load->library('tank_auth_admin');
        $this->lang->load('tank_auth_admin');

        $this->user_id = $this->tank_auth_admin->get_user_id();
        $this->username = $this->tank_auth_admin->get_username();
        $this->role_id = $this->session->userdata('role');
        $this->group_id = $this->session->userdata('gpcom_id');
        $this->load->model('tank_auth/admins','admin');
        $this->load->model('localauth');
    }

    function index() {
        redirect('adminauth/login/');
    }

    /**
     * Login user on the site
     *
     * @return void
     */
    function login($fullpage = true) {
        if ($this->tank_auth_admin->is_logged_in()) {  // logged in
            redirect('admin/main');
        } elseif ($this->tank_auth_admin->is_logged_in(FALSE)) { // logged in, not activated
            
            redirect('/adminauth/send_again/');
        } else {
            $data['login_by_username'] = ($this->config->item('login_by_username', 'tank_auth_admin') AND
                    $this->config->item('use_username', 'tank_auth_admin'));
            $data['login_by_email'] = $this->config->item('login_by_email', 'tank_auth_admin');

            $this->form_validation->set_rules('login', 'Login', 'trim|required|xss_clean');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
            $this->form_validation->set_rules('remember', 'Remember me', 'integer');

            // Get login for counting attempts to login
            if ($this->config->item('login_count_attempts', 'tank_auth_admin') AND
                    ($login = $this->input->post('login'))) {
                $login = $this->security->xss_clean($login);
            } else {
                $login = '';
            }

            $data['use_recaptcha'] = $this->config->item('use_recaptcha', 'tank_auth_admin');
            if ($this->tank_auth_admin->is_max_login_attempts_exceeded($login)) {
                if ($data['use_recaptcha'])
                    $this->form_validation->set_rules('recaptcha_response_field', 'Confirmation Code', 'trim|xss_clean|required|callback__check_recaptcha');
                else
                    $this->form_validation->set_rules('captcha', 'Confirmation Code', 'trim|xss_clean|required|callback__check_captcha');
            }
            $data['errors'] = array();

            if ($this->form_validation->run()) {        // validation ok
                if ($this->tank_auth_admin->login(
                                $this->form_validation->set_value('login'),
                                $this->form_validation->set_value('password'),
                                $this->form_validation->set_value('remember'),
                                $data['login_by_username'],
                                $data['login_by_email'])
                ) {        // success
                    redirect('/admin/main/');
                } else {
                    $errors = $this->tank_auth_admin->get_error_message();
                    if (isset($errors['banned'])) {        // banned user
                        $this->_show_message($this->lang->line('auth_message_banned') . ' ' . $errors['banned']);
                    } elseif (isset($errors['not_activated'])) {    // not activated user
                        redirect('/adminauth/send_again/');
                    } else {             // fail
                        foreach ($errors as $k => $v)
                            $data['errors'][$k] = $this->lang->line($v);
                    }
                }
            }
            $data['show_captcha'] = FALSE;
            if ($this->tank_auth_admin->is_max_login_attempts_exceeded($login)) {
                $data['show_captcha'] = TRUE;
                if ($data['use_recaptcha']) {
                    $data['recaptcha_html'] = $this->_create_recaptcha();
                } else {
                    $data['captcha_html'] = $this->_create_captcha();
                }
            }
            if ($fullpage) {
                $this->load->view('login_form', $data);
            } else {
                $this->load->view('login_block', $data);
            }
        }
    }

    /**
     * Logout user
     *
     * @return void
     */
    function logout() {
        $this->tank_auth_admin->logout();

        $this->_show_message($this->lang->line('auth_message_logged_out'));
    }

    function invite_user() {
        // $file_id = (int) $this->input->post('file_id');
        //echo $file_id; die;
        $data = array();
        $errors = array();
        $merror = '';

        if (!$this->tank_auth_admin->is_logged_in()) {         // Not logged in
            redirect('');
        } elseif ($this->tank_auth_admin->is_logged_in(FALSE)) {      // logged in, not activated
            redirect('/adminauth/send_again/');
        } elseif ($this->input->post('file_id')) {


            $file_id = (int) $this->input->post('file_id');

            $file = $this->db_fnc->getRow('file_id', $file_id, 'files');


            $email_activation = TRUE;
            $filePath = './Documents/' . $file['fileName'];
            // echo $filePath; die;

            if (file_exists($filePath)) {

                $this->load->library('csvreader');
                $patients = $this->csvreader->parse_file($filePath);

                foreach ($patients as $p) {

                    $username = '';
                    $email = $p['email'];
                    $this->load->helper('password');
                    $password = get_random_password();


                    $EMIS = $p['EMIS'];
                    $this->load->model('doctor');
                    $dr = $this->doctor->initByEMIS($EMIS);
                    if ($dr) {
                        $doctor_id = $dr->id;
                    } else {
                        $this->session->set_flashdata('errors', 'The doctor has not been set for the patient.');
                        redirect('admin/patients/invite_patients/');
                    }

                    //echo $email.' '.$password; die;



                    if (!is_null($data = $this->tank_auth_admin->create_user($username, $email, $password, $email_activation))) {

                        $tablename = 'patients';
                        $fieldarray = array(
                            'id' => $data['user_id'],
                            'doctor_id' => $doctor_id,
                            'fname' => $p['fName'],
                            'lname' => $p['lName'],
                            'email' => $p['email'],
                            'NHS' => $p['NINo']
                        );
                        $this->db_fnc->insert($tablename, $fieldarray);

                        $tablename = 'users_roles';
                        $fieldarray = array(
                            'user_id' => $data['user_id'],
                            'role_id' => 2
                        );
                        $this->db_fnc->insert($tablename, $fieldarray);


                        $data['site_name'] = $this->config->item('website_name', 'tank_auth_admin');

                        if ($email_activation) {         // send "activate" email
                            $data['activation_period'] = $this->config->item('email_activation_expire', 'tank_auth_admin') / 3600;
                            $data['password'] = $password;

                            $merror = $this->_send_email('activate', $data['email'], $data);

                            unset($data['password']); // Clear password (just for any case)
                            //$this->_show_message($this->lang->line('auth_message_registration_completed_1')); die;
                            //$this->session->set_flashdata('msg', 'value');
                        } else {
                            if ($this->config->item('email_account_details', 'tank_auth_admin')) { // send "welcome" email
                                $merror = $this->_send_email('welcome', $data['email'], $data);
                            }
                            unset($data['password']); // Clear password (just for any case)
                            //$this->_show_message($this->lang->line('auth_message_registration_completed_2').' '.anchor('/adminauth/login/', 'Login')); die;
                        }
                    } else {
                        $errors = $this->tank_auth_admin->get_error_message();
                        foreach ($errors as $k => $v)
                            $data['errors'][$k] = $this->lang->line($v);
                    }
                }
            }
        }

        $this->session->set_flashdata('msg', 'Message: ' . implode($data['errors']) . '' . $merror);
        redirect('admin/patients/invite_patients/');
    }

    /**
     * Register user on the site
     *
     * @return void
     */
    function create_admin() {
        if ($this->access->has_access($this->role_id, 'adminauth')) {      // logged in, not activated
            

            $isAdmin = $this->admin->isSysAdmin($this->role_id);
            $isLocalAuth = $this->admin->isLocalAuth($this->role_id);
            $isPractice = $this->admin->isPractice($this->role_id);

            $localAuth_id = null;
            $practice_id = null;
            $role_id = null;

            $use_username = $this->config->item('use_username', 'tank_auth_admin');
            if ($use_username) {
                $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean|min_length[' . $this->config->item('username_min_length', 'tank_auth_admin') . ']|max_length[' . $this->config->item('username_max_length', 'tank_auth_admin') . ']|alpha_dash');
            }
            $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|min_length[' . $this->config->item('password_min_length', 'tank_auth_admin') . ']|max_length[' . $this->config->item('password_max_length', 'tank_auth_admin') . ']|alpha_dash');
            $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|xss_clean|matches[password]');

            $captcha_registration = $this->config->item('captcha_registration', 'tank_auth_admin');
            $use_recaptcha = $this->config->item('use_recaptcha', 'tank_auth_admin');
            if ($captcha_registration) {
                if ($use_recaptcha) {
                    $this->form_validation->set_rules('recaptcha_response_field', 'Confirmation Code', 'trim|xss_clean|required|callback__check_recaptcha');
                } else {
                    $this->form_validation->set_rules('captcha', 'Confirmation Code', 'trim|xss_clean|required|callback__check_captcha');
                }
            }
            $data['errors'] = array();

            $email_activation = $this->config->item('email_activation', 'tank_auth_admin');

            $data['gpcoms'] = array('Please select a GP Commissioning');
            $data['gpcoms'] += $this->localauth->getLocalAuth_dd();
            
            $data['gpcom_id'] = 0;


            if ($isAdmin) {
                $localAuth_id = $this->input->post('gpcom');
                $practice_id = $this->input->post('practices');

                $group_id = ($practice_id) ? $practice_id : $localAuth_id ;

                $role_id = (isset($practice_id) && $practice_id != 0) ? 4 : 5;

              
            } elseif ($isLocalAuth) {
                $admins = $this->admin->getAllAdmins(array('pr.commissioning_id' => $this->gpcom_id));
                $role_id = 4;

            } elseif ($isPractice) {
                $admins = $this->admin->getAllAdmins(array('pr.practice_id' => $this->practice_id));
            }

            //echo $role_id; die;

            if ($this->form_validation->run()) {        // validation ok
                if (!is_null($data = $this->tank_auth_admin->create_user(
                                        $use_username ? $this->form_validation->set_value('username') : '',
                                        $this->form_validation->set_value('email'),
                                        $this->form_validation->set_value('password'),
                                        $email_activation,
                                        $group_id
                        ))) {         // success
                    $data['site_name'] = $this->config->item('website_name', 'tank_auth_admin');


                    if ($isAdmin || $isLocalAuth) {
                        $tablename = 'users_roles';
                        $fieldarray['user_id'] = $data['user_id'];
                        $fieldarray['role_id'] = $role_id;

                        $this->db_fnc->insert($tablename, $fieldarray);
                    }



                    if ($email_activation) {         // send "activate" email
                        $data['activation_period'] = $this->config->item('email_activation_expire', 'tank_auth_admin') / 3600;
                        $this->_send_email('activate', $data['email'], $data);
                        unset($data['password']); // Clear password (just for any case)
                        $this->_show_message($this->lang->line('auth_message_registration_completed_1'), 'admin/administrators');
                    } else {
                        if ($this->config->item('email_account_details', 'tank_auth_admin')) { // send "welcome" email
                            $this->_send_email('welcome', $data['email'], $data);
                        }
                        unset($data['password']); // Clear password (just for any case)

                        $this->_show_message($this->lang->line('auth_message_registration_completed_2') . ' ' . anchor('/adminauth/login/', 'Login'), 'admin/administrators');
                    }
                } else {
                    $errors = $this->tank_auth_admin->get_error_message();
                    foreach ($errors as $k => $v)
                        $data['errors'][$k] = $this->lang->line($v);
                }
            }
            if ($captcha_registration) {
                if ($use_recaptcha) {
                    $data['recaptcha_html'] = $this->_create_recaptcha();
                } else {
                    $data['captcha_html'] = $this->_create_captcha();
                }
            }
            $data['use_username'] = $use_username;
            $data['captcha_registration'] = $captcha_registration;
            $data['use_recaptcha'] = $use_recaptcha;
            
            $this->load->view('register_form', $data);
        }
    }

    /**
     * Send activation email again, to the same or new email address
     *
     * @return void
     */
    function send_again() {
        if (!$this->tank_auth_admin->is_logged_in(FALSE)) {       // not logged in or activated
            redirect('/adminauth/login/');
        } else {
            $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');

            $data['errors'] = array();

            if ($this->form_validation->run()) {        // validation ok
                if (!is_null($data = $this->tank_auth_admin->change_email(
                                        $this->form_validation->set_value('email')))) {   // success
                    $data['site_name'] = $this->config->item('website_name', 'tank_auth_admin');
                    $data['activation_period'] = $this->config->item('email_activation_expire', 'tank_auth_admin') / 3600;

                    $this->_send_email('activate', $data['email'], $data);

                    $this->_show_message(sprintf($this->lang->line('auth_message_activation_email_sent'), $data['email']));
                } else {
                    $errors = $this->tank_auth_admin->get_error_message();
                    foreach ($errors as $k => $v)
                        $data['errors'][$k] = $this->lang->line($v);
                }
            }
            $this->load->view('send_again_form', $data);
        }
    }

    /**
     * Activate user account.
     * User is verified by user_id and authentication code in the URL.
     * Can be called by clicking on link in mail.
     *
     * @return void
     */
    function activate() {
        $user_id = $this->uri->segment(3);
        $new_email_key = $this->uri->segment(4);

        // Activate user
        if ($this->tank_auth_admin->activate_user($user_id, $new_email_key)) {  // success
            $this->tank_auth_admin->logout();
            $this->_show_message($this->lang->line('auth_message_activation_completed') . ' ' . anchor('/adminauth/login/', 'Login'));
        } else {                // fail
            $this->_show_message($this->lang->line('auth_message_activation_failed'));
        }
    }

    /**
     * Generate reset code (to change password) and send it to user
     *
     * @return void
     */
    function forgot_password() {
        if ($this->tank_auth_admin->is_logged_in()) {         // logged in
            redirect('');
        } elseif ($this->tank_auth_admin->is_logged_in(FALSE)) {      // logged in, not activated
            redirect('/adminauth/send_again/');
        } else {
            $this->form_validation->set_rules('login', 'Email or username', 'trim|required|xss_clean');

            $data['errors'] = array();

            if ($this->form_validation->run()) {        // validation ok
                if (!is_null($data = $this->tank_auth_admin->forgot_password(
                                        $this->form_validation->set_value('login')))) {

                    $data['site_name'] = $this->config->item('website_name', 'tank_auth_admin');

                    // Send email with password activation link
                    $this->_send_email('forgot_password', $data['email'], $data);

                    $this->_show_message($this->lang->line('auth_message_new_password_sent'));
                } else {
                    $errors = $this->tank_auth_admin->get_error_message();
                    foreach ($errors as $k => $v)
                        $data['errors'][$k] = $this->lang->line($v);
                }
            }
            $this->load->view('forgot_password_form', $data);
        }
    }

    /**
     * Generate reset code (to change password) and send it to user
     *
     * @return void
     */
    function confirm_email() {
        if ($this->tank_auth_admin->is_logged_in()) {         // logged in
            redirect('');
        } elseif ($this->tank_auth_admin->is_logged_in(FALSE)) {      // logged in, not activated
            redirect('/adminauth/send_again/');
        } else {
            $this->form_validation->set_rules('login', 'Email or username', 'trim|required|xss_clean');

            $data['errors'] = array();

            if ($this->form_validation->run()) {        // validation ok
                if (!is_null($data = $this->tank_auth_admin->forgot_password(
                                        $this->form_validation->set_value('login')))) {

                    $data['site_name'] = $this->config->item('website_name', 'tank_auth_admin');

                    // Send email with password activation link
                    $this->_send_email('forgot_password', $data['email'], $data);

                    $this->_show_message($this->lang->line('auth_message_new_password_sent'));
                } else {
                    $errors = $this->tank_auth_admin->get_error_message();
                    foreach ($errors as $k => $v)
                        $data['errors'][$k] = $this->lang->line($v);
                }
            }
            $this->load->view('confirm_email_form', $data);
        }
    }

    /**
     * Replace user password (forgotten) with a new one (set by user).
     * User is verified by user_id and authentication code in the URL.
     * Can be called by clicking on link in mail.
     *
     * @return void
     */
    function reset_password() {
        $user_id = $this->uri->segment(3);
        $new_pass_key = $this->uri->segment(4);

        $this->form_validation->set_rules('new_password', 'New Password', 'trim|required|xss_clean|min_length[' . $this->config->item('password_min_length', 'tank_auth_admin') . ']|max_length[' . $this->config->item('password_max_length', 'tank_auth_admin') . ']|alpha_dash');
        $this->form_validation->set_rules('confirm_new_password', 'Confirm new Password', 'trim|required|xss_clean|matches[new_password]');

        $data['errors'] = array();

        if ($this->form_validation->run()) {        // validation ok
            if (!is_null($data = $this->tank_auth_admin->reset_password(
                                    $user_id, $new_pass_key,
                                    $this->form_validation->set_value('new_password')))) { // success
                $data['site_name'] = $this->config->item('website_name', 'tank_auth_admin');

                // Send email with new password
                $this->_send_email('reset_password', $data['email'], $data);

                $this->_show_message($this->lang->line('auth_message_new_password_activated') . ' ' . anchor('/adminauth/login/', 'Login'));
            } else {              // fail
                $this->_show_message($this->lang->line('auth_message_new_password_failed'));
            }
        } else {
            // Try to activate user by password key (if not activated yet)
            if ($this->config->item('email_activation', 'tank_auth_admin')) {
                $this->tank_auth_admin->activate_user($user_id, $new_pass_key, FALSE);
            }

            if (!$this->tank_auth_admin->can_reset_password($user_id, $new_pass_key)) {
                $this->_show_message($this->lang->line('auth_message_new_password_failed'));
            }
        }
        $this->load->view('reset_password_form', $data);
    }

    /**
     * Change user password
     *
     * @return void
     */
    function change_password() {
        if (!$this->tank_auth_admin->is_logged_in()) {        // not logged in or not activated
            redirect('/adminauth/login/');
        } else {
            $this->form_validation->set_rules('old_password', 'Old Password', 'trim|required|xss_clean');
            $this->form_validation->set_rules('new_password', 'New Password', 'trim|required|xss_clean|min_length[' . $this->config->item('password_min_length', 'tank_auth_admin') . ']|max_length[' . $this->config->item('password_max_length', 'tank_auth_admin') . ']|alpha_dash');
            $this->form_validation->set_rules('confirm_new_password', 'Confirm new Password', 'trim|required|xss_clean|matches[new_password]');

            $data['errors'] = array();

            if ($this->form_validation->run()) {        // validation ok
                if ($this->tank_auth_admin->change_password(
                                $this->form_validation->set_value('old_password'),
                                $this->form_validation->set_value('new_password'))) { // success
                    $this->_show_message($this->lang->line('auth_message_password_changed'));
                } else {              // fail
                    $errors = $this->tank_auth_admin->get_error_message();
                    foreach ($errors as $k => $v)
                        $data['errors'][$k] = $this->lang->line($v);
                }
            }
            $this->load->view('change_password_form', $data);
        }
    }

    /**
     * Change user email
     *
     * @return void
     */
    function change_email() {
        if (!$this->tank_auth_admin->is_logged_in()) {        // not logged in or not activated
            redirect('/adminauth/login/');
        } else {
            $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');

            $data['errors'] = array();

            if ($this->form_validation->run()) {        // validation ok
                if (!is_null($data = $this->tank_auth_admin->set_new_email(
                                        $this->form_validation->set_value('email'),
                                        $this->form_validation->set_value('password')))) {   // success
                    $data['site_name'] = $this->config->item('website_name', 'tank_auth_admin');

                    // Send email with new email address and its activation link
                    $this->_send_email('change_email', $data['new_email'], $data);

                    $this->_show_message(sprintf($this->lang->line('auth_message_new_email_sent'), $data['new_email']));
                } else {
                    $errors = $this->tank_auth_admin->get_error_message();
                    foreach ($errors as $k => $v)
                        $data['errors'][$k] = $this->lang->line($v);
                }
            }
            $this->load->view('change_email_form', $data);
        }
    }

    /**
     * Replace user email with a new one.
     * User is verified by user_id and authentication code in the URL.
     * Can be called by clicking on link in mail.
     *
     * @return void
     */
    function reset_email() {
        $user_id = $this->uri->segment(3);
        $new_email_key = $this->uri->segment(4);

        // Reset email
        if ($this->tank_auth_admin->activate_new_email($user_id, $new_email_key)) { // success
            $this->tank_auth_admin->logout();
            $this->_show_message($this->lang->line('auth_message_new_email_activated') . ' ' . anchor('/adminauth/login/', 'Login'));
        } else {                // fail
            $this->_show_message($this->lang->line('auth_message_new_email_failed'));
        }
    }

    /**
     * Delete user from the site (only when user is logged in)
     *
     * @return void
     */
    function unregister() {
        if (!$this->tank_auth_admin->is_logged_in()) {        // not logged in or not activated
            redirect('/adminauth/login/');
        } else {
            $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');

            $data['errors'] = array();

            if ($this->form_validation->run()) {        // validation ok
                if ($this->tank_auth_admin->delete_user(
                                $this->form_validation->set_value('password'))) {  // success
                    $this->_show_message($this->lang->line('auth_message_unregistered'));
                } else {              // fail
                    $errors = $this->tank_auth_admin->get_error_message();
                    foreach ($errors as $k => $v)
                        $data['errors'][$k] = $this->lang->line($v);
                }
            }
            $this->load->view('unregister_form', $data);
        }
    }

    /**
     * Show info message
     *
     * @param	string
     * @return	void
     */
    function _show_message($message, $page = '/adminauth/') {
        $this->session->set_flashdata('message', $message);
        if ($page)
            redirect($page);
    }

    /**
     * Send email message of given type (activate, forgot_password, etc.)
     *
     * @param	string
     * @param	string
     * @param	array
     * @return	void
     */
    function _send_email($type, $email, &$data) {
        $this->load->library('email');
        $this->email->from($this->config->item('webmaster_email', 'tank_auth_admin'), $this->config->item('website_name', 'tank_auth_admin'));
        $this->email->reply_to($this->config->item('webmaster_email', 'tank_auth_admin'), $this->config->item('website_name', 'tank_auth_admin'));
        $this->email->to($email);
        $this->email->subject(sprintf($this->lang->line('auth_subject_' . $type), $this->config->item('website_name', 'tank_auth_admin')));
        $this->email->message($this->load->view('email/' . $type . '-html', $data, TRUE));
        $this->email->set_alt_message($this->load->view('email/' . $type . '-txt', $data, TRUE));
        //$this->email->send();

        if ($this->email->send()) {
            //$this->_show_message('Your email was sent','');
            return 'Your email was sent';
        } else {
            //$this->_show_message($this->email->print_debugger(),'');
            return $this->email->print_debugger();
        }
    }

    /**
     * Create CAPTCHA image to verify user as a human
     *
     * @return	string
     */
    function _create_captcha() {
        $this->load->helper('captcha');

        $cap = create_captcha(array(
                    'img_path' => './' . $this->config->item('captcha_path', 'tank_auth'),
                    'img_url' => base_url() . $this->config->item('captcha_path', 'tank_auth'),
                    'font_path' => './' . $this->config->item('captcha_fonts_path', 'tank_auth'),
                    'font_size' => $this->config->item('captcha_font_size', 'tank_auth'),
                    'img_width' => $this->config->item('captcha_width', 'tank_auth'),
                    'img_height' => $this->config->item('captcha_height', 'tank_auth'),
                    'show_grid' => $this->config->item('captcha_grid', 'tank_auth'),
                    'expiration' => $this->config->item('captcha_expire', 'tank_auth'),
                ));

        // Save captcha params in session
        $this->session->set_flashdata(array(
            'captcha_word' => $cap['word'],
            'captcha_time' => $cap['time'],
        ));

        return $cap['image'];
    }

    /**
     * Callback function. Check if CAPTCHA test is passed.
     *
     * @param	string
     * @return	bool
     */
    function _check_captcha($code) {
        $time = $this->session->flashdata('captcha_time');
        $word = $this->session->flashdata('captcha_word');

        list($usec, $sec) = explode(" ", microtime());
        $now = ((float) $usec + (float) $sec);

        if ($now - $time > $this->config->item('captcha_expire', 'tank_auth_admin')) {
            $this->form_validation->set_message('_check_captcha', $this->lang->line('auth_captcha_expired'));
            return FALSE;
        } elseif (($this->config->item('captcha_case_sensitive', 'tank_auth_admin') AND
                $code != $word) OR
                strtolower($code) != strtolower($word)) {
            $this->form_validation->set_message('_check_captcha', $this->lang->line('auth_incorrect_captcha'));
            return FALSE;
        }
        return TRUE;
    }

    /**
     * Create reCAPTCHA JS and non-JS HTML to verify user as a human
     *
     * @return	string
     */
    function _create_recaptcha() {
        $this->load->helper('recaptcha');

        // Add custom theme so we can get only image
        $options = "<script>var RecaptchaOptions = {theme: 'custom', custom_theme_widget: 'recaptcha_widget'};</script>\n";

        // Get reCAPTCHA JS and non-JS HTML
        $html = recaptcha_get_html($this->config->item('recaptcha_public_key', 'tank_auth_admin'));

        return $options . $html;
    }

    /**
     * Callback function. Check if reCAPTCHA test is passed.
     *
     * @return	bool
     */
    function _check_recaptcha() {
        $this->load->helper('recaptcha');

        $resp = recaptcha_check_answer($this->config->item('recaptcha_private_key', 'tank_auth_admin'),
                        $_SERVER['REMOTE_ADDR'],
                        $_POST['recaptcha_challenge_field'],
                        $_POST['recaptcha_response_field']);

        if (!$resp->is_valid) {
            $this->form_validation->set_message('_check_recaptcha', $this->lang->line('auth_incorrect_captcha'));
            return FALSE;
        }
        return TRUE;
    }

}

/* End of file auth.php */
/* Location: ./application/controllers/auth.php */