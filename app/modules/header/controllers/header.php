<?php

class Header extends MX_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $data = array();
        $main_menu = array('' => 'home');
        $module = $this->router->fetch_class();
        $role_id = $this->session->userdata('role');
        $user_id = $this->session->userdata('user_id');

        if ($role_id)
            $main_menu += $this->access->getModulesForRole($role_id, 'main_menu');
        
            $main_menu += (!$user_id) ? array('auth/login' => 'login') : array();
            
        $data['main_menu'] = $main_menu;
        $data['active'] = $module;


        Tools::addJS(array(
                    "js/AC_RunActiveContent.js",
                    "js/jquery-1.4.2.min.js",
                    "js/jquery.validate.min.js",
                    "js/swfobject/swfobject.js",
                    "js/fancybox/jquery.mousewheel-3.0.2.pack.js",
                    "js/fancybox/jquery.fancybox-1.3.1.js",
                    //"js/jquery-ui/js/jquery-ui-1.8.14.custom.min.js",
                    "js/jquery-ui/js/jquery-ui-1.8.16.custom.min.js",
                    "js/jquery.form.js",
                    "js/jquery.dropdown.js",
                    "js/hoverIntent.js",
                    "js/jquery.dataTables.js",
                    "js/highcharts-2.1.5/highcharts.js",
                    "js/highcharts-2.1.5/modules/exporting.js",
                    "js/site_js.js"
                ));


        $this->load->view('header/header_view', $data);
    }

}
