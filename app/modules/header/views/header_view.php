<div id="wrapper">
<header>
<div id="header" class="container" >
    <div class="row_wrapper">
        <div class="row">
            <h2 class="logo">PATIENT ADVISER</h2>

            <nav id="menuTabs">
                <ul>
                    <?php
                    if (isset($main_menu) && $main_menu != '') {
                        $menu = "";
                        foreach ($main_menu as $key => $value) {
                            $menu .= '<li><a class="curvesTop10 ';
                            if ( $active == 'home' && $key === '') $menu .= 'active';
                            if ($active == $key) $menu .= 'active';

                            $menu .= '" href="' . base_url() . $key . '" ';
                            $menu .= 'title="' . $value . '">' . str_replace(' ', '&nbsp;', $value) . '</a></li>';
                        }
                        echo $menu;
                    }
                    ?>
                </ul>
            </nav>
        </div>
    </div>
</div>
</header>
<!-- header -->