
<footer>
    <div id="footer">
        <p class="credit"><?php echo $credit; ?></p>
    </div> <!-- footer -->
</footer>
<?php
    Tools::showJS();
?>


<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.4.2.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/DataTables-1.8.2/media/js/jquery.dataTables.min.js"></script>

        <script type="text/javascript" src="<?php echo base_url(); ?>js/admin.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/fileuploader.js"></script>
        <!--script type="text/javascript" src="<?php echo base_url(); ?>js/jQuery.fileinput.js"></script-->
        <script type="text/javascript" src="<?php echo base_url(); ?>js/fancybox/jquery.mousewheel-3.0.2.pack.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/fancybox/jquery.fancybox-1.3.1.js"></script>
        
<script>

$(document).ready(function() {
    oTable = $('.display').dataTable({
        "bJQueryUI": true,
        "sPaginationType": "full_numbers",
        "iDisplayLength": 20,
        "aLengthMenu": [[20, 50, 100, -1], [20, 50, 100, "All"]]
    });
} );
</script>
