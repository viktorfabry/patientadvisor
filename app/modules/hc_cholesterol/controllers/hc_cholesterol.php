<?php

class Hc_cholesterol extends MX_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        } else {
            $this->module = 'hc_cholesterol';
            $this->user_id = $this->tank_auth->get_user_id();
            $this->username = $this->tank_auth->get_username();
            $this->role_id = $this->session->userdata('role');
            $this->patient_id = $this->session->userdata('patient_id');
        }
    }

    function index() {
        $data = array();
        $initArray = $this->db_fnc->getRows('patient_id', $this->patient_id, 'cholesterol', 'sample_date ASC');
        $this->load->model('patient');
        $p = $this->patient->getPatientByID($this->patient_id);
        $this->load->model('color');
        $color = $this->color->getColors(array('module_id'=>0));
        $graphBonds = array();

        if (isset($p) && $p) {
            $bonds[1]['to'] = ($p['diabetes'] == 'Y' || $p['IHD'] == 'Y') ? 4.5 : 5;
            $bonds[2]['from'] = ($p['diabetes'] == 'Y' || $p['IHD'] == 'Y') ? 4.5 : 5;

            $graphBonds = array(
                array('color' => $color['bound_lower'],'from' => 0, 'to' => 2.1),
                array('color' => $color['bound_normal'],'from' => 2.1, 'to' => $bonds[1]['to']),
                array('color' => $color['bound_over'],'from' => $bonds[2]['from'], 'to' => 10),
                );

        }


        $this->load->library('highcharts');
        $this->highcharts->initialize('chart_template');
        $yAxis = array(
            'plotBands' => $graphBonds,
            'showFirstLabel' => false,
            'gridLineColor' => 'rgba(255,255,255,0.5)',
            'allowDecimals' => false,
            'title' => array(
                'text' => null
            )
        );
        $this->highcharts->set_title('Cholesterol');
        $this->highcharts->set_yAxis($yAxis);
        $ch = $this->createChartArray($initArray);


        return $data['charts'] = $this->highcharts->set_serie($ch['HDL'])->set_serie($ch['total'])->render(true);

    }

    function createChartArray($initArray) {
        if ($initArray) {
            $b['HDL'] = array();
            $b['total'] = array();
            $datestring = "Y, n ,j";
            foreach ($initArray as $sample) {
                if (isset($sample['HDL']) && $sample['HDL']) {
                    $b['HDL'][] = "{x:" . gmt_to_local(strtotime($sample['sample_date']), 'UTC').'000'
                            . ",y:" . $sample['HDL']
                            . ",note:'" . (isset($sample['note']) ? $sample['note'] : '')
                            . "'}";
                }
                if (isset($sample['total']) && $sample['total']) {
                    $b['total'][] = "{x:" . gmt_to_local(strtotime($sample['sample_date']), 'UTC').'000'
                            . ",y:" . $sample['total']
                            . ",note:'" . (isset($sample['note']) ? $sample['note'] : '')
                            . "'}";
                }
            }
            $ch['HDL'] = array('name' => 'HDL', 'data' => '['.implode(',', $b['HDL']).']');
            $ch['total'] = array('name' => 'total', 'data' => '['.implode(',', $b['total']).']');
        } else {
            $ch['HDL'] = array('name' => 'HDL', 'data' => '[]');
            $ch['total'] = array('name' => 'total', 'data' => '[]');
        }

        return $ch;
    }

}
