
<script type="text/javascript">

    var url = "<?php echo base_url(); ?>index.php/diabetes/user/";

		
    var chart;
    $(document).ready(function() {
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'container',
                type: 'spline',
                zoomType: 'x',
                backgroundColor: '#D8D8D2',
                borderRadius: 10,
                margin: [50, 20, 40, 60]
            },
            title: {
                text: ''
            },
            subtitle: {
                text: 'Click and drag in the plot area to zoom in',
                align: 'left',
                verticalAlign: 'bottom',
                x: 50,
                y: 7
            },
            xAxis: {
                type: 'datetime',
                maxZoom: 7 * 24 * 3600000, // fourteen days
                title: {
                    text: null
                },
                showFirstLabel: false
            },
            yAxis: {
                title: {
                    text: 'Glucose (mmol/L)'
                },
                //min: 1,
                //max: 25,
                //tickInterval: 5,
                gridLineColor: '#ffffff',
                //minorTickInterval: 1,
                //minorTickLength: 0,
                startOnTick: false,
                endOnTick: false,
                showFirstLabel: false,
                plotBands: [
                    {color: '#DBE0C5', from: 1, to: 6.5},
                    {color: '#F5CCB8', from: 6.5, to: 25}
                ]
            },
            tooltip: {
                formatter: function() {
                    return '<strong>'+ this.series.name + ': '+
                        Highcharts.numberFormat(this.y, 0) + "</strong><br> "+
								
                        Highcharts.dateFormat('%B %e %Y, %A', this.x);
                },
                style: {
                    color: '#333333',
                    fontSize: '12px',
                    padding: '10px 15px'
                }
            },
            legend: {
                align: 'right',
                verticalAlign: 'top',
                backgroundColor: '#fff',
                x: -80,
                y: 10
            },
            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: [0, 0, 0, 300],
                        stops: [
                            [0, '#4572A7'],
                            [1, 'rgba(2,0,0,0)']
                        ]
                    },
                    lineWidth: 1,
                    marker: {
                        enabled: false,
                        states: {
                            hover: {
                                enabled: true,
                                radius: 5
                            }
                        }
                    },
                    shadow: false,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    }
                }
            },
            series: [
                {name: 'Random Blood Sugar', color: '#B60910', data: <?php echo $d['diabetes_jsArray'] ?> }
            ],
            navigation: {
                buttonOptions: {
                    height: 26,
                    width: 26,
                    symbolSize: 12,
                    symbolX: 13,
                    symbolY: 13
                }
            },
            exporting: {
                buttons: {
                    exportButton: {
                        x: -20, y: 20
                    },
                    printButton: {
                        x: -50, y: 20
                    }
                }
            }
        });});
</script>
