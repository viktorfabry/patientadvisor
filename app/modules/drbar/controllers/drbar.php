<?php

class Drbar extends MX_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        } else {
            $this->module = 'drbar';
            $this->user_id = $this->tank_auth->get_user_id();
            $this->admin = $this->load->model('tank_auth/admins');
            $this->username = $this->tank_auth->get_username();
            $this->role_id = $this->session->userdata('role');
            $this->full_name = $this->session->userdata('full_name');
            $this->load->model('doctor');
            $this->load->model('xlink');
            $this->dr = null;
            $this->practice_id = null;
            $this->gpCom_id = null;
        }
    }

    function index() {
        if ($this->access->has_access($this->role_id, $this->module)) {

            if ($this->role_id == 3) {
                $this->dr = $this->doctor->getDrByUserID($this->user_id);

                $this->practice_id = $this->dr['practice_id'];
            }
            $data['is_logged_in'] = $this->tank_auth->is_logged_in();
            $data['full_name'] = $this->full_name;
            $data['patients'][0] = "Please select a patient";
            $data['patients'] += $this->doctor->get_my_patient_dropdown($this->dr['id']);
            

            $data['patient_id'] = $this->session->userdata('patient_id');
            $patient_id = $this->input->post('patient');
            $url = $this->input->post('url');
            if ($this->input->post('patient')) {

                $patient_id = $this->input->post('patient');
                $this->session->set_userdata('patient_id', $patient_id);
            } elseif ($this->session->userdata('patient_id')) {
                $patient_id = $this->session->userdata('patient_id');
            } else {
                $patient_id = 0;
            }


//                    $orderBy = 'link_id ASC, name ASC';
//                    $links = $this->db_fnc->getAll('drlinks', $orderBy);
//                    $menulinks = array(
//                            'items' => array(),
//                            'parents' => array()
//                    );
//                    $i = 0;
//                    foreach ($links as $l) {
//                            $menulinks['items'][$l['link_id']] = $l;
//                            $menulinks['parents'][$l['parent_id']][] = $l['link_id'];
//                            $i++;
//                    }
            $links = $this->xlink->getLinks(2, $this->practice_id, $this->gpCom_id);


            $dr_menu = $this->load->library('multilevelmenu', $links);
            $dr_menu->setFlink('Doctor Links');

            $data['menu'] = $dr_menu->getMenu();
            

            $this->load->view('drbar_view', $data);
        }
    }

    function set_patient() {
        if ($this->access->has_access($this->role_id, $this->module)) {

            $url = $this->input->post('url');
            $patient = $this->input->post('patient');

            if ($patient) {
                $patient_id = $this->input->post('patient');
                $this->session->set_userdata('patient_id', $patient_id);
            }

            if ($url == base_url()) {
                redirect('bloodpressure');
            } elseif ($url) {
                redirect($url);
            } else {
                redirect('');
            }
        }
    }

    function setfontsize($fsize){
        $this->app->setFontSize($fsize);
    }

}

