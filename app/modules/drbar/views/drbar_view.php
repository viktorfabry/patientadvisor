<div id="topBar" class="container ds5">
    <div class="ins clearfix">
        <div class="row">

            <?php
            if ($is_logged_in) {
                echo '<div class="wht right curves5 ds5 last">';
                echo anchor('profile', $full_name);
                echo anchor('auth/logout', '<span>log out</span>', 'class="logout"');
                echo '</div>';
            }
            ?>

            <?php echo anchor('patientbar/setfontsize/0', 'Enlarge the font size <span></span>', 'class="enlargeFont gridMarginRight right"'); ?>

            <form method="POST" id="pickUser" action="<?php echo base_url() . 'drbar/set_patient/'; ?>" name="pickUser" >
                <?php
                $js = 'class="users curves5 right dropSubmit" id="patient_dd"';
                echo form_dropdown('patient', $patients, $patient_id, $js);
                echo form_hidden('url', current_url());
                ?>

            </form>


            <?php echo $menu; ?>

            <span class="errorMsg jsError">Your javaScript is disabled, please enable it to use the site.</span>

                <a id="email_link" class="right marginR15" href="<?php echo base_url() . 'email/send_mail_to/' . $patient_id; ?>">Send email to -></a>

                <div class="invite_patient_box_holder">
                <?php echo anchor('auth/invite_patient', 'Invite patient', 'class="whtBtn right curves5 ds5" id="invitePatientBtn"'); ?>

                <div class="invite_patient_box  ds5 curvesBottom5">
                    <?php
                    $form = '';
                    $form .= form_open('auth/invite_patient', 'id="patient_invite_form"');
                    $form .= form_submit('submit_access', 'send', 'id="invitePatientSubmit" class="btn orange big right "');
                    $form .= form_hidden('url', current_url());
                    $form .= form_input('patient_email', 'class="required email"');
                    $form .= form_close();

                    echo $form;
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>