<?php

class Gchart_cholesterol extends MX_Controller {

	function  __construct() {
            parent::__construct();

            if (!$this->tank_auth->is_logged_in()) {
			redirect('/auth/login/');
            } else {
                $this->module       = 'gchart_cholesterol';
                $this->user_id	= $this->tank_auth->get_user_id();
                $this->username	= $this->tank_auth->get_username();
                $this->role_id      = $this->session->userdata('role');
            }
         }
	
	function index()
	{		
		$this->load->view('welcome_message',$data);
	}
	
	function user($id){
			
		$bp_all = $this->db_fnc->getRows('patient_id',$id,'cholesterol','sample_date ASC');

			
		$data['ch'] = $this->createChartArray($bp_all);

		$data['patient_id'] = $id;
			
		$this->load->view('cholesterol_view',$data);			
	}
	
	function createChartArray($initArray)
	{
		$bp = array();
		$datestring = "Y, n ,j";
		$r = 0;
		$n = 0;
		foreach ($initArray as $sample)
   {
		$bp[$n][0] = $r;
		$bp[$n][1] = 0;
		$bp[$n][2] = "new Date(". date(
									$datestring,
									strtotime($sample['sample_date']. "-1 month")
									).")";
		$n++;
		
		$bp[$n][0] = $r;
		$bp[$n][1] = 1;
		$bp[$n][2] = (int) $sample['HDL'];
		
		$n++;
		
		$bp[$n][0] = $r;
		$bp[$n][1] = 2;
		$bp[$n][2] = (int) $sample['total'];
		
		$n++;
		$r++;
   }
   
   return $bp;
		
}
	
	
	function addSample(){
		$patient_id = $_POST['patient_id'];
		$sample = $this->db_fnc->getMax('sample','patient_id',$patient_id,'cholesterol');
		$sample++;
		
		
		$datestring = "Y-m-d H:i:s";
		$sample_date = date($datestring, time());
		
		$tablename = 'cholesterol';
		$fieldarray = array('patient_id' => $patient_id, 'sample' => $sample, 'sample_date' => $sample_date, 'HDL' => $_POST['HDL'], 'total' => $_POST['total']);
		
		$this->db_fnc->insert($tablename,$fieldarray);

		redirect($_POST['url']);	 	 	 	 	 	
	}
	
}
