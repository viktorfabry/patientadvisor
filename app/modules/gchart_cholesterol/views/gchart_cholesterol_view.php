<!--/////////////////// choloesterol start///////////////////////////-->
<div class="module choloesterol corner10 clearfix">
<h2>Cholesterol</h2>
<script>
var url = "<?php echo base_url();?>index.php/cholesterol/user/";
</script>

<?php echo modules::run('lipid_advice', $patient_id); ?>


<div class="inputfields">
<?php echo form_open('cholesterol/addSample');?>

<input type="hidden" name="patient_id" value="<?php echo $patient_id; ?>">
<input type="hidden" name="url" value="<?php echo $this->uri->uri_string(); ?>">
<label>HDL</label>
<input type="text" name="HDL" value="">
<label>total</label>
<input type="text" name="total" value="">
<input class="submitBtn"  type="submit" value="submit">

<?php echo form_close();?>
</div>

<?php
$this->load->library('qgooglevisualapi/ConfigInc');
	

$chart = new QAnnotatedtimelineGoogleGraph();
$chart	
		//->ignoreContainer()
		->addDrawProperties(
			array(
				"title"=>'Company Performance',
				"thickness"=>'',
				"fill"=>'0',
                "wmode"=>'transparent',
				)
			)
		->addColumns(
			array(
		        array('date', 'Date'),
		        array('number', 'HDL'),
		        array('number', 'total'),
		        array('string', 'title'),
		        array('string', 'text'),			
				)
			)
		->setValues($ch);
echo $chart->render();

?>
</div>
<!--/////////////////// choloesterol end///////////////////////////-->
