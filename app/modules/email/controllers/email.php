<?php

class Email extends MX_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        } else {
            $this->module = 'drbar';
            $this->user_id = $this->tank_auth->get_user_id();
            $this->username = $this->tank_auth->get_username();
            $this->role_id = $this->session->userdata('role');
            $this->dr = $this->load->model('Doctor');
            $this->patient = $this->load->model('Patient');
        }
    }

    function index() {
        if ($this->access->has_access($this->role_id, $this->module)) {

            $this->load->view('email_view', $data);
        } else {
            redirect('/');
        }
    }

    function send_mail_to($patient_id = 0) {
        (int) $patient_id;
        if ($this->access->has_access($this->role_id, $this->module)) {
            $data = array();

            $p = $this->patient->getPatientByID($patient_id);

                $data['email'] = (isset($p['email']) && $p['email']) ? $p['email'] : 'no email';
                $data['uname'] = (isset($p['username']) && $p['username']) ? $p['username'] : '';

            $this->load->view('email_view', $data);
        } else {
            redirect('/');
        }
    }

    function send_mail() {
        //print_r($_POST);

        $subject = $this->input->post('subject');

        $email_body = $this->input->post('email_body');

        $this->load->library('email');
        $config['protocol'] = 'mail';
        $config['mailtype'] = 'html';
        $this->email->initialize($config);

        $this->email->from('info@medcheck.com', 'Medcheck');
        $this->email->to('medcheck.test@gmail.com');
        $this->email->subject($subject);


        $this->email->message($email_body);
        $this->email->send();

        redirect('/practice/');
    }

}
