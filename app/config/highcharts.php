<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

// shared_options : highcharts global settings, like interface or language
$config['shared_options'] = array(
    'chart' => array(
        'backgroundColor' => array(
            'linearGradient' => array(0, 0, 0, 500),
            'stops' => array(
                array(0, '#fff'),
                array(1, '#D8D8D2')
            )
        ),
        'shadow' => true
    )
);

$tooltip = "function() {";
$tooltip .= "var note = (this.point.note) ? '<span style=\"color: #333\">' + this.point.note + '</span><br>' : '';";
$tooltip .= "return '<strong> ' + this.series.name + ': ' + Highcharts.numberFormat(this.y, 0) + '</strong><br>'";
$tooltip .= "+ note + Highcharts.dateFormat('%H:%M %B %e %Y, %A', this.x);}";

$xLabel = "function() {return Highcharts.dateFormat('%a %d %b', this.value);}";


// Template Example
$config['chart_template'] = array(
    'chart' => array(
        'renderTo' => 'graph',
        'defaultSeriesType' => 'spline',
        'plotBackgroundImage' => 'img/noise.png',
        'zoomType' => 'x',
        'borderRadius' => 10,
        'margin' => array(50, 30, 40, 60)
    ),
    'colors' => array(
        '#c00', '#057'
    ),
    'credits' => array(
        'enabled' => true,
        'text' => null,
        'href' => null
    ),
    'title' => array(
        'text' => 'Graph',
        'align' => 'left',
        'x' => 50,
        'y' => 30,
        'style' => array(
            'color' => '#333'
        )
    ),
    'legend' => array(
        'enabled' => true,
        'align' => 'right',
        'verticalAlign' => 'top',
        'backgroundColor' => '#e9e9e9',
        'borderColor' => '#bbb',
        'itemStyle' => array(
            'color' => '#333'
        ),
        'x' => -90,
        'y' => 10
    ),
    'xAxis' => array(
        'title' => array(
            'text' => null
        ),
        'lineColor' => '#fff',
        'tickColor' => '#fff',
        'type' => 'datetime',
        'dateTimeLabelFormats' => array(
            'second' => '%e. %b %H:%M:%S',
            'minute' => '%e. %b %H:%M',
            'hour' => '%e. %b %H:%M',
            'day' => '%e. %b',
            'week'  => '%e. %b',
            'month'  => '%b \'%y',
            'year' => '%Y'
        ),
        'maxZoom' => 1 * 24 * 3600000, // 1 day
        'labels' => array(
            'align' => 'center',
            'x' => 0,
            'y' => 20
        ),
        'showFirstLabel' => false
    ),
    'tooltip' => array(
        'formatter' => $tooltip,
        'style' => array(
            'color' => '#333333',
            'fontSize' => '14px',
            'padding' => '10px 15px'
        )
    ),
    'navigation' => array(
        'buttonOptions' => array(
            'height' => 25,
            'width' => 25,
            'symbolSize' => 10,
            'symbolX' => 13,
            'symbolY' => 13
        )
    ),
    'exporting' => array(
        'buttons' => array(
            'exportButton' => array(
                'x' => -30,
                'y' => 20
            ),
            'printButton' => array(
                'x' => -60,
                'y' => 20
            )
        )
    )
);