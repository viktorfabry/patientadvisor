<?php

// Errors
$lang['auth_incorrect_password'] = 'Incorrect password';


// Notifications
$lang['app_no_access'] = "Hi, <strong> %1 </strong>! <br /> You don't have access to %2 <br />Please logout and login with different credentials.";


// Email subjects
$lang['auth_subject_welcome'] = 'Welcome to %s!';



/* End of file tank_auth_lang.php */
/* Location: ./application/language/english/tank_auth_lang.php */