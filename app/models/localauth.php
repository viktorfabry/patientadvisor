<?php
	
class Localauth extends Db_fnc {

        function  __construct() {
            parent::__construct();
        }
    
        function getLocalauthById($id){
            return $this->getRow('id',$id,'groups');
        }


	function getAuthorities($orderby = 'name'){
		$result = array();
                $this->db->select(array('g.*','g2.name gpcom'));
                $this->db->where(array('g.group_type_id'=>1));
                $this->db->join('groups g2', 'g2.id = g.parent_id','left');
                $query =  $this->db->get( 'groups g' );
                if( $query->num_rows() > 0 ) {
                    return $query->result_array();
                }
        }
        function getPracticeNames($parent_id=null){
		$result = array();
                $this->db->select(array('g.id','g.name'));
                $this->db->join('groups g2', 'g2.id = g.parent_id','left');
                $this->db->where(array('g.group_type_id'=>2));
                if($parent_id!==null) $this->db->where(array('g2.id'=>$parent_id));
                $query =  $this->db->get( 'groups g' );
                if( $query->num_rows() > 0 ) {
                    return $query->result();
                }
        }
        function getLocalAuth_dd(){
           return $this->getKeyValueArray('id', 'name', 'groups',array('group_type_id'=>1),'name ASC');
        }
        
        function updateAuthority($fieldarray, $id){
            return $this->update('groups', $fieldarray,array('id'=>$id));
        }

        function addAuthority($fieldarray){
            //print_r($fieldarray); die;
            $fieldarray['parent_id'] = '';
            $fieldarray['group_type_id'] = 1;
            return $this->insert('groups', $fieldarray);
        }

        function deleteAuthority($id){
            $this->delete('groups','id',$id);
        }
 

}

