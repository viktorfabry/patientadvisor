<?php
	
class Person extends CI_Model {


    function  __construct($id) {
            parent::__construct();
            $this->getRows('patient_id',$id,'patients');
        }
	
	
	
	
	
	
	function getRows($key,$id,$from){
		$data = array();
		$where = array($key => $id);
		$query = $this->db->get_where($from,$where);
		if ($query-> num_rows() > 0){
			foreach ($query-> result_array() as $row){
			$data[] = $row;
			}
		}
		$query-> free_result();
		return $data;
	}
    
    function getRow($key,$id,$from){
		$data = array();
		$where = array($key => $id);
		$query = $this->db->get_where($from,$where,1);
			if ($query-> num_rows() > 0){
				$data = $row;
			}
		$query->free_result();
		return $data;
	}
	
	function getThis($select,$key,$id,$from){
		$data = array();
		$this->db->select($select);
		$where = array($key => $id);
		$query = $this->db->get_where($from,$where,1);
			if ($query-> num_rows() > 0){
			foreach ($query-> result_array() as $row){
			$data = $row;
			}
		}
		$query-> free_result();
		return $data;
	}

	function getAll($from){
		$data = array();
		$query = $this->db->get($from);
		if ($query-> num_rows() > 0){
			foreach ($query-> result_array() as $row){
			$data[] = $row;
			}
		}
		$query-> free_result();
		return $data;
	}
	
	
	
	
	function getThisWhere($select,$key,$id,$from){
		$data = array();
		$this->db->select($select);
		$where = array($key => $id);
		$query = $this->db->get_where($from,$where);
		
		if ($query-> num_rows() > 0){
			foreach ($query-> result_array() as $row){
			$data[] = $row[$key];
			}
		}
		$query-> free_result();
		return $data;
	}
	
	function getKeyValueArray($key, $value, $from){
        $result = array();
        $array_keys_values = $this->db->query('select '.$key.', '.$value.' from '.$from.' ');
       foreach ($array_keys_values->result() as $row)
        {
            $result[$row->$key]= $row->$value;
        }
        return $result;
    }
	
	function delete($id, $table) {
		$query = $this->db->query("DELETE FROM ".$table." WHERE order_id='".$id."' LIMIT 1");

    }

    function update($update, $table) {
        $this->db->update($table, $update);
    }

}

