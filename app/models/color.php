<?php

class Color extends db_fnc {

    function __construct() {
        parent::__construct();
    }

       function getColors($where) {
        $data = array();
        $query = $this->db->get_where('colors', $where);

        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $data[$row['name']] = $row['color'];
            }
        }
        $query->free_result();
        return $data;
    }

}

