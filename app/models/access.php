<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Access
 *
 * This model represents roles.
 */
class Access extends CI_Model
{
        function __construct()
    {
        parent::__construct();
    }


    function has_access($role_id,$moduleName){
        $this->db->select('mr.role_id');
        $this->db->join('modules m', 'm.module_id = mr.module_id');
        $this->db->where( 'mr.role_id', $role_id );
        $this->db->where( 'm.link', $moduleName );
        $query =  $this->db->get( 'modules_roles mr' );
        if( $query->num_rows() > 0 ) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function getModuleByName($moduleName){
        $this->db->select('*');
        $this->db->where( 'link', $moduleName );
        $this->db->limit(1);
        $query =  $this->db->get( 'modules' );
        if ($query-> num_rows() > 0){
		return $query->row_array();
	}
	$query-> free_result();
    }

    function getParent($moduleName){
        $this->db->select(array('mp.*'));
        $this->db->join('modules mp', 'mp.module_id = m.parent','left');
        $this->db->where( 'm.link', $moduleName );
        $this->db->limit(1);
        $query =  $this->db->get( 'modules m' );
                if ($query-> num_rows() > 0){
			return $query->row_array();
  		}

    }

    function getModulesForRole($role_id,$type = "",$parent="",$orderBy="m.sort ASC"){
        $data = array();
        $this->db->select(array('m.link','m.name'));
        $this->db->join('modules m', 'm.module_id = mr.module_id','left');
        $this->db->where( 'mr.role_id', $role_id );
        if ($type!="") $this->db->where( 'type', $type );
        if($parent!="") $this->db->where( 'm.parent', $parent );
        if ($orderBy != '') $this->db->order_by($orderBy);
        $query =  $this->db->get( 'modules_roles mr' );
                if ($query-> num_rows() > 0){
			foreach ($query->result() as $row){
			$data[$row->link] = $row->name;
			}
		}
		$query-> free_result();
		return $data;
    }

    function getMyGroupIDs($user_id){
        $this->db->select(array('a.group_id','g.group_id'));
        $this->db->join('groups g', 'g.parent_id = a.group_id','left');
        $this->db->where( 'a.id', $user_id );
        $this->db->limit(1);
        $query =  $this->db->get( 'admins a' );
                if ($query-> num_rows() > 0){
			return $query->row_array();
		}
    }

        /**
     * return true or false based on the role id
     *
     * @param	int
     * @return	bool
     */
    function isDoctor($role_id) {
        return ($role_id === '3') ? true : false;
    }

    /**
     * return true or false based on the role id
     *
     * @param	int
     * @return	bool
     */
    function isPatient($role_id) {
        return ($role_id === '2') ? true : false;
    }


}

/* End of file roles.php */
/* Location: ./application/models/auth/roles.php */
