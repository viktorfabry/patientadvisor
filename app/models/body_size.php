<?php

class body_size extends CI_Model {

    function  __construct() {
            parent::__construct();
             $this->imp = $this->session->userdata('imp');
        }
    
    public function createWeight( $patient_id ) {
        if (isset($this->imp) && $this->imp == 'Y') {
             $weight = lb2kg($this->input->post('cWeight')); 
        } else {
             $weight = $this->input->post('cWeight');
        }

        $data = array(
            'patient_id'  => $patient_id,
            'sample' => $this->input->post( 'cSample', true ),
            'sample_date' => $this->input->post( 'cSample_date', true ),
            'weight' => $weight,
            'girth' => 0,
            'note' => $this->input->post( 'cNote', true )
        );
        $this->db->insert( 'body_size', $data );
        return $this->db->insert_id();
    }

    public function createGirth( $patient_id ) {
        if (isset($this->imp) && $this->imp == 'Y') {
             $girth = imperial2metric($this->input->post('cGirth').$this->app_functions->getLengthMeasure());
        } else {
             $girth = $this->input->post('cGirth');
        }
        $data = array(
            'patient_id'  => $patient_id,
            'sample' => $this->input->post( 'cSample', true ),
            'sample_date' => $this->input->post( 'cSample_date', true ),
            'weight' => 0,
            'girth' => $girth,
            'note' => $this->input->post( 'cNote', true )
        );

        $this->db->insert( 'body_size', $data );
        return $this->db->insert_id();
    }
    
    public function getWeightById( $id, $measure = 'kg'  ) {
        $id = intval( $id );
        
        $query = $this->db->select(array('sample_id AS id', 'sample', 'sample_date', getSQL_weight($measure),'note'))
                    ->where( 'sample_id', $id )
                    ->limit( 1 )
                    ->get( 'body_size' );
        
        if( $query->num_rows() > 0 ) {
            return $query->row();
        } else {
            return array();
        }
    }

    public function getGirthById( $id, $measure = 'kg'  ) {
        $id = intval( $id );
        $query = $this->db->select(array('sample_id AS id', 'sample', 'sample_date', getSQL_length($measure,'girth'),'note'))
                    ->where( 'sample_id', $id )
                    ->limit( 1 )
                    ->get( 'body_size' );

        if( $query->num_rows() > 0 ) {
            return $query->row();
        } else {
            return array();
        }
    }
    public function getAllWeight( $patient_id, $measure = 'kg' ) {
        $patient_id = intval( $patient_id );
        $query = $this->db->select(array('sample_id AS id', 'sample', 'sample_date', getSQL_weight($measure),'note'))
                ->where( 'patient_id', $patient_id )
                ->where( 'weight >', 0 )
                ->order_by('sample DESC')
                ->get( 'body_size' );
        
        if( $query->num_rows() > 0 ) {
            return $query->result();
        } else {
            return array();
        }
    }

    public function getAllGirth( $patient_id, $measure = 'cm' ) {
        $patient_id = intval( $patient_id );
        $query = $this->db->select(array('sample_id AS id', 'sample', 'sample_date', getSQL_length($measure,'girth'),'note'))
                ->where( 'patient_id', $patient_id )
                ->where( 'girth >', 0 )
                ->order_by('sample DESC')
                ->get( 'body_size' );

        if( $query->num_rows() > 0 ) {
            return $query->result();
        } else {
            return array();
        }
    }

    public function getAllSamples( $patient_id ) {
        $patient_id = intval( $patient_id );
        $query = $this->db->select(array('sample_id AS id', 'sample', 'sample_date','weight', 'girth'))->where( 'patient_id', $patient_id )->order_by('sample DESC')->get( 'body_size' );

        if( $query->num_rows() > 0 ) {
            return $query->result();
        } else {
            return array();
        }
    }

    public function getAll() {
        //get all records from body_size table
        $query = $this->db->get( 'body_size' );
        
        if( $query->num_rows() > 0 ) {
            return $query->result();
        } else {
            return array();
        }
    } //end getAll
    
    public function updateWeight($measure = 'kg') {
        if (isset($this->imp) && $this->imp == 'Y') {
             $weight = lb2kg($this->input->post('weight'));
        } else {
             $weight = $this->input->post('weight');
        }
        $data = array(
            'sample_date' => $this->input->post( 'sample_date', true ),
            'weight' => $weight,
            'girth' => 0,
            'note' => $this->input->post( 'note', true )
        );
        
        $this->db->update( 'body_size', $data, array( 'sample_id' => $this->input->post( 'id', true ) ) );
    }

    public function updateGirth($measure = 'cm') {
        if (isset($this->imp) && $this->imp == 'Y') {
             $girth = imperial2metric($this->input->post('girth'));
        } else {
             $girth = $this->input->post('girth');
        }
        $data = array(
            'sample_date' => $this->input->post( 'sample_date', true ),
            'weight' => 0,
            'girth' => $girth,
            'note' => $this->input->post( 'note', true )
        );

        $this->db->update( 'body_size', $data, array( 'sample_id' => $this->input->post( 'id', true ) ) );
    }

    public function delete( $id ) {
        /*
        * Any non-digit character will be excluded after passing $id
        * from intval function. This is done for security reason.
        */
        $id = intval( $id );
        
        $this->db->delete( 'body_size', array( 'sample_id' => $id ) );
    } //end delete
    
} //end class