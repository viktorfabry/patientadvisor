<?php
	
class Practice extends Db_fnc {

        function  __construct() {
            parent::__construct();
        }

        function getPracticeById($id){
            return $this->getRow('id',$id,'groups');
        }
        
	function getPractices($parent_id=null){
		$result = array();
                $this->db->select(array('g.*','g2.name gpcom'));
                $this->db->join('groups g2', 'g2.id = g.parent_id','left');
                $this->db->where(array('g.group_type_id'=>2));
                if($parent_id!==null) $this->db->where(array('g2.id'=>$parent_id));
                $query =  $this->db->get( 'groups g' );
                if( $query->num_rows() > 0 ) {
                    return $query->result_array();
                }
        }
        function getPracticeNames($parent_id=null){
		$result = array();
                $this->db->select(array('g.id','g.name'));
                $this->db->join('groups g2', 'g2.id = g.parent_id','left');
                $this->db->where(array('g.group_type_id'=>2));
                if($parent_id!==null) $this->db->where(array('g2.id'=>$parent_id));
                $query =  $this->db->get( 'groups g' );
                if( $query->num_rows() > 0 ) {
                    return $query->result();
                }
        }

        function getPractice_dd($parent_id=null){
                return $this->getKeyValueArray('id', 'name', 'groups', array('parent_id'=>$parent_id));
        }

         function updatePractice($fieldarray, $id){
            return $this->update('groups', $fieldarray,array('id'=>$id));
        }

        function addPractice($fieldarray){
            //print_r($fieldarray); die;
            $fieldarray['group_type_id'] = 2;
            return $this->insert('groups', $fieldarray);
        }

        function deletePractice($id){
            $this->delete('groups','id',$id);
        }
 

}

