<?php

class bp extends CI_Model {

    function  __construct() {
            parent::__construct();
        }
    
    public function create( $patient_id ) {
        
        $data = array(
            'patient_id'  => $patient_id,
            'sample' => $this->input->post( 'cSample', true ),
            'sample_date' => $this->input->post( 'cSample_date', true ),
            'systolic' => $this->input->post( 'cSystolic', true ),
            'diastolic' => $this->input->post( 'cDiastolic', true ),
            'note' => $this->input->post( 'cNote', true )

        );
       // print_r($data); 
        $this->db->insert( 'bloodpressure', $data );
        return $this->db->insert_id();
    }
    
    public function getById( $id ) {
        $id = intval( $id );
        
        $query = $this->db->where( 'sample_id', $id )->limit( 1 )->get( 'bloodpressure' );
        
        if( $query->num_rows() > 0 ) {
            return $query->row();
        } else {
            return array();
        }
    }
    
    public function getAllSamples( $patient_id ) {
        $patient_id = intval( $patient_id );
        $query = $this->db->select(array('sample_id AS id', 'sample', 'sample_date', 'systolic', 'diastolic', 'note'))->where( 'patient_id', $patient_id )->order_by('sample DESC')->get( 'bloodpressure' );
        
        if( $query->num_rows() > 0 ) {
            return $query->result();
        } else {
            return array();
        }
    }
    
    public function getAll() {
        //get all records from bloodpressure table
        $query = $this->db->get( 'bloodpressure' );
        
        if( $query->num_rows() > 0 ) {
            return $query->result();
        } else {
            return array();
        }
    } //end getAll
    
    public function update() {
        $data = array(
            'sample_date' => $this->input->post( 'sample_date', true ),
            'systolic' => $this->input->post( 'systolic', true ),
            'diastolic' => $this->input->post( 'diastolic', true ),
            'note' => $this->input->post( 'note', true )
        );
        
        $this->db->update( 'bloodpressure', $data, array( 'sample_id' => $this->input->post( 'id', true ) ) );
    }
    
    public function delete( $id ) {
        /*
        * Any non-digit character will be excluded after passing $id
        * from intval function. This is done for security reason.
        */
        $id = intval( $id );
        
        $this->db->delete( 'bloodpressure', array( 'sample_id' => $id ) );
    } //end delete
    
} //end class