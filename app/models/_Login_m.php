<?php
	
	
class Login_m extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }
    
	function logMeInPatient($username, $password){
		$this->db->select('*');
		$this->db->from('patients');
		//$this->db->where('username = ".$username."');
		$this->db->where('uname',$username);
		//$this->db->where('uname',sha1($password));
		$this->db->where('pword',md5($password));

		$this->db->limit(1);
		$query = $this->db->get();
		if($query->num_rows() == 1){
			$row = $query->row_array();
			return $row['id'];
		}
	}
	function logMeInDoctor($username, $password){
		$this->db->select('*');
		$this->db->from('doctors');
		$this->db->where('uname',$username);
		$this->db->where('pword',sha1($password));
		$this->db->limit(1);
		$query = $this->db->get();
		if($query->num_rows() == 1){
			$row = $query->row_array();
			return $row['doctor_id'];
		}
	}
		
	function logMeInAdmin($adminName, $adminPass){
		$this->db->select('admin_id, uname, role');

		$this->db->from('admin');
		$this->db->where('uname',$adminName);
		$this->db->where('password',sha1($adminPass));
		$this->db->limit(1);
		$query = $this->db->get();
		if($query->num_rows() == 1){
			$row = $query->row_array();
			return $row;
		}
	}

}
