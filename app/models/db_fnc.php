<?php

class Db_fnc extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getRows($key, $id, $from, $orderBy = '') {
        $data = array();
        $where = array($key => $id);
        if ($orderBy != '') {
            $this->db->order_by($orderBy);
        }

        $query = $this->db->get_where($from, $where);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $data[] = $row;
            }
        }
        $query->free_result();
        return $data;
    }

    function getTheseWhere($select, $where, $from, $orderBy = '') {
        $data = array();
        $this->db->select($select);
        if ($orderBy != '') {
            $this->db->order_by($orderBy);
        }

        $query = $this->db->get_where($from, $where);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $data[] = $row;
            }
        }
        $query->free_result();
        return $data;
    }

    function getRow($key, $id, $from) {
        $data = array();
        $where = array($key => $id);
        $query = $this->db->get_where($from, $where, 1);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $data = $row;
            }
        }
        $query->free_result();
        return $data;
    }

    function getThis($select, $key, $id, $from) {
        $data = array();
        $this->db->select($select);
        $where = array($key => $id);
        $query = $this->db->get_where($from, $where, 1);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $data = $row;
            }
        }
        $query->free_result();
        return $data;
    }

    function getThisOne($select, $key, $id, $from) {
        $data = array();
        $this->db->select($select);
        $where = array($key => $id);
        $query = $this->db->get_where($from, $where, 1);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $data = $row[$select];
            }
        }
        $query->free_result();
        return $data;
    }

    function getMax($select, $key, $id, $from) {

        $this->db->select_max($select);
        $where = array($key => $id);

        $query = $this->db->get_where($from, $where, 1);
        // Produces: SELECT MAX(age) as member_age FROM members
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $data = $row[$select];
            }
        }
        $query->free_result();
        return $data;
    }

    function getAll($from, $orderBy = '') {
        $data = array();
        if ($orderBy != '') {
            $this->db->order_by($orderBy);
        }

        $query = $this->db->get($from);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $data[] = $row;
            }
        }
        $query->free_result();
        return $data;
    }

    function getThisWhere($select, $where, $from, $orderBy = '') {
        $data = array();
        $this->db->select($select);
        if ($orderBy != '') {
            $this->db->order_by($orderBy);
        }

        $query = $this->db->get_where($from, $where);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $data[] = $row[$select];
            }
        }
        $query->free_result();
        return $data;
    }

    function getRowsWhere($from, $where) {
        $data = array();
        $query = $this->db->get_where($from, $where);

        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $data[] = $row;
            }
        }
        $query->free_result();
        return $data;
    }

    function getKeyValueArray($key, $value, $from, $where = '', $orderBy = '') {
        // echo $key.$value.$from.$where; die;
        $result = array();
        $this->db->select(array($key, $value));
        if ($where != '')
            $this->db->where($where);
        if ($orderBy != '') {
            $this->db->order_by($orderBy);
        }
        $query = $this->db->get($from);
        foreach ($query->result() as $row) {
            $result[$row->$key] = $row->$value;
        }
        return $result;
    }

    function delete($table, $key, $id) {
        $where = array($key => $id);
        $this->db->delete($table, $where);
    }

    function update($table, $update, $where) {
        $this->db->update($table, $update, $where);
    }

    function insert($table, $fieldarray) {
        $sql = $this->db->insert_string($table, $fieldarray);
        //print_r($sql); die;
        $this->db->query($sql);
        $id = $this->db->insert_id();
        return $id;
    }

    function log($user_id, $usertype, $event) {

        if (intval($user_id) == 0) { // invalid user
            return false;
        }

        $data['event'] = $event;
        $data['user_id'] = intval($user_id);
        $data['usertype'] = $usertype;
        $data['event_date'] = date('Y-m-d H:i:s');
        $this->db->insert('logs', $data);
    }

}

