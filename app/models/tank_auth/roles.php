<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Roles
 *
 * This model represents roles. It operates the following two tables
 *
 * @package    Tank_auth
 * @author    theshiftexchange
 */
class Roles extends CI_Model
{
        function __construct()
    {
        parent::__construct();
    }

    /**
     * Get all user roles and return them as a string with a '%' seperating them
     *
     * @param    int
     * @return    string
     */
    function get_user_role($user_id)
    {
            $query = $this->db->query("SELECT users_roles.role_id
                                       FROM users_roles
                                       INNER JOIN roles ON users_roles.role_id=roles.role_id
                                       WHERE user_id = ".$user_id." LIMIT 1"
                                        );

            if ($query->num_rows() > 0)
            {
               foreach ($query->result() as $row)
               {
                  return $row->role_id;
               }
            }
    }
}

/* End of file roles.php */
/* Location: ./application/models/auth/roles.php */
