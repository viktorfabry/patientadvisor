<?php

class Doctor extends db_fnc {

    function __construct() {
        parent::__construct();
    }

    function getDoctorLogInfo($id) {
        (int) $id;
        $this->db->select(array('d.id AS doctor_id', 'd.fsize', 'd.imp_measure as imp', 'CONCAT(d.fname," ",d.lname) AS "full_name"'));
        $this->db->where('d.user_id', $id);
        $query = $this->db->get('doctors d');
        if ($query->num_rows() == 1)
            return $query->row();
        return NULL;
    }

    function initByEMIS($EMIS) {
        $result = array();
        $this->db->select(array('d.*','u.email', 'g2.id as commissioning_id'));
        $this->db->join('users u', 'u.id = d.user_id');
        $this->db->join('groups g', 'g.id = d.practice_id', 'left');
        $this->db->join('groups g2', 'g2.id = g.parent_id', 'left');
        $this->db->where(array('d.EMIS' => $EMIS));
        $this->db->limit(1);
        $query = $this->db->get('doctors d');
        if ($query->num_rows() > 0) {
            $result = $query->row();
        }
        return $result;
    }

    function getInfo($key, $id, $from) {
        $data = array();
        $where = array($key => $id);
        $query = $this->db->get_where($from, $where, 1);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $data = $row;
            }
        }
        $query->free_result();
        return $data;
    }


    function getDrByUserID($id) {
        (int) $id;
        $result = array();
        $this->db->select(array('d.*','u.email', 'g2.id as gpCom_id'));
        $this->db->join('users u', 'u.id = d.user_id');
        $this->db->join('groups g', 'g.id = d.practice_id', 'left');
        $this->db->join('groups g2', 'g2.id = g.parent_id', 'left');
        $this->db->where(array('d.user_id' => $id));
        $this->db->limit(1);
        $query = $this->db->get('doctors d');
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
    }

    function getAllDoctors($where='') {
        $result = array();
        $this->db->select(array('d.id','d.user_id', 'd.practice_id', 'CONCAT(d.fname," ",d.lname) AS "doctor"', 'd.EMIS', 'g2.name gpcom', 'g.name practice'));
        $this->db->join('groups g', 'g.id = d.practice_id', 'left');
        $this->db->join('groups g2', 'g2.id = g.parent_id', 'left');
        if ($where != '')
            $this->db->where($where);
        $query = $this->db->get('doctors d');
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            return $result;
        }
    }

    function get_my_patient_dropdown($doctor_id) {
        $result = array();
        $this->db->select(array('u.id','u.email', 'p.lname', 'p.fname' ));
        $this->db->join('users u', 'u.id = p.user_id');
        $this->db->where(array('p.doctor_id' => $doctor_id, 'p.current' => 'Y'));
        $this->db->order_by('LOWER(p.lname) ASC, u.email ASC');

        $array_keys_values = $this->db->get('patients p');
        foreach ($array_keys_values->result() as $row) {
            $result[$row->id] = ($row->fname || $row->lname) ? $row->lname.', '.$row->fname : $row->email;
        }
        return $result;
    }

    function get_my_patients($key, $value, $from, $where) {
        $result = array();
        $sql = 'SELECT * FROM patient ';
        $array_keys_values = $this->db->query('select ' . $key . ', ' . $value . ' from ' . $from . ' ');
        foreach ($array_keys_values->result() as $row) {
            $result[$row->$key] = $row->$value;
        }
        return $result;
    }

    public function getDoctors_dd($where='', $orderBy='d.lname ASC') {
        $result = array();
        $this->db->select('d.fname, d.lname, d.id');
        $this->db->join('groups g', 'g.id = d.practice_id', 'left');
        $this->db->join('groups g2', 'g2.id = g.parent_id', 'left');
        if ($where != '')
            $this->db->where($where);
        $this->db->order_by($orderBy);
        $query = $this->db->get('doctors d');
        foreach ($query->result() as $row) {
            $result[$row->id] = 'Dr. ' . $row->lname . ', ' . $row->fname;
        }
        return $result;
    }

    public function update($update, $user_id) {
        $this->db->update('doctors', $update, array('user_id'=>$user_id));
    }

    public function addNewDr($data) {
        $this->insert('doctors', $data);
    }


}

