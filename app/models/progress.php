<?php

class Progress extends db_fnc {

    function __construct() {
        parent::__construct();
    }

   

    function getProgressConditions($type = '') {
        $data = array();
        $this->db->select('id, value');
        if(isset($type) && $type) $this->db->where(array('type'=>$type));
        $query = $this->db->get('progress');
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $data[$row['id']] = $row['value'];
            }
        }
        $query->free_result();
        return $data;
    }

    function getConditions($conditions,$table,$where){
        $data['filled'] = array();
        $data['empty'] = array();
        $select = implode(',', $conditions);
        $query = $this->db->select($select)->where($where)->where(array('current'=>'Y'))->get($table);
        if ($query->num_rows() > 0) {
            foreach ($query->row_array() as $k=>$v) {
                if(isset($v) && ($v !== '') && ($v !== 0) && ($v !== '0')){
                    $data['filled'][] = $v;
                } else {
                    $data['empty'][$k] = $v;
                }
            }
        }
        //print_r($data); die;
        return $data;
    }

    
}