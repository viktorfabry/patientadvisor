<?php

class Patient extends CI_Model {

        function  __construct() {
            parent::__construct();
        }
    

    function getAllPatients($where='', $orderBy='p.fname ASC'){
		$data = array();
                $this->db->select(array('u.email','u.username','g.*','p.user_id AS patient_id', 'p.doctor_id','CONCAT(dr.fname," ",dr.lname) AS "doctor"', 'CONCAT(p.fname," ",p.lname) AS "patient"', 'p.address', 'p.city', 'p.postcode', 'p.phone', 'p.NHS'));
                $this->db->join('doctors dr', 'dr.id = p.doctor_id','left');
                $this->db->join('groups g', 'g.id = dr.practice_id','left');
                $this->db->join('users u', 'u.id = p.user_id','left');
		if($where != ''){$this->db->where($where);}
                $this->db->where(array('current'=>'Y'));
                $this->db->order_by($orderBy);
		$query = $this->db->get('patients p');
		if ($query-> num_rows() > 0){
			foreach ($query-> result_array() as $row){
			$data[] = $row;
			}
		}
		$query-> free_result();
		return $data;
	}

        function getPatientLogInfo($id)
	{
                (int) $id;
                $this->db->select(array('p.id AS patient_id','p.fsize','p.imp_measure as imp', 'CONCAT(p.fname," ",p.lname) AS "full_name"'));
		$this->db->where('p.current','Y');
		$this->db->where('p.user_id', $id);
		$query = $this->db->get('patients p');
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}



        function getPatientByID($id){
		$data = array();
                $this->db->select(array('u.email','u.username','p.*','p.user_id AS patient_id', 'CONCAT(dr.fname," ",dr.lname) AS "doctor"', 'CONCAT(p.fname," ",p.lname) AS "patient"'));
                $this->db->join('doctors dr', 'dr.id = p.doctor_id','left');
                $this->db->join('users u', 'u.id = p.user_id','left');
		$this->db->where(array('p.user_id'=>$id,'p.current'=>'Y'));
		$query = $this->db->get('patients p');
		if ($query-> num_rows() > 0){
			$data = $query->row_array();
		}
		$query-> free_result();
		return $data;
	}

         function getPatientData($id){
		$data = array();
                $this->db->select(array('p.dob','p.height','p.gender','p.smoker','p.date_stopped_smoking','p.diabetes',
                                        'p.hypertension','p.IHD','p.ownbpmachine'));
		$this->db->where(array('p.user_id'=>$id,'p.current'=>'Y'));
		$query = $this->db->get('patients p');
		if ($query-> num_rows() > 0){
			$data = $query->row_array();
		}
		$query-> free_result();
		return $data;
	}

   public function getPatients_dd($where = '', $orderBy = 'p.lname ASC') {
	$result = array();
        $this->db->select(array('p.fname', 'p.lname', 'p.user_id'));
        $this->db->join('doctors dr', 'dr.id = p.doctor_id','left');
        $this->db->join('groups g', 'g.id = dr.practice_id','left');
        $this->db->join('users u', 'u.id = p.user_id','left');

        if($where != ''){$this->db->where($where);}
        $this->db->where(array('p.current'=>'Y'));
        $this->db->order_by($orderBy);
        $query = $this->db->get('patients p');
	foreach ($query->result() as $row) {
            $result[$row->user_id]= $row->lname.', '.$row->fname;
        }
        return $result;
    }

    function createPatient($patientData){
        $sql = $this->db->insert_string('patients', $patientData);
        $this->db->query($sql);
        return $this->db->insert_id();
    }

    function archive($id){
        $this->db->where(array('user_id'=>$id,'current'=>'Y'));
        $query = $this->db->get('patients');
        if ($query-> num_rows() > 0){
			$data = $query->row_array();
		}
        $data['current'] = 'N';
        unset($data['id'],$data['modified']);
        $this->db->insert('patients', $data);
    }

    function update($update, $id) {
        $this->db->update('patients', $update, array('user_id'=>$id,'current'=>'Y'));
    }



} //end class