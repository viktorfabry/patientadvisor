<?php

class Content extends db_fnc {

    function __construct() {
        parent::__construct();
        $this->uri_base = 'contents/';
    }

    function getTitles() {
        return $this->getAll('contents', 'title ASC');
    }

    function getContentType($id) {
        (int) $id;
        $this->db->where(array('cat_id'=>$id));
        $query = $this->db->get('contents');
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $data[] = $row;
            }
        }
        $query->free_result();
        return $data;
    }

    function getContentCats($id) {
        (int) $id;
        $this->db->where(array('id'=>$id));
        $query = $this->db->get('content_categories');
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $data[] = $row;
            }
        }
        $query->free_result();
        return $data;
    }

    function getContentLinksByCatId($id) {
        (int) $id;
        $this->db->select(array("cc.cat_name", "c.title", "CONCAT('contents/', c.url) as url"));
        $this->db->join('content_categories cc', 'cc.id = c.cat_id');
        $this->db->where(array('cc.parent'=>$id));
        $query = $this->db->get('contents c');
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $data[$row['cat_name']][] = $row;
            }
        }
        //print_r($data); die;
        $query->free_result();
        return $data;
    }

    function getContentByLink($link){
        $this->db->select(array('cc.cat_name','c.*'));
        $this->db->join('content_categories cc', 'cc.id = c.cat_id');
        $this->db->where(array('c.url'=>$link));
        $this->db->limit(1);
        $query = $this->db->get('contents c');
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
        }
        $query->free_result();
        return $result;
    }
}