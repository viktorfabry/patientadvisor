<?php

class Xlink extends db_fnc {

    function __construct() {
        parent::__construct();
    }

    
    function getLinks ($linkCat_id=null, $practice_id=null, $gpCom_id=null, $orderBy=null) {
        $result = array(
                            'items' => array(),
                            'parents' => array()
                    );
        $this->db->select(array('id as link_id','name','url','parent_id'));
        if($linkCat_id!==null) $this->db->where(array('linkCat_id'=>$linkCat_id));
        if($practice_id!==null) $this->db->where(array('practice_id'=>$practice_id));
        if($gpCom_id!==null) $this->db->where(array('gpCom_id'=>$gpCom_id));
        if($orderBy!==null){
            $this->db->order_by($orderBy);
        } else {
            $this->db->order_by('name ASC');
        }
        $query =  $this->db->get('links');
       foreach ($query->result_array() as $row)
        {
               $result['items'][$row['link_id']] = $row;
               $result['parents'][$row['parent_id']][] = $row['link_id'];
        }
        return $result;

    }
    
} //end class