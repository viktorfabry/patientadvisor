<?php

class Group extends Db_fnc {

        function  __construct() {
            parent::__construct();
        }
    
        function getGroupByID($id){
		$data = array();
                $this->db->select(array('g.*','g2.name as localAuth'));
                $this->db->join('groups g2', 'g2.id = g.parent_id','left');
		$this->db->where(array('g.id'=>$id));
		$query = $this->db->get('groups g');
		if ($query-> num_rows() > 0){
			$data = $query->row_array();
		}
		$query-> free_result();
		return $data;
	}

        function getGroup_dd($type,$parent= '',$orderBy = 'name ASC'){
            $data = array();
                $this->db->select(array('g.id','g.name'));
                $this->db->where(array('g.group_type_id'=>$type));
                if($parent){
                    $this->db->where(array('g.parent_id'=>$parent));
                }

		$query = $this->db->get('groups g');
		if ($query-> num_rows() > 0){
                    foreach ($query->result_array() as $row) {
                         $data[$row['id']] = $row['name'];
                    }
		}
		$query-> free_result();
		return $data;
        }
    


} //end class