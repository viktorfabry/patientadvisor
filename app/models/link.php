<?php

class Link extends db_fnc {

    function __construct() {
        parent::__construct();
    }

    function getLinks($where, $orderBy = '') {
        $this->db->select(array('id as link_id','name','url','parent_id'));
        if($where != ''){$this->db->where($where);}
        if ($orderBy != ''){$this->db->order_by($orderBy); }
        $query = $this->db->get('links');
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $data[] = $row;
            }
        }
        $query->free_result();
        return $data;
    }

    function getLinks_dd($where, $orderBy = '') {
        $this->getKeyValueArray('link_id', 'name','patientlinks',$where,$orderBy);
    }

}

