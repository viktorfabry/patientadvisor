<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Chart_html {
		public $html = '';	
    function Chart_html($params)
    {
		$CI =& get_instance();
		$XML_URL = $params['XML_URL'];
		$width = $params['width'];
		$height = $params['height'];
		
	$this->html .= '
				<script language="JavaScript" type="text/javascript">
				<!--
				var requiredMajorVersion = 10;
				var requiredMinorVersion = 0;
				var requiredRevision = 45;
				-->
				</script>
		';
		
		$this->html .= "

				<script language='JavaScript' type='text/javascript'>
				<!--
				if (AC_FL_RunContent == 0 || DetectFlashVer == 0) {
					alert('This page requires AC_RunActiveContent.js.');
				} else {
					var hasRightVersion = DetectFlashVer(requiredMajorVersion, requiredMinorVersion, requiredRevision);
					if(hasRightVersion) {  // if we've detected an acceptable version
						// embed the flash movie
						AC_FL_RunContent(
							'codebase', 'http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,45,2',
							'width', '".$width."',
							'height', '".$height."',
							'bgcolor', '#000',
							'movie', 'charts',
							'src', '".base_url()."charts',
							'FlashVars', 'library_path=".base_url()."charts_library&xml_source=".base_url()."chart/xml/1', 
							'wmode', 'opaque',
							'scale', 'noScale',
							'id', 'charts',
							'name', 'charts',
							'menu', 'true',
							'allowFullScreen', 'true',
							'allowScriptAccess','sameDomain',
							'quality', 'high',
							'pluginspage', 'http://www.macromedia.com/go/getflashplayer',
							'align', 'middle',
							'play', 'true',
							'devicefont', 'false',
							'salign', 'TL'
							); //end AC code
					} else {  // flash is too old or we can't detect the plugin
						var alternateContent = '<font color=\"4444ff\"><P>This content requires the latest version of Adobe Flash Player. '
							+ '<u><a href=http://www.macromedia.com/go/getflash/>Get Flash</a></u>.</p><P>Problems installing the Flash Player? See <u><a href=index.php?menu=Player_Troubleshooting>Troubleshooting</a></u>.</font>';
						document.write(alternateContent);  // insert non-flash content
					}
				}
				// -->
				</script>
				<noscript>
					<p>This content requires JavaScript.</p>
				</noscript>
		";
		
    }
}


?>