<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Form_validation extends CI_Form_validation
{
    var $_error_prefix = '<span class="errorMsg">';
    var $_error_suffix = '</span>';

 function run($module = '', $group = '') {
 (is_object($module)) AND $this->CI =& $module;
 return parent::run($group);
 }

 function imp_length($impLength)
{
  $this->set_message('imp_length', "The value must be in the following formats:<br /> *ft *in <br /> *in");

  return (preg_match('/^[a-zA-Z0-9\'\"]{2,16}/', $impLength)) ? TRUE : FALSE;
}


//[^0-9\/\.\+\*\-]/

}
/* End of file MY_Form_validation.php */
/* Location: ./application/libraries/MY_Form_validation.php */