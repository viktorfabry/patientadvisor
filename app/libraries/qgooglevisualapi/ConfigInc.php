<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ConfigInc {
	
	function ConfigInc($className=null) {
		

//function __autoload($className=null) {
	$CI =& get_instance();
	$classes = array (
        'QConfig.class.php', 
        'QInflector.class.php', 
        'QTool.class.php', 
        'QGoogleGraph.class.php', 
        'QVizualisationGoogleGraph.class.php', 
        'QApikeyGoogleGraph.class.php', 
		'QAnnotatedtimelineGoogleGraph.class.php',
	);
	

	foreach($classes as $class) {
		 include_once($class);
		  
	}

	if ($className != 'CI_DB' && $className != '') {
		include_once($className.".class.php");
		}

}
}

?>