<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class App_functions {

    function __construct() {
        $ci =& get_instance();
    }

    function imperial2metric($number, $unit = 'm') {
        // Get rid of whitespace on both ends of the string.
        $number = trim($number);

        // This results in the number of feet getting multiplied by 12 when eval'd
        // which converts them to inches.
        $number = str_replace("'", '*12', $number);

        // We don't need the double quote.
        $number = str_replace('"', '', $number);

        // Convert other whitespace into a plus sign.
        $number = preg_replace('/\s+/', '+', $number);

        // Make sure they aren't making us eval() evil PHP code.
        if (preg_match('/[^0-9\/\.\+\*\-]/', $number)) {
            return false;
        } else {
            $inches = eval("return ($number);");

            switch ($unit) {
                case "inch":
                    return $inches;
                    break;
                case "cm":
                    return $meters = $inches * 2.54;
                    break;
                default;
                    return $meters = $inches * 0.0254;
                    break;
            }
        }
    }



    function setFontSize($fsize) {
        (int) $fsize;
        $ci =& get_instance();
        $ci->load->library('session');
        $ci->session->set_userdata(array('fsize' => $fsize));
    }

    function setImpMeasure($imp) {
        $imp = ($imp == 'Y') ? $imp : 'N';
        $ci =& get_instance();
        $ci->load->library('session');
        $ci->session->set_userdata(array('imp' => $imp));
    }

    function getWeightMeasure() {
        $ci =& get_instance();
        $ci->load->library('session');
        return ($ci->session->userdata('imp') == 'Y') ? 'lb' : 'kg';
    }

    function getLengthMeasure() {
        $ci =& get_instance();
        $ci->load->library('session');
        return ($ci->session->userdata('imp') == 'Y') ? 'inch' : 'cm';
    }

    function getCorrectLengthValue($value){
       $ci =& get_instance();
       $ci->load->library('session');
       return  ($ci->session->userdata('imp') == 'Y') ? metric2imperial($value) : $value;
    }


    function getProfileProgress($user_id){
        $ci =& get_instance();
        $ci->load->model('progress');
        $conditions = $ci->progress->getProgressConditions('profile');
        $cond = $ci->progress->getConditions($conditions,'patients',array('user_id'=>$user_id));
        $percentage = round(count($cond['filled']) / count($conditions) * 100);
        return $percentage;
    }





}

?>