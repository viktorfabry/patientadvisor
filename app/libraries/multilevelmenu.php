<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//include_once(dirname(__FILE__).'/Links.php');

class Multilevelmenu {
		public $out = array();
                protected $menulinks = array();
                protected  $flink;
                protected $output;
                protected $class;


function __construct($menulinks) {
                
		$this->menulinks = $menulinks;
		$this->class = 'dropdown';
                
}

public function setFlink($fLink){
		 $this->flink = $fLink;		
}

public function setClass($class){
		 $this->class = $class;
}

public function getMenu(){

		 $this->output = $this->_buildMenu(0, $this->menulinks, $level = 0);
                 return  $this->output;


}

private function _buildMenu($parentId, $menulinks, $level = 0) {
    $CI =& get_instance();
    $link = $CI->load->helper('linkgenerator');
    $html = '';
//print_r($menuData['parents'][$parentId]); die;

    if (isset($menulinks['parents'][$parentId]))
    {
        if($level == 0){
			$html = "<ul class=\"".$this->class."\"><li>
			<a class=\"flink curves5\" href=\"#\">".$this->flink."</a><ul>\n";
		} else {
			$html = "<ul>\n";
		}
		$level++;
        foreach ($menulinks['parents'][$parentId] as $itemId)
        {
            $html .= "<li>\n";
			
			$html .= createLink($menulinks['items'][$itemId]['url'],$menulinks['items'][$itemId]['name']);
			
			
            $html .= $this->_buildMenu($itemId, $menulinks, $level);
            $html .= "</li>\n";
        }
        
		$level--;
		
		if($level == 0){
			$html .= "</ul></li>\n</ul>\n";
		} else {
			$html .= "</ul>\n";
		}
		
    }

    return $html;
}



		
}


?>