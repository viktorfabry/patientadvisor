<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tools {

    public static function cccCss() {
        global $css_files;
        //inits
        $css_files_by_media = array();
        $compressed_css_files = array();
        $compressed_css_files_not_found = array();
        $compressed_css_files_infos = array();
        $protocolLink = Tools::getCurrentUrlProtocolPrefix();

        // group css files by media
        foreach ($css_files as $filename => $media) {
            if (!array_key_exists($media, $css_files_by_media))
                $css_files_by_media[$media] = array();

            $infos = array();
            $infos['uri'] = $filename;
            $url_data = parse_url($filename);
            $infos['path'] = (_SYSPATH_) ? _SYSPATH_ . Tools::str_replace_once(_SYSPATH_, '', $url_data['path']) : $url_data['path'];
            $css_files_by_media[$media]['files'][] = $infos;
            if (!array_key_exists('date', $css_files_by_media[$media]))
                $css_files_by_media[$media]['date'] = 0;
            $css_files_by_media[$media]['date'] = max(
                            file_exists($infos['path']) ? filemtime($infos['path']) : 0,
                            $css_files_by_media[$media]['date']
            );

            if (!array_key_exists($media, $compressed_css_files_infos))
                $compressed_css_files_infos[$media] = array('key' => '');
            $compressed_css_files_infos[$media]['key'] .= file_get_contents($filename);
        }

        // get compressed css file infos
        foreach ($compressed_css_files_infos as $media => &$info) {
            $key = md5($info['key'] . $protocolLink);
            $filename = _SYSPATH_ . 'cache/' . $key . '_' . $media . '.css';
            $info = array(
                'key' => $key,
                'date' => file_exists($filename) ? filemtime($filename) : 0
            );
        }
        // aggregate and compress css files content, write new caches files
        foreach ($css_files_by_media as $media => $media_infos) {
            $cache_filename = 'cache/' . $compressed_css_files_infos[$media]['key'] . '_' . $media . '.css';
            if ($media_infos['date'] > $compressed_css_files_infos[$media]['date']) {
                $compressed_css_files[$media] = '';
                foreach ($media_infos['files'] as $file_infos) {
                    if (file_exists($file_infos['path']))
                        $compressed_css_files[$media] .= Tools::minifyCSS(file_get_contents($file_infos['path']), $file_infos['uri']);
                    else
                        $compressed_css_files_not_found[] = $file_infos['path'];
                }
                if (!empty($compressed_css_files_not_found))
                    $content = '/* WARNING ! file(s) not found : "' .
                            implode(',', $compressed_css_files_not_found) .
                            '" */' . "\n" . $compressed_css_files[$media];
                else
                    $content = $compressed_css_files[$media];
                file_put_contents($cache_filename, $content);
                chmod($cache_filename, 0777);
            }
            $compressed_css_files[$media] = $cache_filename;
        }

        // rebuild the original css_files array
        $css_files = array();
        foreach ($compressed_css_files as $media => $filename) {
            $css_files[base_url() . $filename] = $media;
        }
    }

    public static function minifyCSS($css_content, $fileuri = false) {
        global $current_css_file;

        $current_css_file = $fileuri;
        if (strlen($css_content) > 0) {
            $css_content = preg_replace('#/\*.*?\*/#s', '', $css_content);

            $css_content = preg_replace('#\s+#', ' ', $css_content);
            $css_content = str_replace("\t", '', $css_content);
            $css_content = str_replace("\n", '', $css_content);

            $css_content = str_replace('; ', ';', $css_content);
            $css_content = str_replace(': ', ':', $css_content);
            $css_content = str_replace(' {', '{', $css_content);
            $css_content = str_replace('{ ', '{', $css_content);
            $css_content = str_replace(', ', ',', $css_content);
            $css_content = str_replace('} ', '}', $css_content);
            $css_content = str_replace(' }', '}', $css_content);
            $css_content = str_replace(';}', '}', $css_content);
            $css_content = str_replace(':0px', ':0', $css_content);
            $css_content = str_replace(' 0px', ' 0', $css_content);
            $css_content = str_replace(':0em', ':0', $css_content);
            $css_content = str_replace(' 0em', ' 0', $css_content);
            $css_content = str_replace(':0pt', ':0', $css_content);
            $css_content = str_replace(' 0pt', ' 0', $css_content);
            $css_content = str_replace(':0%', ':0', $css_content);
            $css_content = str_replace(' 0%', ' 0', $css_content);

            return trim($css_content);
        }
        return false;
    }

    /**
     * addCSS allows you to add stylesheet at any time.
     *
     * @param mixed $css_uri
     * @param string $css_media_type
     * @return true
     */
    public static function addCSS($css_uri, $css_media_type = 'all') {
        global $css_files;

        if (is_array($css_uri)) {
            foreach ($css_uri as $file => $media_type)
                Tools::addCSS($file, $media_type);
            return true;
        }

        // detect mass add
        $css_uri = array($css_uri => $css_media_type);

        // adding file to the big array...
        if (is_array($css_files))
            $css_files = array_merge($css_files, $css_uri);
        else
            $css_files = $css_uri;

        return true;
    }

    public static function showCss() {
        global $css_files;

        if (count($css_files) < 1) {
            Tools::addCSS(_SYSPATH_ . '/css/style.css', 'all'); //default
        }

        // echo _SYSPATH_.'cache'; die;

        if (_CSS_COMPILE_ === true && is_writable(_SYSPATH_ . 'cache')) {
            Tools::cccCss();
        }

        foreach ($css_files as $id => $key) {

            $uri = str_replace(_SYSPATH_, '', $id);
            echo '<link href="' . $uri . '" rel="stylesheet" type="text/css" media="' . $key . '" />';
        }
    }

    /**
     * Check if the current page use SSL connection on not
     */
    public static function usingSecureMode() {
        return!(empty($_SERVER['HTTPS']) OR strtolower($_SERVER['HTTPS']) == 'off');
    }

    /**
     * Get the current url prefix protocole (https/http)
     */
    public static function getCurrentUrlProtocolPrefix() {
        if (Tools::usingSecureMode())
            return 'https://';
        else
            return 'http://';
    }

    public static function str_replace_once($needle, $replace, $haystack) {
        $pos = strpos($haystack, $needle);
        if ($pos === false)
            return $haystack;
        return substr_replace($haystack, $replace, $pos, strlen($needle));
    }

    /**
     * Combine Compress and Cache (ccc) JS calls
     *
     */
    public static function cccJS() {
        global $js_files;
        $protocolLink = Tools::getCurrentUrlProtocolPrefix();
        //inits
        $compressed_js_files_not_found = array();
        $js_files_infos = array();
        $js_files_date = 0;
        $compressed_js_file_date = 0;
        $compressed_js_filename = '';

        // get js files infos
        foreach ($js_files as $filename) {
            $infos = array();
            $infos['uri'] = $filename;
            $url_data = parse_url($filename);
            $infos['path'] = _SYSPATH_ . str_replace(_SYSPATH_, '/', $url_data['path']);
            $js_files_infos[] = $infos;

            $js_files_date = max(
                            file_exists($infos['path']) ? filemtime($infos['path']) : 0,
                            $js_files_date
            );
            $compressed_js_filename .= file_get_contents($filename);
        }

        // get compressed js file infos
        $compressed_js_filename = md5($compressed_js_filename);

        $compressed_js_path = _SYSPATH_ . 'cache/' . $compressed_js_filename . '.js';
        $compressed_js_file_exist = file_exists($compressed_js_path);


        // aggregate and compress js files content, write new caches files
        if (!$compressed_js_file_exist) {
            $content = '';
            foreach ($js_files_infos as $file_infos) {
                if (file_exists($file_infos['path']))
                    $content .= file_get_contents($file_infos['path']) . ';';
                else
                    $compressed_js_files_not_found[] = $file_infos['path'];
            }
            $content = Tools::packJS($content);

            if (!empty($compressed_js_files_not_found))
                $content = '/* WARNING ! file(s) not found : "' .
                        implode(',', $compressed_js_files_not_found) .
                        '" */' . "\n" . $content;
            else
                $content = $content;
            file_put_contents($compressed_js_path, $content);
            chmod($compressed_js_path, 0777);
        }

        // rebuild the original js_files array
        $url = str_replace(_SYSPATH_, '', $compressed_js_path);
        $js_files = array(base_url() . $url);
    }

    /**
     * addJS load a javascript file in the header
     *
     * @param mixed $js_uri
     * @return void
     */
    public static function addJS($js_uri) {
        global $js_files;

        ksort($js_files);

        // avoid useless operation...
        if (in_array($js_uri, $js_files))
            return true;

        // detect mass add
        if (!is_array($js_uri))
            $js_uri = array($js_uri);

        // adding file to the big array...
        $js_files = array_merge($js_files, $js_uri);

        return true;
    }

    /**
     * getHttpHost return the <b>current</b> host used, with the protocol (http or https) if $http is true
     * This function should not be used to choose http or https domain name.
     * Use Tools::getShopDomain() or Tools::getShopDomainSsl instead
     *
     * @param boolean $http
     * @param boolean $entities
     * @return void
     */
    public static function getHttpHost($http = false, $entities = false) {
        $host = (isset($_SERVER['HTTP_X_FORWARDED_HOST']) ? $_SERVER['HTTP_X_FORWARDED_HOST'] : $_SERVER['HTTP_HOST']);
        if ($entities)
            $host = htmlspecialchars($host, ENT_COMPAT, 'UTF-8');
        if ($http)
            $host = (Configuration::get('PS_SSL_ENABLED') ? 'https://' : 'http://') . $host;
        return $host;
    }

    public static function packJS($js_content) {
        if (strlen($js_content) > 0) {
            require_once(APPPATH . 'helpers/jsmin.php');
            return JSMin::minify($js_content);
        }
        return false;
    }

    public static function packJSinHTML($html_content) {
        if (strlen($html_content) > 0) {
            $html_content = preg_replace_callback(
                            '/\\s*(<script\\b[^>]*?>)([\\s\\S]*?)(<\\/script>)\\s*/i'
                            , array('Tools', 'packJSinHTMLpregCallback')
                            , $html_content);
            return $html_content;
        }
        return false;
    }

    public static function packJSinHTMLpregCallback($preg_matches) {
        $preg_matches[1] = $preg_matches[1] . '/* <![CDATA[ */';
        $preg_matches[2] = self::packJS($preg_matches[2]);
        $preg_matches[count($preg_matches) - 1] = '/* ]]> */' . $preg_matches[count($preg_matches) - 1];
        unset($preg_matches[0]);
        $output = implode('', $preg_matches);
        return $output;
    }

    public static function showJS() {
        global $js_files;

        if (_JS_COMPILE_ === true && is_writable(_SYSPATH_ . 'cache')) {
            Tools::cccJS();
        }
        //print_r($js_files); die;
        foreach ($js_files as $val) {
            $js_uri = str_replace(_SYSPATH_, '', $val);
            echo '<script type="text/javascript" src="' . $js_uri . '"></script>';
        }
    }




}

?>