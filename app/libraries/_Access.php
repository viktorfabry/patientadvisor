<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Access extends Tank_auth
{
	private $error = array();
        private $hasAccess = FALSE;
        private $page = '';

	function __construct()
	{
		$this->ci =& get_instance();


                    $this->ci->load->library('session');
                    $this->ci->load->database();
                    $this->ci->load->model('tank_auth/users');
                    $this->user['user_id'] = $this->get_user_id();

		// Try to autologin
	}
        function getPage($page) {
            $this->page = $page;
	}

	function hasAccess($page = '',$user_id = '') {
            $page = ($page=='') ? $this->ci->uri->segment(3) : $page;
            $user_id = ($user_id=='') ? $this->user['user_id'] : $user_id;

           return $this->ci->users->hasAccessToPageByID($page,$user_id);
	}

        function authorisedLinks($links) {
          // print_r(array_intersect($links,$this->access)); die;

            return array_intersect($links,$this->access);
	}

        function selected($link) {
            return ($link == $this->page) ? 'selected' : '';
	}

}

/* End of file Tank_auth.php */
/* Location: ./application/libraries/Tank_auth.php */