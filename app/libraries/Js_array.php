<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Js_array {

	function __construct() {
		$CI =& get_instance();
	}
	
	function get_js_array($array) {
	//print_r($array); die;
	
	$js_array = '[';
	foreach ($array as $value) {
		if(is_array($value)) {
			$js_array .= '[';
			$js_array .= implode(',', $value);
			$js_array .= "],";
		} else {
			$js_array .= '[';
			$js_array .= implode(',', $array);
			$js_array .= '],';
		}
	}

	return substr($js_array, 0, -1).']';
	//echo $this->out; die;
	//return $this->out;
	}
	
	function get_js_object($array) {
	//print_r($array); die;
	
	$js_array = '{';
	foreach ($array as $value) {
		if(is_array($value)) {
			$js_array .= '[';
			$js_array .= implode(',', $value);
			$js_array .= "},";
		} else {
			$js_array .= '{';
			$js_array .= implode(',', $array);
			$js_array .= '},';
		}
	}

	return substr($js_array, 0, -1).']';
	//echo $this->out; die;
	//return $this->out;
	}
}

?>