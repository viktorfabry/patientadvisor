<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends MX_Controller {

    function __construct() {
        parent::__construct();

//        if (!$this->tank_auth->is_logged_in()) {
//            redirect('/auth/login/');
//        } else {
            $this->module = 'bloodpressure';
            $this->user_id = $this->tank_auth->get_user_id();
            $this->username = $this->tank_auth->get_username();
            $this->role_id = $this->session->userdata('role');
//        }
    }

    function index() {
        $data['user_id'] = $this->tank_auth->get_user_id();
        $data['username'] = $this->tank_auth->get_username();
        $data['msg'] = modules::run('msg');

        $data['header'] = modules::run('header');
        $data['topBar'] = ($this->access->isDoctor($this->role_id)) ? modules::run('drbar') : modules::run('patientbar');
        $data['head'] = modules::run('head');
        $data['footer'] = modules::run('footer');

        $this->load->view('home_view', $data);
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */