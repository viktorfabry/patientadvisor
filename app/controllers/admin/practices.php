<?php

class Practices extends MX_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->tank_auth->is_logged_in()) {
            redirect('adminauth/login/');
        } else {
            $this->module = 'admin/practices';
            $this->user_id = $this->tank_auth->get_user_id();
            $this->username = $this->tank_auth->get_username();
            $this->role_id = $this->session->userdata('role');
            $this->group_id = $this->session->userdata('group_id');
            $this->load->model('localauth');
            $this->load->model('tank_auth/admins', 'admins');
            $this->load->model('practice');
            $this->load->model('patient');
        }
    }

    function index() {
        if ($this->access->has_access($this->role_id, $this->module)) {
            $data = array();
            if ($this->admins->isSysAdmin($this->role_id)) {
                $practices = $this->practice->getPractices();
            } elseif ($this->admins->isLocalAuth($this->role_id)) {
                $practices = $this->practice->getPractices($this->group_id);
            }
            $data['practices'] = (isset($practices) && $practices) ? $practices : array();
            //$data['mainactive'] = 'admin/practices';
            $this->load->view('admin/practices_view', $data);
        }
    }

    function add() {
        if ($this->access->has_access($this->role_id, $this->module)) {
            $gpcom_id = $this->input->post('gpcom');
            $name = $this->input->post('name');
            $description = $this->input->post('description');


            if ($this->admins->isSysAdmin($this->role_id)) {
                $data['admin'] = TRUE;
                $data['gpcoms'] = array('Please select a GP Commissioning');
                $data['gpcoms'] += $this->localauth->getLocalAuth_dd();
                $data['gpcom_id'] = 0;
            } elseif ($this->admins->isLocalAuth($this->role_id)) {
                $data['admin'] = FALSE;
                $gpcom_id = $this->group_id;
            }





            if ($name) {
                $ins = $this->practice->addPractice(array('parent_id' => $gpcom_id, 'name' => $name, 'description' => $description));

                $data['title'] = "New GP Practice";
                $data['msg'] = anchor('admin/practice', 'OK', 'class="fancyClose btn orange big"');
                $this->load->view('msg_pop', $data);
            } else {
                $this->load->view('admin/add_practice_pop', $data);
            }
        }
    }

    function edit($id = '') {
        (int) $id;
        if ($this->access->has_access($this->role_id, $this->module)) {

            $practice_id = $this->input->post('practice_id');

            $name = $this->input->post('name');
            $description = $this->input->post('description');

            if ($name) {
                if ($this->admins->isSysAdmin($this->role_id)) {
                    $gpcom_id = $this->input->post('gpcom');
                } else {
                    $gpcom_id = $this->group_id;
                }
                $ins = $this->db_fnc->update('groups', array('name' => $name, 'description' => $description, 'parent_id' => $gpcom_id), array('id' => $practice_id));
                $data['title'] = "Edit GP Practice";
                $data['msg'] = anchor('admin/practices', 'OK', 'class="fancyClose btn orange big"');
                $this->load->view('msg_pop', $data);
            } else {
                $practice = $this->practice->getPracticeById($id);
                if ($this->admins->isSysAdmin($this->role_id)) {
                    $data['admin'] = TRUE;
                    $data['gpcoms'] = array('Please select a GP Commissioning');
                    $data['gpcoms'] += $this->localauth->getLocalAuth_dd();
                } else {
                    $data['admin'] = FALSE;
                }
                $data['practice_id'] = $id;
                $data['gpcom_id'] = $practice['parent_id'];
                $data['name'] = $practice['name'];
                $data['description'] = $practice['description'];
                $this->load->view('admin/edit_practice_pop', $data);
            }
        }
    }

    function delete($id) {
        (int) $id;
        if ($this->access->has_access($this->role_id, $this->module)) {
            $gpcom = $this->practice->deletePractice($id);
            redirect('admin/practices');
        }
    }

    function getPracticeNames($gpCom_id=null, $format='json') {
        if ($this->access->has_access($this->role_id, $this->module)) {
            $this->load->model('practice');
            if ($format == 'json') {
                $practices = array();
                $p = $this->practice->getPracticeNames($gpCom_id);

                if (is_array($p)) {
                    //$practice;
                    (object) $practice->practice_id = 0;
                    $practice->name = 'Please select a practice';
                    $ps[] = $practice;

                    $practices = array_merge($ps, $p);
                } else {
                    $practices = $p;
                }


                $result = json_encode($practices);
            } elseif ($format == 'html') {
                $result = $this->practice->getPracticeNames($gpCom_id);
            } else {
                $result = json_encode(array('error' => 'Pramaters are not valid'));
            }
            echo $result;
        }
    }

}

