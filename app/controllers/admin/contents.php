<?php

class Contents extends MX_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->tank_auth->is_logged_in()) {
            redirect('adminauth/login/');
        } else {
            $this->module = 'admin/contents';
            $this->user_id = $this->tank_auth->get_user_id();
            $this->username = $this->tank_auth->get_username();
            $this->role_id = $this->session->userdata('role');
            $this->load->model('content');
            $this->uri_base = 'contents/';
        }
    }

    function index() {
        if ($this->access->has_access($this->role_id, $this->module)) {
            $contents = $this->content->getTitles();
            $data['contents'] = $contents;

            $this->load->view('admin/contents_view', $data);
        }
    }

    function newcontent() {
        if ($this->access->has_access($this->role_id, $this->module)) {
            $data = array();
            $admin = $this->session->userdata('user_id');
            $cat_id = 0;
            $data['cat_id'] = $cat_id;
            $data['categories'] = array('0' => 'Please select a category');

            $data['categories'] += $this->db_fnc->getKeyValueArray('id', 'cat_name', 'content_categories');

            $this->load->view('admin/content_add_view', $data);
        }
    }

    function edit($content_id) {
        if ($this->access->has_access($this->role_id, $this->module)) {

            $c = $this->db_fnc->getRow('id', $content_id, 'contents');
            $data['content'] = $c;
            $data['content_id'] = $content_id;

            $data['cat_id'] = ($c['cat_id']) ? $c['cat_id'] : 0;
            $data['categories'] = array('0' => 'Please select a category');

            $data['categories'] += $this->db_fnc->getKeyValueArray('id', 'cat_name', 'content_categories');
            $this->load->view('admin/content_edit_view', $data);
        }
    }

    function add() {
        if ($this->access->has_access($this->role_id, $this->module)) {
            $data = array();

            $url = ($this->input->post('url')) ? $this->input->post('url') : url_title(strtolower($this->input->post('title')),'underscore');

            $admin = $this->session->userdata('user_id');
            $tablename = 'contents';
            $fieldarray = array(
                'cat_id' => $this->input->post('categories'),
                'title' => $this->input->post('title'),
                'url' => $url,
                'tags' => $this->input->post('tags'),
                'body' => $this->input->post('content')
            );
            $content_id = $this->db_fnc->insert($tablename, $fieldarray);

            redirect('admin/contents');
        }
    }

    function update($content_id) {
        if ($this->access->has_access($this->role_id, $this->module)) {
            $data = array();

            $admin = $this->session->userdata('user_id');
            $url = ($this->input->post('url')) ? $this->input->post('url') : url_title(strtolower($this->input->post('title')),'underscore');


            $table = 'contents';
            $update = array(
                'cat_id' => $this->input->post('categories'),
                'title' => $this->input->post('title'),
                'url' => $this->input->post('url'),
                'tags' => $this->input->post('tags'),
                'body' => $this->input->post('content')
            );
            $where = array('id' => $content_id);
            $this->db_fnc->update($table, $update, $where);

            redirect('admin/contents/edit/' . $content_id . '/');
        }
    }

    function delete($content_id) {
        if ($this->access->has_access($this->role_id, $this->module)) {
            $this->content->delete('contents', 'id', $content_id);
            redirect('admin/contents');
        }
    }

}

