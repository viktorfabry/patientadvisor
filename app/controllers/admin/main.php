<?php

class Main extends MX_Controller {

    function __construct() {
        parent::__construct();

//        if (!$this->tank_auth->is_logged_in()) {
//            redirect('adminauth/login/');
//        } else {
            $this->module = 'admin/main';
            $this->user_id = $this->tank_auth->get_user_id();
            $this->username = $this->tank_auth->get_username();
            $this->role_id = $this->session->userdata('role');
     //   }
    }

    function index() {
     //   if ($this->access->has_access($this->role_id, $this->module)) {
            $data = array();
            $data['main_menu'] = $this->access->getModulesForRole($this->role_id, 'admin_menu');
            $data['active'] = $this->router->fetch_class();
            $this->load->view('admin/admin_view', $data);
//        } else {
//            $this->session->set_flashdata('errors', 'no access rights.');
//            redirect('adminauth/login/');
//        }
    }

    function phpInfo() {
        if ($this->access->has_access($this->role_id, $this->module)) {
            phpinfo();
        }
    }

}

