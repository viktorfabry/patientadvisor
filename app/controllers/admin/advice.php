<?php

class Advice extends MX_Controller {

	function  __construct() {
            parent::__construct();

            if (!$this->tank_auth->is_logged_in()) {
			redirect('adminauth/login/');
            } else {
                $this->module   = 'admin/advice';
                $this->user_id	= $this->tank_auth->get_user_id();
                $this->username	= $this->tank_auth->get_username();
                $this->role_id  = $this->session->userdata('role');
                
            }
         }
	
	function index() {
            redirect('admin/advice/edit/');
	}
	
	
	function edit(){
            if($this->access->has_access($this->role_id ,$this->module)){
		$data['advice'] = $this->db_fnc->getAll('advice_bp');
		$this->load->view('admin/editAdvice_view',$data);
            }
	}
	
	function saveAdvice($id){
            if($this->access->has_access($this->role_id ,$this->module)){
		$update = $_POST;
		$where = array('id' => $id);
		$data['update'] = $this->db_fnc->update('advice_bp',$update,$where);
		redirect('admin/advice/edit');
            }
	}
	
	function newAdvice(){
            if($this->access->has_access($this->role_id ,$this->module)){
		$insert = $_POST;
		$data['insert'] = $this->db_fnc->insert('advice_bp',$insert);
		redirect('admin/advice/edit');
            }
	}
	
	function _goAdminLogin () {
		$this->load->view('admin/adminlogin_view',$data);	
	}
}
