<?php

class Rolesaccess extends MX_Controller {

	function  __construct() {
            parent::__construct();

            if (!$this->tank_auth->is_logged_in()) {
			redirect('adminauth/login/');
            } else {
                $this->module   = 'admin/rolesaccess';
                $this->user_id	= $this->tank_auth->get_user_id();
                $this->username	= $this->tank_auth->get_username();
                $this->role_id  = $this->session->userdata('role');
            }
         }


        function index(){
            if($this->access->has_access($this->role_id ,$this->module)){
                        if($this->input->post('refresh_modules')){
                            $this->_refreshModules();
                        }
                        if($this->input->post('submit_access')){
                            $this->_setAccess();
                        }
                        
                        $data['user_id']	= $this->user_id;
                        $data['username']	= $this->username;

                        $data['rolesList'] = array('Please select a role');
                        $data['rolesList'] += $this->db_fnc->getKeyValueArray('role_id','name','roles');

                        $modules = $this->db_fnc->getAll('modules','type ASC');
                        $data['modules'] = $modules;

                        $selectRole_id = ($this->input->post('role')) ? (int) $this->input->post('role') : (int) $this->input->post('accessRole_id');
                        $data['selectRole_id'] = ($selectRole_id) ? $selectRole_id : 0;

                        $this->roleModules  = $this->db_fnc->getKeyValueArray('module_id','module_id','modules_roles', array('role_id'=>$selectRole_id));
                        $data['roleModules'] = $this->roleModules;
                            //print_r($_POST);
                        //print_r($data);
                        
                           
                       $this->load->view('admin/roles_view', $data);
            }
                  
	}

        function _refreshModules(){
           if($this->access->has_access($this->role_id ,$this->module)){
                $modules = $this->db_fnc->getKeyValueArray('module_id','link','modules');
               

                foreach(glob(APPPATH . 'controllers/*' . EXT) as $controller) {
                        $clink = basename($controller, EXT);
                        $controllers[] = $clink;
                }
                foreach(glob(APPPATH . 'controllers/admin/*' . EXT) as $controller) {
                        $clink = basename($controller, EXT);
                        $controllers[] = 'admin/'.$clink;
                }
               foreach(glob(APPPATH . 'modules/*/controllers/*' . EXT) as $controller) {
                        $clink = basename($controller, EXT);
                        $controllers[] = $clink;
                }


                $new_controllers = array_diff($controllers, $modules);

                foreach($new_controllers as $new){
                    $this->db_fnc->insert('modules',array('link'=>$new,'name'=>str_replace('_',' ',$new)));
                }
           }
        }


        function _setAccess(){
             $access = $this->input->post('access');
             $accessRole_id = (int) $this->input->post('accessRole_id');
             $this->roleModules  = $this->db_fnc->getKeyValueArray('module_id','module_id','modules_roles', array('role_id'=>$accessRole_id));

             $delete_modules = array_diff($this->roleModules,$access);
             foreach($delete_modules as $module){
                    $this->db->delete('modules_roles',array('module_id'=>$module,'role_id'=>$accessRole_id));
                }

             $new_modules = array_diff($access, $this->roleModules);
             //print_r($new_modules); die;
             foreach($new_modules as $module){
                    $this->db_fnc->insert('modules_roles',array('module_id'=>$module,'role_id'=>$accessRole_id));
                }

             

        }

	
	
	
	
	
}

