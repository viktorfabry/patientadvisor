<?php

class Lipid_advice extends MX_Controller {

	function  __construct() {
            parent::__construct();

            if (!$this->tank_auth->is_logged_in()) {
			redirect('adminauth/login/');
            } else {
                $this->module   = 'admin/lipid_advice';
                $this->user_id	= $this->tank_auth->get_user_id();
                $this->username	= $this->tank_auth->get_username();
                $this->role_id  = $this->session->userdata('role');
            }
         }
	
	function index() {
            redirect('admin/lipid_advice/edit/');
	}
	
	
	
	function edit(){
            if($this->access->has_access($this->role_id ,$this->module)){
		$data['advice'] = $this->db_fnc->getAll('advice_lipid');
		$this->load->view('admin/edit_lipid_advice_view',$data);
            }
	}
	
	function saveAdvice($id){
            if($this->access->has_access($this->role_id ,$this->module)){
		$update = $_POST;
		$where = array('id' => $id);
		$data['update'] = $this->db_fnc->update('advice_lipid', $update, $where);
		redirect('admin/lipid_advice/edit/');
            }
	}
	
	function newAdvice(){
            if($this->access->has_access($this->role_id ,$this->module)){
		$insert = $_POST;
		$data['insert'] = $this->db_fnc->insert('advice_lipid',$insert);
		redirect('admin/lipid_advice/edit/');
            }
	}
	
	function _goAdminLogin () {
	        $data['msg']= ($this->usertype == "admin" && $this->role != 'S' ) ? 'You need to be a superuser to be able to see this page' : 'Please Login';
		$this->load->view('admin/adminlogin_view',$data);	
	}
	
}
