<?php

class Administrators extends MX_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->tank_auth->is_logged_in()) {
			redirect('adminauth/login/');
        } else {
            $this->module = 'admin/doctors';
            $this->user_id = $this->tank_auth->get_user_id();
            $this->username = $this->tank_auth->get_username();
            $this->role_id = $this->session->userdata('role');
            $this->gpcom_id = $this->session->userdata('gpcom_id');
            $this->practice_id = $this->session->userdata('practice_id');
            $this->load->model('group');
            $this->load->model('practice');
            $this->load->model('localauth');
            $this->load->model('tank_auth/admins','admin');
            $this->isAdmin = $this->admin->isSysAdmin($this->role_id);
            $this->isLocalAuth = $this->admin->isLocalAuth($this->role_id);
            $this->isPractice = $this->admin->isPractice($this->role_id);
        }
    }

    function index() {
        if ($this->access->has_access($this->role_id, $this->module)) {
            $data = array();
                            //print_r($this->admin->isSysAdmin($this->role_id)); die;

            if ($this->admin->isSysAdmin($this->role_id)) {
                $admins = $this->admin->getAllAdmins();
            } elseif ($this->admin->isLocalAuth($this->role_id)) {
                $admins = $this->admin->getAllAdmins(array('pr.commissioning_id' => $this->gpcom_id));
            } elseif ($this->admin->isPractice($this->role_id)) {
                $admins = $this->admin->getAllAdmins(array('pr.practice_id' => $this->practice_id));
            }
            //print_r($doctors); die;
            $data['admins'] = (isset($admins)) ? $admins : array();
            $this->load->view('admin/admins_view', $data);
        }
    }

    function add() {
        if ($this->access->has_access($this->role_id, $this->module)) {
            $data['gpcoms'] = array('Please select a GP Commissioning');
            $data['gpcoms'] += $this->db_fnc->getKeyValueArray('commissioning_id', 'name', 'gp_commissionings');
            $data['gpcom_id'] = 0;
            $data['practices'] = array('Please select a GP Practice');
            $data['practices'] += $this->db_fnc->getKeyValueArray('practice_id', 'name', 'gp_practices');
            $data['practice_id'] = 0;

            $fname = $this->input->post('fname');
            $lname = $this->input->post('lname');
            $gpcom_id = $this->input->post('gpcom');
            $practice_id = $this->input->post('practice');


            if ($fname && $lname) {
                $ins = $this->db_fnc->insert('doctors', array(
                            'fname' => $fname, 'lname' => $lname, 'practice_id' => $practice_id, 'commissioning_id' => $gpcom_id ));

                $data['title'] = "New GP Practice";
                $data['msg'] = anchor('admin/doctors', 'OK', 'class="fancyClose btn orange big"');
                $this->load->view('msg_pop', $data);
            } else {
                $this->load->view('admin/add_doctor_pop', $data);
            }
        }
    }

    function edit($id='') {
        (int) $id;
        if ($this->access->has_access($this->role_id, $this->module)) {

            if ($_POST) {
            $admin_id = $this->input->post('admin_id');
            $username = $this->input->post('username');
            $email = $this->input->post('email');
            //$role_id = $this->input->post('role');
            $LA_id = $this->input->post('localAuth');
            $practice_id = $this->input->post('practice');

            $group_id = ($practice_id) ? $practice_id : $LA_id;
            $role_id = ($practice_id) ? 4 : 5;

            //$fname = $this->input->post('fname');
            //$lname = $this->input->post('lname');
            //$address = $this->input->post('address');
            //$city = $this->input->post('city');
            //$postcode = $this->input->post('postcode');
            //$phone = $this->input->post('phone');
            
            

            
            $ins = $this->admin->update(
                                array(
                                    //'fname' => $fname,
                                    //'lname' => $lname,
                                    //'address' => $address,
                                    //'city' => $city,
                                    //'postcode' => $postcode,
                                    
                                    'username' => $username,
                                    'email' => $email,
                                    'role_id' => $role_id,
                                    'group_id' => $group_id
                                    ),
                                    $admin_id);

                $data['title'] = "Edit Admin User profile";
                $data['msg'] = anchor('admin/doctors', 'OK', 'class="fancyClose btn orange big"');
                $this->load->view('msg_pop', $data);
            } else {
                $data['admin'] = $this->admin->getAdminByID($id);

                $group = $this->group->getGroupByID($data['admin']['group_id']);
                
                $data['LASelector'] = '';
                $data['practiceSelector'] = '';


                

                if ($this->isAdmin) {

                    if(!isset($group['group_type_id']) || $group['group_type_id']==1){
                        $LAs = array('Please select a Local Authority');
                        $LAs += $this->localauth->getLocalAuth_dd();
                        $LA_id = (isset($group['id'])) ? $group['id'] : 0;

                        $data['LASelector'] = form_label('Local Authority Name', 'localAuth');
                        $data['LASelector'] .= form_dropdown('localAuth', $LAs, $LA_id, 'id="localAuth"');

                    }

                    
                    if(!isset($group['group_type_id']) || $group['group_type_id']==2){
                        $LAs = array('Please select a Local Authority');
                        $LAs += $this->localauth->getLocalAuth_dd();
                        $LA_id = (isset($group['id'])) ? $group['id'] : 0;

                        $data['LASelector'] = form_label('Local Authority Name', 'localAuth');
                        $data['LASelector'] .= form_dropdown('localAuth', $LAs, $LA_id, 'id="localAuth"');


                        $practices = array('Please select a GP Practice');
                        $LA_id = (isset($group['parent_id'])) ? $group['parent_id'] : 0;
                        $practices += $this->practice->getPractice_dd($LA_id);
                        $practice_id = (isset($group['id'])) ? $group['id'] : 0;
                        $data['practiceSelector'] .= form_label('GP Practice Name', 'practice');
                        $data['practiceSelector'] .= form_dropdown('practices', $practices, $practice_id);
                    }

                } elseif ($this->isLocalAuth) {

                    $practices = array('Please select a GP Practice');
                    $practices += $this->practice->getPractice_dd($group['id']);
                    $practice_id = $group['id'];
                     $data['practiceSelector'] .= form_label('GP Practice Name', 'practices');
                     $data['practiceSelector'] .= form_dropdown('practice', $practices, $practice_id);

                }

                $this->load->view('admin/edit_admin_pop', $data);
            }
        }
    }

    function delete($id) {
        (int) $id;
        if ($this->access->has_access($this->role_id, $this->module)) {
            $this->admins->delete_user($id);
            $this->db_fnc->delete('users_roles', 'user_id', $id);
            $this->db_fnc->delete('user_profiles', 'user_id', $id);
            redirect('admin/administrators');
        }
    }

}

