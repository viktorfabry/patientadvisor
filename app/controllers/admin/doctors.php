<?php

class Doctors extends MX_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->tank_auth->is_logged_in()) {
            redirect('adminauth/login/');
        } else {
            $this->module = 'admin/doctors';
            $this->user_id = $this->tank_auth->get_user_id();
            $this->username = $this->tank_auth->get_username();
            $this->role_id = $this->session->userdata('role');
            $this->group_id = $this->session->userdata('group_id');
            $this->load->model('doctor');
            $this->load->model('group');
            $this->load->model('practice');
            $this->load->model('localauth');
            $this->load->model('tank_auth/admins', 'admin');
            $this->load->model('tank_auth/users', 'users');
            $this->isAdmin = $this->admin->isSysAdmin($this->role_id);
            $this->isLocalAuth = $this->admin->isLocalAuth($this->role_id);
            $this->isPractice = $this->admin->isPractice($this->role_id);
        }
    }

    function index() {
        if ($this->access->has_access($this->role_id, $this->module)) {
            $data = array();

            if ($this->isAdmin) {
                $doctors = $this->doctor->getAllDoctors();
            } elseif ($this->isLocalAuth) {
                $doctors = $this->doctor->getAllDoctors(array('g2.id' => $this->group_id));
            } elseif ($this->isPractice) {
                $doctors = $this->doctor->getAllDoctors(array('d.practice_id' => $this->group_id));
            }
            //print_r($doctors); die;
            $data['doctors'] = (isset($doctors)) ? $doctors : array();
            $this->load->view('admin/doctors_view', $data);
        }
    }


    function assign() {
        if ($this->access->has_access($this->role_id, $this->module)) {

            $this->load->model('patient');

            $doctor_id = $this->input->post('doctor');
            $patient_id = $this->input->post('patient');

            if ($doctor_id && $patient_id) {
                //$ins = $this->db_fnc->insert('doctor_patient', array('doctor_id' => $doc, 'patient_id' => $patient));
                //if ($ins) $this->session->set_flashdata('msg', $patient.' has been assigned to '.$doc.'.' );
                $ins = $this->patient->update(array('doctor_id' => $doctor_id), $patient_id);
                if (isset($ins) && $ins)
                    $this->session->set_flashdata('msg', $patient_id . ' has been assigned to ' . $doctor_id . '.');
            }


            $patients_dd = array('Please select a patient to');
            $doctors_dd = array('the doctor');
            $data['doctor_id'] = 0;
            $data['patient_id'] = 0;
            $doctors = array();
            $patients = array();

            if ($this->isAdmin) {
                $doctors = $this->doctor->getDoctors_dd();
                $patients = $this->patient->getPatients_dd();
            } elseif ($this->isLocalAuth) {
                $doctors = $this->doctor->getDoctors_dd(array('g.parent_id' => $this->group_id));
                $patients = $this->patient->getPatients_dd(array('g.parent_id' => $this->group_id));
            } elseif ($this->isPractice) {
                $doctors = $this->doctor->getDoctors_dd(array('d.practice_id' => $this->group_id));
                $patients = $this->patient->getPatients_dd(array('g.id' => $this->group_id));
            }


            $data['doctors_dd'] = $doctors_dd + $doctors;
            $data['patients_dd'] = $patients_dd + $patients;
            $this->load->view('admin/PD_assign_view', $data);
        }
    }

    function edit($user_id='') {
        if ($this->access->has_access($this->role_id, $this->module)) {

            if ($_POST) {

                $fname = $this->input->post('fname');
                $lname = $this->input->post('lname');
                $practice_id = $this->input->post('practice');
                $EMIS = $this->input->post('EMIS');
                $address = $this->input->post('address');
                $city = $this->input->post('city');
                $postcode = $this->input->post('postcode');
                $email = $this->input->post('email');
                $phone = $this->input->post('phone');
                $opening = $this->input->post('opening');

                $ins = $this->doctor->update(
                                array(
                                    'fname' => $fname,
                                    'lname' => $lname,
                                    'practice_id' => $practice_id,
                                    'EMIS' => $EMIS,
                                    'address' => $address,
                                    'city' => $city,
                                    'postcode' => $postcode,
                                    'phone' => $phone,
                                    'opening' => $opening
                                ),
                                $user_id);

                $this->users->update_user_by_admin(
                        array(
                            'email' => $email
                        ),
                        $user_id);

//                $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');
//                $data['errors'] = array();
//
//                if ($this->form_validation->run()) {        // validation ok
//                    if (!is_null($data = $this->tank_auth->set_new_email(
//                                            $this->form_validation->set_value('email'),
//                                            $this->form_validation->set_value('password')))) {   // success
//                        $data['site_name'] = $this->config->item('website_name', 'tank_auth');
//
//                        // Send email with new email address and its activation link
//                        $this->_send_email('change_email', $data['new_email'], $data);
//
//                        $this->_show_message(sprintf($this->lang->line('auth_message_new_email_sent'), $data['new_email']));
//                    } else {
//                        $errors = $this->tank_auth->get_error_message();
//                        foreach ($errors as $k => $v)
//                            $data['errors'][$k] = $this->lang->line($v);
//                    }
//                }

                $data['title'] = "New GP Addes";
                $data['msg'] = anchor('admin/doctors', 'OK', 'class="fancyClose btn orange big"');
                $this->load->view('msg_pop', $data);
            } else {
                $data['dr'] = $this->doctor->getDrByUserID($user_id);

                $group_id = $data['dr']['practice_id'];

                $group = $this->group->getGroupByID($group_id);

                $data['LASelector'] = '';
                $data['practiceSelector'] = '';

                if ($this->isAdmin) {

                    $LAs = array('Please select a Local Authority');
                    $LAs += $this->localauth->getLocalAuth_dd();
                    $LA_id = (isset($group['parent_id']) && $group['parent_id']) ? $group['parent_id'] : 0;

                    $data['LASelector'] = form_label('Local Authority Name', 'localAuth');
                    $data['LASelector'] .= form_dropdown('localAuth', $LAs, $LA_id, 'id="localAuth"');


                    if (isset($group['parent_id']) && $group['parent_id']) {
                        $practices = array('Please select a GP Practice');
                        $practices += $this->practice->getPractice_dd($group['parent_id']);
                        $practice_id = (isset($group['id']) && $group['id']) ? $group['id'] : 0;
                        $data['practiceSelector'] .= form_label('GP Practice Name', 'practice');
                        $data['practiceSelector'] .= form_dropdown('practice', $practices, $practice_id);
                    }
                } elseif ($this->isLocalAuth) {

                    $practices = array('Please select a GP Practice');
                    $practices += $this->practice->getPractice_dd($group['parent_id']);
                    $practice_id = (isset($group['id']) && $group['id']) ? $group['id'] : 0;
                    $data['practiceSelector'] .= form_label('GP Practice Name', 'practice');
                    $data['practiceSelector'] .= form_dropdown('practice', $practices, $practice_id);
                }


                $this->load->view('admin/edit_doctor_pop', $data);
            }
        }
    }

    function delete($id) {
        (int) $id;
        if ($this->access->has_access($this->role_id, $this->module)) {
            $this->db_fnc->delete('doctors', 'user_id', $id);
            $this->db_fnc->delete('users_roles', 'user_id', $id);
            $this->db_fnc->delete('users', 'id', $id);
            $this->db_fnc->delete('user_profiles', 'user_id', $id);
            redirect('admin/doctors');
        }
    }

}

