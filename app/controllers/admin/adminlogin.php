<?php

class Adminlogin extends MX_Controller {

	function __construct()
	{
		parent::__construct();
		$this->usertype = $this->session->userdata('usertype');
	}
	
function index() {
		$data = array();
		
		if ($this->usertype == 'admin') {
			$admin = $this->session->userdata('user_id');			
			$this->load->view('admin/admin_view',$data);
		
		} else {
			$this->load->view('admin/adminlogin_view');	
		}
		
    }		
	
}