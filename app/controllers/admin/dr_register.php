<?php

require_once(APPPATH . 'modules/auth/controllers/auth.php');

class Dr_register extends Auth {

    function __construct() {
        parent::__construct();

        if (!$this->tank_auth->is_logged_in()) {
            redirect('adminauth/login/');
        } else {
            $this->module = 'admin/doctors';
            $this->user_id = $this->tank_auth->get_user_id();
            $this->username = $this->tank_auth->get_username();
            $this->role_id = $this->session->userdata('role');
            $this->group_id = $this->session->userdata('group_id');
            $this->dr = $this->load->model('doctor');
            $this->load->model('practice');
            $this->load->model('localauth');
            $this->load->model('tank_auth/admins', 'admin');
            $this->isAdmin = $this->admin->isSysAdmin($this->role_id);
            $this->isLocalAuth = $this->admin->isLocalAuth($this->role_id);
            $this->isPractice = $this->admin->isPractice($this->role_id);
        }
    }

    function index() {
        if ($this->access->has_access($this->role_id, $this->module)) {

            $use_username = $this->config->item('use_username', 'tank_auth');
            if ($use_username) {
                $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean|min_length[' . $this->config->item('username_min_length', 'tank_auth') . ']|max_length[' . $this->config->item('username_max_length', 'tank_auth') . ']|alpha_dash');
            }
            $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|min_length[' . $this->config->item('password_min_length', 'tank_auth') . ']|max_length[' . $this->config->item('password_max_length', 'tank_auth') . ']|alpha_dash');
            $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|xss_clean|matches[password]');

            $this->form_validation->set_rules('fname', 'Firstname', 'trim|required');
            $this->form_validation->set_rules('lname', 'Lasttname', 'trim|required');

            $this->form_validation->set_rules('phone', 'phone', '');

            $this->form_validation->set_rules('opening', 'opening', '');
            $this->form_validation->set_rules('EMIS', 'EMIS', '');
            $this->form_validation->set_rules('date_created', 'date_created', '');
            $this->form_validation->set_rules('date_updated', 'date_updated', '');


            $captcha_registration = $this->config->item('captcha_registration', 'tank_auth');
            $use_recaptcha = $this->config->item('use_recaptcha', 'tank_auth');
            if ($captcha_registration) {
                if ($use_recaptcha) {
                    $this->form_validation->set_rules('recaptcha_response_field', 'Confirmation Code', 'trim|xss_clean|required|callback__check_recaptcha');
                } else {
                    $this->form_validation->set_rules('captcha', 'Confirmation Code', 'trim|xss_clean|required|callback__check_captcha');
                }
            }
            $data['errors'] = array();

            $email_activation = $this->config->item('email_activation', 'tank_auth');
            

            if ($this->form_validation->run()) {
                // validation ok
                $fname = $this->form_validation->set_value('fname');
                $lname = $this->form_validation->set_value('lname');

                // print_r($_POST); die;
                if (!is_null($data = $this->tank_auth->create_user(
                                        $use_username ? $this->form_validation->set_value('username') : '',
                                        $this->form_validation->set_value('email'),
                                        $this->form_validation->set_value('password'),
                                        $email_activation))) {
                    //print_r($data); die;
                    $datestring = "Y-m-d H:i:s";
                    $now = date($datestring, time());
                    $tablename = 'doctors';
                    $practice_id = 0;

                    if($this->isPractice){
                        $practice_id = $this->group_id;
                    } else {
                        $practice_id = $this->input->post('practices');
                    }


                    $fieldarray = array(
                        'user_id' => $data['user_id'],
                        'fname' => $fname,
                        'lname' => $lname,
                        'practice_id' => $practice_id,
                        'EMIS' => $this->input->post('EMIS'),
                        'phone' => $this->input->post('phone'),
                        'opening' => $this->input->post('opening')
                    );
                    $this->doctor->addNewDr($fieldarray);

                    $tablename = 'users_roles';
                    $fieldarray = array(
                        'user_id' => $data['user_id'],
                        'role_id' => 3
                    );
                    $this->db_fnc->insert($tablename, $fieldarray);


                    $data['site_name'] = $this->config->item('website_name', 'tank_auth');

                    if ($email_activation) {         // send "activate" email
                        $data['activation_period'] = $this->config->item('email_activation_expire', 'tank_auth') / 3600;

                        $this->_send_email('activate', $data['email'], $data);

                        unset($data['password']); // Clear password (just for any case)

                        $this->_show_message(app_msg(sprintf($this->lang->line('doctor_registration_completed_1'),$fname.' '.$lname,$data['email']),'success'), 'admin/doctors');
                    } else {
                        if ($this->config->item('email_account_details', 'tank_auth')) { // send "welcome" email
                            $this->_send_email('welcome', $data['email'], $data);
                        }
                        unset($data['password']); // Clear password (just for any case)

                        $this->_show_message($this->lang->line('auth_message_registration_completed_2') . ' ' . anchor('/auth/login/', 'Login'), 'admin/doctors');
                    }
                    redirect('admin/doctors/');
                } else {
                    $errors = $this->tank_auth->get_error_message();
                    foreach ($errors as $k => $v)
                        $data['errors'][$k] = $this->lang->line($v);
                }
            }
            if ($captcha_registration) {
                if ($use_recaptcha) {
                    $data['recaptcha_html'] = $this->_create_recaptcha();
                } else {
                    $data['captcha_html'] = $this->_create_captcha();
                }
            }


            $data['use_username'] = $use_username;
            $data['captcha_registration'] = $captcha_registration;
            $data['use_recaptcha'] = $use_recaptcha;
            $data['groupSelector'] = '';

            if ($this->isAdmin) {

                $gpcoms = array('Please select a GP Commissioning');
                $gpcoms += $this->localauth->getLocalAuth_dd();
                $gpcom_id = 0;

                $data['groupSelector'] .= form_label('GP Commissioning Name', 'gpcom');
                $data['groupSelector'] .= form_dropdown('gpcom', $gpcoms, $gpcom_id, 'id="gpcom"');
            } elseif ($this->isLocalAuth) {

                $practices = array('Please select a GP Practice');
                $practices += $this->practice->getPractice_dd($this->group_id);
                $practice_id = 0;
                
                 $data['groupSelector'] .= form_label('GP Practice Name', 'practice');
                 $data['groupSelector'] .= form_dropdown('practice', $practices, $practice_id);
                  
            }



            $this->load->view('admin/dr_register_form', $data);
        }
    }

}
