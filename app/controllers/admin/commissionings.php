<?php

class Commissionings extends MX_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->tank_auth->is_logged_in()) {
            redirect('adminauth/login/');
        } else {
            $this->module = 'admin/commissionings';
            $this->user_id = $this->tank_auth->get_user_id();
            $this->username = $this->tank_auth->get_username();
            $this->role_id = $this->session->userdata('role');
            $this->load->model('localauth');
            $this->load->model('doctor', 'dr');
            $this->load->model('patient');
        }
    }

    function index() {
        if ($this->access->has_access($this->role_id, $this->module)) {
            $data = array();
            $data['gpcoms'] = $this->localauth->getAuthorities('name ASC');
            $this->load->view('admin/gpcoms_view', $data);
        }
    }

    function add() {
        if ($this->access->has_access($this->role_id, $this->module)) {
            $name = $this->input->post('name');
            $description = $this->input->post('description');

            if ($name) {
                $ins = $this->localauth->addAuthority(array('name' => $name, 'description' => $description));
                if ($ins) {
                    $this->session->set_flashdata('msg', $name . ' has been added.');
                    $data['msg'] = anchor('admin/commissionings', 'OK', 'class="fancyClose btn orange big"');
                }

                $data['title'] = "New GP Commissionings";

                $this->load->view('msg_pop', $data);
            } else {
                $this->load->view('admin/add_gpcomm_pop');
            }
        }
    }

    function edit($id = '') {
        (int) $id;
        if ($this->access->has_access($this->role_id, $this->module)) {

            $localAuth_id = $this->input->post('id');
            $name = $this->input->post('name');
            $description = $this->input->post('description');

            if ($name && $localAuth_id) {
                $ins = $this->localauth->updateAuthority(array('name' => $name, 'description' => $description), $localAuth_id);
                $data['title'] = "Edit GP Commissionings";
                $data['msg'] = anchor('admin/commissionings', 'OK', 'class="fancyClose btn orange big"');
                $this->load->view('msg_pop', $data);
            } else {
                $localAuth = $this->localauth->getLocalauthById($id);
                $data['id'] = $id;
                $data['name'] = $localAuth['name'];
                $data['description'] = $localAuth['description'];
                $this->load->view('admin/edit_gpcomm_pop', $data);
            }
        }
    }

    function delete($id) {
        (int) $id;
        if ($this->access->has_access($this->role_id, $this->module)) {
            $this->localauth->deleteAuthority($id);
            redirect('admin/commissionings');
        }
    }

}

