<?php

class Patient_links extends MX_Controller {

	function  __construct() {
            parent::__construct();

            if (!$this->tank_auth->is_logged_in()) {
			redirect('adminauth/login/');
            } else {
                $this->module   = 'admin/patient_links';
                $this->user_id	= $this->tank_auth->get_user_id();
                $this->username	= $this->tank_auth->get_username();
                $this->role_id  = $this->session->userdata('role');
                $this->load->model('link');
            }
         }
	
function index() {
    if($this->access->has_access($this->role_id ,$this->module)){
		$data = array();
		$admin = $this->session->userdata('user_id');
				$link_id = 0;
				$data['link_id'] = $link_id;
				$data['links'] = array('0' => 'Please select a category');
				$data['links'] += $this->link->getLinks_dd(array('linkCat_id'=>1),'name ASC');
	
		$orderBy = 'link_id ASC, name ASC';
		$links = $this->db_fnc->getAll('patientlinks', $orderBy);
		
		$menulinks = array(
			'type' => 'patient_links',
			'items' => array(),
			'parents' => array()
		);
		
		foreach ($links as $l) {
			$menulinks['items'][$l['link_id']] = $l;
			$menulinks['parents'][$l['parent_id']][] = $l['link_id'];
			
		}
		$linklist = $this->load->library('multilevel_admin',$menulinks);
		$data['linklist'] = $linklist->html;
			
		$this->load->view('admin/patient_links_view',$data);
    }
}
	
function add() {
    if($this->access->has_access($this->role_id ,$this->module)){
		$data = array();
		$admin = $this->session->userdata('user_id');
		
								
		$tablename = 'patientlinks';
		$fieldarray = array(
					'name' => $_POST['name'],
					'url' => $_POST['url'],
					'parent_id' => $_POST['links']
				);
		$this->db_fnc->insert($tablename,$fieldarray);
					
		redirect('admin/patient_links/');
    }
}		
	
function delete($link_id) {
    if($this->access->has_access($this->role_id ,$this->module)){
		$data = array();
		
		$admin = $this->session->userdata('user_id');
		
		$this->db_fnc->delete('patientlinks','link_id',$link_id);
					
		redirect('admin/patient_links/');
    }
}

function update($id) {
    if($this->access->has_access($this->role_id ,$this->module)){
		$data = array();
		$admin = $this->session->userdata('user_id');
		
								
		$tablename = 'patientlinks';
		$fieldarray = array(
				'name' => $_POST['name'],
				'url' => $_POST['url'],
				'parent_id' => $_POST['parent_id']
				);
		$where = array('link_id' => $id);
		$this->db_fnc->update($tablename,$fieldarray,$where);
					
		redirect('admin/patient_links/');
    }
	
}
		
	
}

