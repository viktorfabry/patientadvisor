<?php

class Linkadmin extends MX_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->tank_auth->is_logged_in()) {
            redirect('adminauth/login/');
        } else {
            $this->module = 'admin/linkadmin/doctor';
            $this->user_id = $this->tank_auth->get_user_id();
            $this->username = $this->tank_auth->get_username();
            $this->role_id = $this->session->userdata('role');
            $this->dr = $this->load->model('Doctor');
            $this->link = $this->load->model('link');
        }
    }

    function index() {
        if ($this->access->has_access($this->role_id, $this->module)) {
            $data = array();
            $this->load->view('admin/msg_view', $data);
        }
    }

    function doctor(){
        $this->links('doctor');
    }
    
    function patient(){
        $this->links('patient');
    }

    function links($type = '') {
        if ($this->access->has_access($this->role_id, $this->module)) {
            if ($type) {

                $linkCat_id = ($type === 'doctor') ? 2 : 1;
                $data['pageTitle'] = ($type === 'doctor') ? 'Doctor links' : 'Patient links';
                $data['type'] = $type;

                $link_id = 0;
                $data['link_id'] = $link_id;
                $data['links'] = array('0' => 'Please select a category');
                $data['links'] += $this->db_fnc->getKeyValueArray('id', 'name', 'links', array('linkCat_id' => $linkCat_id));

                $orderBy = 'link_id ASC, name ASC';
                $links = $this->link->getLinks(array('linkCat_id' => $linkCat_id));

                $menulinks = array(
                    'type' => $type,
                    'items' => array(),
                    'parents' => array()
                );

                foreach ($links as $l) {
                    $menulinks['items'][$l['link_id']] = $l;
                    $menulinks['parents'][$l['parent_id']][] = $l['link_id'];
                }


                $linklist = $this->load->library('multilevel_admin', $menulinks);

                $data['linklist'] = $linklist->html;

                $this->load->view('admin/links_view', $data);
            } else {
                $data = array();
                $this->load->view('admin/msg_view', $data);
            }
        }
    }

    function add($type) {
        if ($this->access->has_access($this->role_id, $this->module)) {
            $data = array();
            $linkCat_id = ($type === 'doctor') ? 2 : 1;

            //print_r($_FILES); die;

            $name = $this->input->post('name');
            $url = $this->input->post('url');
            $parent_id = $this->input->post('links');

            if ($_FILES["userfile"]["name"]) {
                $filename = str_replace(' ', '_', $name);
                $ext = '.pdf';

                if ($filename)
                    $config['file_name'] = $filename;
                $config['upload_path'] = './Documents/';
                $config['allowed_types'] = 'pdf';
                $config['max_size'] = '5000';
                $this->load->library('upload', $config);
                //$this->upload->data()
                $errors = ($this->upload->do_upload()) ? $_FILES["userfile"]["name"] . ' has been uploaded as ' . $filename . $ext : $this->upload->display_errors();

                //print_r($_FILES); die;
                if ($errors) {
                    $errors = (is_array($errors)) ? implode(', ', $errors) : $errors;
                    $this->session->set_flashdata('errors', $errors);
                }


                $url = base_url() . 'Documents/' . $filename . $ext;
            }

            if ($url != '') {
                $url = $this->_addProtocol($url);
            } else {
                $url = '#';
            }

            $tablename = 'links';
            $fieldarray = array('name' => $name, 'url' => $url, 'parent_id' => $parent_id, 'linkCat_id' => $linkCat_id);
            $this->db_fnc->insert($tablename, $fieldarray);

            redirect('admin/linkadmin/'.$type);
        }
    }

    function _addProtocol($link) {
        if ((substr($link, 0, 4)) == "http") {
            $out = $link;
        } else {
            $out = 'http://' . $link;
        }
        return $out;
    }

    function delete($type,$id) {
        if ($this->access->has_access($this->role_id, $this->module)) {
            $data = array();
            $admin = $this->session->userdata('user_id');

            $this->db_fnc->delete('links', 'id', $id);

            redirect('admin/linkadmin/'.$type);
        }
    }

    function update($type,$id) {
        if ($this->access->has_access($this->role_id, $this->module)) {
            $data = array();
            $admin = $this->session->userdata('user_id');


            $tablename = 'drlinks';
            $fieldarray = array(
                'name' => $this->input->post('name'),
                'url' => $this->input->post('url'),
                'parent_id' => $this->input->post('parent_id')
            );
            $where = array('id' => $id);
            $this->link->update('links', $fieldarray, $where);

            redirect('admin/linkadmin/'.$type);
        }
    }

}

