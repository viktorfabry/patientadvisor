<?php

class Patients extends MX_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->tank_auth->is_logged_in()) {
            redirect('adminauth/login/');
        } else {
            $this->module = 'admin/patients';
            $this->user_id = $this->tank_auth->get_user_id();
            $this->username = $this->tank_auth->get_username();
            $this->role_id = $this->session->userdata('role');
            $this->group_id = $this->session->userdata('group_id');
            $this->load->model('doctor');
            $this->load->model('patient');
            $this->load->model('tank_auth/admins', 'admin');
            $this->isAdmin = $this->admin->isSysAdmin($this->role_id);
            $this->isLocalAuth = $this->admin->isLocalAuth($this->role_id);
            $this->isPractice = $this->admin->isPractice($this->role_id);
        }
    }

    function index() {
        if ($this->access->has_access($this->role_id, $this->module)) {
            $data = array();

            if ($this->isAdmin)
                $patients = $this->patient->getAllPatients();
            if ($this->isLocalAuth)
                $patients = $this->patient->getAllPatients(array('g.parent_id' => $this->group_id));
            if ($this->isPractice)
                $patients = $this->patient->getAllPatients(array('g.id' => $this->group_id));
            //print_r($patients); die;
            $data['patients'] = (isset($patients)) ? $patients : array();
            $this->load->view('admin/patients_view', $data);
        } else {
            redirect('adminauth/login/');
        }
    }

    function invite_patients() {
        if ($this->access->has_access($this->role_id, $this->module)) {
            if ($this->access->has_access($this->role_id, $this->module)) {
                $data = array();
                $this->load->view('admin/invite_patients_view', $data);
            }
        }
    }

    function upload_patient_list() {
        if ($this->access->has_access($this->role_id, $this->module)) {
            if ($this->access->has_access($this->role_id, $this->module)) {
                $this->load->helper('patients_csv_imp');
                //print_r('WTF'); die;
                // list of valid extensions, ex. array("jpeg", "xml", "bmp")
                $allowedExtensions = array('csv');
                // max file size in bytes
                $sizeLimit = 10 * 1024 * 1024;

                $uploader = new Patients_csv_imp($allowedExtensions, $sizeLimit);
                $result = $uploader->handleUpload('Documents/');

                // to pass data through iframe you will need to encode all html tags
                echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
            }
        }
    }

    function generate_patient_table($file_id = '') {
        if ($this->access->has_access($this->role_id, $this->module)) {
            $filePath = '';
            $patients = '';

            if ($file_id) {
                $fileName = $this->db_fnc->getThisOne('fileName', 'file_id', $file_id, 'files');
                $filePath = './Documents/' . $fileName;
            }

            //echo $filePath;

            if (file_exists($filePath)) {
                $this->load->library('csvreader');
                $patients = $this->csvreader->parse_file($filePath);
                // print_r($patients);
            }

            $list = '<table  class="styled" >
                    <tr>
                        <th>First name</th>
                        <th>Last name</th>
                        <th>Email</th>
                        <th>NI No.</th>
                        <th>EMIS</th>
                        <th>Dr name</th>
                    </tr>';
            foreach ($patients as $p) {

                $fName = (isset($p['fName']) && $p['fName']) ? $p['fName'] : '<span class="error">No firstname in the file</span>';
                $lName = (isset($p['lName']) && $p['lName']) ? $p['lName'] : '<span class="error">No lastname in the file</span>';
                $email = (isset($p['email']) && $p['email']) ? $p['email'] : '<span class="error">No email in the file</span>';
                $NINo = (isset($p['NINo']) && $p['NINo']) ? $p['NINo'] : '<span class="error">No National Insurance Number in the file</span>';
                $doctor = '<span class="error">No Doctor has been assigned</span>';
                if (isset($p['EMIS']) && $p['EMIS']) {
                    $EMIS = $p['EMIS'];
                    $this->load->model('doctor');
                    $dr = $this->doctor->initByEMIS($EMIS);
                    $doctor = ($dr) ? $dr->fname . ' ' . $dr->lname : '<span class="error">This EMIS number not in the database.</span>';
                } else {
                    $EMIS = '<span class="error">No EMIS number in the file</span>';
                }

                $list .= '<tr>';
                $list .= '<td>' . $fName . '</td>';
                $list .= '<td>' . $lName . '</td>';
                $list .= '<td>' . $email . '</td>';
                $list .= '<td>' . $NINo . '</td>';
                $list .= '<td>' . $EMIS . '</td>';
                $list .= '<td>' . $doctor . '</td>';
                $list .= '</tr>';
            }
            $list .= '<table/>';

            echo $list;
        }
    }

    function add() {
        if ($this->access->has_access($this->role_id, $this->module)) {


            if ($this->isAdmin)
                    $doctors = $this->doctor->getDoctors_dd();
            if ($this->isLocalAuth)
                    $doctors = $this->doctor->getDoctors_dd(array('g.parent_id' => $this->group_id));
            if ($this->isPractice)
                    $doctors = $this->doctor->getDoctors_dd(array('g.id' => $this->group_id));


            $data['doctors'] = array('Please select a GP');
            $data['doctors'] += $doctors;
            $data['doctor_id'] = 0;


            //$patient_id      = $this->input->post('patient_id');
            $doctor_id = $this->input->post('doctor');
            $fname = $this->input->post('fname');
            $lname = $this->input->post('lname');
            $address = $this->input->post('address');
            $city = $this->input->post('city');
            $postcode = $this->input->post('postcode');
            $email = $this->input->post('email');
            $phone = $this->input->post('phone');
            $NHS = $this->input->post('NHS');

            if ($fname && $lname) {
                $ins = $this->db_fnc->insert(
                        'patients', array(
                            'fname' => $fname, 'lname' => $lname, 'doctor_id' => $doctor_id, 'address' => $address, 'city' => $city, 'postcode' => $postcode, 'phone' => $phone, 'NHS' => $NHS));

                $data['title'] = "New GP Practice";
                $data['msg'] = anchor('admin/patients', 'OK', 'class="fancyClose btn orange big"');
                $this->load->view('msg_pop', $data);
            } else {
                $this->load->view('admin/add_patient_pop', $data);
            }
        }
    }

    function edit($id='') {
        (int) $id;
        if ($this->access->has_access($this->role_id, $this->module)) {


            if ($this->isAdmin)
                    $doctors = $this->doctor->getDoctors_dd();
            if ($this->isLocalAuth)
                    $doctors = $this->doctor->getDoctors_dd(array('g.parent_id' => $this->group_id));
            if ($this->isPractice)
                    $doctors = $this->doctor->getDoctors_dd(array('g.id' => $this->group_id));

            $patient_id = $this->input->post('patient_id');
            $doctor_id = $this->input->post('doctor');
            $fname = $this->input->post('fname');
            $lname = $this->input->post('lname');
            $address = $this->input->post('address');
            $city = $this->input->post('city');
            $postcode = $this->input->post('postcode');
            $email = $this->input->post('email');
            $phone = $this->input->post('phone');
            $NHS = $this->input->post('NHS');

            if ($_POST) {
                //print_r($_POST); die;
                $ins = $this->patient->update(
                                array('fname' => $fname,
                                    'lname' => $lname,
                                    'doctor_id' => $doctor_id,
                                    'address' => $address,
                                    'city' => $city,
                                    'postcode' => $postcode,
                                    'phone' => $phone,
                                    'NHS' => $NHS),
                                $patient_id);
                $this->users->update_user_by_admin(
                        array(
                            'email' => $email
                        ),
                        $patient_id);

                $data['title'] = "Edit Patient";
                $data['msg'] = anchor('admin/patients', 'OK', 'class="fancyClose btn orange big"');
                $this->load->view('msg_pop', $data);
            } else {
                $p = $this->patient->getPatientByID($id);
                //print_r($id); die;
                $data['p'] = $p;
                $data['doctors'] = array('Please select a GP');
                $data['doctors'] += $doctors;
                $data['doctor_id'] = $p['doctor_id'];

                $this->load->view('admin/edit_patient_pop', $data);
            }
        }
    }

    function delete($id) {
        (int) $id;
        if ($this->access->has_access($this->role_id, $this->module)) {
            $this->db_fnc->delete('patients', 'id', $id);
            $this->db_fnc->delete('users_roles', 'user_id', $id);
            $this->db_fnc->delete('users', 'id', $id);
            $this->db_fnc->delete('user_profiles', 'user_id', $id);
            redirect('admin/patients');
        }
    }

}

