<?php

//update  `users` set `email` = 'ian.fletcher2@btinternet.com' WHERE `email` = 'ian.fletcher1@btinternet.com'

class Patient extends MX_Controller {

function  __construct() {
            parent::__construct();

            if (!$this->tank_auth->is_logged_in()) {
			redirect('/auth/login/');
		} else {

                    $this->user_id	= $this->tank_auth->get_user_id();
                    $this->username	= $this->tank_auth->get_username();
                    $this->module       = $this->router->fetch_class();
                    $this->role_id      = $this->session->userdata('role');

                    if(!$this->access->has_access($this->role_id ,$this->module)){
                        redirect('/auth/login/');
                    }
                }
         }
	
function index() {
	$data['id'] = $this->user_id;
	$data['patient_id'] = $this->user_id;
	$this->load->view('dashboard_view',$data);
} 
	
function bloodpressure() {
	$data['patient_id'] = $this->user_id;
	$data['username'] = $this->username;
	$this->load->view('patient_bp_view',$data);
}
function cholesterol() {
	$data['patient_id'] = $this->user_id;
	$data['username'] = $this->username;
	$this->load->view('patient_cholesterol_view',$data);
}
function bodysize() {
	$data['patient_id'] = $this->user_id;
	$data['username'] = $this->username;
	$this->load->view('patient_bodysize_view',$data);
}
function diabetes() {
	$data['patient_id'] = $this->user_id;
	$data['username'] = $this->username;
	$this->load->view('patient_diabetes_view',$data);
}
					
	
	
function profile() {

		$data['id'] = $this->user_id;
		$data['patient_id'] = $this->user_id;
	
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('uname', 'Username', 'callback_uname_check');
		//$this->form_validation->set_rules('pword', 'Password', 'trim|required|matches[passconf]|md5');
		//$this->form_validation->set_rules('passconf', 'Password Confirmation', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		//$this->form_validation->set_rules('email_confirmed', 'Email', 'trim|required|valid_email');
		
		$this->form_validation->set_rules('fname', 'Firstname', 'trim|required');
		$this->form_validation->set_rules('lname', 'Lasttname', 'trim|required');
		
		$this->form_validation->set_rules('address', 'address', '');
		$this->form_validation->set_rules('city', 'city', '');
		$this->form_validation->set_rules('postcode', 'postcode', '');
		$this->form_validation->set_rules('phone', 'phone', '');
		
		$this->form_validation->set_rules('EMIS', 'EMIS', '');
		$this->form_validation->set_rules('NHS', 'NHS', '');
		$this->form_validation->set_rules('date_stopped_smoking', 'date_stopped_smoking', '');
		$this->form_validation->set_rules('date_created', 'date_created', '');
		$this->form_validation->set_rules('date_updated', 'date_updated', '');
				
		$this->form_validation->set_rules('imp_measure', 'imp_measure', '');
		$this->form_validation->set_rules('active', 'active', '');
		$this->form_validation->set_rules('imp_measure', 'imp_measure', '');
		$this->form_validation->set_rules('dob', 'dob', '');
				
		$this->form_validation->set_rules('height', 'height', '');
		$this->form_validation->set_rules('gender', 'gender', '');
		$this->form_validation->set_rules('smoker', 'smoker', '');
		$this->form_validation->set_rules('diabetes', 'diabetes', '');
		$this->form_validation->set_rules('hypertension', 'hypertension', '');
		$this->form_validation->set_rules('IHD', 'IHD', '');
		$this->form_validation->set_rules('ownbpmachine', 'ownbpmachine', '');
								
		if ($this->form_validation->run($this) == FALSE) {
			$this->session->set_userdata('patient_id', $this->user_id);
			$data['id'] = $this->user_id;
			$data['patient_id'] = $this->user_id;
			
			$p = $this->db_fnc->getRow('id',$this->user_id,'patients');
			$p_secured = $this->db_fnc->getRow('id',$this->user_id,'people_secured');
			$dp = $this->db_fnc->getRow('patient_id',$this->user_id,'doctor_patient');
			$data = $p + $p_secured;
			
			$data['dr_id'] = $dp['id'];
			$this->load->model('doctor');
			$data['dr_dd'] = $this->doctor->getDoctors_dd();
			//print_r($p['imp_measure']); die;
			if($p['imp_measure'] == 'Y') {
				$data['height'] = $this->app_functions->meter2imperial($p['height']/100);
			}
			$this->load->view('profile_view',$data); 
		} else {
			
			$tablename = 'people_secured';
			$fieldarray = array(
					'id' => $this->user_id,
					'fname' => $_POST['fname'],
					'lname' => $_POST['lname'],
					'email' => $_POST['email'],
					'address' => $_POST['address'],
					'city' => $_POST['city'],
					'postcode' => $_POST['postcode'],
					'phone' => $_POST['phone'],
					'EMIS' => $_POST['EMIS'],
					'NHS' => $_POST['NHS']
				);
			
			$where = array('id' => $this->user_id);
			$this->db_fnc->update($tablename,$fieldarray,$where);
			
			$tablename = 'doctor_patient';
			$fieldarray = array('patient_id' => $this->user_id,'doctor_id' => $_POST['doctor']);
			$where = array('patient_id' => $this->user_id);
			$this->user_id = $this->db_fnc->update($tablename,$fieldarray,$where);
			
			$imp_measure = $_POST['imp_measure'];
			$datestring = "Y-m-d H:i:s";
			$now = date($datestring, time());
			
			
			$tablename = 'patients';
			$fieldarray = array(
					//'id' => $this->user_id,
					'uname' => $_POST['uname'],
					//'pword' => $_POST['pword'],
					//'email_confirmed' => 'N',
					'date_stopped_smoking' => $_POST['date_stopped_smoking'],
					
					
					'imp_measure' => $_POST['imp_measure'],
					'active' => $_POST['active'],
					'imp_measure' => $imp_measure,
					'dob' => $_POST['dob'],
					'height' => $_POST['height'],
					
					'gender' => $_POST['gender'],
					'smoker' => $_POST['smoker'],
					'diabetes' => $_POST['diabetes'],
					'hypertension' => $_POST['hypertension'],
					'IHD' => $_POST['IHD'],
					'ownbpmachine' => $_POST['ownbpmachine'],
					'date_created' => $now,
					'date_updated' => $now,
					'date_last_logon' => $now
					);
			
			//print_r($fieldarray); die;
			$where = array('id' => $this->user_id);
			$this->db_fnc->update($tablename,$fieldarray,$where);
						
			
			redirect('patient/profile/');
		}							
		
	}
	
		
function uname_check($uname){
		$ifExist = $this->db_fnc->getThisOne('id','uname',$uname,'patients');
		if ($ifExist != $this->user_id)	{
			$this->form_validation->set_message('uname_check', 'The "'.$uname.'" username has already been taken please try a different name');
			return FALSE;
		} else {
			$this->form_validation->set_message('uname_check', 'The %s field can be the word "test"');

			return TRUE;
		}
	}
	
	


	
	
	

	
	
	
	
	
	
}

