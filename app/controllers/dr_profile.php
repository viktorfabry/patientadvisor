<?php

class dr_profile extends MX_Controller {

	function  __construct() {
            parent::__construct();

            if (!$this->tank_auth->is_logged_in()) {
			redirect('/auth/login/');
		} else {

                    $this->user_id	= $this->tank_auth->get_user_id();
                    $this->username	= $this->tank_auth->get_username();
                    $this->module       = $this->router->fetch_class();
                    $this->role_id      = $this->session->userdata('role');

                    if(!$this->access->has_access($this->role_id ,$this->module)){
                        redirect('/auth/login/');
                    }
                }
         }
	
function index() {
		$id = $this->session->userdata('user_id');
		$usertype = $this->session->userdata('usertype');
		
	if (isset($id) && $id != '' && $usertype == 'doctor' ) {
		$dr = $this->db_fnc->getRow('id',$id,'doctors');
		$data = $dr;
		
		
		
		$this->load->view('dr_profile_view',$data);
	} else {
		$data = array( 'username'  => '','user_id'  => '', 'usertype' => '', 'logged_in'  => '' );
		$this->session->unset_userdata($data);	
		$this->load->view('doctorlogin_view');	}
		
	}
	
	
function update() {
	$id = $this->session->userdata('user_id');
		$usertype = $this->session->userdata('usertype');
		
	if (isset($id) && $id != '' && $usertype == 'doctor' ) {
		$data['id'] = $id;
		
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('uname', 'Username', 'callback_uname_check');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		
		$this->form_validation->set_rules('fname', 'Firstname', 'trim|required');
		$this->form_validation->set_rules('lname', 'Lasttname', 'trim|required');
		
		$this->form_validation->set_rules('address', 'address', '');
		$this->form_validation->set_rules('city', 'city', '');
		$this->form_validation->set_rules('postcode', 'postcode', '');
		$this->form_validation->set_rules('phone', 'phone', '');
		$this->form_validation->set_rules('opening', 'Opening Hours', '');
		$this->form_validation->set_rules('EMIS', 'EMIS', '');
		$this->form_validation->set_rules('date_created', 'date_created', '');
		$this->form_validation->set_rules('date_updated', 'date_updated', '');

		
		
						
		if ($this->form_validation->run($this) == FALSE) {
			$this->load->view('dr_profile_view'); 
		} else {
			$tablename = 'doctors';
			$datestring = "Y-m-d H:i:s";
			$now = date($datestring, time());
			$fieldarray = array(
					'uname' => $_POST['uname'],
					'fname' => $_POST['fname'],
					'lname' => $_POST['lname'],
					'email' => $_POST['email'],
					'address' => $_POST['address'],
					'city' => $_POST['city'],
					'postcode' => $_POST['postcode'],
					'phone' => $_POST['phone'],
					'EMIS' => $_POST['EMIS'],					
					'date_updated' => $now
					);
			
			$where = array('id' => $id);
			$this->db_fnc->update($tablename,$fieldarray,$where);
						
			
			redirect('dr_profile');
		}
		
	} else {
		$data = array( 'username'  => '','user_id'  => '', 'usertype' => '', 'logged_in'  => '' );
		$this->session->unset_userdata($data);	
		$this->load->view('welcome_view');	}
		
	}					
		
}
	
		
function uname_check($uname){
		$ifExist = $this->db_fnc->getThis('id','uname',$uname,'doctors');
		
		if ($ifExist)	{
			$this->form_validation->set_message('uname_check', 'The "'.$uname.'" username has already been taken please try a different name');
			return FALSE;
		} else {
			$this->form_validation->set_message('uname_check', 'The %s field can be the word "test"');

			return TRUE;
		}
	}
	

?>