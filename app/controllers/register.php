<?php

require_once(APPPATH . 'modules/auth/controllers/auth.php');

class Register extends Auth {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('security');
        $this->load->library('tank_auth');
        $this->lang->load('tank_auth');
        
        $this->load->model('doctor');
    }

    function index() {
        if (!$this->config->item('allow_registration', 'tank_auth')) { // registration is off
            $this->_show_message($this->lang->line('auth_message_registration_disabled'));
        } else {

            $use_username = $this->config->item('use_username', 'tank_auth');
            if ($use_username) {
                $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean|min_length[' . $this->config->item('username_min_length', 'tank_auth') . ']|max_length[' . $this->config->item('username_max_length', 'tank_auth') . ']|alpha_dash');
            }
            $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|min_length[' . $this->config->item('password_min_length', 'tank_auth') . ']|max_length[' . $this->config->item('password_max_length', 'tank_auth') . ']|alpha_dash');
            $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|xss_clean|matches[password]');

            $this->form_validation->set_rules('fname', 'Firstname', 'trim|required');
            $this->form_validation->set_rules('lname', 'Lasttname', 'trim|required');

            $this->form_validation->set_rules('address', 'address', '');
            $this->form_validation->set_rules('city', 'city', '');
            $this->form_validation->set_rules('postcode', 'postcode', '');
            $this->form_validation->set_rules('phone', 'phone', '');

            //$this->form_validation->set_rules('EMIS', 'EMIS', '');
            $this->form_validation->set_rules('doctor', 'doctor', '');
            $this->form_validation->set_rules('NHS', 'NHS', '');
            $this->form_validation->set_rules('date_stopped_smoking', 'date_stopped_smoking', '');
            $this->form_validation->set_rules('date_created', 'date_created', '');
            $this->form_validation->set_rules('date_updated', 'date_updated', '');

            $this->form_validation->set_rules('MOI', 'MOI', '');
            $this->form_validation->set_rules('active', 'active', '');
            $this->form_validation->set_rules('imp_measure', 'imp_measure', '');
            $this->form_validation->set_rules('dob', 'dob', '');

            $this->form_validation->set_rules('height', 'height', '');
            $this->form_validation->set_rules('gender', 'gender', '');
            $this->form_validation->set_rules('smoker', 'smoker', '');
            $this->form_validation->set_rules('diabetes', 'diabetes', '');
            $this->form_validation->set_rules('hypertension', 'hypertension', '');
            $this->form_validation->set_rules('IHD', 'IHD', '');
            $this->form_validation->set_rules('ownbpmachine', 'ownbpmachine', '');


//Update `users` set `email` = 'viktorfabry1@gmail.com' WHERE `email` = 'viktorfabry@gmail.com'


            $captcha_registration = $this->config->item('captcha_registration', 'tank_auth');
            $use_recaptcha = $this->config->item('use_recaptcha', 'tank_auth');
            if ($captcha_registration) {
                if ($use_recaptcha) {
                    $this->form_validation->set_rules('recaptcha_response_field', 'Confirmation Code', 'trim|xss_clean|required|callback__check_recaptcha');
                } else {
                    $this->form_validation->set_rules('captcha', 'Confirmation Code', 'trim|xss_clean|required|callback__check_captcha');
                }
            }
            $data['errors'] = array();

            $email_activation = $this->config->item('email_activation', 'tank_auth');

            if ($this->form_validation->run()) {
                // validation ok
                //print_r($_POST); die;
                if (!is_null($data = $this->tank_auth->create_user(
                                        $use_username ? $this->form_validation->set_value('username') : '',
                                        $this->form_validation->set_value('email'),
                                        $this->form_validation->set_value('password'),
                                        $email_activation))) {
                    //print_r($data); die;
                    $datestring = "Y-m-d H:i:s";
                    $now = date($datestring, time());
                    $imp_measure = ($this->input->post('MOI') == 'imperial') ? "Y" : "N";

                    $tablename = 'patients';
                    $fieldarray = array(
                        'user_id' => $data['user_id'],
                        'doctor_id' => $this->input->post('doctor'),
                        'fname' => $this->input->post('fname'),
                        'lname' => $this->input->post('lname'),
                        'address' => $this->input->post('address'),
                        'city' => $this->input->post('city'),
                        'postcode' => $this->input->post('postcode'),
                        'phone' => $this->input->post('phone'),
                        //'EMIS' => $this->input->post('EMIS'),
                        'NHS' => $this->input->post('NHS'),
                        'date_stopped_smoking' => $this->input->post('date_stopped_smoking'),
                        'imp_measure' => $imp_measure,
                        'active' => $this->input->post('active'),
                        'dob' => $this->input->post('dob'),
                        'height' => $this->input->post('height'),
                        'gender' => $this->input->post('gender'),
                        'smoker' => $this->input->post('smoker'),
                        'diabetes' => $this->input->post('diabetes'),
                        'hypertension' => $this->input->post('hypertension'),
                        'IHD' => $this->input->post('IHD'),
                        'ownbpmachine' => $this->input->post('ownbpmachine'),
                        'current'=>'Y'
                    );
                    $this->db_fnc->insert($tablename, $fieldarray);

                    $tablename = 'users_roles';
                    $fieldarray = array(
                        'user_id' => $data['user_id'],
                        'role_id' => 2
                    );
                    $this->db_fnc->insert($tablename, $fieldarray);


                    $data['site_name'] = $this->config->item('website_name', 'tank_auth');

                    if ($email_activation) {         // send "activate" email
                        $data['activation_period'] = $this->config->item('email_activation_expire', 'tank_auth') / 3600;

                        $this->_send_email('activate', $data['email'], $data);

                        unset($data['password']); // Clear password (just for any case)

                        $this->_show_message($this->lang->line('auth_message_registration_completed_1'));
                    } else {
                        if ($this->config->item('email_account_details', 'tank_auth')) { // send "welcome" email
                            $this->_send_email('welcome', $data['email'], $data);
                        }
                        unset($data['password']); // Clear password (just for any case)

                        $this->_show_message($this->lang->line('auth_message_registration_completed_2') . ' ' . anchor('/auth/login/', 'Login'));
                    }
                } else {
                    $errors = $this->tank_auth->get_error_message();
                    foreach ($errors as $k => $v)
                        $data['errors'][$k] = $this->lang->line($v);
                   // print_r($data); die;
                }
            } else {
               // print_r($this->form_validation); die;
            }
            if ($captcha_registration) {
                if ($use_recaptcha) {
                    $data['recaptcha_html'] = $this->_create_recaptcha();
                } else {
                    $data['captcha_html'] = $this->_create_captcha();
                }
            }
            $data['use_username'] = $use_username;
            $data['captcha_registration'] = $captcha_registration;
            $data['use_recaptcha'] = $use_recaptcha;

            //print_r($data['errors']); die;
            $data['msg'] = $data['errors'];

            $data['dr_dd'] = array('Please select a your GP');
            $data['dr_dd'] += $this->doctor->getDoctors_dd();
            $data['dr_id'] = 0;



            $data['header'] = modules::run('header');
            $data['patientBar'] = modules::run('patientbar');
            $data['head'] = modules::run('head');
            $data['footer'] = modules::run('footer');

            $this->load->view('register_view', $data);
        }
    }

}
