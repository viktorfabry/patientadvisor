<?php

class Mywelcome extends MX_Controller {

	function  __construct() {
            parent::__construct();

            if (!$this->tank_auth->is_logged_in()) {
			redirect('/auth/login/');
		} else {

                    $this->user_id	= $this->tank_auth->get_user_id();
                    $this->username	= $this->tank_auth->get_username();
                    $this->module       = $this->router->fetch_class();
                    $this->role_id      = $this->session->userdata('role');

                    if(!$this->access->has_access($this->role_id ,$this->module)){
                        redirect('/auth/login/');
                    }
                }
         }

	function index() {

//                if (!$this->access->is_logged_in()) {
//			redirect('/auth/login');
//		} elseif($this->access->hasAccess()) {
//
//                        redirect('/patient/bloodpressure/',$data);
//		} else {
//                        $this->load->view('welcome_view');
//                }
           // redirect('/auth/login/');
		$this->load->view('welcome_view');

	}
	
	function user($chart = 'bloodpressure'){
		
		$usertype = $this->session->userdata('usertype');
		if ($this->input->post('people')) {
			$patient_id = $this->input->post('people');
			$this->session->set_userdata('user_id', $user_id);
		} elseif ($this->session->userdata('user_id')) {
			$patient_id = $this->session->userdata('user_id');
		} else {
			$patient_id = 0;
		}
		$data['patient_id'] = $patient_id;
		
		//echo $patient_id; die;
			
		if($patient_id){
				
			if ($chart == 'bloodpressure') $this->load->view('bloodpressure_view',$data);
			if ($chart == 'cholesterol') $this->load->view('cholesterol_view',$data);
			if ($chart == 'bodysize') $this->load->view('bodysize_view',$data);
		} else {
			$this->load->view('welcome_view',$data);
		}
	
			
	}	
	
	
}

