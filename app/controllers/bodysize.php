<?php

class Bodysize extends MX_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        } else {
            $this->module = 'bodysize';
            $this->user_id = $this->tank_auth->get_user_id();
            $this->username = $this->tank_auth->get_username();
            $this->role_id = $this->session->userdata('role');
            $this->patient_id = $this->session->userdata('patient_id');
            $this->imp = $this->session->userdata('imp');
            $this->load->library('form_validation');
        }
    }

    function index() {
        if ($this->access->has_access($this->role_id, $this->module)) {

            if ($this->patient_id != 0) {

                $data['header'] = modules::run('header');
                $data['topBar'] = ($this->access->isDoctor($this->role_id)) ? modules::run('drbar') : modules::run('patientbar');
                $data['hc_weight'] = modules::run('hc_weight');
                $data['hc_bmi'] = modules::run('hc_bmi');
                $data['hc_waistgirth'] = modules::run('hc_waistgirth');
                $data['head'] = modules::run('head');
                $data['footer'] = modules::run('footer');


                $data['weight_label'] = 'Weight (' . $this->app_functions->getWeightMeasure() . ')';
                $data['girth_label'] = 'Waist girth (' . $this->app_functions->getLengthMeasure() . ')';

                $submitedForm = '';

                $lengthValidation = (isset($this->imp) && $this->imp == 'Y') ? 'imp_length' : 'numeric';

                //Validation
                if ($this->input->post('girthForm')) {
                    $this->form_validation->set_rules('weight', 'Weight', 'trim|numeric');
                    $this->form_validation->set_rules('girth', 'Waist girth', 'required|trim|' . $lengthValidation);
                    $this->form_validation->set_rules('girthNote', 'Note', 'trim|max_length[160]');
                    $submitedForm = 'girthForm';
                }

                if ($this->input->post('weightForm')) {
                    $this->form_validation->set_rules('girth', 'Waist girth', 'trim|' . $lengthValidation);
                    $this->form_validation->set_rules('weight', 'Weight', 'required|trim|numeric');
                    $this->form_validation->set_rules('note', 'Note', 'trim|max_length[160]');
                    $submitedForm = 'weightForm';
                }



                if ($this->form_validation->run($this) === TRUE) {
                    $sample = $this->db_fnc->getMax('sample', 'patient_id', $this->patient_id, 'body_size');
                    $sample++;

                   // date_default_timezone_set('UTC');
                    
                    $sample_date = unix_to_human(now(), TRUE, 'eu');

                    $note = str_replace(array("\r", "\r\n", "\n"), ' ', $this->form_validation->set_value('note'));
                    $girthNote = str_replace(array("\r", "\r\n", "\n"), ' ', $this->form_validation->set_value('girthNote'));

                    $note = ($note) ? $note : $girthNote;

                    if (isset($this->imp) && $this->imp == 'Y') {
                        $weight = lb2kg($this->form_validation->set_value('weight'));
                        $girth = imperial2metric($this->form_validation->set_value('girth'));
                    } else {
                        $weight = $this->form_validation->set_value('weight');
                        $girth = $this->form_validation->set_value('girth');
                    }

                    if (!$weight && !$girth) {
                        $this->session->set_flashdata($submitedForm, 'Data entry failed, please enter the date in the correct format.');
                        $this->load->view('chart_bodysize_view', $data);
                    } else {


                        $tablename = 'body_size';
                        $fieldarray = array(
                            'patient_id' => $this->patient_id,
                            'sample' => $sample,
                            'sample_date' => $sample_date,
                            'weight' => $weight,
                            'girth' => $girth,
                            'note' => $note,
                        );
                        $this->db_fnc->insert($tablename, $fieldarray);


                        $this->session->set_flashdata($submitedForm, 'Your data have been saved successfully.');
                        redirect('bodysize');
                    }
                } else {
                    $this->load->view('chart_bodysize_view', $data);
                }
            } else {
                $this->load->view('practice_view');
            }
        } else {
            $this->session->set_flashdata('errors', 'You need to be logged in to be able to see this page. Please login.');
            redirect('/');
        }
    }

}

