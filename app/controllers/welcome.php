<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends MX_Controller
{
	function  __construct() {
            parent::__construct();

            if (!$this->tank_auth->is_logged_in()) {
			$this->__destruct();
		} else {

                    $this->user_id	= $this->tank_auth->get_user_id();
                    $this->username	= $this->tank_auth->get_username();
                    $this->module       = $this->router->fetch_class();
                    $this->role_id      = $this->session->userdata('role');

                    if(!$this->access->has_access($this->role_id ,$this->module)){
                        $this->__destruct();
                    }
                }
         }

         function __destruct() {
               parent::__destruct();
        }

	function index()
	{
		if (!$this->tank_auth->is_logged_in()) {
			redirect('/auth/login/');
		} else {
                    $data['user_id']	= $this->tank_auth->get_user_id();
                    $data['username']	= $this->tank_auth->get_username();

                    if ($this->tank_auth->check_roles('gp')) {
			$this->load->view('welcome', $data);
                    } else {
                        $this->load->view('msg', $data);
                    }
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */