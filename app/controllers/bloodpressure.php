<?php

class Bloodpressure extends MX_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        } else {
            $this->module = 'bloodpressure';
            $this->user_id = $this->tank_auth->get_user_id();
            $this->username = $this->tank_auth->get_username();
            $this->role_id = $this->session->userdata('role');
            $this->patient_id = $this->session->userdata('patient_id');
            $this->load->library('form_validation');
        }
    }

    function index() {
        if ($this->access->has_access($this->role_id, $this->module)) {

            if ($this->patient_id != 0) {


            $this->form_validation->set_rules('systolic', 'Systolic', 'required|trim|numeric|greater_than[35]|less_than[260]');
            $this->form_validation->set_rules('diastolic', 'Diastolic', 'required|trim|numeric|greater_than[35]|less_than[260]');
            $this->form_validation->set_rules('note', 'Note', 'trim|max_length[160]');

            $this->form_validation->set_message('greater_than', "The %s field must heigher than 35.");
            $this->form_validation->set_message('less_than', "The %s field must less 260.");



            if ($this->form_validation->run($this) === TRUE) {
                $note = str_replace(array("\r", "\r\n", "\n"), ' ', $this->form_validation->set_value('note'));

                $sample = $this->db_fnc->getMax('sample', 'patient_id', $this->patient_id, 'bloodpressure');
                $sample++;

                $datestring = "Y-m-d H:i:s";
                $sample_date = date($datestring, time());

                $tablename = 'bloodpressure';
                $fieldarray = array(
                    'patient_id' => $this->patient_id,
                    'sample' => $sample,
                    'sample_date' => $sample_date,
                    'systolic' => $this->form_validation->set_value('systolic'),
                    'diastolic' => $this->form_validation->set_value('diastolic'),
                    'note' => $note
                );
                $this->db_fnc->insert($tablename, $fieldarray);
                $params = array('patient_id' => $this->patient_id);
            }

                $data['header'] = modules::run('header');
                $data['topBar'] = ($this->access->isDoctor($this->role_id)) ? modules::run('drbar') : modules::run('patientbar');
                $data['hc_bp'] = modules::run('hc_bp');
                $data['head'] = modules::run('head');
                $data['footer'] = modules::run('footer');

                $this->load->view('chart_bp_view', $data);
            } else {
                $this->load->view('practice_view');
            }
        } else {
            $this->session->set_flashdata('errors', 'You need to be logged in to be able to see this page. Please login.');
            redirect('/');
        }
    }

    function addSample() {
        if ($this->access->has_access($this->role_id, $this->module)) {

            
        }
    }

}

