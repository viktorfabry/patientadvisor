<?php

class Diabetes extends MX_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        } else {
            $this->module = 'diabetes';
            $this->user_id = $this->tank_auth->get_user_id();
            $this->username = $this->tank_auth->get_username();
            $this->role_id = $this->session->userdata('role');
            $this->patient_id = $this->session->userdata('patient_id');
        }
    }

    function index() {
        if ($this->access->has_access($this->role_id, $this->module)) {

            if ($this->patient_id != 0) {
                $data['header'] = modules::run('header');
                $data['topBar'] = ($this->access->isDoctor($this->role_id)) ? modules::run('drbar') : modules::run('patientbar');
                $data['hc_ltbs'] = modules::run('hc_ltbs');
                $data['hc_diabetes'] = modules::run('hc_diabetes');
                $data['head'] = modules::run('head');
                $data['footer'] = modules::run('footer');
                $this->load->view('chart_diabetes_view',$data);
            } else {
                $this->load->view('practice_view');
            }
        } else {
            $this->session->set_flashdata('errors', 'You need to be logged in to be able to see this page. Please login.');
            redirect('/');
        }
    }

    function addDiabetes() {
        $sample = $this->db_fnc->getMax('sample', 'patient_id', $this->patient_id, 'diabetes');
        $sample++;


        $datestring = "Y-m-d H:i:s";
        $sample_date = date($datestring, time());

        $tablename = 'diabetes';
        $fieldarray = array('patient_id' => $this->patient_id, 'sample' => $sample, 'sample_date' => $sample_date, 'diabetes' => $this->input->post('diabetes'));

        $this->db_fnc->insert($tablename, $fieldarray);

        redirect($this->input->post('url'));
    }

    function addLtbs() {
        $sample = $this->db_fnc->getMax('sample', 'patient_id', $this->patient_id, 'ltbs');
        $sample++;


        $datestring = "Y-m-d H:i:s";
        $sample_date = date($datestring, time());

        $tablename = 'ltbs';
        $fieldarray = array('patient_id' => $this->patient_id, 'sample' => $sample, 'sample_date' => $sample_date, 'ltbs' => $this->input->post('ltbs'));

        $this->db_fnc->insert($tablename, $fieldarray);

        redirect($this->input->post('url'));
    }

}

