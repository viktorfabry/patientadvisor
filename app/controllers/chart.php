<?php

class Chart extends MX_Controller {

	function  __construct() {
            parent::__construct();

            if (!$this->tank_auth->is_logged_in()) {
			redirect('/auth/login/');
		} else {

                    $this->user_id	= $this->tank_auth->get_user_id();
                    $this->username	= $this->tank_auth->get_username();
                    $this->module       = $this->router->fetch_class();
                    $this->role_id      = $this->session->userdata('role');

                    if(!$this->access->has_access($this->role_id ,$this->module)){
                        redirect('/auth/login/');
                    }
                }
         }
	
	function index(){
		
	}
	
	function xml($type) {
				
		$bp_all = $this->db_fnc->getRowsOrderBy('patient_id',1,'bloodpressure','sample_date','asc');
		
		$data['chart'] = $this->chart($bp_all);
		
		//print_r($data['chart']); die;
	
		if($type == 1){
				$this->load->view('chart1_XML',$data);
		} else {
				$this->load->view('chart2_XML',$data);
		}
	}
	
		function dateRangeArray($start, $end) {
				$range = array();
				
				if (is_string($start) === true) $start = strtotime($start);
				if (is_string($end) === true ) $end = strtotime($end);
				
				do {
				$range[] = date('Y-m-d', $start);
				$start = strtotime("+ 1 day", $start);
				} while ($start <= $end);
				
				return $range;
		}
		
		
		function chart($bp_all) {
				$chart = array();
				$i = 0;
				$lines = 0;
				
				while ($i < count($bp_all)) {
						//echo $i.' <> '.count($bp_all).'<br />';
						$sample_date = substr($bp_all[$i]['sample_date'], 0, 10);
						if ($i < count($bp_all)-1) { $sample_nextdate = substr($bp_all[$i+1]['sample_date'], 0, 10);	}
						$day = strtotime($sample_date);
						$nextday = strtotime($sample_nextdate);
						$tomorrow = strtotime("+ 1 day", $day);
						//echo $tomorrow.' <> '.$nextday.'<br />';
						
						$chart['dates'][] = '<string>'.$sample_date.'</string>';
						$chart['systolic'][] = '<number>'.$bp_all[$i]['systolic'].'</number>';
						$chart['diastolic'][] = '<number>'.$bp_all[$i]['diastolic'].'</number>';
						$lines++;
						
						if ($tomorrow < $nextday) {
								$b = 0;
								$missingDateRange = $this->dateRangeArray($tomorrow, $nextday);
								//print_r($missingDateRange);
								while ($b < count($missingDateRange)-1) {										
										$chart['dates'][] = '<string>'.$missingDateRange[$b].'</string>';
										$chart['systolic'][] = '<null/>';
										$chart['diastolic'][] = '<null/>';
										$b++;
										$lines++;
								} 
								
						} 
						
						$i++;
				};
				$chart['lines'] = $lines;
				//echo $lines; die;
				return $chart;
		
	}	
	
	
}

