<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Contents extends MX_Controller {

    function __construct() {
        parent::__construct();

            $this->user_id = $this->tank_auth->get_user_id();
            $this->username = $this->tank_auth->get_username();
            $this->role_id = $this->session->userdata('role');
            $this->load->model('content');
    }


    function page($page) {
        $data['user_id'] = $this->tank_auth->get_user_id();
        $data['username'] = $this->tank_auth->get_username();
        $data['msg'] = modules::run('msg');
        $data['header'] = modules::run('header');
        $data['topBar'] = ($this->access->isDoctor($this->role_id)) ? modules::run('drbar') : modules::run('patientbar');
        $data['head'] = modules::run('head');
        $data['footer'] = modules::run('footer');

        $data['side_links'] = $this->content->getContentLinksByCatId(1);

         $p = $this->content->getContentByLink($page);
         $data['title'] = (isset($p['title']) && $p['title']) ? $p['title'] : '';
         $data['body'] = (isset($p['body']) && $p['body']) ? $p['body'] : '';

        $this->load->view('content_view', $data);
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */