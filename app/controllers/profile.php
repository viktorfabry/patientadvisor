<?php

class Profile extends MX_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        } else {
            $this->module = 'bloodpressure';
            $this->user_id = $this->tank_auth->get_user_id();
            $this->username = $this->tank_auth->get_username();
            $this->role_id = $this->session->userdata('role');
            $this->patient_id = $this->session->userdata('patient_id');
            $this->fsize = $this->session->userdata('fsize');
            $this->load->model('patient');
            $this->load->helper(array('form', 'url'));
            $this->load->library('form_validation');
            $this->load->library('security');
            $this->load->library('tank_auth');
            $this->lang->load('tank_auth');
        }
    }

    function index() {
        if ($this->access->has_access($this->role_id, $this->module)) {

            $this->form_validation->set_rules('username', 'Username', 'trim|xss_clean|required|callback__username_check');
            //$this->form_validation->set_rules('pword', 'Password', 'trim|required|matches[passconf]|md5');
            //$this->form_validation->set_rules('passconf', 'Password Confirmation', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|xss_clean|required|valid_email');
            //$this->form_validation->set_rules('email_confirmed', 'Email', 'trim|required|valid_email');

            $this->form_validation->set_rules('fname', 'Firstname', 'trim|required');
            $this->form_validation->set_rules('lname', 'Lasttname', 'trim|required');

            $this->form_validation->set_rules('address', 'address', 'trim');
            $this->form_validation->set_rules('city', 'city', 'trim');
            $this->form_validation->set_rules('postcode', 'postcode', 'trim');
            $this->form_validation->set_rules('phone', 'phone', 'trim');

            $this->form_validation->set_rules('NHS', 'NHS', '');
            $this->form_validation->set_rules('date_stopped_smoking', 'date_stopped_smoking', 'trim');
            $this->form_validation->set_rules('date_created', 'date_created', 'trim');

            $this->form_validation->set_rules('imp_measure', 'imp_measure', 'trim');
            $this->form_validation->set_rules('active', 'active', 'trim');
            $this->form_validation->set_rules('dob', 'dob', 'trim');

            $this->form_validation->set_rules('height_m', 'Height', 'trim');
            $this->form_validation->set_rules('height_ft', 'Height', 'trim');
            $this->form_validation->set_rules('height_in', 'Height', 'trim');

            $this->form_validation->set_rules('gender', 'gender', 'trim');
            $this->form_validation->set_rules('smoker', 'smoker', 'trim');
            $this->form_validation->set_rules('diabetes', 'diabetes', 'trim');
            $this->form_validation->set_rules('hypertension', 'hypertension', 'trim');
            $this->form_validation->set_rules('IHD', 'IHD', 'trim');
            $this->form_validation->set_rules('ownbpmachine', 'ownbpmachine', 'trim');

            $p = $this->patient->getPatientByID($this->patient_id);

            //print_r($p); die;

            $data['username'] = isset($p['username']) ? $p['username'] : '' ;
            $data['email'] = isset($p['email']) ? $p['email'] : '' ;
            $data['fname'] = isset($p['fname']) ? $p['fname'] : '' ;
            $data['lname'] = isset($p['lname']) ? $p['lname'] : '' ;
            $data['address'] = isset($p['address']) ? $p['address'] : '' ;
            $data['city'] = isset($p['city']) ? $p['city'] : '' ;
            $data['postcode'] = isset($p['postcode']) ? $p['postcode'] : '' ;
            $data['phone'] = isset($p['phone']) ? $p['phone'] : '' ;
            $data['NHS'] = isset($p['NHS']) ? $p['NHS'] : '' ;
            $data['date_stopped_smoking'] = isset($p['date_stopped_smoking']) ? $p['date_stopped_smoking'] : '' ;
            $data['date_created'] = isset($p['date_created']) ? $p['date_created'] : '' ;
            $data['imp_measure'] = isset($p['imp_measure']) ? $p['imp_measure'] : '' ;
            $data['active'] = isset($p['active']) ? $p['active'] : '' ;
            $data['gender'] = isset($p['gender']) ? $p['gender'] : '' ;
            $data['smoker'] = isset($p['smoker']) ? $p['smoker'] : '' ;
            $data['diabetes'] = isset($p['diabetes']) ? $p['diabetes'] : '' ;
            $data['hypertension'] = isset($p['hypertension']) ? $p['hypertension'] : '' ;
            $data['IHD'] = isset($p['IHD']) ? $p['IHD'] : '' ;
            $data['ownbpmachine'] = isset($p['ownbpmachine']) ? $p['ownbpmachine'] : '' ;

            $data['progress'] = $this->app_functions->getProfileProgress($this->user_id);
            
            //print_r($p); die;

            $data['metricClass'] = '';
            $data['impClass'] = '';

            if (isset($p['imp_measure']) && $p['imp_measure'] === 'Y') {
                $data['imp'] = TRUE;
                $data['metric'] = '';
                $data['metricClass'] = 'hidden';
                $data['height_imp'] = (isset($p['height'])) ? metric2imperial($p['height'],'cm','ft_in_array') : '';
                $data['height'] = '';
            } elseif(isset($p['height'])) {
                $data['metric'] = TRUE;
                $data['imp'] = '';
                $data['height'] = $p['height'];
                $data['impClass'] = 'hidden';
                $data['height_imp']['ft'] = '';
                $data['height_imp']['in'] = '';
            } else {
                $data['metric'] = TRUE;
                $data['imp'] = '';
                $data['impClass'] = 'hidden';
                $data['height'] = '';
                $data['height_imp']['ft'] = '';
                $data['height_imp']['in'] = '';
            }

            if (isset($p['gender']) && $p['gender'] === 'M') {
                $data['male'] = TRUE;
                $data['female'] = '';
            } elseif(isset($p['gender']) && $p['gender'] === 'F') {
                $data['female'] = TRUE;
                $data['male'] = '';
            } else {
                $data['female'] = '';
                $data['male'] = '';
            }

            $data['yepnope_dd'] = array('0' => 'Please select', 'Y' => 'Yes', 'N' => 'No');

            $datestring = "Y-m-d";
            $data['dob'] = isset($p['dob']) ? date($datestring, strtotime($p['dob'])) : '' ;

            //$data['height'] = meter2imperial($p['height']);
            $data['unit_height'] = 0;
            $data['dr_id'] = 0;
            $this->load->model('doctor');
            $data['dr_dd'] = $this->doctor->getDoctors_dd();


            if ($this->form_validation->run($this) == FALSE) {
                $data['dr_id'] = 0;
                $this->load->model('doctor');
                $data['dr_dd'] = $this->doctor->getDoctors_dd();

                $data['header'] = modules::run('header');
                $data['topBar'] = ($this->access->isDoctor($this->role_id)) ? modules::run('drbar') : modules::run('patientbar');
                $data['msgBlock'] = modules::run('msg',$msg = '');
                $data['head'] = modules::run('head');
                $data['footer'] = modules::run('footer');


                if($this->access->isPatient($this->role_id)){
                    $this->load->view('profile_view', $data);
                } else {
                    $this->load->view('patient_profile_view', $data);
                }

            } else {
                $height_m = $this->form_validation->set_value('height_m');
                $height_ft = $this->form_validation->set_value('height_ft');
                $height_in = $this->form_validation->set_value('height_in');
                $fname = $this->form_validation->set_value('fname');
                $lname = $this->form_validation->set_value('lname');
                $username = $this->form_validation->set_value('username');
                $email = $this->form_validation->set_value('email');
                $imp = $this->form_validation->set_value('imp_measure');

                $datestring = "Y-m-d H:i:s";
                $now = date($datestring, time());

                $tablename = 'users';
                $fieldarray = array(
                    'username' => $username,
                    'email' => $email
                );
                $where = array('id' => $this->patient_id);
                $this->db_fnc->update($tablename, $fieldarray, $where);

                $datestring = "Y-m-d";
                $dob = date($datestring, strtotime($this->form_validation->set_value('dob')));

                

                
                //echo (int)$height_ft.'ft '.(int)$height_in.'in'; die;

                $height = ($height_m) ? $height_m : imperial2metric((int)$height_ft.'ft '.(int)$height_in.'in');

                $tablename = 'patients';
                $fieldarray = array(
                    'fname' => $fname,
                    'lname' => $lname,
                    'address' => $this->form_validation->set_value('address'),
                    'city' => $this->form_validation->set_value('city'),
                    'postcode' => $this->form_validation->set_value('postcode'),
                    'phone' => $this->form_validation->set_value('phone'),
                    //'EMIS' => $this->input->post('EMIS'],
                    'fsize' =>$this->fsize,
                    'NHS' => $this->form_validation->set_value('NHS'),
                    'date_stopped_smoking' => $this->form_validation->set_value('date_stopped_smoking'),
                    'active' => $this->form_validation->set_value('active'),
                    'imp_measure' => $imp,
                    'dob' => $dob,
                    'height' => $height,
                    'gender' => $this->form_validation->set_value('gender'),
                    'smoker' => $this->form_validation->set_value('smoker'),
                    'diabetes' => $this->form_validation->set_value('diabetes'),
                    'hypertension' => $this->form_validation->set_value('hypertension'),
                    'IHD' => $this->form_validation->set_value('IHD'),
                    'ownbpmachine' => $this->form_validation->set_value('ownbpmachine'),
                    'current'=>'Y'
                );
                $this->patient->archive($this->patient_id);
                $this->patient->update($fieldarray, $this->patient_id);

                

                //$this->app_functions->setImpMeasure($this->form_validation->set_value('imp_measure'));
                $full_name = $fname .' '. $lname;
                $this->session->set_userdata(array(
                    'username' => $username,
                    'full_name' => $full_name,
                    'imp' => $imp
                ));

                $fieldarray = array('patient_id' => $this->patient_id, 'doctor_id' => $this->input->post('doctor'));
                $where = array('patient_id' => $this->patient_id);
                $id = $this->db_fnc->update('doctor_patient', $fieldarray, $where);

                $this->session->set_flashdata('msg',app_msg('Your profile data has been updated.','success'));
                redirect('profile');
            }
        } else {
            $this->session->set_flashdata('errors', 'You need to be logged in to be able to see this page. Please login.');
            redirect('/');
        }
    }

    function _username_check($username) {
        if ($username === $this->username) {
            return TRUE;
        } else {
            if($this->tank_auth->is_username_available($username)) {
                $this->form_validation->set_message('_username_check', 'The new user name: '. $username .' has been saved ');
                return TRUE;
            } else {
                $this->form_validation->set_message('_username_check', 'The "' . $username . '" username has already been taken please try a different name');
                return FALSE;
            }
        }
    }
    
}
?>