<?php

class Practice extends MX_Controller {

	function  __construct() {
            parent::__construct();

            if (!$this->tank_auth->is_logged_in()) {
			redirect('/auth/login/');
            } else {
                $this->module       = 'bloodpressure';
                $this->user_id	= $this->tank_auth->get_user_id();
                $this->username	= $this->tank_auth->get_username();
                $this->role_id      = $this->session->userdata('role');
            }
         }


        function index() {
            if($this->access->has_access($this->role_id ,$this->module)){
                    $data['user_id']	= $this->user_id;
                    $data['username']	= $this->username;
                        
                        
                    $doc = $this->session->userdata('user_id');
                    $this->load->model('Doctor');
                    $myPatients = $this->Doctor->get_my_patient_dropdown($doc);
                    $myPatients = array(0=>'Please select a patient...')+$myPatients;
                    $data['people'] = $myPatients;
			
                    $patient_id = $this->session->userdata('patient_id');

                    $data['patient_id'] = $patient_id;
			
                    if($patient_id != 0){
			redirect('/bloodpressure/');
                    } else {
                        $this->load->view('practice_view',$data);
                    }
                        
    
		}
	}


	
	
	
	
	
}

